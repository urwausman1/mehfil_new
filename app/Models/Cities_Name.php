<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cities_Name extends Model
{
    use HasFactory;
    protected $fillable = [
    				'country_id' ,
    				'cc02' ,
    				'cc03' ,
    				'cc02_city' ,
    				'cc03_city' ,
    				'city_name_en' ,
    				'city_name_ur' ,
    			];
}
