<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AverageHazri extends Model
{
    use HasFactory;

    protected $fillable = ['mehfil_id' , 'dates' , 'average_visitors'];
}
