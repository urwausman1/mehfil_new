<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EhadRegister extends Model
{
    use HasFactory;

    protected $fillable = [
                            'er_id_number' , 
                            'er_name' , 
                            'er_name_en' , 
    						'er_fname' , 
    						'er_phone' , 
    						'er_email' , 
    						'er_address' , 
    						'er_city' , 
    						'er_country' , 
    						'er_cnic',
                            'er_dob',
                            'er_doe',
                            'er_marfat',
                            'mehfil_id',
    					];
}
