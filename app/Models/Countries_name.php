<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Countries_name extends Model
{
    use HasFactory;

    protected $fillable = [
    			'cc02' ,
    			'cc03' ,
    			'country_name_en' ,
    			'country_name_ur' ,
    ];
}
