<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Mehfil extends Model
{
    use HasFactory;

    public function karkuns()
    {
    	return $this->hasMany(Karkun::class, 'mehfil_id');
    }

}
