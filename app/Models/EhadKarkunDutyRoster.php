<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EhadKarkunDutyRoster extends Model
{
    use HasFactory;

    protected $fillable = ['day' , 'mehfil_id' , 'ehad_karkun_id' , 'city' , 'month' , 'year'];
}
