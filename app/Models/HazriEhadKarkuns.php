<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HazriEhadKarkuns extends Model
{
    use HasFactory;
    protected $fillable = ['mehfil_id' , 'ehad_karkun_id' , 'dates'];
}
