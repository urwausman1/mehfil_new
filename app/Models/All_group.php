<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class All_group extends Model
{
    use HasFactory;

    protected $fillable = [
    		'com_id' ,
    		'category' ,
    		'city_name' ,
    		'mehfil_id' ,
    		'karkun_id' , ];
}
