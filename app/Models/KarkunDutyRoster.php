<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KarkunDutyRoster extends Model
{
    use HasFactory;

    protected $fillable = ['mehfil_id' , 
                            'k_duty_days' , 
                            'k_main_gatian' , 
                            'k_cycle_stand' , 
                            'k_security' , 
                            'ehad_karkun' , 
                            'day_coordinator' , 
                            'monthly_cordinator' , 
                            'month_name' , 
                            'year_name',];
}
