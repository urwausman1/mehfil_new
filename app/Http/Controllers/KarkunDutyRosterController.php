<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\KarkunDutyRoster;
use App\Models\Mehfil;

class KarkunDutyRosterController extends Controller{


    public function showDataForAll(){

        $all_mehfils = DB::table('mehfils')->get();
        return view('duty_roster_karkun.all_dutyroster')->with(compact(['all_mehfils']));
    }

    public function showData(){
        $all_k_duty_roster = DB::table('karkun_duty_rosters')->get();
        $all_mehfils = DB::table('mehfils')->get();
        $all_karkun = DB::table('karkuns')->get();
        $all_ehad_karkun = DB::table('ehad_karkuns')->get();

	    $user_role = session('user_role');
	    $mehfil_id = session('mehfil_id');

        if ($user_role == 'mehfil') {
        	return redirect('dutyrosterkarkun/edit/'.$mehfil_id);
        }else{
	        return view('duty_roster_karkun.dutyroster')->with(compact(['all_k_duty_roster' , 'all_mehfils' , 'all_karkun' , 'all_ehad_karkun']));
        }
    }


    public function editDutyKarkun($id , Request $request){

		// DB::enableQueryLog();

		// $mehfils = Mehfil::with('karkuns')->get();
		// foreach($mehfils as $mehfil){
		// 	dd($mehfil->karkuns);
		// }
		// dd($mehfil);

	    $user_role = session('user_role');
	    $mehfil_id = session('mehfil_id');

		$security_msg = '';
	    
	    if ($user_role == 'mehfil') {
			
			if ($mehfil_id != $id) {

				$security_msg = "    آپ کو اس ڈیٹا کی رسائی  حاصل نہیں ہے۔ برائے مہربانی  متعلقہ بھائی سے رابطہ کریں۔   ";

	    	    return view('duty_roster_karkun.edit_dutyroster')->with(compact(['security_msg']));
			}	    	
	    }


    		// arrays to save the selected names
	        	// for friday
	    	$fdco = []; // day co ordinator
	        $fmd = []; // main duty
	        $fcs = []; // cycle stand
	        $fs = []; // security
	        	// for saturday
	    	$sdco = []; // day co ordinator
	        $smd = []; // main duty
	        $scs = []; // cycle stand
	        $ss = []; // security
	        	// for sunday
	    	$sndco = []; // day co ordinator
	        $snmd = []; //main duty
	        $sncs = []; // cycle stand
	        $sns = []; // security
	        	// for monday
	    	$mdco = []; // day co ordinator
	        $mmd = []; //main duty
	        $mcs = []; // cycle stand
	        $ms = []; //  security
	        	// for tuesday
	    	$tdco = []; // day co ordinator
	        $tmd = []; //main duty
	        $tcs = []; // cycle stand
	        $ts = []; //  security
	        	// for wednesday
	    	$wdco = []; // day co ordinator
	        $wmd = []; //main duty
	        $wcs = []; // cycle stand
	        $ws = []; // security
	        	// for thursday
	    	$thdco = []; // day co ordinator
	        $thmd = []; //main duty
	        $thcs = []; // cycle stand
	        $ths = []; // security
	        	// for monthly coordinator
	        $m_c = []; //

        $all_mehfils = DB::table('mehfils')->get();
        $all_ehad_karkun = DB::table('ehad_karkuns')->get();
        // $all_karkun = DB::table('karkuns')->get();

		$all_karkun = DB::table('karkuns')
							->where('mehfil_id' , $id)
							->get();



		// if we don't have data for duty roster then mehfil name will be according to the given parameter
        $get_mehfil_name_check = DB::table('mehfils')->where('id' , $id)->first();
        if (isset($get_mehfil_name_check)) {
        	$mehfil_id = $id; 
        	$mehfil_name_ne = $get_mehfil_name_check->mehfil_name; 
        }


        $month_is = date("m");
        $year_is = date("Y");
        $not_save_msg = "";
		$all_k_duty_roster_for_check_month = DB::table('karkun_duty_rosters')
										->join('mehfils', 'karkun_duty_rosters.mehfil_id', '=', 'mehfils.id')
										->leftJoin('ehad_karkuns', 'karkun_duty_rosters.ehad_karkun', '=', 'ehad_karkuns.id')
										->join('karkuns', 'karkun_duty_rosters.day_coordinator', '=', 'karkuns.id')
										->select('karkun_duty_rosters.*', 'mehfils.mehfil_name', 'ehad_karkuns.ekp_name' , 'karkuns.kp_name')
        								->where('karkun_duty_rosters.mehfil_id' , $id)
        								->where('k_duty_days' , 'friday')
										->where('month_name' , $month_is)
										->where('year_name' , $year_is)
        								->first();

        if (empty($all_k_duty_roster_for_check_month)) {
	        $month_is = date("m") - 1;
        	$not_save_msg = "  اس محفل کا  اس مہینے کا ڈیٹا محفوظ نہیں ہے۔ براہ کرم محفوظ کرلیں۔ شکریہ۔  ";
        }


        // for friday
        $get_ehad_karkun_juma = DB::table('ehad_karkun_duty_rosters')
								->join('ehad_karkuns' , 'ehad_karkuns.id' , 'ehad_karkun_duty_rosters.ehad_karkun_id')
								->select('ehad_karkun_duty_rosters.*' , 'ehad_karkuns.ekp_name')
								->where('day' , 'friday')
								->where('mehfil_id' , $id)
								->where('month' , $month_is)
								->where('year' , $year_is)
								->first();

        $get_ehad_karkun_hafta = DB::table('ehad_karkun_duty_rosters')
								->join('ehad_karkuns' , 'ehad_karkuns.id' , 'ehad_karkun_duty_rosters.ehad_karkun_id')
								->select('ehad_karkun_duty_rosters.*' , 'ehad_karkuns.ekp_name')
								->where('day' , 'saturday')
								->where('mehfil_id' , $id)
								->where('month' , $month_is)
								->where('year' , $year_is)
								->first();

        $get_ehad_karkun_itwar = DB::table('ehad_karkun_duty_rosters')
								->join('ehad_karkuns' , 'ehad_karkuns.id' , 'ehad_karkun_duty_rosters.ehad_karkun_id')
								->select('ehad_karkun_duty_rosters.*' , 'ehad_karkuns.ekp_name')
								->where('day' , 'sunday')
								->where('mehfil_id' , $id)
								->where('month' , $month_is)
								->where('year' , $year_is)
								->first();

        $get_ehad_karkun_peer = DB::table('ehad_karkun_duty_rosters')
								->join('ehad_karkuns' , 'ehad_karkuns.id' , 'ehad_karkun_duty_rosters.ehad_karkun_id')
								->select('ehad_karkun_duty_rosters.*' , 'ehad_karkuns.ekp_name')
								->where('day' , 'monday')
								->where('mehfil_id' , $id)
								->where('month' , $month_is)
								->where('year' , $year_is)
								->first();

        $get_ehad_karkun_mangal = DB::table('ehad_karkun_duty_rosters')
								->join('ehad_karkuns' , 'ehad_karkuns.id' , 'ehad_karkun_duty_rosters.ehad_karkun_id')
								->select('ehad_karkun_duty_rosters.*' , 'ehad_karkuns.ekp_name')
								->where('day' , 'tuesday')
								->where('mehfil_id' , $id)
								->where('month' , $month_is)
								->where('year' , $year_is)
								->first();

        $get_ehad_karkun_budh = DB::table('ehad_karkun_duty_rosters')
								->join('ehad_karkuns' , 'ehad_karkuns.id' , 'ehad_karkun_duty_rosters.ehad_karkun_id')
								->select('ehad_karkun_duty_rosters.*' , 'ehad_karkuns.ekp_name')
								->where('day' , 'wednesday')
								->where('mehfil_id' , $id)
								->where('month' , $month_is)
								->where('year' , $year_is)
								->first();

        $get_ehad_karkun_jumerat = DB::table('ehad_karkun_duty_rosters')
								->join('ehad_karkuns' , 'ehad_karkuns.id' , 'ehad_karkun_duty_rosters.ehad_karkun_id')
								->select('ehad_karkun_duty_rosters.*' , 'ehad_karkuns.ekp_name')
								->where('day' , 'thursday')
								->where('mehfil_id' , $id)
								->where('month' , $month_is)
								->where('year' , $year_is)
								->first();

		// $all_k_duty_roster_friday = Karkun_duty_rosters::with('mehfils')->with
		$all_k_duty_roster_friday = DB::table('karkun_duty_rosters')
										->join('mehfils', 'karkun_duty_rosters.mehfil_id', '=', 'mehfils.id')
										->leftJoin('ehad_karkuns', 'karkun_duty_rosters.ehad_karkun', '=', 'ehad_karkuns.id')
										->join('karkuns', 'karkun_duty_rosters.day_coordinator', '=', 'karkuns.id')
										->select('karkun_duty_rosters.*', 'mehfils.mehfil_name', 'ehad_karkuns.ekp_name' , 'karkuns.kp_name')
        								->where('karkun_duty_rosters.mehfil_id' , $id)
        								->where('k_duty_days' , 'friday')
										->where('month_name' , $month_is)
										->where('year_name' , $year_is)
        								->first();

		// dd($all_k_duty_roster_friday);        								
        if (isset($all_k_duty_roster_friday)) {

	        // $fdco = [];
	        $karkun_day_coord_selected_friday = DB::table('karkuns')
	                                  ->whereIn('id' , explode(',' , $all_k_duty_roster_friday->day_coordinator))
	                                  ->get();

	        foreach ($karkun_day_coord_selected_friday as $fdco_single) {
	        	array_push($fdco, $fdco_single->id);
	        }

	        	// only for main gatian friday
	        // $fmd = [];
	        $all_karkun_mainduty_friday_selected = DB::table('karkuns')
	                                  ->whereIn('id' , explode(',' , $all_k_duty_roster_friday->k_main_gatian))
	                                  ->get();

	        // dd($all_karkun_mainduty_friday_selected);

	        foreach ($all_karkun_mainduty_friday_selected as $fmd_single) {
	        	array_push($fmd, $fmd_single->id);
	        }

	        	// only for cycle stand friday
	        // $fcs = [];
	        $all_karkun_cyclestand_friday_selected = DB::table('karkuns')
	                                  ->whereIn('id' , explode(',' , $all_k_duty_roster_friday->k_cycle_stand))
	                                  ->get();

	        foreach ($all_karkun_cyclestand_friday_selected as $fcs_single) {
	        	array_push($fcs, $fcs_single->id);
	        }

	        	// only for security friday
	        // $fs = [];
	        $all_karkun_security_friday_selected = DB::table('karkuns')
	                                  ->whereIn('id' , explode(',' , $all_k_duty_roster_friday->k_security))
	                                  ->get();

	        foreach ($all_karkun_security_friday_selected as $fs_single) {
	        	array_push($fs, $fs_single->id);
	        }
	    }
	        // code end for friday

		//////////////////////////////////////////////////////////////////////////////////////////////////////

		/// code starts for saturday
        $all_k_duty_roster_saturday = DB::table('karkun_duty_rosters')
										->join('mehfils', 'karkun_duty_rosters.mehfil_id', '=', 'mehfils.id')
										->leftJoin('ehad_karkuns', 'karkun_duty_rosters.ehad_karkun', '=', 'ehad_karkuns.id')
										->select('karkun_duty_rosters.*', 'mehfils.mehfil_name', 'ehad_karkuns.ekp_name')
        								->where('mehfil_id' , $id)
        								->where('k_duty_days' , 'saturday')
										->where('month_name' , $month_is)
										->where('year_name' , $year_is)
        								->first();

        if (isset($all_k_duty_roster_saturday)) {

	        // $sdco = [];
	        $karkun_day_coord_selected_saturday = DB::table('karkuns')
	                                  ->whereIn('id' , explode(',' , $all_k_duty_roster_saturday->day_coordinator))
	                                  ->get();

	        foreach ($karkun_day_coord_selected_saturday as $sdco_single) {
	        	array_push($sdco, $sdco_single->id);
	        }


	        	// only for main gatian saturday
	        // $smd = [];
	        $all_karkun_mainduty_saturday_selected = DB::table('karkuns')
	                                  ->whereIn('id' , explode(',' , $all_k_duty_roster_saturday->k_main_gatian))
	                                  ->get();

	        foreach ($all_karkun_mainduty_saturday_selected as $smd_single) {
	        	array_push($smd, $smd_single->id);
	        }

	        	// only for cycle stand saturday
	        // $scs = [];
	        $all_karkun_cyclestand_saturday_selected = DB::table('karkuns')
	                                  ->whereIn('id' , explode(',' , $all_k_duty_roster_saturday->k_cycle_stand))
	                                  ->get();

	        foreach ($all_karkun_cyclestand_saturday_selected as $scs_single) {
	        	array_push($scs, $scs_single->id);
	        }

	        	// only for security saturday
	        $ss = [];
	        $all_karkun_security_saturday_selected = DB::table('karkuns')
	                                  ->whereIn('id' , explode(',' , $all_k_duty_roster_saturday->k_security))
	                                  ->get();

	        foreach ($all_karkun_security_saturday_selected as $ss_single) {
	        	array_push($ss, $ss_single->id);
	        }
	    }
        // code end for saturday

		//////////////////////////////////////////////////////////////////////////////////////////////////////

		/// code starts for sunday
        $all_k_duty_roster_sunday = DB::table('karkun_duty_rosters')
										->join('mehfils', 'karkun_duty_rosters.mehfil_id', '=', 'mehfils.id')
										->leftJoin('ehad_karkuns', 'karkun_duty_rosters.ehad_karkun', '=', 'ehad_karkuns.id')
										->select('karkun_duty_rosters.*', 'mehfils.mehfil_name', 'ehad_karkuns.ekp_name')
        								->where('mehfil_id' , $id)
        								->where('k_duty_days' , 'sunday')
										->where('month_name' , $month_is)
										->where('year_name' , $year_is)
        								->first();

        if (isset($all_k_duty_roster_sunday)) {

	        // $sndco = [];
	        $karkun_day_coord_selected_sunday = DB::table('karkuns')
	                                  ->whereIn('id' , explode(',' , $all_k_duty_roster_sunday->day_coordinator))
	                                  ->get();

	        foreach ($karkun_day_coord_selected_sunday as $sndco_single) {
	        	array_push($sndco, $sndco_single->id);
	        }

	        	// only for main gatian sunday
	        // $snmd = [];
	        $all_karkun_mainduty_sunday_selected = DB::table('karkuns')
	                                  ->whereIn('id' , explode(',' , $all_k_duty_roster_sunday->k_main_gatian))
	                                  ->get();

	        foreach ($all_karkun_mainduty_sunday_selected as $snmd_single) {
	        	array_push($snmd, $snmd_single->id);
	        }

	        	// only for cycle stand sunday
	        // $sncs = [];
	        $all_karkun_cyclestand_sunday_selected = DB::table('karkuns')
	                                  ->whereIn('id' , explode(',' , $all_k_duty_roster_sunday->k_cycle_stand))
	                                  ->get();

	        foreach ($all_karkun_cyclestand_sunday_selected as $sncs_single) {
	        	array_push($sncs, $sncs_single->id);
	        }

	        	// only for security sunday
	        // $sns = [];
	        $all_karkun_security_sunday_selected = DB::table('karkuns')
	                                  ->whereIn('id' , explode(',' , $all_k_duty_roster_sunday->k_security))
	                                  ->get();

	        foreach ($all_karkun_security_sunday_selected as $sns_single) {
	        	array_push($sns, $sns_single->id);
	        }
	    }

        // code end for sunday

		//////////////////////////////////////////////////////////////////////////////////////////////////////

		/// code starts for monday
        $all_k_duty_roster_monday = DB::table('karkun_duty_rosters')
										->join('mehfils', 'karkun_duty_rosters.mehfil_id', '=', 'mehfils.id')
										->leftJoin('ehad_karkuns', 'karkun_duty_rosters.ehad_karkun', '=', 'ehad_karkuns.id')
										->select('karkun_duty_rosters.*', 'mehfils.mehfil_name', 'ehad_karkuns.ekp_name')
        								->where('mehfil_id' , $id)
        								->where('k_duty_days' , 'monday')
										->where('month_name' , $month_is)
										->where('year_name' , $year_is)
        								->first();

        if (isset($all_k_duty_roster_monday)) {

	        // $mdco = [];
	        $karkun_day_coord_selected_monday = DB::table('karkuns')
	                                  ->whereIn('id' , explode(',' , $all_k_duty_roster_monday->day_coordinator))
	                                  ->get();

	        foreach ($karkun_day_coord_selected_monday as $mdco_single) {
	        	array_push($mdco, $mdco_single->id);
	        }
	        	// only for main gatian monday
	        // $mmd = [];
	        $all_karkun_mainduty_monday_selected = DB::table('karkuns')
	                                  ->whereIn('id' , explode(',' , $all_k_duty_roster_monday->k_main_gatian))
	                                  ->get();

	        foreach ($all_karkun_mainduty_monday_selected as $mmd_single) {
	        	array_push($mmd, $mmd_single->id);
	        }

	        	// only for cycle stand monday
	        // $mcs = [];
	        $all_karkun_cyclestand_monday_selected = DB::table('karkuns')
	                                  ->whereIn('id' , explode(',' , $all_k_duty_roster_monday->k_cycle_stand))
	                                  ->get();

	        foreach ($all_karkun_cyclestand_monday_selected as $mcs_single) {
	        	array_push($mcs, $mcs_single->id);
	        }

	        	// only for security monday
	        // $ms = [];
	        $all_karkun_security_monday_selected = DB::table('karkuns')
	                                  ->whereIn('id' , explode(',' , $all_k_duty_roster_monday->k_security))
	                                  ->get();

	        foreach ($all_karkun_security_monday_selected as $ms_single) {
	        	array_push($ms, $ms_single->id);
	        }
	    }

       // code end for monday

		//////////////////////////////////////////////////////////////////////////////////////////////////////

		/// code starts for tuesday
        $all_k_duty_roster_tuesday = DB::table('karkun_duty_rosters')
										->join('mehfils', 'karkun_duty_rosters.mehfil_id', '=', 'mehfils.id')
										->leftJoin('ehad_karkuns', 'karkun_duty_rosters.ehad_karkun', '=', 'ehad_karkuns.id')
										->select('karkun_duty_rosters.*', 'mehfils.mehfil_name', 'ehad_karkuns.ekp_name')
        								->where('mehfil_id' , $id)
        								->where('k_duty_days' , 'tuesday')
										->where('month_name' , $month_is)
										->where('year_name' , $year_is)
        								->first();

        if (isset($all_k_duty_roster_tuesday)) {

	        // $tdco = [];
	        $karkun_day_coord_selected_tuesday = DB::table('karkuns')
	                                  ->whereIn('id' , explode(',' , $all_k_duty_roster_tuesday->day_coordinator))
	                                  ->get();

	        foreach ($karkun_day_coord_selected_tuesday as $tdco_single) {
	        	array_push($tdco, $tdco_single->id);
	        }

	        // only for main gatian tuesday
	        // $tmd = [];
	        $all_karkun_mainduty_tuesday_selected = DB::table('karkuns')
	                                  ->whereIn('id' , explode(',' , $all_k_duty_roster_tuesday->k_main_gatian))
	                                  ->get();

	        foreach ($all_karkun_mainduty_tuesday_selected as $tmd_single) {
	        	array_push($tmd, $tmd_single->id);
	        }

	        	// only for cycle stand tuesday
	        // $tcs = [];
	        $all_karkun_cyclestand_tuesday_selected = DB::table('karkuns')
	                                  ->whereIn('id' , explode(',' , $all_k_duty_roster_tuesday->k_cycle_stand))
	                                  ->get();

	        foreach ($all_karkun_cyclestand_tuesday_selected as $tcs_single) {
	        	array_push($tcs, $tcs_single->id);
	        }

	        	// only for security tuesday
	        // $ts = [];
	        $all_karkun_security_tuesday_selected = DB::table('karkuns')
	                                  ->whereIn('id' , explode(',' , $all_k_duty_roster_tuesday->k_security))
	                                  ->get();

	        foreach ($all_karkun_security_tuesday_selected as $ts_single) {
	        	array_push($ts, $ts_single->id);
	        }
	    }

       // code end for tuesday

		//////////////////////////////////////////////////////////////////////////////////////////////////////

		/// code starts for wednesday
        $all_k_duty_roster_wednesday = DB::table('karkun_duty_rosters')
										->join('mehfils', 'karkun_duty_rosters.mehfil_id', '=', 'mehfils.id')
										->leftJoin('ehad_karkuns', 'karkun_duty_rosters.ehad_karkun', '=', 'ehad_karkuns.id')
										->select('karkun_duty_rosters.*', 'mehfils.mehfil_name', 'ehad_karkuns.ekp_name')
        								->where('mehfil_id' , $id)
        								->where('k_duty_days' , 'wednesday')
										->where('month_name' , $month_is)
										->where('year_name' , $year_is)
        								->first();

        if (isset($all_k_duty_roster_wednesday)) {

	        // $wdco = [];
	        $karkun_day_coord_selected_wednesday = DB::table('karkuns')
	                                  ->whereIn('id' , explode(',' , $all_k_duty_roster_wednesday->day_coordinator))
	                                  ->get();

	        foreach ($karkun_day_coord_selected_wednesday as $wdco_single) {
	        	array_push($wdco, $wdco_single->id);
	        }

	        	// only for main gatian wednesday
	        // $wmd = [];
	        $all_karkun_mainduty_wednesday_selected = DB::table('karkuns')
	                                  ->whereIn('id' , explode(',' , $all_k_duty_roster_wednesday->k_main_gatian))
	                                  ->get();

	        foreach ($all_karkun_mainduty_wednesday_selected as $wmd_single) {
	        	array_push($wmd, $wmd_single->id);
	        }

	        	// only for cycle stand wednesday
	        // $wcs = [];
	        $all_karkun_cyclestand_wednesday_selected = DB::table('karkuns')
	                                  ->whereIn('id' , explode(',' , $all_k_duty_roster_wednesday->k_cycle_stand))
	                                  ->get();

	        foreach ($all_karkun_cyclestand_wednesday_selected as $wcs_single) {
	        	array_push($wcs, $wcs_single->id);
	        }

	        	// only for security wednesday
	        // $ws = [];
	        $all_karkun_security_wednesday_selected = DB::table('karkuns')
	                                  ->whereIn('id' , explode(',' , $all_k_duty_roster_wednesday->k_security))
	                                  ->get();

	        foreach ($all_karkun_security_wednesday_selected as $ws_single) {
	        	array_push($ws, $ws_single->id);
	        }
	    }
       // code end for wednesday

		//////////////////////////////////////////////////////////////////////////////////////////////////////

		/// code starts for thursday
        $all_k_duty_roster_thursday = DB::table('karkun_duty_rosters')
										->join('mehfils', 'karkun_duty_rosters.mehfil_id', '=', 'mehfils.id')
										->leftJoin('ehad_karkuns', 'karkun_duty_rosters.ehad_karkun', '=', 'ehad_karkuns.id')
										->select('karkun_duty_rosters.*', 'mehfils.mehfil_name', 'ehad_karkuns.ekp_name')
        								->where('mehfil_id' , $id)
        								->where('k_duty_days' , 'thursday')
										->where('month_name' , $month_is)
										->where('year_name' , $year_is)
        								->first();

        	// only for main gatian thursday

        if (isset($all_k_duty_roster_thursday)) {

	        // $thdco = [];
	        $karkun_day_coord_selected_thursday = DB::table('karkuns')
	                                  ->whereIn('id' , explode(',' , $all_k_duty_roster_thursday->day_coordinator))
	                                  ->get();

	        foreach ($karkun_day_coord_selected_thursday as $thdco_single) {
	        	array_push($thdco, $thdco_single->id);
	        }

	        // $thmd = [];
	        $all_karkun_mainduty_thursday_selected = DB::table('karkuns')
	                                  ->whereIn('id' , explode(',' , $all_k_duty_roster_thursday->k_main_gatian))
	                                  ->get();

	        foreach ($all_karkun_mainduty_thursday_selected as $thmd_single) {
	        	array_push($thmd, $thmd_single->id);
	        }

	        	// only for cycle stand thursday
	        // $thcs = [];
	        $all_karkun_cyclestand_thursday_selected = DB::table('karkuns')
	                                  ->whereIn('id' , explode(',' , $all_k_duty_roster_thursday->k_cycle_stand))
	                                  ->get();

	        foreach ($all_karkun_cyclestand_thursday_selected as $thcs_single) {
	        	array_push($thcs, $thcs_single->id);
	        }

	        	// only for security thursday
	        // $ths = [];
	        $all_karkun_security_thursday_selected = DB::table('karkuns')
	                                  ->whereIn('id' , explode(',' , $all_k_duty_roster_thursday->k_security))
	                                  ->get();

	        foreach ($all_karkun_security_thursday_selected as $ths_single) {
	        	array_push($ths, $ths_single->id);
	        }
	    }
        /// code end for thursday

        // code for monthly_cordinator starts here
		if (isset($all_k_duty_roster_friday)) {

        // $m_c = [];
        $all_karkun_mainduty_friday_selected = DB::table('karkuns')
                                  ->whereIn('id' , explode(',' , $all_k_duty_roster_friday->monthly_cordinator))
                                  ->get();

	        foreach ($all_karkun_mainduty_friday_selected as $m_c_single) {
	        	array_push($m_c, $m_c_single->id);
	        }
	    }

	    // dd($fmd);
        return view('duty_roster_karkun.edit_dutyroster')
        							->with(compact([
        										'all_mehfils' , 
        										'all_ehad_karkun',
        										'all_karkun',
        										
        										// variables for friday
        										'all_k_duty_roster_friday' , 
        										'fmd', //main duty on friday
        										'fcs', // cycle stand duty on friday
        										'fs', // security duty on friday
        										'fdco', // day co ordinator on friday

        										// variables for saturday
        										'all_k_duty_roster_saturday' , 
        										'smd', //main duty on saturday
        										'scs', // cycle stand duty on saturday
        										'ss', // security duty on saturday
        										'sdco', // day co ordinator on saturday

        										// variables for sunday
        										'all_k_duty_roster_sunday' , 
        										'snmd', //main duty on sunday
        										'sncs', // cycle stand duty on sunday
        										'sns', // security duty on sunday
        										'sndco', // day co ordinator on sunday

        										// variables for monday
        										'all_k_duty_roster_monday' , 
        										'mmd', //main duty on monday
        										'mcs', // cycle stand duty on monday
        										'ms', // security duty on monday
        										'mdco', // day co ordinator on monday


        										// variables for tuesday
        										'all_k_duty_roster_tuesday' , 
        										'tmd', //main duty on tuesday
        										'tcs', // cycle stand duty on tuesday
        										'ts', // security duty on tuesday
        										'tdco', // day co ordinator on tuesday


        										// variables for wednesday
        										'all_k_duty_roster_wednesday' , 
        										'wmd', //main duty on wednesday
        										'wcs', // cycle stand duty on wednesday
        										'ws', // security duty on wednesday
        										'wdco', // day co ordinator on wednesday

        										// variables for thursday
        										'all_k_duty_roster_thursday' , 
        										'thmd', //main duty on thursday
        										'thcs', // cycle stand duty on thursday
        										'ths', // security duty on thursday
        										'thdco', // day co ordinator on thursday

        										// for monthly coordinator
        										'm_c',
        										'mehfil_id',
        										'mehfil_name_ne',

        										/// to get the ehad karkuns duty details
        										'get_ehad_karkun_juma',
        										'get_ehad_karkun_hafta',
        										'get_ehad_karkun_itwar',
        										'get_ehad_karkun_peer',
        										'get_ehad_karkun_mangal',
        										'get_ehad_karkun_budh',
        										'get_ehad_karkun_jumerat',
        										'not_save_msg',
        									]));
    }

    public function insertDutyKarkun(Request $request){


	    // all karkuns who have duty will be saved in this array and then their status will be updated by 1 as active
		$save_all_duty_waly_karkuns = array();

        $validator = Validator::make($request->all() , [
            'mehfils' => 'required',
        ]);

        $month = date('m');
        $year = date('Y');

		// dd($request->all());


        // DB::table('karkun_duty_rosters')->where('month_name' , $month)->where('mehfil_id' , $request->mehfils)->delete();
        // DB::table('karkun_duty_rosters')->where('month_name' , $month)->where('year_name', $year)->where('mehfil_id' , $request->mehfils)->delete();
        DB::table('karkun_duty_rosters')->where('month_name' , $month)
        								->where('year_name' , $year)
        								->where('mehfil_id' , $request->mehfils)
        								->delete();


        $mahana_coordinator_final = '';

        $main_duty_juma_final = '';
        $main_duty_hafta_final = '';
        $main_duty_itwar_final = '';
        $main_duty_peer_final = '';
        $main_duty_mangal_final = '';
        $main_duty_budh_final = '';
        $main_duty_jumerat_final = '';

        $security_juma_final = '';
        $security_hafta_final = '';
        $security_itwar_final = '';
        $security_peer_final = '';
        $security_mangal_final = '';
        $security_budh_final = '';
        $security_jumerat_final = '';

        $cycle_juma_final = '';
        $cycle_hafta_final = '';
        $cycle_itwar_final = '';
        $cycle_peer_final = '';
        $cycle_mangal_final = '';
        $cycle_budh_final = '';
        $cycle_jumerat_final = '';

        if (isset($request->mahana_coordinator) && $request->mahana_coordinator != 'null') {
 	        $mahana_coordinator_final = implode(',', $request->mahana_coordinator);
	        // mahana co-ordinators karkun
	        foreach($request->mahana_coordinator as $value) {
	        	array_push($save_all_duty_waly_karkuns, $value);	
	        }
        }

        if (isset($request->main_duty_juma) && $request->main_duty_juma != 'null') {
	        // juma k din kam krny waly
	        foreach($request->main_duty_juma as $value) {
	        	array_push($save_all_duty_waly_karkuns, $value);	
	        }
	        $main_duty_juma_final = implode(',', $request->main_duty_juma);
		}


        if (isset($request->cycle_stand_juma) && $request->cycle_stand_juma != 'null') {
	        // juma k din kam krny waly
	        foreach($request->cycle_stand_juma as $value) {
	        	array_push($save_all_duty_waly_karkuns, $value);	
	        }
	        $cycle_juma_final = implode(',', $request->cycle_stand_juma);
		}

        if (isset($request->security_juma) && $request->security_juma != 'null') {
	        // juma k din kam krny waly
	        foreach($request->security_juma as $value) {
	        	array_push($save_all_duty_waly_karkuns, $value);	
	        }        	
	        $security_juma_final = implode(',', $request->security_juma);
		}

        // for juma
        $data_juma = array(
        			'k_duty_days' => 'friday' ,
                    'ehad_karkun' =>  $request->ehd_karkun_juma,  
                    'k_main_gatian' =>  $main_duty_juma_final,  
                    'k_cycle_stand' =>  $cycle_juma_final,  
                    'k_security' =>  $security_juma_final,  
                    'mehfil_id' =>  $request->mehfils,  
                    'day_coordinator' =>  $request->day_coordinator_juma,  
                    'monthly_cordinator' =>  $mahana_coordinator_final,  
                    'month_name' =>  $month,  
                    'year_name' =>  $year,  
        );
        DB::table('karkun_duty_rosters')->insert($data_juma);


        if (isset($request->main_duty_hafta) && $request->main_duty_hafta != 'null') {
	        // hafta k din kam krny waly
	        foreach($request->main_duty_hafta as $value) {
	        	array_push($save_all_duty_waly_karkuns, $value);	
	        }
	        $main_duty_hafta_final = implode(',', $request->main_duty_hafta);
	    }

	    if (isset($request->cycle_stand_hafta) && $request->cycle_stand_hafta != 'null') {
	        // hafta k din kam krny waly
	        foreach($request->cycle_stand_hafta as $value) {
	        	array_push($save_all_duty_waly_karkuns, $value);	
	        }
	        $cycle_hafta_final = implode(',', $request->cycle_stand_hafta);
	    }

	    if (isset($request->security_hafta) && $request->security_hafta != 'null') {
	        // hafta k din kam krny waly
	        foreach($request->security_hafta as $value) {
	        	array_push($save_all_duty_waly_karkuns, $value);	
	        }
	        $security_hafta_final = implode(',', $request->security_hafta);
	    }

		// for hafta
        $data_hafta = array(
        			'k_duty_days' => 'saturday' ,
                    'ehad_karkun' =>  $request->ehd_karkun_hafta,  
                    'k_main_gatian' =>  $main_duty_hafta_final,  
                    'k_cycle_stand' =>  $cycle_hafta_final,  
                    'k_security' =>  $security_hafta_final,  
                    'mehfil_id' =>  $request->mehfils,  
                    'day_coordinator' =>  $request->day_coordinator_hafta,  
                    'monthly_cordinator' =>  $mahana_coordinator_final,  
                    'month_name' =>  $month,  
                    'year_name' =>  $year,  
        );
        DB::table('karkun_duty_rosters')->insert($data_hafta);


        if (isset($request->main_duty_itwar) && $request->main_duty_itwar != 'null') {
	        // itwar k din kam krny waly
	        foreach($request->main_duty_itwar as $value) {
	        	array_push($save_all_duty_waly_karkuns, $value);	
	        }
	        $main_duty_itwar_final = implode(',', $request->main_duty_itwar);
	    }

        if (isset($request->cycle_stand_itwar) && $request->cycle_stand_itwar != 'null') {
	        // itwar k din kam krny waly
	        foreach($request->cycle_stand_itwar as $value) {
	        	array_push($save_all_duty_waly_karkuns, $value);	
	        }
	        $cycle_itwar_final = implode(',', $request->cycle_stand_itwar);
	    }

        if (isset($request->security_itwar) && $request->security_itwar != 'null') {
	        // itwar k din kam krny waly
	        foreach($request->security_itwar as $value) {
	        	array_push($save_all_duty_waly_karkuns, $value);	
	        }
	        $security_itwar_final = implode(',', $request->security_itwar);
	    }
	
        // for sunday
        $data_itwar = array(
        			'k_duty_days' => 'sunday' ,
                    'ehad_karkun' =>  $request->ehd_karkun_itwar,  
                    'k_main_gatian' =>  $main_duty_itwar_final,  
                    'k_cycle_stand' =>  $cycle_itwar_final,  
                    'k_security' =>  $security_itwar_final,  
                    'mehfil_id' =>  $request->mehfils,  
                    'day_coordinator' =>  $request->day_coordinator_itwar,  
                    'monthly_cordinator' =>  $mahana_coordinator_final,  
                    'month_name' =>  $month,  
                    'year_name' =>  $year,  
        );
        DB::table('karkun_duty_rosters')->insert($data_itwar);


        if (isset($request->main_duty_peer) && $request->main_duty_peer != 'null') {
	        // peer k din kam krny waly
	        foreach($request->main_duty_peer as $value) {
	        	array_push($save_all_duty_waly_karkuns, $value);	
	        }
	        $main_duty_peer_final = implode(',', $request->main_duty_peer);
	    }


        if (isset($request->cycle_stand_peer) && $request->cycle_stand_peer != 'null') {
	        // peer k din kam krny waly
	        foreach($request->cycle_stand_peer as $value) {
	        	array_push($save_all_duty_waly_karkuns, $value);	
	        }
	        $cycle_peer_final = implode(',', $request->cycle_stand_peer);
	    }

        if (isset($request->security_peer) && $request->security_peer != 'null') {
	        // peer k din kam krny waly
	        foreach($request->security_peer as $value) {
	        	array_push($save_all_duty_waly_karkuns, $value);	
	        }
	        $security_peer_final = implode(',', $request->security_peer);
	    }

        // for monday
        $data_peer = array(
        			'k_duty_days' => 'monday' ,
                    'ehad_karkun' =>  $request->ehd_karkun_peer,  
                    'k_main_gatian' =>  $main_duty_peer_final,  
                    'k_cycle_stand' =>  $cycle_peer_final,  
                    'k_security' =>  $security_peer_final,  
                    'mehfil_id' =>  $request->mehfils,  
                    'day_coordinator' =>  $request->day_coordinator_peer,  
                    'monthly_cordinator' =>  $mahana_coordinator_final,  
                    'month_name' =>  $month,  
                    'year_name' =>  $year,  
        );
        DB::table('karkun_duty_rosters')->insert($data_peer);


        if (isset($request->main_duty_mangal) && $request->main_duty_mangal != 'null') {
	        // mangal k din kam krny waly
	        foreach($request->main_duty_mangal as $value) {
	        	array_push($save_all_duty_waly_karkuns, $value);	
	        }
	        $main_duty_mangal_final = implode(',', $request->main_duty_mangal);
	    }

        if (isset($request->cycle_stand_mangal) && $request->cycle_stand_mangal != 'null') {
	        // mangal k din kam krny waly
	        foreach($request->cycle_stand_mangal as $value) {
	        	array_push($save_all_duty_waly_karkuns, $value);	
	        }
	        $cycle_mangal_final = implode(',', $request->cycle_stand_mangal);
	    }

        if (isset($request->security_mangal) && $request->security_mangal != 'null') {
	        // mangal k din kam krny waly	    
	        foreach($request->security_mangal as $value) {
	        	array_push($save_all_duty_waly_karkuns, $value);	
	        }
	        $security_mangal_final = implode(',', $request->security_mangal);
		}

        // for mangal
        $data_mangal = array(
        			'k_duty_days' => 'tuesday' ,
                    'ehad_karkun' =>  $request->ehd_karkun_mangal,  
                    'k_main_gatian' =>  $main_duty_mangal_final,  
                    'k_cycle_stand' =>  $cycle_mangal_final,  
                    'k_security' =>  $security_mangal_final,  
                    'mehfil_id' =>  $request->mehfils,  
                    'day_coordinator' =>  $request->day_coordinator_mangal,  
                    'monthly_cordinator' =>  $mahana_coordinator_final,  
                    'month_name' =>  $month,  
                    'year_name' =>  $year,  
        );
        DB::table('karkun_duty_rosters')->insert($data_mangal);


        if (isset($request->main_duty_budh) && $request->main_duty_budh != 'null') {
	        // budh k din kam krny waly
	        foreach($request->main_duty_budh as $value) {
	        	array_push($save_all_duty_waly_karkuns, $value);	
	        }
	        $main_duty_budh_final = implode(',', $request->main_duty_budh);
	    }

        if (isset($request->cycle_stand_budh) && $request->cycle_stand_budh != 'null') {
	        // budh k din kam krny waly
	        foreach($request->cycle_stand_budh as $value) {
	        	array_push($save_all_duty_waly_karkuns, $value);	
	        }
	        $cycle_budh_final = implode(',', $request->cycle_stand_budh);
	    }

        if (isset($request->security_budh) && $request->security_budh != 'null') {
	        // budh k din kam krny waly
	        foreach($request->security_budh as $value) {
	        	array_push($save_all_duty_waly_karkuns, $value);	
	        }
	        $security_budh_final = implode(',', $request->security_budh);
	    }

   		// for wednesday
        $data_budh = array(
        			'k_duty_days' => 'wednesday' ,
                    'ehad_karkun' =>  $request->ehd_karkun_budh,  
                    'k_main_gatian' =>  $main_duty_budh_final,  
                    'k_cycle_stand' =>  $cycle_budh_final,  
                    'k_security' =>  $security_budh_final,  
                    'mehfil_id' =>  $request->mehfils,  
                    'day_coordinator' =>  $request->day_coordinator_budh,  
                    'monthly_cordinator' =>  $mahana_coordinator_final,  
                    'month_name' =>  $month,  
                    'year_name' =>  $year,  
        );
        DB::table('karkun_duty_rosters')->insert($data_budh);


        if (isset($request->main_duty_jumerat) && $request->main_duty_jumerat != 'null') {
	        // jumerat k din kam krny waly
	        foreach($request->main_duty_jumerat as $value) {
	        	array_push($save_all_duty_waly_karkuns, $value);	
	        }
	        $main_duty_jumerat_final = implode(',', $request->main_duty_jumerat);
	    }

        if (isset($request->cycle_stand_jumerat) && $request->cycle_stand_jumerat != 'null') {
	        // jumerat k din kam krny waly
	        foreach($request->cycle_stand_jumerat as $value) {
	        	array_push($save_all_duty_waly_karkuns, $value);	
	        }
	        $cycle_jumerat_final = implode(',', $request->cycle_stand_jumerat);
	    }

        if (isset($request->security_jumerat) && $request->security_jumerat != 'null') {
	        // jumerat k din kam krny waly
	        foreach($request->security_jumerat as $value) {
	        	array_push($save_all_duty_waly_karkuns, $value);	
	        }
	        $security_jumerat_final = implode(',', $request->security_jumerat);
	    }

        // for thursday
        $data_jumerat = array(
        			'k_duty_days' => 'thursday' ,
                    'ehad_karkun' =>  $request->ehd_karkun_jumerat,  
                    'k_main_gatian' =>  $main_duty_jumerat_final,  
                    'k_cycle_stand' =>  $cycle_jumerat_final,  
                    'k_security' =>  $security_jumerat_final,  
                    'mehfil_id' =>  $request->mehfils,  
                    'day_coordinator' =>  $request->day_coordinator_jumerat,  
                    'monthly_cordinator' =>  $mahana_coordinator_final,  
                    'month_name' =>  $month,  
                    'year_name' =>  $year,  
        );

        DB::table('karkun_duty_rosters')->insert($data_jumerat);


	        $data_duty_active = array('duty_active' => 0);
	        DB::table('karkuns')->where('mehfil_id' , $request->mehfils)->update($data_duty_active);		
		
			$without_duplicate = array_unique($save_all_duty_waly_karkuns);
	        foreach($without_duplicate as $values1) {
		        $data_duty_active = array('duty_active' => 1);
		        DB::table('karkuns')
		        				->where('mehfil_id' , $request->mehfils)
		        				->where('id' , $values1)
		        				->update($data_duty_active);
	        }

            $get_last_id = DB::table('karkun_duty_rosters')
            					->orderByDesc('id')
            					->limit(1)
            					->first();
    	
        return redirect('dutyrosterkarkun/edit/'.$get_last_id->mehfil_id);

    }
   
   // for printing the page
    public function printDutyKarkun($id , Request $request){

        $month = date('m');
        $year = date('Y');

		$karkun_duty_rosters = DB::table('karkun_duty_rosters')
										->join('mehfils', 'karkun_duty_rosters.mehfil_id', '=', 'mehfils.id')
										->leftJoin('ehad_karkuns', 'karkun_duty_rosters.ehad_karkun', '=', 'ehad_karkuns.id')
										->select('karkun_duty_rosters.*', 'mehfils.mehfil_name', 'ehad_karkuns.ekp_name' , 'ehad_karkuns.ekp_phone' , 'ehad_karkuns.id')
        								->where('karkun_duty_rosters.mehfil_id' , $id)
        								->where('karkun_duty_rosters.month_name' , $month)
        								->where('karkun_duty_rosters.year_name' , $year)
        								->get();

        $single_mehfil = DB::table('mehfils')->where('id' , $id)->first();
        // $mehfil_dtl = $single_mehfil->mehfil_address . ' ' . $single_mehfil->mehfil_city;

        return view('duty_roster_karkun.print_duty_roster')
        							->with(compact([
        										'karkun_duty_rosters',
        										'single_mehfil'
        									]));
    }


}
