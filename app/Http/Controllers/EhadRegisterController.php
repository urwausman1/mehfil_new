<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\EhadRegister;

use PragmaRX\Countries\Package\Countries;

class EhadRegisterController extends Controller{

    public function showData(){

        $mehfil_id = session('mehfil_id');
        $city = session('city');
        $role = session('user_role');

        if($role == 'mehfil'){
            $ehad_registers = DB::table('ehad_registers')
                                ->join('mehfils', 'ehad_registers.mehfil_id', '=', 'mehfils.id')
                                ->select('ehad_registers.*' , 'mehfils.mehfil_name' , 'mehfils.mehfil_number' , 'mehfils.mehfil_city' ,'mehfils.mehfil_country')
                                ->where('mehfil_id' , $mehfil_id)
                                ->get();
        }else{
            $ehad_registers = DB::table('ehad_registers')
                                    ->join('mehfils', 'ehad_registers.mehfil_id', '=', 'mehfils.id')
                                    ->select('ehad_registers.*', 'mehfils.mehfil_name' , 'mehfils.mehfil_number' , 'mehfils.mehfil_city' ,'mehfils.mehfil_country')
                                    ->get();
        }

                                
        return view('just_ehad.ehad_register')->with(compact('ehad_registers'));

    }
    
    public function addEhadRegisterPage(){
        
        // $countries = new Countries();
        // $all_mulk = $countries->all();

        // $mehfil_id = session('mehfil_id');
        $city = session('city');
        $role = session('user_role');

        $all_mulk = DB::table('countries_names')->get();

        $all_mahafil = DB::table('mehfils')->get();


        if ($role == 'mehfil') {
            $mehfil_id = session("mehfil_id");
            $mehfils = DB::table('mehfils')->where('id' , $mehfil_id)->first();
            $mehfil_name = $mehfils->mehfil_name;

            return view('just_ehad.add_ehad_register')->with(compact(['all_mulk' , 'all_mahafil' , 'city' , 'mehfil_id' , 'mehfil_name']));
        }else{
            // agar cleint kahy ga to yaha pr ba city variable ko call kr len gy compact mn
            return view('just_ehad.add_ehad_register')->with(compact(['all_mulk' , 'all_mahafil']));
        }
    }

    public function insertEhadRegister(Request $request){
        

        $validator = Validator::make($request->all() , [
            // 'id_number' => 'required',
            'name' => 'required',
            'er_name_en' => 'required',
            'fname' => 'required',
            'phone' => 'required',
            // 'email' => 'email',
            'marfat' => 'required',
            // 'cnic' => 'required',
            // 'address' => 'required',
            // 'city' => 'required',
            // 'country' => 'required',
            'dob' => 'required',
            'mehfilname' => 'required',
            'doe' => 'required',

            // 'cc02_city' => 'required',
            // 'cc03_city' => 'required',
            // 'city_name_ur' => 'required',
        ]);
        
        if ($validator->passes()) {
        // dd($request->all());


            // $data_admin = array(
            //             'admin_username' => $request->name , 
            //             'admin_email' => $request->email , 
            //             'admin_password' => 'usman' , 
            //             'admin_access_level' => 'ehdkarkun' , 
            //             'admin_status' => 1 , 
            // );

            // DB::table('users')->insert($data_admin);
            // $last = DB::table('users')->orderBy('id', 'desc')->first();

            // dd($last);

            // dd($last->id);
            $data = array(
                        // 'ekp_id_number' => $request->id_number, 
                        'er_name' => $request->name, 
                        'er_name_en' => $request->er_name_en, 
                        'er_fname' => $request->fname, 
                        'er_phone' => $request->phone, 
                        'er_email' => $request->email, 
                        'er_address' => $request->address, 
                        'er_city' => $request->cc03_city, 
                        'er_country' => $request->country, 
                        'er_dob' => $request->dob, 
                        'er_doe' => $request->doe, 
                        'er_marfat' => $request->marfat, 
                        'er_cnic' => $request->cnic, 
                        'mehfil_id' => $request->mehfilname, 
                        // 'admin_id' => $last->id, 
                    );
    
            DB::table('ehad_registers')->insert($data);

            // to update city data in cities table
            // $data_city = array(
            //                 'cc02_city' => $request->cc02_city , 
            //                 'cc03_city' => $request->cc03_city , 
            //                 'city_name_ur' => $request->city_name_ur , 
            //             );

            // DB::table('cities__names')->where('city_name_en' , $request->city)->update($data_city);

            $request->session()->flash('msg' , 'معلومات کا اندراج ہو چکا ہے.');
    
            return redirect('ehadregister/add');

        }else{
            return redirect('ehadregister/add')->withErrors($validator)->withInput();
        }
    }


    function editEhadRegister($id, Request $request){

        // $countries = new Countries();
        // $all_mulk = $countries->all();
        $all_mulk = DB::table('countries_names')->get();
        $all_mahafil = DB::table('mehfils')->get();
        $single_ehdregister = DB::table('ehad_registers')->where('id', $id)->first();

        $get_city_dtl = DB::table('cities__names')->where('cc03_city', $single_ehdregister->er_city)->first();

        $mehfil_ka_name = DB::table('mehfils')->where('id' , $single_ehdregister->mehfil_id)->first();
        $mehfil_ka_name_final = $mehfil_ka_name->mehfil_name;


        if (!$single_ehdregister) {
            $request->session()->flash('msg' , 'آپکا مطلوبہ مواد موجود نہیں ہے۔');
            return redirect('ehadregister');
        }else{
            return view('just_ehad.edit_ehad_register')->with(compact(['single_ehdregister' , 'all_mulk' , 'all_mahafil' , 'get_city_dtl']))->with('mehfil_ka_name' , $mehfil_ka_name_final);
        }
    
    }

    function updateEhadRegister($id , Request $request){



        $validator = Validator::make($request->all() , [
            // 'id_number' => 'required',
            'er_name_en' => 'required',
            'name' => 'required',
            'fname' => 'required',
            'phone' => 'required',
            // 'email' => 'required|email',
            // 'cnic' => 'required',
            // 'address' => 'required',
            // 'city' => 'required',
            'doe' => 'required',
            'dob' => 'required',
            // 'country' => 'required',
            'mehfilname' => 'required',

            // 'cc02_city' => 'required',
            'cc03_city' => 'required',
            // 'city_name_ur' => 'required',
        ]);
        
        if ($validator->passes()) {

            $data = array(
                        // 'er_id_number' => $request->id_number, 
                        'er_name_en' => $request->er_name_en, 
                        'er_name' => $request->name, 
                        'er_fname' => $request->fname, 
                        'er_phone' => $request->phone, 
                        'er_email' => $request->email, 
                        'er_address' => $request->address, 
                        'er_city' => $request->cc03_city, 
                        'er_country' => $request->country, 
                        'er_dob' => $request->dob, 
                        'er_doe' => $request->doe, 
                        'er_marfat' => $request->marfat, 
                        'er_cnic' => $request->cnic, 
                        'mehfil_id' => $request->mehfilname, 
                    );
    
            DB::table('ehad_registers')->where('id' , $id)->update($data);

            // to update city data in cities table
            // $data_city = array(
            //                 'cc02_city' => $request->cc02_city , 
            //                 'cc03_city' => $request->cc03_city , 
            //                 'city_name_ur' => $request->city_name_ur , 
            //             );

            // DB::table('cities__names')->where('city_name_en' , $request->city)->update($data_city);

            // get the last id to update the data
            // $getIdForUpdate = DB::table('ehad_karkuns')->where('id' , $id)->first();

            // $data_admin = array(
            //             'admin_username' => $request->name , 
            //             'admin_email' => $request->email , 
            //             'admin_password' => 'usman' , 
            //             'admin_access_level' => 'ehdkarkun' , 
            //             'admin_status' => 1 , 
            // );

            // DB::table('users')->where('id' , $getIdForUpdate->admin_id)->update($data_admin);


            $request->session()->flash('msg' , 'معلومات میں تبدیلی کر دی گیئ ہے۔');    
            return redirect('ehadregister');

        }else{
            return redirect('ehadregister/edit/'.$id)->withErrors($validator)->withInput();
        }
    }



}
