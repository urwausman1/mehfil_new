<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

use PragmaRX\Countries\Package\Countries;
use App\Karkun;

// use Khsing\World\World;



class KarkunController extends Controller
{

    public function showData()
    {
        $mehfil_id = session('mehfil_id');
        $city = session('city');
        $role = session('user_role');

        $all_karkun = '';
        if ($role == 'superuser') {
            $all_karkun = DB::table('karkuns')->orderby('id' , 'desc')->get();            
        }else if($role == 'ehdkarkun'){
            $all_karkun = DB::table('karkuns')->where('kp_city' , $city)->orderby('id' , 'desc')->get();
        }else{
            $all_karkun = DB::table('karkuns')->where('mehfil_id' , $mehfil_id)->orderby('id' , 'desc')->get();
        }

        return view('karkun.karkun')->with(compact('all_karkun'));
    }

    public function get_duty_waly_karkun()
    {
        $mehfil_id = session('mehfil_id');
        
        $not_showing = '';
        
        if ($mehfil_id == '' || $mehfil_id == null) {
            $not_showing = ' برائے مہربانی محفل کے مندرجات  سے  داخل ہوں۔  ';
        }

        $all_karkun = DB::table('karkuns')->where('mehfil_id' , $mehfil_id)->where('duty_active' , 1)->get();
        return view('karkun.karkunroster')->with(compact(['all_karkun' , 'not_showing']));
    }

    public function insertKarkun_multiple()
    {

        // client  ny kaha k jo b (mehfil ya ehad karkun) login hy us ka city show hona chahhiye
        $city = session("city");
        $role = session('user_role');

        $all_mulk = DB::table('countries_names')->get();

        $all_mahafil = DB::table('mehfils')->get();

        if ($role == 'mehfil') {
            $mehfil_id = session("mehfil_id");
            $mehfils = DB::table('mehfils')->where('id' , $mehfil_id)->first();
            $mehfil_name = $mehfils->mehfil_name;

            return view('karkun.karkun_multiple')->with(compact(['all_mahafil', 'all_mulk' , 'city' , 'mehfil_id' , 'mehfil_name']));
        }else{    
            return view('karkun.karkun_multiple')->with(compact(['all_mulk', 'all_mahafil' , 'city']));
        }


    }

    public function addKarkunPage()
    {

        // client  ny kaha k jo b (mehfil ya ehad karkun) login hy us ka city show hona chahhiye
        $city = session("city");
        $role = session('user_role');

        $all_mulk = DB::table('countries_names')->get();

        $all_mahafil = DB::table('mehfils')->get();

        if ($role == 'mehfil') {
            $mehfil_id = session("mehfil_id");
            $mehfils = DB::table('mehfils')->where('id' , $mehfil_id)->first();
            $mehfil_name = $mehfils->mehfil_name;

            return view('karkun.add_karkun')->with(compact(['all_mahafil', 'all_mulk' , 'city' , 'mehfil_id' , 'mehfil_name']));
        }else{
            return view('karkun.add_karkun')->with(compact(['all_mahafil', 'all_mulk' , 'city']));
        }

    }

    public function insertKarkun(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'name_en' => 'required',
            'id_number' => 'required',
            'fname' => 'required',
            'phone' => 'required',
            // 'email' => 'required|email',
            'cnic' => 'required',
            // 'last_wazaif_tarteeb' => 'required',
            // 'address' => 'required',
            // 'city' => 'required',
            // 'country' => 'required',
            'mehfilname' => 'required',

            // 'cc02_city' => 'required',
            'cc03_city' => 'required',
            // 'city_name_ur' => 'required',
        ]);


            // to make id number for karkun
            $num = $request->id_number; 
            $str_length = 3; 
            // Left padding if number < $str_length 
            $str = substr("000{$num}", -$str_length); 
            $id_number = sprintf($str); 

            $get_mehfil_number = DB::table("mehfils")->where('id' , $request->mehfilname)->first();

            // to add zeros before mehfil number
            $num_m = $get_mehfil_number->mehfil_number; 
            $str_length_m = 2; 
            // Left padding if number < $str_length 
            $str_m = substr("00{$num_m}", -$str_length_m); 
            $id_number_m = sprintf($str_m); 

            $new_id_number = $id_number_m.$id_number;
            // till here
            // dd($new_id_number);

        if ($validator->passes()) {

            $data_admin = array(
                'admin_username' => $new_id_number,
                'admin_email' => $request->email,
                'admin_password' => 'usman',
                'admin_access_level' => 'karkun',
                'admin_status' => 1,
            );

            DB::table('users')->insert($data_admin);
            $last = DB::table('users')->orderBy('id', 'desc')->first();


            $data = array(
                'kp_id_number' => $new_id_number,
                'kp_name' => $request->name,
                'kp_name_en' => $request->name_en,
                'kp_fname' => $request->fname,
                'kp_fname_en' => $request->fname_en,
                'kp_phone' => $request->phone,
                'kp_email' => $request->email,
                'kp_address' => $request->address,
                'kp_city' => $request->cc03_city,
                'kp_country' => $request->country,
                'kp_cnic' => $request->cnic,
                'kp_dob' => $request->dob."-01-01",
                'kp_doe' => $request->doe."-01-01",
                'kp_marfat' => $request->marfat,
                'mehfil_id' => $request->mehfilname,
                'last_wazaif_date' => $request->last_wazaif_tarteeb,
                'admin_id' => $last->id,
            );

            DB::table('karkuns')->insert($data);

            // to update city data in cities table
            // $data_city = array(
            //     'cc02_city' => $request->cc02_city,
            //     'cc03_city' => $request->cc03_city,
            //     'city_name_ur' => $request->city_name_ur,
            // );

            // DB::table('cities__names')->where('city_name_en', $request->city)->update($data_city);


            $request->session()->flash('msg', 'معلومات کا اندراج ہو چکا ہے.');

            return redirect('karkun/add');
        } else {
            return redirect('karkun/add')->withErrors($validator)->withInput();
        }
    }


    // to insert multiple karkuns
    public function insertKarkun_multiple_karkun(Request $request){

        // single variables
        $mulk_name = $request->mulk_name;
        // $city_name = $request->city_name;
        // $city_cc02 = $request->city_cc02;
        $city_cc03 = $request->city_cc03;
        // $city_name_ur = $request->city_name_ur;
        $mehfil_id = $request->mehfil_id;


        // to update city data in cities table
        // $data_city = array(
        //     'cc02_city' => $city_cc02,
        //     'cc03_city' => $city_cc03,
        //     'city_name_ur' => $city_name_ur,
        // );
        // DB::table('cities__names')->where('city_name_en', $city_name)->update($data_city);


        // arrays
        $hazri_code = $request->hazri_code;
        $name_en = $request->name_en;
        $fname_en = $request->fname_en;
        $name_ur = $request->name_ur;
        $fname_ur = $request->fname_ur;
        $mobile_number = $request->mobile_number;
        $cnic = $request->cnic;
        $marfat = $request->marfat;
        $dob = $request->dob;
        $doe = $request->doe;


        for ($i = 0; $i < count($hazri_code); $i++) {
            $hazri_code_loop =  $hazri_code[$i];
            $name_en_loop =  $name_en[$i];
            $fname_en_loop =  $fname_en[$i];
            $name_ur_loop =  $name_ur[$i];
            $fname_ur_loop =  $fname_ur[$i];
            $mobile_number_loop =  $mobile_number[$i];
            $cnic_loop =  $cnic[$i];
            $marfat_loop =  $marfat[$i];
            $dob_loop =  $dob[$i];
            $doe_loop =  $doe[$i];


            // // to make id number for karkun
            // $num = $request->id_number; 
            // $str_length = 3; 
            // // Left padding if number < $str_length 
            // $str = substr("000{$num}", -$str_length); 
            // $id_number = sprintf($str); 

            // $get_mehfil_number = DB::table("mehfils")->where('id' , $request->mehfilname)->first();

            // // to add zeros before mehfil number
            // $num_m = $get_mehfil_number->mehfil_number; 
            // $str_length_m = 2; 
            // // Left padding if number < $str_length 
            // $str_m = substr("00{$num_m}", -$str_length_m); 
            // $id_number_m = sprintf($str_m); 

            // $new_id_number = $id_number_m.$id_number;
            // // till here
            // // dd($new_id_number);





            // $total_age_years = $dob_loop . " years";
            // $date = date_create(date('Y-m-d'));
            // date_sub($date, date_interval_create_from_date_string($total_age_years));
            // $final_age_date = date_format($date, "Y-m-d");

            // $total_ehad_years = $doe_loop . " years";
            // $date1 = date_create(date('Y-m-d'));
            // date_sub($date1, date_interval_create_from_date_string($total_ehad_years));
            // $final_ehad_date = date_format($date1, "Y-m-d");


            $email = "karkun" . $hazri_code_loop . "@gmail.com";

            $data_admin = array(
                'admin_username' => $hazri_code_loop,
                'admin_email' => $email,
                'admin_password' => 'usman',
                'admin_access_level' => 'karkun',
                'admin_status' => 1,
            );

            DB::table('users')->insert($data_admin);
            $last = DB::table('users')->orderBy('id', 'desc')->first();

            $data = array(
                'kp_id_number' => $hazri_code_loop,
                'kp_name' => $name_ur_loop,
                'kp_name_en' => $name_en_loop,
                'kp_fname' => $fname_ur_loop,
                'kp_fname_en' => $fname_en_loop,
                'kp_phone' => $mobile_number_loop,
                'kp_email' => $email,
                'kp_city' => $city_cc03,
                // 'kp_country' => $mulk_name,
                'kp_cnic' => $cnic_loop,
                'kp_dob' => $dob_loop."-01-01",
                'kp_doe' => $doe_loop."-01-01",
                'kp_marfat' => $marfat_loop,
                'mehfil_id' => $mehfil_id,
                'admin_id' => $last->id,
            );

            DB::table('karkuns')->insert($data);
        }

        echo 'معلومات کا اندراج ہو چکا ہے.';
    }



    function editKarkun($id, Request $request)
    {
        $all_mulk = DB::table('countries_names')->get();

        $all_mahafil = DB::table('mehfils')->get();

        $single_karkun = DB::table('karkuns')
            // ->join('mehfils', 'karkuns.mehfil_id' , '=', 'mehfils.id')
            ->where('karkuns.id', $id)
            ->first();

        $get_city_dtl = DB::table('cities__names')->where('cc03_city', $single_karkun->kp_city)->first();

        $mehfil_ka_name = DB::table('mehfils')->where('id', $single_karkun->mehfil_id)->first();
        $mehfil_ka_name_final = $mehfil_ka_name->mehfil_name;


        if (!$single_karkun) {
            $request->session()->flash('msg', 'آپکا مطلوبہ مواد موجود نہیں ہے۔');
            return redirect('karkun');
        } else {
            // dd($single_karkun);
            return view('karkun.edit_karkun')->with(compact(['single_karkun', 'all_mahafil', 'all_mulk', 'get_city_dtl']))->with('mehfil_ka_name', $mehfil_ka_name_final);
        }
    }

    function updateKarkun($id, Request $request)
    {



        $validator = Validator::make($request->all(), [
            'id_number' => 'required',
            'name' => 'required',
            'name_en' => 'required',
            'fname' => 'required',
            'phone' => 'required',
            // 'email' => 'required|email',
            // 'last_wazaif_tarteeb' => 'required',
            'cnic' => 'required',
            'address' => 'required',
            // 'city' => 'required',
            // 'country' => 'required',
            'mehfilname' => 'required',

            // 'cc02_city' => 'required',
            'cc03_city' => 'required',
            // 'city_name_ur' => 'required',
        ]);

        if ($validator->passes()) {

            $data = array(
                'kp_id_number' => $request->id_number,
                'kp_name' => $request->name,
                'kp_name_en' => $request->name_en,
                'kp_fname' => $request->fname,
                'kp_fname_en' => $request->fname_en,
                'kp_phone' => $request->phone,
                'kp_email' => $request->email,
                'kp_address' => $request->address,
                'kp_city' => $request->cc03_city,
                'kp_country' => $request->country,
                'kp_cnic' => $request->cnic,
                'kp_dob' => $request->dob."-01-01",
                'kp_doe' => $request->doe."-01-01",
                'kp_marfat' => $request->marfat,
                'last_wazaif_date' => $request->last_wazaif_tarteeb,
                'mehfil_id' => $request->mehfilname,
            );

            DB::table('karkuns')->where('id', $id)->update($data);


            // get the last id to update the data
            $getIdForUpdate = DB::table('karkuns')->where('id', $id)->first();

            // // to update city data in cities table
            // $data_city = array(
            //     'cc02_city' => $request->cc02_city,
            //     'cc03_city' => $request->cc03_city,
            //     'city_name_ur' => $request->city_name_ur,
            // );

            // DB::table('cities__names')->where('city_name_en', $request->city)->update($data_city);

            // dd($getIdForUpdate);

            $data_admin = array(
                'admin_username' => $request->id_number,
                'admin_email' => $request->email,
                'admin_password' => 'usman',
                'admin_access_level' => 'karkun',
                'admin_status' => 1,
            );

            DB::table('users')->where('id', $getIdForUpdate->admin_id)->update($data_admin);


            $request->session()->flash('msg', 'معلومات میں تبدیلی کر دی گیئ ہے۔');
            return redirect('karkun');
        } else {
            return redirect('karkun/edit/' . $id)->withErrors($validator)->withInput();
        }
    }


    public function cityNameGet(Request $request)
    {

        $mulkname = $request->mulkname;

        // $countries = new Countries();
        // $urwa = $countries->where('cca2', $mulkname)
        //                         ->first()
        //                         ->hydrate('cities')
        //                         ->cities;

        $urwa = DB::table('cities__names')->where('cc02', $mulkname)->get();

        foreach ($urwa as $key => $value) {
            if ($value->city_name_ur == null || $value->city_name_ur == '') {
                echo "<option value='$value->city_name_en'>$value->city_name_en</option>";
            } else {
                echo "<option value='$value->city_name_en'>$value->city_name_ur</option>";
            }
        }
        exit();
    }


    public function cityNameRelevantDtl(Request $request)
    {

        $cityname_en = $request->cityname;
        $urwa = DB::table('cities__names')->where('city_name_en', $cityname_en)->first();

        // dd($urwa);

        // $cc02_city = "<div class='form-group col-sm-12'><label>  دو حرفی نام لکھیں   </lable>";    
        // $cc02_city .= "<input class='form-control' name='cc02_city' id='cc02_city' type='text' value='$urwa->cc02_city' ></div>";

        // $cc03_city = "<div class='form-group col-sm-12'><label>  تین  حرفی نام لکھیں    </lable>";    
        // $cc03_city .= "<input class='form-control' name='cc03_city' id='cc03_city' type='text' value='$urwa->cc03_city' ></div>";

        // $city_name_ur = "<div class='form-group col-sm-12'><label>  اردو میں نام لکھیں    </lable>";    
        // $city_name_ur .= "<input class='form-control' name='city_name_ur' id='city_name_ur' type='text' value='$urwa->city_name_ur' ></div>";

        // $data = "<div class='col-md-4'>$cc02_city";
        // $data .= "</div>";

        // $data .= "<div class='col-md-4'>$cc03_city";
        // $data .= "</div>";

        // $data .= "<div class='col-md-4'>$city_name_ur";
        // $data .= "</div>";

        $cc02 = $urwa->cc02_city;
        $cc03 = $urwa->cc03_city;
        $city_name_ur = $urwa->city_name_ur;


        $data = array(
            'cc02' => $cc02,
            'cc03' => $cc03,
            'city_name_ur' => $city_name_ur,
        );

        echo json_encode($data);

        exit();
    }
}
