<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\All_group;

class All_GroupsController extends Controller{


    public function show_data($id , Request $request){

        $committee_id = $id;
        $get_committe_dtls = DB::table('committees')->where('id' , $id)->first();
        $check_level = $get_committe_dtls->committee_category;
        $committee_name = $get_committe_dtls->committee_name;

        $get_all_members = '';
        $city_logged_in = session('city');

        $get_all_selected = '';

        // dd($check_level);

        if ($check_level == 'city') {

            // to get all of the selected ehad karkuns
            $get_all_selected = DB::table('all_groups')
                                ->join('ehad_karkuns' , 'ehad_karkuns.id' , 'all_groups.karkun_id')
                                ->where('all_groups.category' , $check_level)
                                ->where('all_groups.com_id' , $committee_id)
                                ->where('ehad_karkuns.ekp_city' , $city_logged_in)
                                ->get();

            // to get all of the deslected ehad karkuns
           $get_all_members_not_selected = DB::select(DB::raw("SELECT * FROM ehad_karkuns WHERE ekp_city = '$city_logged_in' AND id NOT IN (SELECT karkun_id FROM all_groups WHERE category = '$check_level' AND com_id = $committee_id )"));

            return view('groups.groups')->with(compact([
                                            'check_level' , 
                                            'city_logged_in' , 
                                            'committee_id' , 
                                            'get_all_members_not_selected' , 
                                            'get_all_selected' , 
                                            'committee_name'
                                        ]));

        }
        else{

            $get_id_mehfil = session('mehfil_id');

            // to get all of the selected karkuns
            $get_all_selected = DB::table('all_groups')
                                ->join('karkuns' , 'karkuns.id' , 'all_groups.karkun_id')
                                ->where('category' , $check_level)
                                ->where('com_id' , $committee_id)
                                ->get();


            // to get all of the deslected ehad karkuns
            $get_all_members_not_selected = DB::select(DB::raw("SELECT * FROM karkuns WHERE mehfil_id = '$get_id_mehfil' AND id 
                                                                    
                                                                    NOT IN 

                                                                    (SELECT karkun_id FROM all_groups WHERE category = '$check_level' AND com_id = $committee_id AND mehfil_id = $get_id_mehfil )"
                                                            ));
            
            return view('groups.groups')->with(compact([
                                                'check_level' , 
                                                'city_logged_in' , 
                                                'committee_id' , 
                                                'get_id_mehfil' , 
                                                'get_all_members_not_selected' , 
                                                'get_all_selected' , 
                                                'committee_name'
                                        ]));
        }

    } // end of show data function

    public function insert_data(Request $request){
        
        $committee_id = $request->committee_id;
        $city_logged_in = $request->city_logged_in;
        $get_id_mehfil = $request->get_id_mehfil;
        $check_level = $request->check_level;

        $ids = $request->ids;

        DB::table('all_groups')
                            ->where('com_id' , $committee_id)
                            ->where('category' , $check_level)
                            ->where('city_name' , $city_logged_in)
                            ->delete();


        if (isset($ids)) {

            foreach ($ids as $id) {
                
                $data = array(
                        'com_id' => $committee_id, 
                        'city_name' => $city_logged_in, 
                        'mehfil_id' => $get_id_mehfil, 
                        'category' => $check_level, 
                        'karkun_id' => $id, 
                    );
                DB::table('all_groups')->insert($data);
            }
            echo ' ڈیٹا کا اندراج ہو چکا ہے۔۔۔  ';
        }else{
            echo ' ڈیٹا  میں تبدیلی کر دی گئی ہے۔ ';            
        }
        exit();
    } // end of insert data funtion

    public function insert_data_single(Request $request){
        
        $committee_id = $request->committee_id;
        $city_logged_in = $request->city_logged_in;
        $get_id_mehfil = $request->get_id_mehfil;
        $check_level = $request->check_level;

        $id = $request->id;

        $data = array(
                'com_id' => $committee_id, 
                'city_name' => $city_logged_in, 
                'mehfil_id' => $get_id_mehfil, 
                'category' => $check_level, 
                'karkun_id' => $id, 
        );

        DB::table('all_groups')->insert($data);

        echo ' ڈیٹا کا اندراج ہو چکا ہے۔۔۔  ';

        exit();
    } // end of insert data funtion


    public function delete_data_single(Request $request){
        
        $committee_id = $request->committee_id;
        $city_logged_in = $request->city_logged_in;
        $get_id_mehfil = $request->get_id_mehfil;
        $check_level = $request->check_level;

        $id = $request->id;

        DB::table('all_groups')
                            ->where('com_id' , $committee_id)
                            ->where('category' , $check_level)
                            ->where('karkun_id' , $id)
                            ->delete();

        echo ' ڈیٹا کا اندراج ہو چکا ہے۔۔۔  ';

        exit();
    } // end of insert data funtion



}
