<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\EhadKarkunDutyRoster;


class EhadKameetiDutyRosterController extends Controller{

    public function showData(){
        $city = session('city');

        
        $all_mehfils = DB::table('mehfils')->get();

        $check_duty_roster = DB::table('ehad_karkun_duty_rosters')
                                            ->join('ehad_karkuns', 'ehad_karkun_duty_rosters.ehad_karkun_id', '=', 'ehad_karkuns.id')
                                            ->select('ehad_karkun_duty_rosters.*', 'ehad_karkuns.ekp_name', 'ehad_karkuns.ekp_fname', 'ehad_karkuns.ekp_id_number')
                                            ->where('city' , $city)
                                            ->get();

        $all_ehad_karkun = DB::table('ehad_karkuns')->get();

        return view('duty_roster_ehad_kameeti.dutyroster')->with(compact([
                                                    'all_mehfils' , 
                                                    'all_ehad_karkun' , 
                                                    'check_duty_roster'
                                                ]));
    }


    public function insertDutyKarkun(Request $request){


        // $validator = Validator::make($request->all() , [
        //     'mehfils' => 'required',
        // ]);

        $city = $request->city;
        $month = date('m');
        $year = date('Y');

        DB::table('ehad_karkun_duty_rosters')->where('month' , $month)->where('city' , $request->city)->delete();

            for ($i=0; $i < sizeof($request->mehfil_id); $i++) { 

        // for juma
            $data_juma = array(
                        'day' => 'friday' ,
                        'mehfil_id' =>  $request->mehfil_id[$i] ,  
                        'ehad_karkun_id' => $request->ehad_karkun_juma[$i] ,  
                        'city' =>  $request->city ,  
                        'month' => $month ,  
                        'year' =>  $year ,  
            );

            // dd($data_juma);

        // for hafta
            $data_hafta = array(
                        'day' => 'saturday' ,
                        'mehfil_id' =>  $request->mehfil_id[$i] ,  
                        'ehad_karkun_id' => $request->ehad_karkun_hafta[$i] ,  
                        'city' =>  $request->city ,  
                        'month' => $month ,  
                        'year' =>  $year ,  
            );

        // for itwar
            $data_itwar = array(
                        'day' => 'sunday' ,
                        'mehfil_id' =>  $request->mehfil_id[$i] ,  
                        'ehad_karkun_id' => $request->ehad_karkun_itwar[$i] ,  
                        'city' =>  $request->city ,  
                        'month' => $month ,  
                        'year' =>  $year ,  
            );

        // for peer
            $data_peer = array(
                        'day' => 'monday' ,
                        'mehfil_id' =>  $request->mehfil_id[$i] ,  
                        'ehad_karkun_id' => $request->ehad_karkun_peer[$i] ,  
                        'city' =>  $request->city ,  
                        'month' => $month ,  
                        'year' =>  $year ,  
            );

        // for mangal
            $data_mangal = array(
                        'day' => 'tuesday' ,
                        'mehfil_id' =>  $request->mehfil_id[$i] ,  
                        'ehad_karkun_id' => $request->ehad_karkun_mangal[$i] ,  
                        'city' =>  $request->city ,  
                        'month' => $month ,  
                        'year' =>  $year ,  
            );

        // for budh
            $data_budh = array(
                        'day' => 'wednesday' ,
                        'mehfil_id' =>  $request->mehfil_id[$i] ,  
                        'ehad_karkun_id' => $request->ehad_karkun_budh[$i] ,  
                        'city' =>  $request->city ,  
                        'month' => $month ,  
                        'year' =>  $year ,  
            );

        // for jumerat
            $data_jumerat = array(
                        'day' => 'thursday' ,
                        'mehfil_id' =>  $request->mehfil_id[$i] ,  
                        'ehad_karkun_id' => $request->ehad_karkun_jumerat[$i] ,  
                        'city' =>  $request->city ,  
                        'month' => $month ,  
                        'year' =>  $year ,  
            );

                DB::table('ehad_karkun_duty_rosters')->insert($data_juma);
                DB::table('ehad_karkun_duty_rosters')->insert($data_hafta);
                DB::table('ehad_karkun_duty_rosters')->insert($data_itwar);
                DB::table('ehad_karkun_duty_rosters')->insert($data_peer);
                DB::table('ehad_karkun_duty_rosters')->insert($data_mangal);
                DB::table('ehad_karkun_duty_rosters')->insert($data_budh);
                DB::table('ehad_karkun_duty_rosters')->insert($data_jumerat);
            }

        return redirect('dutyroster/ehadkameeti/');

    }
}
