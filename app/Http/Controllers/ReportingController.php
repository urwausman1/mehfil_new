<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\EhadRegister;

use PragmaRX\Countries\Package\Countries;


class ReportingController extends Controller{

    public function showData(){

        $role = session('user_role');
        
        if ($role == 'mehfil') {
            $mehfil_id = session('mehfil_id');
            $all_mahafil = DB::table('mehfils')->where('id' , $mehfil_id)->get();
            $all_karkuns = DB::table('karkuns')
                                    ->join('mehfils', 'karkuns.mehfil_id', '=', 'mehfils.id')
                                    ->select('karkuns.*', 'mehfils.*')
                                    ->where('karkuns.mehfil_id' , $mehfil_id)
                                    ->get();
            return view('reporting.reporting_karkun')->with(compact(['all_karkuns' , 'all_mahafil']));
        }else{
            $all_mahafil = DB::table('mehfils')->get();
            $all_karkuns = DB::table('karkuns')
                                    ->join('mehfils', 'karkuns.mehfil_id', '=', 'mehfils.id')
                                    ->select('karkuns.*', 'mehfils.*')
                                    ->get();
            return view('reporting.reporting_karkun')->with(compact(['all_karkuns' , 'all_mahafil']));
        }
    }


    public function get_data_accordingly(Request $request){

        $all_mahafil = DB::table('mehfils')->get();

        // dd($request->all());
        $marfat_search = $request->marfat_search;
        $mehfilname = $request->mehfilname;
        $mahina = $request->mahina;
        $saal = $request->saal;
        
        $get_name = DB::table('mehfils')->where('id' , $mehfilname)->first();

        $starting_date = $saal."-".$mahina."-01";
        $ending_date = $saal."-".$mahina."-31";

        $where = 'WHERE karkuns.id > 0 ';
        if ($marfat_search != null) {
            $where .= " AND karkuns.kp_marfat = '$marfat_search' ";
        }

        if ($mehfilname != null) {
            $where .= " AND karkuns.mehfil_id = $mehfilname";
        }


        if ($mahina != null && $saal != null) {
            $where .= " AND kp_doe >= '$starting_date' AND kp_doe < '$ending_date' ";
        }

        $all_karkuns = DB::select(DB::raw("SELECT * FROM karkuns INNER JOIN mehfils ON karkuns.mehfil_id = mehfils.id  $where ") );


        if (sizeof($all_karkuns) > 0) {
            return view('reporting.reporting_karkun')->with(compact(['all_karkuns' , 'all_mahafil' , 'marfat_search' , 'mehfilname' , 'mahina' , 'saal' , 'get_name']));                
        }
        else{
            $all_data = "nothing found here";
            return view('reporting.reporting_karkun')->with(compact(['all_data' , 'all_mahafil', 'marfat_search' , 'mehfilname' , 'mahina' , 'saal' , 'get_name']));                
        }
    }

    public function showData_hazri(){

        $role = session('user_role');
        
        if ($role == 'mehfil') {
            $mehfil_id = session('mehfil_id');
            $all_karkuns = DB::table('karkuns')->where('mehfil_id' , $mehfil_id)->get();
            $all_mahafil = DB::table('mehfils')->where('id' , $mehfil_id)->first();                                
            return view('reporting.reporting_hazri_karkun')->with(compact(['mehfil_id' , 'all_mahafil' , 'all_karkuns']));
        }else{
            $all_mahafil = DB::table('mehfils')->get();                                
            return view('reporting.reporting_hazri_karkun')->with(compact(['all_mahafil']));
        }
    }


// this function is just to get the karkuns name according to the given given mehfil number
    public function get_karkun_name_according_to_mehfil(Request $request){
        $mehfil_name = $request->mehfil_name;
        $all_karkuns = DB::table('karkuns')->where('mehfil_id' , $mehfil_name)->get();
        $for_options = "<option value=''>  کارکن کا  انتخاب کریں۔۔۔  </option>"; 

        foreach ($all_karkuns as $single_karkun) {
            $fullname = $single_karkun->kp_name . " / ". $single_karkun->kp_fname . " / ". $single_karkun->kp_id_number;
            $for_options .= "<option value='$single_karkun->id'> $fullname </option>"; 
        }

        $dataarr = array('options' => $for_options );
        echo  json_encode($dataarr);
        exit();  
    }


    public function filter_karkun_and_mehfil(Request $request){

        // all variables gotten by ajax request
        $mehfil_name = $request->mehfil_name;
        $karkun_name = $request->karkun_name;
        $month_name = $request->month_name;
        $year = $request->year_name;

        $data = "";

        $where = ' WHERE hazri_karkuns.id > 0 ';

        if ($karkun_name != null) {
            $where .= " AND hazri_karkuns.karkun_id = '$karkun_name' ";
        }

        if ($month_name != null) {

            if ($year == null) {

                    $year = date('Y');
                    $starting_date = $year."-".$month_name."-01";
                    $ending_date = $year."-".$month_name."-31";

                    $where .= " AND hazri_karkuns.dates >= '$starting_date' AND hazri_karkuns.dates < '$ending_date' ";
            }
            else{
                if ($year != null) {
                    
                    $starting_date = $year."-".$month_name."-01";
                    $ending_date = $year."-".$month_name."-31";            

                    $where .= " AND hazri_karkuns.dates >= '$starting_date' AND hazri_karkuns.dates < '$ending_date' ";
                }
            }
        }



        $QUERY = "SELECT * FROM hazri_karkuns 
                                        INNER JOIN karkuns ON hazri_karkuns.karkun_id = karkuns.id
                                        $where 
                                        GROUP BY hazri_karkuns.dates
                                        ORDER BY hazri_karkuns.dates DESC ";
        // exit();
        $get_all_hazri = DB::select(DB::raw($QUERY));



            foreach ($get_all_hazri as $single) {

                $back = 'style="background: lightgreen !important;" ';
                $att = '';
                $timeis = '';
                $dateis = date('d-m-Y', strtotime($single->dates));

                if ($single->status == 'p') {
                    $att = "حاضر ";
                    $timeis = $single->times;
                }else{
                    $back = 'style="background: lightblue !important;" ';
                    $att = "غیر حاضر"  ;
                    $timeis = '';
                }

                if ($single->flag == 1 && $single->status == 'a') {
                    $back = 'style="background: red !important;" ';                    
                }
                elseif ($single->flag == 0 && $single->status == 'p') {
                    $back = 'style="background: white !important;" ';                    
                }

                $data .= "<tr>";
                $data .= "<td $back> $single->kp_name </td>";
                $data .= "<td $back> ". $dateis ."</td>";
                $data .= "<td $back> $timeis </td>";
                $data .= "<td $back>  $att  </td>";
                $data .= "</tr>";
            }

            $dataarr = array('tbody' => $data);
            echo  json_encode($dataarr);
            exit();  
    } // end of function



///// these functions are to show the page and then filtering for mahafils

    public function showData_hazri_mehfil(){

        $role = session('user_role');
        
        if ($role == 'mehfil') {
            $mehfil_id = session('mehfil_id');
            $all_mahafil = DB::table('mehfils')->where('id' , $mehfil_id)->first();                                
            return view('reporting.reporting_hazri_mehfil')->with(compact(['mehfil_id' , 'all_mahafil']));
        }else{
            $all_mahafil = DB::table('mehfils')->get();                                
            return view('reporting.reporting_hazri_mehfil')->with(compact(['all_mahafil']));
        }
   }

    public function filter_mehfil(Request $request){

        // all variables gotten by ajax request
        $mehfil_name = $request->mehfil_name;
        $month_name = $request->month_name;
        $year = $request->year_name;

        $data = "";

        $where = ' WHERE hazri_karkuns.id > 0 ';

        if ($mehfil_name != null) {
            $where .= " AND hazri_karkuns.mehfil_id = '$mehfil_name' ";
        }

        $where2 = '';
        $where3 = '';
        if ($month_name != null) {

            if ($year == null) {

                    $year = date('Y');
                    $starting_date = $year."-".$month_name."-01";
                    $ending_date = $year."-".$month_name."-31";

                    $where .= " AND hazri_karkuns.dates >= '$starting_date' AND hazri_karkuns.dates < '$ending_date' ";
                    $where2 .= " AND hazri_karkuns.dates >= '$starting_date' AND hazri_karkuns.dates < '$ending_date' ";
                    
                    $where3 .= " AND monhtly_report_coordinators.month = $month_name 
                                    AND monhtly_report_coordinators.year = $year ";
            }
            else{
                if ($year != null) {
                    
                    $starting_date = $year."-".$month_name."-01";
                    $ending_date = $year."-".$month_name."-31";            

                    $where .= " AND hazri_karkuns.dates >= '$starting_date' AND hazri_karkuns.dates < '$ending_date' ";
                    $where2 .= " AND hazri_karkuns.dates >= '$starting_date' AND hazri_karkuns.dates < '$ending_date' ";

                    $where3 .= " AND monhtly_report_coordinators.month = $month_name 
                                    AND monhtly_report_coordinators.year = $year";
                }
            }
        }elseif($month_name == null) {
                    $month = date('m');
                    $year = date('Y');
                    $where3 .= " AND monhtly_report_coordinators.month = $month 
                                    AND monhtly_report_coordinators.year = $year ";
        }

        $get_all_hazri = DB::select(DB::raw("SELECT * FROM hazri_karkuns 
                                            INNER JOIN karkuns ON hazri_karkuns.karkun_id = karkuns.id
                                            $where 
                                            GROUP BY karkun_id
                                            ORDER BY karkun_id asc ") 
                                        );

        // get data for karkunan meeting from mahana kameeti report
        $QUERY1 = "SELECT meeting_date FROM monhtly_report_coordinators 
                            WHERE 
                            mehfil_id = $mehfil_name  
                            $where3 ";
        $get_meeting_date = DB::select(DB::raw($QUERY1));
        $meeting_date = '';
        foreach ($get_meeting_date as $single_meeting) {
            $meeting_date = $single_meeting->meeting_date;
        }

        foreach ($get_all_hazri as $single) {

                // check krny k liye k kia karkun mahana meeting me hazri tha ya nai
                $get_mahana_meeting_hazri = DB::select(DB::raw("SELECT * FROM hazri_karkuns 
                                                WHERE 
                                                    karkun_id = $single->karkun_id
                                                    AND dates = '$meeting_date'
                                                    AND status = 'p' "));


                // karkun k din jin mn us ki duty lagi hoi ho
                $get_days = DB::select(DB::raw("SELECT * FROM hazri_karkuns 
                                                WHERE 
                                                    karkun_id = $single->karkun_id
                                                    AND flag = 1
                                                    $where2
                                                    -- AND status = 'p'
                                                GROUP BY day_name "));
                
                // karkun ki total hazrian jo lagai gai hnn
                $total_duty_k_din = DB::select(DB::raw("SELECT * FROM hazri_karkuns 
                                                WHERE 
                                                    karkun_id = $single->karkun_id
                                                    AND flag = 1
                                                    $where2
                                                ")); 

                // karkun ki duty waly dinoon mn hazriann
                $duty_pr_hazrian = DB::select(DB::raw("SELECT * FROM hazri_karkuns 
                                                WHERE 
                                                    karkun_id = $single->karkun_id
                                                    AND flag = 1
                                                    AND status = 'p'
                                                    $where2
                                                ")); 

                // check kul hazrian in a month 
                $total_hazri_k_din_monthly = DB::select(DB::raw("SELECT * FROM hazri_karkuns 
                                                WHERE 
                                                    karkun_id = $single->karkun_id
                                                    $where2
                                                ")); 

                // check karkun ki kul hazrian in a month 
                $hazrian_whole_month = DB::select(DB::raw("SELECT * FROM hazri_karkuns 
                                                WHERE 
                                                    karkun_id = $single->karkun_id
                                                    AND status = 'p'
                                                    $where2
                                                ")); 

                // check krny k liye k kia karkun meeting pr hazri tha ya nai

                $singledays = array();
                foreach ($get_days as $single_day) {
                    array_push($singledays, $single_day->day_name);
                }

                $total_duty_waly_din = 0;
                $total_hazri_waly_din = 0;
                if (!empty($total_duty_k_din)) {
                    $total_duty_waly_din = sizeof($total_duty_k_din);
                }
                if (!empty($duty_pr_hazrian)) {
                    $total_hazri_waly_din = sizeof($duty_pr_hazrian);
                }

                $total_hazri_k_din_monthly_final = 0;
                $hazrian_whole_month_final = 0;
                if (!empty($total_hazri_k_din_monthly)) {
                    $total_hazri_k_din_monthly_final = sizeof($total_hazri_k_din_monthly);
                }
                if (!empty($hazrian_whole_month)) {
                    $hazrian_whole_month_final = sizeof($hazrian_whole_month);
                }

                $hazir_or_not = '';
                $back = '';
                if (!empty($get_mahana_meeting_hazri)){
                    $hazir_or_not = ' حاضر  ';    
                    $back = "style='background:lightblue' ";
                }else{                    
                    $hazir_or_not = ' غیر حاضر  ';    
                    $back = "";
                }

                $week_day_name = array(
                'friday' => 'جمعہ',
                'saturday' => 'ہفتہ',
                'sunday' => 'اتوار',
                'monday' => 'سوموار',
                'tuesday' => 'منگل',
                'wednesday' => 'بدھ',
                'thursday' => 'جمعرات'
                );

                $urdu_name = array();
                foreach ($week_day_name as $key => $day){
                    foreach($singledays as $single_duty){
                        if($single_duty != $key){
                            continue;
                        }
                        // echo $day;
                        array_push($urdu_name, $day);
                    }
                }

                $days = implode('، ', $urdu_name);
                $data .= "<tr $back>";
                $data .= "<td> $single->kp_id_number </td>";
                $data .= "<td> $single->kp_name </td>";
                $data .= "<td> $single->kp_phone </td>";
                $data .= "<td> $days </td>";
                $data .= "<td> $total_duty_waly_din / $total_hazri_waly_din </td>";
                $data .= "<td> $total_hazri_k_din_monthly_final / $hazrian_whole_month_final </td>";
                $data .= "<td> $hazir_or_not  </td>";
                $data .= "</tr>";                    
        } // end foreach

            $dataarr = array('tbody' => $data);
            echo  json_encode($dataarr);
            exit();  
    } // end of fuction






    public function showData_hazri_mehfil_monthly(){

        $mehfil_id = session('mehfil_id');
        // $city = session('city');
        $role = session('user_role');

        $all_mahafil = [];                                
        if($role == 'mehfil'){
            $all_mahafil = DB::table('mehfils')->where('id' , $mehfil_id)->get();
        }else{
            $all_mahafil = DB::table('mehfils')->get();                                
        }

        return view('reporting.monthly_report')->with(compact(['all_mahafil']));
    }

    public function mehfil_monthly_report(Request $request){

        // all variables gotten by ajax request
        $month = date('m');
        // $year = date('Y');

        $mehfil_name = $request->mehfil_name;
        $month_name = $request->month_name;
        $year = $request->year_name;

        $data = "";

        $check_already_printed = DB::table('monhtly_report_coordinators')
                                            ->where('mehfil_id' , $mehfil_name)
                                            ->where('month' , $month_name)
                                            ->where('year' , $year)
                                            ->first();


        if ($check_already_printed != null) {
            $all_data_decode = json_decode($check_already_printed->dump_data);
            // dd($all_data_decode->mehfil_number);
            // dd($all_data_decode);

            $url = url("reports/hazri/mehfil/monthly/edit/".$mehfil_name."/".$month_name."/".$year);
            $edit_btn ="<a onclick='edit_monthly_report(".$mehfil_name." , ".$month_name." , ".$year.")' class='btn btn-primary'> تبدیل کریں  </a>";

            $data .="                       <div class='row'>
                                            <div class='text-right'>
                                                  تاریخ ۔۔۔۔۔۔۔۔۔۔۔   
                                            </div>
                                            <center> <h1> محفل کی  ماہانہ رپورٹ      </h1> </center>
                                        </div>

                                        <div class='row'>
                                            <div class='col-md-8'>

                                            <input type='hidden' name='mehfil_id' id='hidden_mehfil_id' value='$mehfil_name' >";

                                            // <input type='hidden' name='month_number' value='$month' >
                                            // <input type='hidden' name='year_number' value='$year' >

            $data .="                       <table width='100%' border='2'> ";
            if(isset($all_data_decode->mehfil_ka_name)){

            $data .="<tr>
                                                        <td>1</td>
                                                        <td>  محترم شیخ صاحب کی  محفل نمبر   </td>
                                                        <td> 
                                                            <input name='mehfil_number' type='hidden' value='$all_data_decode->mehfil_number' />
                                                            $all_data_decode->mehfil_ka_name
            </tr>";
            }else{
                $data .="<tr>
                                                            <td>1</td>
                                                            <td>  محترم شیخ صاحب کی  محفل نمبر   </td>
                                                            <td> 
                                                                <input name='mehfil_number' type='hidden' value='$all_data_decode->mehfil_number' />
                                                                $all_data_decode->mehfil_number
                </tr>";
            }
            if(isset($all_data_decode->first_mehfil_coordinator_name)){
                
                $data .="<tr>
                                                        <td>2</td>
                                                        <td>  نام محفل کوآڈینیٹر  (1st)  </td>
                                                        <td>

                                                            <input name='first_mehfil_coordinator_name' type='hidden' 
                                                                    value='$all_data_decode->first_mehfil_coordinator_name' />

                                                            <input name='first_mehfil_coordinator_id' type='hidden' 
                                                                    value='$all_data_decode->first_mehfil_coordinator_id' />

                                                                    $all_data_decode->first_mehfil_coordinator_name

                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td>3</td>
                                                        <td>  نام محفل کوآڈینیٹر  (2nd)  </td>
                                                        <td>

                                                            <input name='second_mehfil_coordinator_name' type='hidden' 
                                                                    value='$all_data_decode->second_mehfil_coordinator_name' />

                                                            <input name='second_mehfil_coordinator_id' type='hidden' 
                                                                    value='$all_data_decode->second_mehfil_coordinator_id' />

                                                                    $all_data_decode->second_mehfil_coordinator_name

                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td>4</td>
                                                        <td> 
                                                          کوآڈینیٹر کی محفل پر  ماہانہ حاضری  (کتنے دن) (1st)  
                                                        </td>
                                                        <td>
                                                            <input name='first_coordinator_hazri_count' type='hidden' value='$all_data_decode->first_coordinator_hazri_count' />

                                                            $all_data_decode->first_coordinator_hazri_count

                                                        </td>
                                                    </tr>


                                                    <tr>
                                                        <td>5</td>
                                                        <td> 
                                                          کوآڈینیٹر کی محفل پر  ماہانہ حاضری  (کتنے دن) (2nd)  
                                                        </td>
                                                        <td>
                                                            <input name='second_coordinator_hazri_count' type='hidden' value='$all_data_decode->second_coordinator_hazri_count' />

                                                            $all_data_decode->second_coordinator_hazri_count

                                                        </td>
                                                    </tr>";
            }else{

                $data .="<tr>
                                                        <td>2</td>
                                                        <td>  نام محفل کوآڈینیٹر  (1st)  </td>
                                                        <td>

                                                            <input name='mehfil_coordinator_name' type='hidden' 
                                                                    value='$all_data_decode->mehfil_coordinator_name' />

                                                            <input name='mehfil_coordinator_id' type='hidden' 
                                                                    value='$all_data_decode->mehfil_coordinator_id' />

                                                                    $all_data_decode->mehfil_coordinator_name

                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td>3</td>
                                                        <td> 
                                                          کوآڈینیٹر کی محفل پر  ماہانہ حاضری  (کتنے دن) (1st)  
                                                        </td>
                                                        <td>
                                                            <input name='coordinator_hazri_count' type='hidden' value='$all_data_decode->coordinator_hazri_count' />

                                                            $all_data_decode->coordinator_hazri_count

                                                        </td>
                                                    </tr>";

            }


                    $data .=                                "<tr>
                                                        <td>6</td>
                                                        <td>  محفل  پر ڈیوٹی والے بھائیوں کی کل تعداد  </td>
                                                        <td>
                                                            <input name='tamam_dutywaly_bai' type='hidden' value='$all_data_decode->tamam_dutywaly_bai' />

                                                            $all_data_decode->tamam_dutywaly_bai

                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td>7</td>
                                                        <td>
                                                            50% سے  کم حاضر رہنے  والے بھایئوں کی کل تعداد   
                                                        </td>
                                                        <td>
                                                            $all_data_decode->fifty_percent_hazri_sy_kam_waly
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td>8</td>
                                                        <td> 
                                                            مسلسل غیر حاضر رہنے والے بھائیوں کی کل تعداد  
                                                        </td>
                                                        <td>
                                                            <input name='musalsal_ghairhazri_bai' type='hidden' value='$all_data_decode->musalsal_ghairhazri_bai' />
                                                        
                                                                $all_data_decode->musalsal_ghairhazri_bai
                                                        </td>
                                                    </tr>


                                                    <tr>
                                                        <td>9</td>
                                                        <td>
                                                             عہد والے بھائیوں کی  محفل  پر  ماہانہ حاضری  (کتنے دن)
                                                        </td>
                                                        <td>

                                                            <input name='ehad_waly_baion_ki_mahana_hazri' type='hidden' value='$all_data_decode->ehad_waly_baion_ki_mahana_hazri' />

                                                            $all_data_decode->ehad_waly_baion_ki_mahana_hazri

                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td>10</td>
                                                        <td>  ایک ماہ میں نئے ہونے والے کل عہد    </td>
                                                        <td>

                                                            <input name='aik_maah_mein_hony_waly_ehad' type='hidden' value='$all_data_decode->aik_maah_mein_hony_waly_ehad' />

                                                                $all_data_decode->aik_maah_mein_hony_waly_ehad

                                                            </td>
                                                    </tr>

                                                    <tr>
                                                        <td>11</td>
                                                        <td> ملتان شریف ڈیوٹی والے بھائیوں کی کل تعداد </td>
                                                        <td>
                                                            $all_data_decode->multan_shrif_dutywaly_total
                                                        </td>
                                                    </tr>


                                                    <tr>
                                                        <td>12</td>
                                                        <td>  محفل پر تعلیمات کریمہ اس بار پڑھی گئی؟      </td>
                                                        <td>";
        
                                                if (isset($all_data_decode->ayat_krima) && $all_data_decode->ayat_krima == 'on') {
                                                    $data .= "  جی ہاں   ";
                                                }else{
                                                    $data .= "  نہیں   ";
                                                }

            $data .=                                       "</td>
                                                    </tr>


                                                    <tr>
                                                        <td>13</td>
                                                        <td>   محفل پر اس ماہ شاہی سواری بھنگڑہ ہوا؟   </td>
                                                        <td>";
                                                
                                                if (isset($all_data_decode->bhangra) && $all_data_decode->bhangra == 'on') {
                                                    $data .= "  جی ہاں   ";
                                                }else{
                                                    $data .= "  نہیں   ";
                                                }

            $data .=                                        "</td>
                                                    </tr>


                                                    <tr>
                                                        <td colspan='3'>
                                                            محفل پر بھائیوں کی حاضری کی تفصیل 
                                                        </td>
                                                    </tr>


                                                    <tr>
                                                        <td>14</td>
                                                        <td>  محفل پر  روزانہ بھائیوں کی  تعداد   </td>
                                                        <td>
                                                            <input name='get_avg_visitors_on_mehfil' type='hidden' value='$all_data_decode->get_avg_visitors_on_mehfil' />

                                                            $all_data_decode->get_avg_visitors_on_mehfil
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td>15</td>
                                                        <td>  بڑی محفل پر بھائیوں کی تعداد  </td>
                                                        <td>
                                                            $all_data_decode->bari_mehfil_per_tadad
                                                        </td>
                                                    </tr>


                                                    <tr>
                                                        <td>16</td>
                                                        <td> 
                                                            محترم  شیخ  صاحب کی  نام مبارک میٹنگ پر  بھائیوں کی تعداد  
                                                         </td>
                                                        <td>
                                                            $all_data_decode->ameer_k_nam_per_meeting_waly
                                                        </td>
                                                    </tr>

                                                    <!--<tr>
                                                        <td>15</td>
                                                        <td>
                                                            آل کارکنان  میٹنگ پر بھائیوں کی تعداد
                                                        </td>
                                                        <td>";
                                                            // $all_data_decode->all_karkunan_per_meeting_waly
                                                        $data .= "</td>
                                                    </tr> --!>

                                                    <tr>
                                                        <td colspan='3'> 
                                                         محفل پر  بھائیوں کی  مشاورت  میٹنگ کی تفصٰل    
                                                        </td>
                                                    </tr>


                                                    <tr>
                                                        <td>17</td>
                                                        <td>  میٹنگ کی تاریخ   </td>
                                                        <td>".date('d-m-Y' , strtotime($all_data_decode->meeting_date))."</td>
                                                    </tr>


                                                    <tr>
                                                        <td>18</td>
                                                        <td>  میٹنگ پر بھائیوں کی تعداد   </td>
                                                        <td>
                                                            $all_data_decode->meeting_per_bai_total
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td>19</td>
                                                        <td>   ممبر عہد کارکن  / میٹنگ کوآڈینیٹر   </td>
                                                        <td>";
        $get_all_ehadkarkun_name = DB::table('ehad_karkuns')
                                ->where('id' , $all_data_decode->member_coordinator)
                                ->first();
       $data .=" $get_all_ehadkarkun_name->ekp_name </td>
                                                    </tr>

                                                    <!-- <tr>
                                                        <td colspan='3'>  ایجنڈا برائے میٹنگ   </td>
                                                    </tr>

                                                    <tr>
                                                        <td colspan='3'>";                                                          
                                                            // $all_data_decode->agenda_for_meeting

                                                $data .="</td>
                                                    </tr>

                                                    <tr>
                                                        <td colspan='3'>  مشاورت   </td>
                                                    </tr>

                                                    <tr>
                                                        <td colspan='3'>";
                                                            // $all_data_decode->mushawrat
                                                $data .="</td>
                                                    </tr> --!>

                                                </table>
                                            </div>

                                            <div class='col-md-4'>
                                                
                                                <table width='100%' border='2'>
                                                    <!-- <tr>
                                                        <td>
                                                          محفل پر  رابطہ کمیٹی کی کارکردگی کی تفصیل   
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style='height: 150px;'>";
                                                            // $all_data_decode->kameeti_karkardagi
                                            $data .="</td>
                                                    </tr>

                                                    <tr>
                                                        <td>
                                                             کوئی ذاتی مشاہدہ ، مسائل  جو محفل  کے بنیادی کاموں میں خلل  کا باعث ہیں۔  
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style='height: 150px;'>";                           
                                                            // $all_data_decode->zati_mushahda
                                            $data .="</td>
                                                    </tr> --!>

                                                    <tr>
                                                        <td>
                                                            ایجنڈا برائے آل کارکنان  میٹنگ
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style='height: 150px;'>
                                                            $all_data_decode->agenda_for_all_karkunan_meeting
                                                        </td>
                                                    </tr>


                                                </table>
                                            </div>
                                        </div>";

            $dataarr = array('all_data' => $data , "btn_edit" => $edit_btn);
            echo json_encode($dataarr);
            exit();
        }


        //when above defined if condition does not true then this code will be run
        $get_mehfil_name_coordinator = DB::table('karkun_duty_rosters')
                                        ->join('mehfils' , 'karkun_duty_rosters.mehfil_id' , 'mehfils.id')
                                        ->select('karkun_duty_rosters.*' , 'mehfils.mehfil_number'  , 'mehfils.mehfil_city' , 'mehfil_name')
                                        ->where('mehfil_id' , $mehfil_name)
                                        ->where('month_name' , $month_name)
                                        ->where('year_name' , $year)
                                        ->first(); 

        $to_get_all_duty_karkuns = DB::table('karkun_duty_rosters')
                                        ->where('mehfil_id' , $mehfil_name)
                                        ->where('month_name' , $month_name)
                                        ->where('year_name' , $year)
                                        ->get(); 

        // dd($get_mehfil_name_coordinator);

        if ($get_mehfil_name_coordinator == null ) {

            $data .= "    اس ماہ کا  ڈیوٹی روسٹر ہی موجود نہیں ہے۔     "; 
            $dataarr = array('all_data' => $data);
            echo json_encode($dataarr);
            exit();
        }


        $get_all_dutywaly = array();
        $get_all_dutywaly_final = array();


        for ($i=0; $i < sizeof($to_get_all_duty_karkuns); $i++) { 

                array_push($get_all_dutywaly, explode(',', $to_get_all_duty_karkuns[$i]->k_main_gatian));
                array_push($get_all_dutywaly, explode(',', $to_get_all_duty_karkuns[$i]->k_cycle_stand));
                array_push($get_all_dutywaly, explode(',', $to_get_all_duty_karkuns[$i]->k_security));
                array_push($get_all_dutywaly, explode(',', $to_get_all_duty_karkuns[$i]->monthly_cordinator));
        }

        for ($i=0; $i < count($get_all_dutywaly); $i++) { 
            for ($j=0; $j < count($get_all_dutywaly[$i]); $j++) {                 
                array_push($get_all_dutywaly_final, $get_all_dutywaly[$i][$j]);
            }
        }

        $monthly_coordinator = explode(',' , $get_mehfil_name_coordinator->monthly_cordinator);

        // dd($monthly_coordinator);

        $data .="<form class='table_report'>"; 
        $data .= "<div class='row'>
                                            <div class='text-right'>
                                                   تاریخ ۔۔۔۔۔۔۔۔۔۔۔  
                                            </div>
                                            <center> <h1> محفل کی  ماہانہ رپورٹ      </h1> </center>
                                        </div>

                                        <div class='row'>
                                            <div class='col-md-8'>

                                            <input type='hidden' name='mehfil_id' id='hidden_mehfil_id' value='$mehfil_name' >";

                                            // <input type='hidden' name='month_number' value='$month' >
                                            // <input type='hidden' name='year_number' value='$year' >

                                            // ";



        $data .=                                "<table width='100%' border='2'> 
                                                    <tr>
                                                        <td>1</td>
                                                        <td>  محترم شیخ صاحب کی  محفل نمبر   </td>
                                                        <td> 
                                                            <input name='mehfil_number' type='hidden' value='$get_mehfil_name_coordinator->mehfil_number' />

                                                            <input name='mehfil_ka_name' type='hidden' value='$get_mehfil_name_coordinator->mehfil_name' />

                                                         $get_mehfil_name_coordinator->mehfil_name  </td>
                                                    </tr>
                                                    <tr>
                                                        <td>2</td>
                                                        <td>  نام محفل کوآڈینیٹر  (1st)  </td>
                                                        <td>";

                                                            $karkun_name = DB::table('karkuns')->where('id' , $monthly_coordinator[0])->first();

        $data .=                                            $karkun_name->kp_name;


        $get_count =                DB::table('hazri_karkuns')
                                        ->select('hazri_karkuns.status' , 'hazri_karkuns.dates' , DB::raw("count(case hazri_karkuns.status when 'p' then 1 else null end) as hazirfirst"))
                                        ->whereBetween('dates' , [$year."-".$month_name."-01" , $year."-".$month_name."-31"] )
                                        ->where('karkun_id' , $monthly_coordinator[0])
                                        ->first();

        // $get_count =                DB::table('hazri_karkuns')
        //                                 ->select('hazri_karkuns.status' , 'hazri_karkuns.dates' , DB::raw("count(case hazri_karkuns.status when 'p' then 1 else null end) as hazir"))
        //                                 ->whereBetween('dates' , [$year."-".$month_name."-01" , $year."-".$month_name."-31"] )
        //                                 ->where('karkun_id' , $monthly_coordinator[0])
        //                                 ->toSql();

// DB::enableQueryLog(); // Enable query log
// // Your Eloquent query executed by using get()
// dd($get_count->getBindings() );


        $data .=                                        "<input name='first_mehfil_coordinator_name' type='hidden' 
                                                                    value='$karkun_name->kp_name' />

                                                            <input name='first_mehfil_coordinator_id' type='hidden' 
                                                                    value='$karkun_name->id' />

                                                        </td>
                                                    </tr>


                                                    <tr>
                                                        <td>3</td>
                                                        <td>  نام محفل کوآڈینیٹر  (2nd)  </td>
                                                        <td>";

                                                            $karkun_name = DB::table('karkuns')->where('id' , $monthly_coordinator[1])->first();

        $data .=                                            $karkun_name->kp_name;


        $get_count_second =                DB::table('hazri_karkuns')
                                        ->select('hazri_karkuns.status' , 'hazri_karkuns.dates' , DB::raw("count(case hazri_karkuns.status when 'p' then 1 else null end) as hazirsecond"))
                                        ->whereBetween('dates' , [$year."-".$month_name."-01" , $year."-".$month_name."-31"] )
                                        ->where('karkun_id' , $monthly_coordinator[1])
                                        ->first();

        $data .=                                        "<input name='second_mehfil_coordinator_name' type='hidden' 
                                                                    value='$karkun_name->kp_name' />

                                                            <input name='second_mehfil_coordinator_id' type='hidden' 
                                                                    value='$karkun_name->id' />

                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td>4</td>
                                                        <td> 
                                                          کوآڈینیٹر کی محفل پر  ماہانہ حاضری  (کتنے دن)  (1st) 
                                                        </td>
                                                        <td>

                                                            <input name='first_coordinator_hazri_count' type='hidden' value='$get_count->hazirfirst' />
                                                        ";

        $data .= $get_count->hazirfirst;
        $data .=                                        "</td>
                                                    </tr>



                                                    <tr>
                                                        <td>5</td>
                                                        <td> 
                                                          کوآڈینیٹر کی محفل پر  ماہانہ حاضری  (کتنے دن)   (2nd)
                                                        </td>
                                                        <td>

                                                            <input name='second_coordinator_hazri_count' type='hidden' value='$get_count_second->hazirsecond' />
                                                        ";

        $data .= $get_count_second->hazirsecond;
        $data .=                                        "</td>
                                                    </tr>


                                                    <tr>
                                                        <td>6</td>
                                                        <td>  محفل  پر ڈیوٹی والے بھائیوں کی کل تعداد  </td>
                                                        <td>

                                                        "; 
        $total_dty_waly = sizeof(array_unique($get_all_dutywaly_final));
        $data .= $total_dty_waly;

        $data .=                                        "
                                                            <input name='tamam_dutywaly_bai' type='hidden' value='$total_dty_waly' />

                                                            </td>
                                                    </tr>

                                                    <tr>
                                                        <td>7</td>
                                                        <td>
                                                            50% سے  کم حاضر رہنے  والے بھایئوں کی کل تعداد   
                                                        </td>
                                                        <td>";

        $get_all_from_duty_rosters = DB::table('karkun_duty_rosters')
                                            ->where('mehfil_id' , $mehfil_name)
                                            ->where('month_name' , $month_name)
                                            ->where('year_name' , $year)
                                            ->get();

   
        $tamam_duty_waly = array();
        $dino_k_hisab_sy_duty_waly = array();
        $tamam_duty_waly_final = array();

        foreach($get_all_from_duty_rosters as $single_from_duty_rosters) {

            $main_gatian = $single_from_duty_rosters->k_main_gatian;           
            $cycle_stand = $single_from_duty_rosters->k_cycle_stand;
            $security = $single_from_duty_rosters->k_security;

            $all_members = $main_gatian .",". $cycle_stand .",". $security;
            $days = $single_from_duty_rosters->k_duty_days;

            $dino_k_hisab_sy_duty_waly[$days] = explode(',', $all_members);
        } // end of main foreach

        foreach ($dino_k_hisab_sy_duty_waly as $key => $values) {
            $tamam_duty_waly_final[$key] = array_unique($dino_k_hisab_sy_duty_waly[$key]);
        }



        $duplicate_check = array();
        foreach ($tamam_duty_waly_final as $key => $value) {
            foreach ($value as $key2 => $value2) {

                if(isset($duplicate_check[$value2]) && $duplicate_check[$value2] > 0){
                    $duplicate_check[$value2]++;
                }else{
                    $duplicate_check[$value2] = 1;     
                }
            }
        } // end of foreach

        // jo total hazri nikli thi usko 4 sy zarab deny k liyee
        $zarb_deny_wali_array = array();
        foreach ($duplicate_check as $key => $value) {
            $zarb_deny_wali_array[$key] = $value * 4;
        } // end of foreach

        // hazri waly table sy hazrian nikalny k lyee har har admi ki k jis ki duty lagi hoi thi duty roster mn
        $hazri_waly_karkun = DB::table('hazri_karkuns')
                                ->where('mehfil_id' , $mehfil_name)
                                ->whereBetween('dates' , [$year."-".$month_name."-01" , $year."-".$month_name."-31"])
                                ->where('flag' , 1)
                                ->where('status' , 'p')
                                ->get();


        $duplicate_check_hazri = array();
        foreach ($hazri_waly_karkun as $values) {

            if(isset($duplicate_check_hazri[$values->karkun_id]) && $duplicate_check_hazri[$values->karkun_id] > 0){
                $duplicate_check_hazri[$values->karkun_id]++;
            }else{
                $duplicate_check_hazri[$values->karkun_id] = 1;     
            }
        }

        $sb_logon_ki_hazri_minus_kr_k = array();
        $jo_musalsal_ghair_hazir_thy = array();
        foreach ($duplicate_check_hazri as $key=>$value) {

            $percentage = 50;
            $total_is = $zarb_deny_wali_array[$key];
            $after_percentage = ($percentage / 100) * $total_is;

            // echo $key . " key is:  <br>";
            // echo $zarb_deny_wali_array[$key] . " zarb deny k bd : <br>";
            // echo $after_percentage . " after percentage: <br>";
            // echo  $value . " hazri wali count:  <br><br><br><br>";

            if ($value < $after_percentage) {
                $sb_logon_ki_hazri_minus_kr_k[$key] = ($zarb_deny_wali_array[$key] - $value);
            }
            if ($value < 1) {
                $jo_musalsal_ghair_hazir_thy[$key] = ($zarb_deny_wali_array[$key] - $value);
            }

        }

        // dd($jo_musalsal_ghair_hazir_thy);
        // dd($zarb_deny_wali_array);
        // dd($duplicate_check_hazri);
        // dd($duplicate_check);
        // dd($sb_logon_ki_hazri_minus_kr_k);
        $sab_logon_ki_hazri_after_minus = sizeof($sb_logon_ki_hazri_minus_kr_k);
        $data .= $sab_logon_ki_hazri_after_minus;
        

        $data .=                                        "
                                                            <input name='fifty_percent_hazri_sy_kam_waly' type='hidden' value='$sab_logon_ki_hazri_after_minus' />

                                                            </td>
                                                    </tr>


                                                    <tr>
                                                        <td>8</td>
                                                        <td> 
                                                            مسلسل غیر حاضر رہنے والے بھائیوں کی کل تعداد  
                                                        </td>
                                                        <td>";
        $musalsal_ghair_hazir_karkun = sizeof($jo_musalsal_ghair_hazir_thy);
        $data .= $musalsal_ghair_hazir_karkun;

        $data .=                                        "
                                                            <input name='musalsal_ghairhazri_bai' type='hidden' value='$musalsal_ghair_hazir_karkun' />

                                                            </td>
                                                    </tr>


                                                    <tr>
                                                        <td>9</td>
                                                        <td>
                                                             عہد والے بھائیوں کی  محفل  پر  ماہانہ حاضری  (کتنے دن)
                                                        </td>
                                                        <td>";
        $get_count_of_ehad_karkun = DB::table('hazri_ehad_karkuns')
                                            ->where('mehfil_id' , $mehfil_name)
                                            ->where('ehad_karkun_id', '!=' , '')
                                            ->whereBetween('dates' , 
                                                            [
                                                            $year."-".$month_name."-01" , 
                                                            $year."-".$month_name."-31"
                                                            ])
                                            ->count();

// DB::enableQueryLog(); // Enable query log
// // Your Eloquent query executed by using get()
// dd($get_count->getBindings() );

        $data .= $get_count_of_ehad_karkun;
        $data .=                                        "
                                                            <input name='ehad_waly_baion_ki_mahana_hazri' type='hidden' value='$get_count_of_ehad_karkun' />

                                                            </td>
                                                    </tr>";


        $data .=                                    "<tr>
                                                        <td>10</td>
                                                        <td>  ایک ماہ میں نئے ہونے والے کل عہد    </td>
                                                        <td>";

            $all_ehad_register = DB::table('ehad_registers')
                                            ->where('mehfil_id' , $mehfil_name)
                                            ->whereBetween('er_doe' , [$year."-".$month_name."-01" , $year."-".$month_name."-31"] )
                                            ->get();
        $tmam_ehad_register = sizeof($all_ehad_register);
        $data .= $tmam_ehad_register;
        $data .=                                        "
                                                            <input name='aik_maah_mein_hony_waly_ehad' type='hidden' value='$tmam_ehad_register' />

                                                            </td>
                                                    </tr>


                                                    <tr>
                                                        <td>11</td>
                                                        <td> ملتان شریف ڈیوٹی والے بھائیوں کی کل تعداد </td>
                                                        <td>
                                                            <input name='multan_shrif_dutywaly_total' type='number' class='form-control' />

                                                        </td>
                                                    </tr>


                                                    <tr>
                                                        <td>12</td>
                                                        <td>  محفل پر تعلیمات کریمہ اس بار پڑھی گئی؟      </td>
                                                        <td>
                                                            <label class='switch'>
                                                                <input type='checkbox' name='ayat_krima' >
                                                                <div class='slider round'></div>
                                                            </label>
                                                        </td>
                                                    </tr>


                                                    <tr>
                                                        <td>13</td>
                                                        <td>   محفل پر اس ماہ شاہی سواری بھنگڑہ ہوا؟   </td>
                                                        <td>
                                                            <label class='switch'>
                                                                <input type='checkbox' name='bhangra' >
                                                                <div class='slider round'></div>
                                                            </label>
                                                        </td>
                                                    </tr>


                                                    <tr>
                                                        <td colspan='3'>
                                                            محفل پر بھائیوں کی حاضری کی تفصیل 
                                                        </td>
                                                    </tr>


                                                    <tr>
                                                        <td>14</td>
                                                        <td>  محفل پر  روزانہ بھائیوں کی  تعداد   </td>
                                                        <td>";

        $get_average = DB::table('average_hazris')
                    ->where('mehfil_id' , $mehfil_name)
                    ->whereBetween('dates', [$year."-".$month_name."-01" , $year."-".$month_name."-31"])
                    ->avg('average_visitors');

        $get_avg = number_format($get_average);
        $data .= $get_avg;

        $data .=                                        "<input name='get_avg_visitors_on_mehfil' type='hidden' value='$get_avg' />
                                                        
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td>15</td>
                                                        <td>  بڑی محفل پر بھائیوں کی تعداد  </td>
                                                        <td>
                                                            <input name='bari_mehfil_per_tadad' type='number' class='form-control' />
                                                        </td>
                                                    </tr>


                                                    <tr>
                                                        <td>16</td>
                                                        <td> 
                                                            محترم  شیخ  صاحب کی  نام مبارک میٹنگ پر  بھائیوں کی تعداد  
                                                         </td>
                                                        <td>
                                                            <input name='ameer_k_nam_per_meeting_waly' type='number' class='form-control' />
                                                        </td>
                                                    </tr>


                                                    <!-- <tr>
                                                        <td>17</td>
                                                        <td>
                                                            آل کارکنان  میٹنگ پر بھائیوں کی تعداد
                                                        </td>
                                                        <td>
                                                            <input name='all_karkunan_per_meeting_waly' type='number' class='form-control' />
                                                        </td>
                                                    </tr> --!>

                                                    <tr>
                                                        <td colspan='3'> 
                                                         محفل پر  بھائیوں کی  مشاورت  میٹنگ کی تفصٰل    
                                                        </td>
                                                    </tr>


                                                    <tr>
                                                        <td>17</td>
                                                        <td>  میٹنگ کی تاریخ   </td>
                                                        <td>  <input type='date' name='meeting_date' class='form-control' /> </td>
                                                    </tr>


                                                    <tr>
                                                        <td>18</td>
                                                        <td>  میٹنگ پر بھائیوں کی تعداد   </td>
                                                        <td>
                                                            <input name='meeting_per_bai_total' type='number' class='form-control' />
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td>19</td>
                                                        <td>   ممبر عہد کارکن  / میٹنگ کوآڈینیٹر   </td>
                                                        <td>
                                                            <select class='form-control' name='member_coordinator'>
                                                                <option value=''> 
                                                                عہد کارکن کا انتخاب کریں ۔۔۔ 
                                                                </option> 
                                                        ";

        $get_all_ehadkarkuns = DB::table('ehad_karkuns')
                                                ->where('ekp_city' , $get_mehfil_name_coordinator->mehfil_city)
                                                ->get();

            foreach ($get_all_ehadkarkuns as $single_k) {
                $data .= "<option value='$single_k->id'> $single_k->ekp_name </option>";
            }

        $data .=                                       "</select>
                                                        </td>
                                                    </tr>

                                                    <!-- <tr>
                                                        <td colspan='3'>  ایجنڈا برائے میٹنگ   </td>
                                                    </tr>

                                                    <tr>
                                                        <td colspan='3'>  
                                                            <textarea class='form-control' name='agenda_for_meeting'></textarea>
                                                        </td>
                                                    </tr>
                                                    --!>
                                                    <!-- <tr>
                                                        <td colspan='3'></td>
                                                    </tr>

                                                    <tr>
                                                        <td colspan='3'></td>
                                                    </tr> -->

                                                    <!-- <tr>
                                                        <td colspan='3'>  مشاورت   </td>
                                                    </tr>

                                                    <tr>
                                                        <td colspan='3'>
                                                            <textarea class='form-control' name='mushawrat'></textarea>
                                                        </td>
                                                    </tr> --!>

                                                    <!-- <tr>
                                                        <td colspan='3'></td>
                                                    </tr>

                                                    <tr>
                                                        <td colspan='3'></td>
                                                    </tr> -->
                                                </table>
                                            </div>

                                            <div class='col-md-4'>
                                                
                                                <table width='100%' border='2'>
                                                   <!-- <tr>
                                                        <td>
                                                          محفل پر  رابطہ کمیٹی کی کارکردگی کی تفصیل   
                                                        </td>
                                                    </tr> 

                                                    <tr>
                                                        <td>
                                                            <textarea name='kameeti_karkardagi' style='height: 170px' class='form-control'></textarea>
                                                        </td>
                                                    </tr> --!>

                                                    <!-- <tr>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                    </tr> -->

                                                    <!-- <tr>
                                                        <td>
                                                             کوئی ذاتی مشاہدہ ، مسائل  جو محفل  کے بنیادی کاموں میں خلل  کا باعث ہیں۔  
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td>
                                                            <textarea name='zati_mushahda' style='height: 170px' class='form-control'></textarea>
                                                        </td>
                                                    </tr> --!>

                                                    <tr>
                                                        <td>
                                                            ایجنڈا برائے آل کارکنان  میٹنگ
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td>
                                                            <textarea name='agenda_for_all_karkunan_meeting' style='height: 160px' class='form-control'></textarea>
                                                        </td>
                                                    </tr>


                                                </table>
                                            </div>
                                        </div>
                                            <br>
                                            <button class='btn btn-primary' onclick='submit_form(event);'>   ڈیٹا کا اندراج کریں   </button>
                                        </form>";

            // $edit_btn ="<a href='".$url."' class='btn btn-primary'> تبدیل کریں  </a>";
            $edit_btn ="";

            $dataarr = array('all_data' => $data , "btn_edit" => $edit_btn);
            echo  json_encode($dataarr);
            exit();  
    } // end of fuction

    public function array_push_assoc($array, $key, $value){
        $array[$key] = $value;
        return $array;
    }


    public function insert_mehfil_monthly_report_coordinator(Request $request){

        $mehfil_id = $request->mehfil_id;
        $meeting_date = $request->meeting_date;
        $month_number = $request->mahina;
        $year_number = $request->saal;
        $printing_date = date('Y-m-d');
        $dump_data = json_encode($request->all());

        $data = array(
                    'mehfil_id' => $mehfil_id , 
                    'meeting_date' => $meeting_date , 
                    'month' => $month_number , 
                    'year' => $year_number , 
                    'printing_date' => $printing_date , 
                    'dump_data' => $dump_data ,
                    );
        DB::table('monhtly_report_coordinators')->where('month' , $month_number)->where('year' , $year_number)->delete();
        DB::table('monhtly_report_coordinators')->insert($data);
        dd(json_encode($request->all()));
    }




    public function mehfil_monthly_report_edit($id, $month_id, $year){

        // all variables gotten by ajax request
        $month = date('m');
        // $year = date('Y');

        $mehfil_name = $id;
        $month_name = sprintf("%02d", $month_id);
        // $month_name = $month_id;
        $year = $year;

        $data = "";

        $check_already_printed = DB::table('monhtly_report_coordinators')
                                            ->where('mehfil_id' , $mehfil_name)
                                            ->where('month' , $month_name)
                                            ->where('year' , $year)
                                            ->first();

        /*
        if ($check_already_printed != null) {
            $all_data_decode = json_decode($check_already_printed->dump_data);
            // dd($all_data_decode->mehfil_number);
            // dd($all_data_decode);
            $url = url("reports/hazri/mehfil/monthly/edit/".$mehfil_name."/".$month_name."/".$year);
            $edit_btn ="<a onclick='edit_monthly_report(".$mehfil_name." , ".$month_name." , ".$year." , ".$url." )' class='btn btn-primary'> تبدیل کریں  </a>";

            $data .="                       <div class='row'>
                                            <div class='text-right'>
                                                  تاریخ ۔۔۔۔۔۔۔۔۔۔۔   
                                            </div>
                                            <center> <h1> محفل کی  ماہانہ رپورٹ      </h1> </center>
                                        </div>

                                        <div class='row'>
                                            <div class='col-md-8'>

                                            <input type='hidden' name='mehfil_id' id='hidden_mehfil_id' value='$mehfil_name' >";

                                            // <input type='hidden' name='month_number' value='$month' >
                                            // <input type='hidden' name='year_number' value='$year' >

            $data .="                       <table width='100%' border='2'> 
                                                    <tr>
                                                        <td>1</td>
                                                        <td>  محترم شیخ صاحب کی  محفل نمبر   </td>
                                                        <td> 
                                                            <input name='mehfil_number' type='hidden' value='$all_data_decode->mehfil_number' />

                                                            $all_data_decode->mehfil_number

                                                        </td>
                                                    </tr>";
            if(isset($all_data_decode->first_mehfil_coordinator_name)){
                
                $data .="<tr>
                                                        <td>2</td>
                                                        <td>  نام محفل کوآڈینیٹر  (1st)  </td>
                                                        <td>

                                                            <input name='first_mehfil_coordinator_name' type='hidden' 
                                                                    value='$all_data_decode->first_mehfil_coordinator_name' />

                                                            <input name='first_mehfil_coordinator_id' type='hidden' 
                                                                    value='$all_data_decode->first_mehfil_coordinator_id' />

                                                                    $all_data_decode->first_mehfil_coordinator_name

                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td>3</td>
                                                        <td>  نام محفل کوآڈینیٹر  (2nd)  </td>
                                                        <td>

                                                            <input name='second_mehfil_coordinator_name' type='hidden' 
                                                                    value='$all_data_decode->second_mehfil_coordinator_name' />

                                                            <input name='second_mehfil_coordinator_id' type='hidden' 
                                                                    value='$all_data_decode->second_mehfil_coordinator_id' />

                                                                    $all_data_decode->second_mehfil_coordinator_name

                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td>3</td>
                                                        <td> 
                                                          کوآڈینیٹر کی محفل پر  ماہانہ حاضری  (کتنے دن) (1st)  
                                                        </td>
                                                        <td>
                                                            <input name='first_coordinator_hazri_count' type='hidden' value='$all_data_decode->first_coordinator_hazri_count' />

                                                            $all_data_decode->first_coordinator_hazri_count

                                                        </td>
                                                    </tr>


                                                    <tr>
                                                        <td>4</td>
                                                        <td> 
                                                          کوآڈینیٹر کی محفل پر  ماہانہ حاضری  (کتنے دن) (2nd)  
                                                        </td>
                                                        <td>
                                                            <input name='second_coordinator_hazri_count' type='hidden' value='$all_data_decode->second_coordinator_hazri_count' />

                                                            $all_data_decode->second_coordinator_hazri_count

                                                        </td>
                                                    </tr>";
            }else{

                $data .="<tr>
                                                        <td>2</td>
                                                        <td>  نام محفل کوآڈینیٹر  (1st)  </td>
                                                        <td>

                                                            <input name='mehfil_coordinator_name' type='hidden' 
                                                                    value='$all_data_decode->mehfil_coordinator_name' />

                                                            <input name='mehfil_coordinator_id' type='hidden' 
                                                                    value='$all_data_decode->mehfil_coordinator_id' />

                                                                    $all_data_decode->mehfil_coordinator_name

                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td>3</td>
                                                        <td> 
                                                          کوآڈینیٹر کی محفل پر  ماہانہ حاضری  (کتنے دن) (1st)  
                                                        </td>
                                                        <td>
                                                            <input name='coordinator_hazri_count' type='hidden' value='$all_data_decode->coordinator_hazri_count' />

                                                            $all_data_decode->coordinator_hazri_count

                                                        </td>
                                                    </tr>";

            }


                    $data .=                                "<tr>
                                                        <td>4</td>
                                                        <td>  محفل  پر ڈیوٹی والے بھائیوں کی کل تعداد  </td>
                                                        <td>
                                                            <input name='tamam_dutywaly_bai' type='hidden' value='$all_data_decode->tamam_dutywaly_bai' />

                                                            $all_data_decode->tamam_dutywaly_bai

                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td>5</td>
                                                        <td>
                                                            50% سے  کم حاضر رہنے  والے بھایئوں کی کل تعداد   
                                                        </td>
                                                        <td>
                                                            $all_data_decode->fifty_percent_hazri_sy_kam_waly
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td>6</td>
                                                        <td> 
                                                            مسلسل غیر حاضر رہنے والے بھائیوں کی کل تعداد  
                                                        </td>
                                                        <td>
                                                            <input name='musalsal_ghairhazri_bai' type='hidden' value='$all_data_decode->musalsal_ghairhazri_bai' />
                                                        
                                                                $all_data_decode->musalsal_ghairhazri_bai
                                                        </td>
                                                    </tr>


                                                    <tr>
                                                        <td>7</td>
                                                        <td>
                                                             عہد والے بھائیوں کی  محفل  پر  ماہانہ حاضری  (کتنے دن)
                                                        </td>
                                                        <td>

                                                            <input name='ehad_waly_baion_ki_mahana_hazri' type='hidden' value='$all_data_decode->ehad_waly_baion_ki_mahana_hazri' />

                                                            $all_data_decode->ehad_waly_baion_ki_mahana_hazri

                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td>8</td>
                                                        <td>  ایک ماہ میں نئے ہونے والے کل عہد    </td>
                                                        <td>

                                                            <input name='aik_maah_mein_hony_waly_ehad' type='hidden' value='$all_data_decode->aik_maah_mein_hony_waly_ehad' />

                                                                $all_data_decode->aik_maah_mein_hony_waly_ehad

                                                            </td>
                                                    </tr>

                                                    <tr>
                                                        <td>9</td>
                                                        <td> ملتان شریف ڈیوٹی والے بھائیوں کی کل تعداد </td>
                                                        <td>
                                                            $all_data_decode->multan_shrif_dutywaly_total
                                                        </td>
                                                    </tr>


                                                    <tr>
                                                        <td>10</td>
                                                        <td>  محفل پر تعلیمات کریمہ اس بار پڑھی گئی؟      </td>
                                                        <td>";
        
                                                if (isset($all_data_decode->ayat_krima) && $all_data_decode->ayat_krima == 'on') {
                                                    $data .= "  جی ہاں   ";
                                                }else{
                                                    $data .= "  نہیں   ";
                                                }

            $data .=                                       "</td>
                                                    </tr>


                                                    <tr>
                                                        <td>11</td>
                                                        <td>   محفل پر اس ماہ شاہی سواری بھنگڑہ ہوا؟   </td>
                                                        <td>";
                                                
                                                if (isset($all_data_decode->bhangra) && $all_data_decode->bhangra == 'on') {
                                                    $data .= "  جی ہاں   ";
                                                }else{
                                                    $data .= "  نہیں   ";
                                                }

            $data .=                                        "</td>
                                                    </tr>


                                                    <tr>
                                                        <td colspan='3'>
                                                            محفل پر بھائیوں کی حاضری کی تفصیل 
                                                        </td>
                                                    </tr>


                                                    <tr>
                                                        <td>12</td>
                                                        <td>  محفل پر  روزانہ بھائیوں کی  تعداد   </td>
                                                        <td>
                                                            <input name='get_avg_visitors_on_mehfil' type='hidden' value='$all_data_decode->get_avg_visitors_on_mehfil' />

                                                            $all_data_decode->get_avg_visitors_on_mehfil
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td>13</td>
                                                        <td>  بڑی محفل پر بھائیوں کی تعداد  </td>
                                                        <td>
                                                            $all_data_decode->bari_mehfil_per_tadad
                                                        </td>
                                                    </tr>


                                                    <tr>
                                                        <td>14</td>
                                                        <td> 
                                                            محترم  شیخ  صاحب کی  نام مبارک میٹنگ پر  بھائیوں کی تعداد  
                                                         </td>
                                                        <td>
                                                            $all_data_decode->ameer_k_nam_per_meeting_waly
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td>15</td>
                                                        <td>
                                                            آل کارکنان  میٹنگ پر بھائیوں کی تعداد
                                                        </td>
                                                        <td>
                                                            $all_data_decode->all_karkunan_per_meeting_waly
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td colspan='3'> 
                                                         محفل پر  بھائیوں کی  مشاورت  میٹنگ کی تفصٰل    
                                                        </td>
                                                    </tr>


                                                    <tr>
                                                        <td>16</td>
                                                        <td>  میٹنگ کی تاریخ   </td>
                                                        <td>".date('d-m-Y' , strtotime($all_data_decode->meeting_date))."</td>
                                                    </tr>


                                                    <tr>
                                                        <td>17</td>
                                                        <td>  میٹنگ پر بھائیوں کی تعداد   </td>
                                                        <td>
                                                            $all_data_decode->meeting_per_bai_total
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td>18</td>
                                                        <td>   ممبر عہد کارکن  / میٹنگ کوآڈینیٹر   </td>
                                                        <td>
                                                            $all_data_decode->member_coordinator
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td colspan='3'>  ایجنڈا برائے میٹنگ   </td>
                                                    </tr>

                                                    <tr>
                                                        <td colspan='3'>                                                          
                                                            $all_data_decode->agenda_for_meeting

                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td colspan='3'>  مشاورت   </td>
                                                    </tr>

                                                    <tr>
                                                        <td colspan='3'>
                                                            $all_data_decode->mushawrat
                                                        </td>
                                                    </tr>

                                                </table>
                                            </div>

                                            <div class='col-md-4'>
                                                
                                                <table width='100%' border='2'>
                                                    <tr>
                                                        <td>
                                                          محفل پر  رابطہ کمیٹی کی کارکردگی کی تفصیل   
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style='height: 150px;'>
                                                            $all_data_decode->kameeti_karkardagi
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td>
                                                             کوئی ذاتی مشاہدہ ، مسائل  جو محفل  کے بنیادی کاموں میں خلل  کا باعث ہیں۔  
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style='height: 150px;'>                                                        
                                                            $all_data_decode->zati_mushahda
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td>
                                                            ایجنڈا برائے آل کارکنان  میٹنگ
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style='height: 150px;'>
                                                            $all_data_decode->agenda_for_all_karkunan_meeting
                                                        </td>
                                                    </tr>


                                                </table>
                                            </div>
                                        </div>";

            $dataarr = array('all_data' => $data , "btn_edit" => $edit_btn);
            echo json_encode($dataarr);
            exit();
        }
        */

        // DB::enableQueryLog();

        //when above defined if condition does not true then this code will be run
        $get_mehfil_name_coordinator = DB::table('karkun_duty_rosters')
                                        ->join('mehfils' , 'karkun_duty_rosters.mehfil_id' , 'mehfils.id')
                                        ->select('karkun_duty_rosters.*' , 'mehfils.mehfil_number'  , 'mehfils.mehfil_city')
                                        ->where('mehfil_id' , $mehfil_name)
                                        ->where('month_name' , $month_name)
                                        ->where('year_name' , $year)
                                        ->first(); 

        // $query = DB::getQueryLog();
        // print_r(end($query));

        $to_get_all_duty_karkuns = DB::table('karkun_duty_rosters')
                                        ->where('mehfil_id' , $mehfil_name)
                                        ->where('month_name' , $month_name)
                                        ->where('year_name' , $year)
                                        ->get(); 

        // dd($get_mehfil_name_coordinator);

        if ($get_mehfil_name_coordinator == null ) {

            $data .= "    اس ماہ کا  ڈیوٹی روسٹر ہی موجود نہیں ہے۔     "; 
            $dataarr = array('all_data' => $data);
            echo json_encode($dataarr);
            exit();
        }


        $get_all_dutywaly = array();
        $get_all_dutywaly_final = array();


        for ($i=0; $i < sizeof($to_get_all_duty_karkuns); $i++) { 

                array_push($get_all_dutywaly, explode(',', $to_get_all_duty_karkuns[$i]->k_main_gatian));
                array_push($get_all_dutywaly, explode(',', $to_get_all_duty_karkuns[$i]->k_cycle_stand));
                array_push($get_all_dutywaly, explode(',', $to_get_all_duty_karkuns[$i]->k_security));
                array_push($get_all_dutywaly, explode(',', $to_get_all_duty_karkuns[$i]->monthly_cordinator));
        }

        for ($i=0; $i < count($get_all_dutywaly); $i++) { 
            for ($j=0; $j < count($get_all_dutywaly[$i]); $j++) {                 
                array_push($get_all_dutywaly_final, $get_all_dutywaly[$i][$j]);
            }
        }

        $monthly_coordinator = explode(',' , $get_mehfil_name_coordinator->monthly_cordinator);


        $data .="<form class='table_report'>"; 
        $data .= "<div class='row'>
                                            <div class='text-right'>
                                                   تاریخ ۔۔۔۔۔۔۔۔۔۔۔  
                                            </div>
                                            <center> <h1> محفل کی  ماہانہ رپورٹ      </h1> </center>
                                        </div>

                                        <div class='row'>
                                            <div class='col-md-8'>

                                            <input type='hidden' name='mehfil_id' id='hidden_mehfil_id' value='$mehfil_name' >";

                                            // <input type='hidden' name='month_number' value='$month' >
                                            // <input type='hidden' name='year_number' value='$year' >

                                            // ";



        $data .=                                "<table width='100%' border='2'> 
                                                    <tr>
                                                        <td>1</td>
                                                        <td>  محترم شیخ صاحب کی  محفل نمبر   </td>
                                                        <td> 
                                                            <input name='mehfil_number' type='hidden' value='$get_mehfil_name_coordinator->mehfil_number' />

                                                         $get_mehfil_name_coordinator->mehfil_number  </td>
                                                    </tr>
                                                    <tr>
                                                        <td>2</td>
                                                        <td>  نام محفل کوآڈینیٹر  (1st)  </td>
                                                        <td>";

                                                            $karkun_name = DB::table('karkuns')->where('id' , $monthly_coordinator[0])->first();

        $data .=                                            $karkun_name->kp_name;


        $get_count =                DB::table('hazri_karkuns')
                                        ->select('hazri_karkuns.status' , 'hazri_karkuns.dates' , DB::raw("count(case hazri_karkuns.status when 'p' then 1 else null end) as hazir"))
                                        ->whereBetween('dates' , [$year."-".$month_name."-01" , $year."-".$month_name."-31"] )
                                        ->where('karkun_id' , $monthly_coordinator[0])
                                        ->first();

        $data .=                                        "<input name='first_mehfil_coordinator_name' type='hidden' 
                                                                    value='$karkun_name->kp_name' />

                                                            <input name='first_mehfil_coordinator_id' type='hidden' 
                                                                    value='$karkun_name->id' />

                                                        </td>
                                                    </tr>


                                                    <tr>
                                                        <td>3</td>
                                                        <td>  نام محفل کوآڈینیٹر  (2nd)  </td>
                                                        <td>";

                                                            $karkun_name = DB::table('karkuns')->where('id' , $monthly_coordinator[1])->first();

        $data .=                                            $karkun_name->kp_name;

        // DB::enableQueryLog();
        $get_count =                DB::table('hazri_karkuns')
                                        ->select('hazri_karkuns.status' , 'hazri_karkuns.dates' , DB::raw("count(case hazri_karkuns.status when 'p' then 1 else null end) as hazir"))
                                        ->whereBetween('dates' , [$year."-".$month_name."-01" , $year."-".$month_name."-31"] )
                                        ->where('karkun_id' , $monthly_coordinator[1])
                                        ->first();

        // $query = DB::getQueryLog();
        // print_r(end($query));

        // dd($get_count);

        $data .=                                        "<input name='second_mehfil_coordinator_name' type='hidden' 
                                                                    value='$karkun_name->kp_name' />

                                                            <input name='second_mehfil_coordinator_id' type='hidden' 
                                                                    value='$karkun_name->id' />

                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td>4</td>
                                                        <td> 
                                                          کوآڈینیٹر کی محفل پر  ماہانہ حاضری  (کتنے دن)  (1st) 
                                                        </td>
                                                        <td>

                                                            <input name='first_coordinator_hazri_count' type='hidden' value='$get_count->hazir' />
                                                        ";

        $data .= $get_count->hazir;
        $data .=                                        "</td>
                                                    </tr>



                                                    <tr>
                                                        <td>5</td>
                                                        <td> 
                                                          کوآڈینیٹر کی محفل پر  ماہانہ حاضری  (کتنے دن)   (2nd)
                                                        </td>
                                                        <td>

                                                            <input name='second_coordinator_hazri_count' type='hidden' value='$get_count->hazir' />
                                                        ";

        $data .= $get_count->hazir;
        $data .=                                        "</td>
                                                    </tr>


                                                    <tr>
                                                        <td>4</td>
                                                        <td>  محفل  پر ڈیوٹی والے بھائیوں کی کل تعداد  </td>
                                                        <td>

                                                        "; 
        $total_dty_waly = sizeof(array_unique($get_all_dutywaly_final));
        $data .= $total_dty_waly;

        $data .=                                        "
                                                            <input name='tamam_dutywaly_bai' type='hidden' value='$total_dty_waly' />

                                                            </td>
                                                    </tr>

                                                    <tr>
                                                        <td>5</td>
                                                        <td>
                                                            50% سے  کم حاضر رہنے  والے بھایئوں کی کل تعداد   
                                                        </td>
                                                        <td>";

        $get_all_from_duty_rosters = DB::table('karkun_duty_rosters')
                                            ->where('mehfil_id' , $mehfil_name)
                                            ->where('month_name' , $month_name)
                                            ->where('year_name' , $year)
                                            ->get();

   
        $tamam_duty_waly = array();
        $dino_k_hisab_sy_duty_waly = array();
        $tamam_duty_waly_final = array();

        foreach($get_all_from_duty_rosters as $single_from_duty_rosters) {

            $main_gatian = $single_from_duty_rosters->k_main_gatian;           
            $cycle_stand = $single_from_duty_rosters->k_cycle_stand;
            $security = $single_from_duty_rosters->k_security;

            $all_members = $main_gatian .",". $cycle_stand .",". $security;
            $days = $single_from_duty_rosters->k_duty_days;

            $dino_k_hisab_sy_duty_waly[$days] = explode(',', $all_members);
        } // end of main foreach

        foreach ($dino_k_hisab_sy_duty_waly as $key => $values) {
            $tamam_duty_waly_final[$key] = array_unique($dino_k_hisab_sy_duty_waly[$key]);
        }



        $duplicate_check = array();
        foreach ($tamam_duty_waly_final as $key => $value) {
            foreach ($value as $key2 => $value2) {

                if(isset($duplicate_check[$value2]) && $duplicate_check[$value2] > 0){
                    $duplicate_check[$value2]++;
                }else{
                    $duplicate_check[$value2] = 1;     
                }
            }
        } // end of foreach

        // jo total hazri nikli thi usko 4 sy zarab deny k liyee
        $zarb_deny_wali_array = array();
        foreach ($duplicate_check as $key => $value) {
            $zarb_deny_wali_array[$key] = $value * 4;
        } // end of foreach

        // hazri waly table sy hazrian nikalny k lyee har har admi ki k jis ki duty lagi hoi thi duty roster mn
        $hazri_waly_karkun = DB::table('hazri_karkuns')
                                ->where('mehfil_id' , $mehfil_name)
                                ->whereBetween('dates' , [$year."-".$month_name."-01" , $year."-".$month_name."-31"])
                                ->where('flag' , 1)
                                ->where('status' , 'p')
                                ->get();


        $duplicate_check_hazri = array();
        foreach ($hazri_waly_karkun as $values) {

            if(isset($duplicate_check_hazri[$values->karkun_id]) && $duplicate_check_hazri[$values->karkun_id] > 0){
                $duplicate_check_hazri[$values->karkun_id]++;
            }else{
                $duplicate_check_hazri[$values->karkun_id] = 1;     
            }
        }

        $sb_logon_ki_hazri_minus_kr_k = array();
        $jo_musalsal_ghair_hazir_thy = array();
        foreach ($duplicate_check_hazri as $key=>$value) {

            $percentage = 50;
            $total_is = $zarb_deny_wali_array[$key];
            $after_percentage = ($percentage / 100) * $total_is;

            // echo $key . " key is:  <br>";
            // echo $zarb_deny_wali_array[$key] . " zarb deny k bd : <br>";
            // echo $after_percentage . " after percentage: <br>";
            // echo  $value . " hazri wali count:  <br><br><br><br>";

            if ($value < $after_percentage) {
                $sb_logon_ki_hazri_minus_kr_k[$key] = ($zarb_deny_wali_array[$key] - $value);
            }
            if ($value < 1) {
                $jo_musalsal_ghair_hazir_thy[$key] = ($zarb_deny_wali_array[$key] - $value);
            }

        }

        // dd($jo_musalsal_ghair_hazir_thy);
        // dd($zarb_deny_wali_array);
        // dd($duplicate_check_hazri);
        // dd($duplicate_check);
        // dd($sb_logon_ki_hazri_minus_kr_k);
        $sab_logon_ki_hazri_after_minus = sizeof($sb_logon_ki_hazri_minus_kr_k);
        $data .= $sab_logon_ki_hazri_after_minus;
        

        $data .=                                        "
                                                            <input name='fifty_percent_hazri_sy_kam_waly' type='hidden' value='$sab_logon_ki_hazri_after_minus' />

                                                            </td>
                                                    </tr>


                                                    <tr>
                                                        <td>6</td>
                                                        <td> 
                                                            مسلسل غیر حاضر رہنے والے بھائیوں کی کل تعداد  
                                                        </td>
                                                        <td>";
        $musalsal_ghair_hazir_karkun = sizeof($jo_musalsal_ghair_hazir_thy);
        $data .= $musalsal_ghair_hazir_karkun;

        $data .=                                        "
                                                            <input name='musalsal_ghairhazri_bai' type='hidden' value='$musalsal_ghair_hazir_karkun' />

                                                            </td>
                                                    </tr>


                                                    <tr>
                                                        <td>7</td>
                                                        <td>
                                                             عہد والے بھائیوں کی  محفل  پر  ماہانہ حاضری  (کتنے دن)
                                                        </td>
                                                        <td>";
        $get_count_of_ehad_karkun = DB::table('hazri_ehad_karkuns')
                                            ->where('mehfil_id' , $mehfil_name)
                                            ->whereBetween('dates' , 
                                                            [
                                                            $year."-".$month_name."-01" , 
                                                            $year."-".$month_name."-31"
                                                            ])
                                            ->count();

        $data .= $get_count_of_ehad_karkun;
        $data .=                                        "
                                                            <input name='ehad_waly_baion_ki_mahana_hazri' type='hidden' value='$get_count_of_ehad_karkun' />

                                                            </td>
                                                    </tr>";


        $data .=                                    "<tr>
                                                        <td>8</td>
                                                        <td>  ایک ماہ میں نئے ہونے والے کل عہد    </td>
                                                        <td>";

            $all_ehad_register = DB::table('ehad_registers')
                                            ->where('mehfil_id' , $mehfil_name)
                                            ->whereBetween('er_doe' , [$year."-".$month_name."-01" , $year."-".$month_name."-31"] )
                                            ->get();
        $tmam_ehad_register = sizeof($all_ehad_register);
        $data .= $tmam_ehad_register;
        $data .=                                        "
                                                            <input name='aik_maah_mein_hony_waly_ehad' type='hidden' value='$tmam_ehad_register' />

                                                            </td>
                                                    </tr>


                                                    <tr>
                                                        <td>9</td>
                                                        <td> ملتان شریف ڈیوٹی والے بھائیوں کی کل تعداد </td>
                                                        <td>
                                                            <input name='multan_shrif_dutywaly_total' type='number' class='form-control' />

                                                        </td>
                                                    </tr>


                                                    <tr>
                                                        <td>10</td>
                                                        <td>  محفل پر تعلیمات کریمہ اس بار پڑھی گئی؟      </td>
                                                        <td>
                                                            <label class='switch'>
                                                                <input type='checkbox' name='ayat_krima' >
                                                                <div class='slider round'></div>
                                                            </label>
                                                        </td>
                                                    </tr>


                                                    <tr>
                                                        <td>11</td>
                                                        <td>   محفل پر اس ماہ شاہی سواری بھنگڑہ ہوا؟   </td>
                                                        <td>
                                                            <label class='switch'>
                                                                <input type='checkbox' name='bhangra' >
                                                                <div class='slider round'></div>
                                                            </label>
                                                        </td>
                                                    </tr>


                                                    <tr>
                                                        <td colspan='3'>
                                                            محفل پر بھائیوں کی حاضری کی تفصیل 
                                                        </td>
                                                    </tr>


                                                    <tr>
                                                        <td>12</td>
                                                        <td>  محفل پر  روزانہ بھائیوں کی  تعداد   </td>
                                                        <td>";

        $get_average = DB::table('average_hazris')
                    ->where('mehfil_id' , $mehfil_name)
                    ->whereBetween('dates', [$year."-".$month_name."-01" , $year."-".$month_name."-31"])
                    ->avg('average_visitors');

        $get_avg = number_format($get_average);
        $data .= $get_avg;

        $data .=                                        "<input name='get_avg_visitors_on_mehfil' type='hidden' value='$get_avg' />
                                                        
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td>13</td>
                                                        <td>  بڑی محفل پر بھائیوں کی تعداد  </td>
                                                        <td>
                                                            <input name='bari_mehfil_per_tadad' type='number' class='form-control' />
                                                        </td>
                                                    </tr>


                                                    <tr>
                                                        <td>14</td>
                                                        <td> 
                                                            محترم  شیخ  صاحب کی  نام مبارک میٹنگ پر  بھائیوں کی تعداد  
                                                         </td>
                                                        <td>
                                                            <input name='ameer_k_nam_per_meeting_waly' type='number' class='form-control' />
                                                        </td>
                                                    </tr>


                                                    <tr>
                                                        <td>15</td>
                                                        <td>
                                                            آل کارکنان  میٹنگ پر بھائیوں کی تعداد
                                                        </td>
                                                        <td>
                                                            <input name='all_karkunan_per_meeting_waly' type='number' class='form-control' />
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td colspan='3'> 
                                                         محفل پر  بھائیوں کی  مشاورت  میٹنگ کی تفصٰل    
                                                        </td>
                                                    </tr>


                                                    <tr>
                                                        <td>16</td>
                                                        <td>  میٹنگ کی تاریخ   </td>
                                                        <td>  <input type='date' name='meeting_date' class='form-control' /> </td>
                                                    </tr>


                                                    <tr>
                                                        <td>17</td>
                                                        <td>  میٹنگ پر بھائیوں کی تعداد   </td>
                                                        <td>
                                                            <input name='meeting_per_bai_total' type='number' class='form-control' />
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td>18</td>
                                                        <td>   ممبر عہد کارکن  / میٹنگ کوآڈینیٹر   </td>
                                                        <td>
                                                            <select class='form-control' name='member_coordinator'>
                                                                <option value=''> 
                                                                عہد کارکن کا انتخاب کریں ۔۔۔ 
                                                                </option> 
                                                        ";

        $get_all_ehadkarkuns = DB::table('ehad_karkuns')
                                                ->where('ekp_city' , $get_mehfil_name_coordinator->mehfil_city)
                                                ->get();

            foreach ($get_all_ehadkarkuns as $single_k) {
                $data .= "<option value='$single_k->id'> $single_k->ekp_name </option>";
            }

        $data .=                                       "</select>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td colspan='3'>  ایجنڈا برائے میٹنگ   </td>
                                                    </tr>

                                                    <tr>
                                                        <td colspan='3'>  
                                                            <textarea class='form-control' name='agenda_for_meeting'></textarea>
                                                        </td>
                                                    </tr>

                                                    <!-- <tr>
                                                        <td colspan='3'></td>
                                                    </tr>

                                                    <tr>
                                                        <td colspan='3'></td>
                                                    </tr> -->

                                                    <tr>
                                                        <td colspan='3'>  مشاورت   </td>
                                                    </tr>

                                                    <tr>
                                                        <td colspan='3'>
                                                            <textarea class='form-control' name='mushawrat'></textarea>
                                                        </td>
                                                    </tr>

                                                    <!-- <tr>
                                                        <td colspan='3'></td>
                                                    </tr>

                                                    <tr>
                                                        <td colspan='3'></td>
                                                    </tr> -->
                                                </table>
                                            </div>

                                            <div class='col-md-4'>
                                                
                                                <table width='100%' border='2'>
                                                    <tr>
                                                        <td>
                                                          محفل پر  رابطہ کمیٹی کی کارکردگی کی تفصیل   
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td>
                                                            <textarea name='kameeti_karkardagi' style='height: 170px' class='form-control'></textarea>
                                                        </td>
                                                    </tr>

                                                    <!-- <tr>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                    </tr> -->

                                                    <tr>
                                                        <td>
                                                             کوئی ذاتی مشاہدہ ، مسائل  جو محفل  کے بنیادی کاموں میں خلل  کا باعث ہیں۔  
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td>
                                                            <textarea name='zati_mushahda' style='height: 170px' class='form-control'></textarea>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td>
                                                            ایجنڈا برائے آل کارکنان  میٹنگ
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td>
                                                            <textarea name='agenda_for_all_karkunan_meeting' style='height: 160px' class='form-control'></textarea>
                                                        </td>
                                                    </tr>


                                                </table>
                                            </div>
                                        </div>
                                            <br>
                                            <button class='btn btn-primary' onclick='submit_form(event);'>   ڈیٹا کا اندراج کریں   </button>
                                        </form>";

            // $edit_btn ="<a href='".$url."' class='btn btn-primary'> تبدیل کریں  </a>";
            $edit_btn ="";

            $dataarr = array('all_data' => $data , "btn_edit" => $edit_btn);
            echo  json_encode($dataarr);
            exit();  
    } // end of fuction



}
