<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\HazriKarkun;



class HazriKarkunController extends Controller
{

    public function showData($datess = null)
    {
        // $mydate = '19';
        $all_mehfils = DB::table('mehfils')->get();
        // $day_name = strtolower(date("l" , strtotime("2020-10-".$mydate)));    

        $day_name = '';
        $actual_date = '';

        if (isset($datess) && $datess != '') {
            $day_name = strtolower(date("l", strtotime($datess)));
            $actual_date = $datess;
        } else {
            $day_name = strtolower(date("l"));
            $actual_date = date('Y-m-d');
        }

        // dd($actual_date);
        // dd($day_name);

        $admin_id = session('userid');
        $get_mehfil_id = DB::table('mehfils')->where('admin_id', $admin_id)->first();
        // get city name... bcz in the next we have to get the ehad karkuns according to city
        $mehfil_city = '';
        $get_ehad_karkuns = array();
        if (isset($get_mehfil_id->mehfil_city)) {
            $mehfil_city = $get_mehfil_id->mehfil_city;
            $get_ehad_karkuns = DB::table('ehad_karkuns')->where('ekp_city', $mehfil_city)->get();
        }




        if ($get_mehfil_id == null) {
            $not_showing = " محفل کے  مندرجات سے   پورٹل میں داخل ہوں۔   ";
            return view('hazri_karkun.hazri_karkun')->with(compact(['not_showing']));
        }

        $all_k_duty_roster = DB::table('karkun_duty_rosters')
            ->where('k_duty_days', $day_name)
            ->where('mehfil_id', $get_mehfil_id->id)
            ->where('month_name', date('m'))
            ->where('year_name', date('Y'))
            ->first();


        $ehad_karkun_duty_roster = DB::table('ehad_karkun_duty_rosters')
                                        ->join('ehad_karkuns', 'ehad_karkun_duty_rosters.ehad_karkun_id', '=', 'ehad_karkuns.id')
                                        ->select('ehad_karkun_duty_rosters.*', 'ehad_karkuns.id' , 'ehad_karkuns.ekp_name' ,'ehad_karkuns.ekp_id_number' , 'ehad_karkuns.ekp_fname')
                                        ->where('mehfil_id' , $get_mehfil_id->id)
                                        ->where('day', $day_name)
                                        ->where('month', date('m'))
                                        ->where('year', date('Y'))
                                        ->first();
        // dd($all_k_duty_roster);

        if ($all_k_duty_roster == null) {
            $not_showing = "  اس محفل کا  ڈیوٹی روسٹر موجود نہیں ہے۔  ";
            return view('hazri_karkun.hazri_karkun')->with(compact(['not_showing']));
        }


        // convertinng the string into array to check the duties
        $gatian = explode(',', $all_k_duty_roster->k_main_gatian);
        $security = explode(',', $all_k_duty_roster->k_security);
        $cycle_stand = explode(',', $all_k_duty_roster->k_cycle_stand);

        $arr_gatian = [];
        $arr_cs = [];
        $arr_s = [];

        foreach ($gatian as $single_gatian) {
            array_push($arr_gatian, $single_gatian);
        }

        foreach ($cycle_stand as $single_cycle_stand) {
            array_push($arr_cs, $single_cycle_stand);
        }

        foreach ($security as $single_security) {
            array_push($arr_s, $single_security);
        }

        $all_karkun = DB::table('karkuns')->where('mehfil_id', $get_mehfil_id->id)->get();

        // for checking the hazri details in table
        $get_hazri = DB::table('hazri_karkuns')
            ->join('mehfils', 'hazri_karkuns.mehfil_id', '=', 'mehfils.id')
            ->select('hazri_karkuns.*', 'mehfils.mehfil_name')
            ->where('mehfil_id', $get_mehfil_id->id)
            ->groupBy('dates')
            ->orderby('dates', 'desc')
            ->limit(31)
            ->get();

        /// code starts here which is to check that whether data of this days is already entered or not
        $date_is = date('d');
        $month_is = date("m");
        $year_is = date("Y");

        $date_check = '';

        if (isset($datess) && $datess != '') {
            $date_check = $datess;
        } else {
            $date_check = $year_is . '-' . $month_is . '-' . $date_is;
        }
        $hazri_karkuns = DB::table('hazri_karkuns')
            ->join('karkuns', 'hazri_karkuns.karkun_id', '=', 'karkuns.id')
            ->select('hazri_karkuns.*', 'karkuns.kp_name' , 'karkuns.kp_id_number' , 'karkuns.kp_fname')
            ->where('hazri_karkuns.dates', $date_check)
            ->where('hazri_karkuns.mehfil_id', $get_mehfil_id->id)
            ->get();

        if (sizeof($hazri_karkuns) > 0) {

            $present_karkuns_count = DB::table('hazri_karkuns')
            ->join('karkuns', 'hazri_karkuns.karkun_id', '=', 'karkuns.id')
            ->select('karkuns.id')
            ->where('hazri_karkuns.dates', $date_check)
            ->where('hazri_karkuns.mehfil_id', $get_mehfil_id->id)
            ->where('hazri_karkuns.status', 'p')
            ->get()->count();

            $avarage_hazri_karkun = DB::table('average_hazris')
                ->where('mehfil_id', $get_mehfil_id->id)
                ->where('dates', $date_check)
                ->first();

            $avarage_hazri_karkun_ki_tadad = $avarage_hazri_karkun->average_visitors;

            $hazri_ehad_karkuns_name_id = DB::table('hazri_ehad_karkuns')
                ->join('ehad_karkuns', 'hazri_ehad_karkuns.ehad_karkun_id', '=', 'ehad_karkuns.id')
                ->select('hazri_ehad_karkuns.*', 'ehad_karkuns.ekp_name' , 'ehad_karkuns.id' , 'ehad_karkuns.ekp_id_number', 'ehad_karkuns.ekp_fname')
                ->where('mehfil_id', $get_mehfil_id->id)
                ->where('dates', $date_check)
                ->first();

            return view('hazri_karkun.hazri_karkun')->with(compact([
                                                    'avarage_hazri_karkun_ki_tadad', 
                                                    'datess', 'hazri_karkuns', 'present_karkuns_count',
                                                    'all_mehfils', 
                                                    'get_hazri', 
                                                    'get_mehfil_id', 
                                                    'arr_gatian', 
                                                    'arr_cs', 
                                                    'arr_s' , 
                                                    'get_ehad_karkuns' , 
                                                    'hazri_ehad_karkuns_name_id']));
        }
        /// code ends here which is to check that whether data of this days is already entered or not
        else {
            $present_karkuns_count = 0;
            foreach ($all_karkun as $key => $single_karkun){
                if (in_array($single_karkun->id, $arr_gatian) || in_array($single_karkun->id, $arr_cs) || in_array($single_karkun->id, $arr_s)) {
                    $present_karkuns_count += 1;
                }
            }    

            return view('hazri_karkun.hazri_karkun')->with(compact(['datess', 'all_k_duty_roster', 'all_mehfils', 'present_karkuns_count', 'all_karkun', 'get_mehfil_id', 'arr_gatian', 'arr_cs', 'arr_s', 'get_hazri' , 'get_ehad_karkuns' , 'ehad_karkun_duty_roster']));
        }
    }

    public function insertHazriKarkun(Request $request)
    {

        // $mydate = '19';

        // dd($request->all());

        $tareekh_f = date('d');
        $mahina_f = date('m');
        $year_f = date('Y');

        // $date_time = $year_f.'-'.$mahina_f.'-'.$tareekh_f;
        $date_time = $request->selected_date;
        // $date_time = $year_f.'-'.$mahina_f.'-'.$mydate;

        $day_name = strtolower(date('l', strtotime($date_time)));

        DB::table('hazri_karkuns')
            ->where('mehfil_id', $request->mehfils)
            ->where('dates', $date_time)
            ->delete();

        DB::table('hazri_ehad_karkuns')
            ->where('mehfil_id', $request->mehfils)
            ->where('dates', $date_time)
            ->delete();

        DB::table('average_hazris')
            ->where('mehfil_id', $request->mehfils)
            ->where('dates', $date_time)
            ->delete();

        $ehad_karkun_id = $request->ehad_karkun_id;
        $data_ehazri = array(
            'ehad_karkun_id' => $request->ehad_karkun_id,
            'mehfil_id' => $request->mehfils,
            'dates' => $date_time,
        );
        DB::table('hazri_ehad_karkuns')->insert($data_ehazri);

        $average_hazri = $request->average_hazri;
        $data1 = array(
            'mehfil_id' => $request->mehfils,
            'dates' => $date_time,
            'average_visitors' => $average_hazri,
        );
        DB::table('average_hazris')->insert($data1);

        $karkun_id = $request->karkun_id; // for loop
        for ($i = 0; $i < sizeof($karkun_id); $i++) {
            $hour = $request->hour[$i];
            $minute = $request->minute[$i];
            // $tareekh = $request->tareekh[$i]; 
            // $mahina = $request->mahina[$i]; 
            // $year = date('Y');
            $am_pm = $request->am_pm[$i];
            // $am_pm = '';

            // dd($hour);
            // if ($tareekh == null || $mahina == null) {
            //     $mahina = date('m');
            //     $tareekh = date('d');
            // }
            
            if ($am_pm == null && $hour == null && $minute == null) {
                $hour = date('h');
                $minute = date('i');
                $am_pm = date('a');
            }elseif($hour == null && $minute == null) {
                $hour = date('h');
                $minute = date('i');
            }

            // $dates = $year.'-'.$mahina.'-'.$tareekh;
            //$dates = $year.'-'.$mahina.'-'.$mydate;
            $times = $hour . ':' . $minute . $am_pm;

            $data = array(
                'karkun_id' => $request->karkun_id[$i],
                'mehfil_id' => $request->mehfils,
                'status' => $request->hidden_hazri[$i],
                'day_name' => $day_name,
                'flag' => $request->flag1[$i],
                'dates' => $date_time,
                'times' => $times,
            );

            DB::table('hazri_karkuns')->insert($data);
        } // end of foreach
    } // end of function

    // public function editHazriKarkun($id , Request $request){
    // }


    public function showData_ehad()
    {
        // dd('dfsa');
        $day_name = strtolower(date("l"));
        $admin_id = session('userid');
        $city = session('city');
        $user_role = session('user_role');
        $mehfil_id = session('mehfil_id');

        $all_mehfils = '';
        if ($user_role == 'mehfil') {
            $all_mehfils = DB::table('mehfils')->where('id', $mehfil_id)->get();
        } else {
            $all_mehfils = DB::table('mehfils')->where('mehfil_city', $city)->get();
        }


        return view('hazri_ehad_karkun.hazri_ehad_karkun')
            ->with(compact(['all_mehfils']));
    } // end of function


    public function get_all_ehad_karkuns(Request $request)
    {

        $mehfil_id = $request->mehfil_id;
        $date_hazri = $request->date_hazri;


        $day_name = '';
        if (isset($date_hazri) && !empty($date_hazri)) {
            $day_name = strtolower(date("l" , strtotime($date_hazri)));            
        }else{
            $day_name = strtolower(date("l"));
        }

        $city = session('city');
        $all_ehad_karkuns = DB::table('ehad_karkuns')->where('ekp_city', $city)->get();


        $all_hazri_ehad = DB::table('hazri_ehad_karkuns')
            ->join('ehad_karkuns', 'hazri_ehad_karkuns.ehad_karkun_id', 'ehad_karkuns.id')
            ->select('hazri_ehad_karkuns.*', 'ehad_karkuns.ekp_name')
            ->where('mehfil_id', $mehfil_id)
            ->get();

        $data1 = '';

        foreach ($all_hazri_ehad as $single_hazri) {
            $data1 .= "<tr>";
            $data1 .= "<td>" . date('d-m-Y', strtotime($single_hazri->dates)) . " </td>";
            $data1 .= "<td> $single_hazri->ekp_name  </td>";
            $data1 .= "</tr>";
        }



        $all_k_duty_roster = DB::table('ehad_karkun_duty_rosters')
            ->join('ehad_karkuns', 'ehad_karkun_duty_rosters.ehad_karkun_id', 'ehad_karkuns.id')
            ->select('ehad_karkun_duty_rosters.*', 'ehad_karkuns.ekp_name')
            ->where('ehad_karkun_duty_rosters.day', $day_name)
            ->where('ehad_karkun_duty_rosters.mehfil_id', $mehfil_id)
            ->where('ehad_karkun_duty_rosters.month', date('m'))
            ->where('ehad_karkun_duty_rosters.year', date('Y'))
            ->first();

        // dd($all_k_duty_roster);

        $data = "<option value='$all_k_duty_roster->ehad_karkun_id'> $all_k_duty_roster->ekp_name </option>";

        foreach ($all_ehad_karkuns as $single_ehad) {
            $data .= "<option value='$single_ehad->id'> $single_ehad->ekp_name </option>";
        }

        $arr = array('options' => $data, 'tbody' => $data1);
        echo json_encode($arr);
        exit();
    }

    public function insert_hazri_ehad_karkun(Request $request)
    {

        $date_hazri = $request->date_hazri;

        $date = '';
        if(isset($date_hazri) && !empty($date_hazri)){
            $date = date('Y-m-d' , strtotime($date_hazri));
        }else{
            $tareekh_f = date('d');
            $mahina_f = date('m');
            $year_f = date('Y');
            $date = $year_f . '-' . $mahina_f . '-' . $tareekh_f;
        }
        // $date = $year_f.'-'.$mahina_f.'-17';
        $mehfil_id = $request->mehfil_id;
        $ehad_karkun_id = $request->ehad_karkun_id;

        DB::table('hazri_ehad_karkuns')
            ->where('mehfil_id', $mehfil_id)
            ->where('dates', $date)
            ->delete();

        $data = array(
            'mehfil_id' => $mehfil_id,
            'ehad_karkun_id' => $ehad_karkun_id,
            'dates' => $date
        );

        DB::table('hazri_ehad_karkuns')->insert($data);

        echo '  ڈیٹا داخل کریں ';
        exit();
    }
}
