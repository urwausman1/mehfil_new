<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

use PragmaRX\Countries\Package\Countries;

use App\Mehfil;

class MehfilController extends Controller{

    public function showData(){

        $all_mehfil = DB::table('mehfils')->get();
        return view('mahafil.mehfil')->with(compact('all_mehfil'));
    }
    
    public function addMehfilPage(){
        
        // $countries = new Countries();
        // $all_mulk = $countries->all();

        $all_mulk = DB::table('countries_names')->get();

        return view('mahafil.add_mehfil')->with(compact(['all_mulk']));
    }

    public function insertMehfil(Request $request){
        
        $password = $request->password;
        if (empty($password)) {
            $password = "usman";
        }else{
            $password = $request->password;
        }

        $validator = Validator::make($request->all() , [
            'name' => 'required',
            'number' => 'required',
            'phone' => 'required',
            'mediaphone' => 'required',
            'email' => 'required|email',
            'address' => 'required',
            'city' => 'required',
            'country' => 'required',
            'coordinates' => 'required',

            'cc02_city' => 'required',
            'cc03_city' => 'required',
            'city_name_ur' => 'required',
        ]);
        
        if ($validator->passes()) {

            $mehfil_code = $request->number."_".$request->cc03_city."_".$request->country;
            $mehfil_code_admin = $request->country.$request->cc03_city.$request->number;

            $data_admin = array(
                        'admin_username' => strtolower($mehfil_code_admin) , 
                        'admin_email' => $request->email , 
                        'admin_password' => $password , 
                        'admin_access_level' => 'mehfil' , 
                        'admin_status' => 1 , 
            );

            DB::table('users')->insert($data_admin);
            $last = DB::table('users')->orderBy('id', 'desc')->first();

            $data = array(
                        'mehfil_code' => $mehfil_code, 
                        'mehfil_name' => $request->name, 
                        'mehfil_number' => $request->number, 
                        'mehfil_mobile' => $request->phone, 
                        'mehfil_media_cell_phone' => $request->mediaphone, 
                        'mehfil_email' => $request->email, 
                        'mehfil_address' => $request->address, 
                        'mehfil_city' => $request->cc03_city, 
                        'mehfil_country' => $request->country, 
                        'mehfil_coordinates' => $request->coordinates, 
                        'admin_id' => $last->id, 
                    );
    
            DB::table('mehfils')->insert($data);

            // to update city data in cities table
            $data_city = array(
                            'cc02_city' => $request->cc02_city , 
                            'cc03_city' => $request->cc03_city , 
                            'city_name_ur' => $request->city_name_ur , 
                        );

            DB::table('cities__names')->where('city_name_en' , $request->city)->update($data_city);


            $request->session()->flash('msg' , 'معلومات کا اندراج ہو چکا ہے.');
    
            return redirect('mahafil/add');

        }else{
            return redirect('mahafil/add')->withErrors($validator)->withInput();
        }
    }


    function editMehfil($id, Request $request){

        $all_mulk = DB::table('countries_names')->get();

        // $countries = new Countries();
        // $all_mulk = $countries->all();

        $single_mehfil = DB::table('mehfils')
                        ->join('users', 'users.id', '=', 'mehfils.admin_id')
                        ->select('mehfils.*' , 'users.admin_password')
                        ->where('mehfils.id', $id)
                        ->first();
        $get_city_dtl = DB::table('cities__names')->where('cc03_city', $single_mehfil->mehfil_city)->first();

        if (!$single_mehfil) {
            $request->session()->flash('msg' , 'آپکا مطلوبہ مواد موجود نہیں ہے۔');
            return redirect('mahafil');
        }else{
            return view('mahafil.edit_mehfil')->with(compact(['single_mehfil' , 'all_mulk' , 'get_city_dtl']));
        }
    
    }

    function updateMehfil($id , Request $request){

        // $password = $request->password;
        // if (empty($password)) {
        //     $password = "usman";
        // }else{
            $password = $request->password;
        // }

        $validator = Validator::make($request->all() , [
            'name' => 'required',
            'number' => 'required',
            'phone' => 'required',
            'mediaphone' => 'required',
            'email' => 'required|email',
            'address' => 'required',
            'city' => 'required',
            'country' => 'required',
            
            'cc02_city' => 'required',
            'cc03_city' => 'required',
            'city_name_ur' => 'required',
        ]);
        
        if ($validator->passes()) {


            $mehfil_code = $request->number."_".$request->cc03_city."_".$request->country;
            $mehfil_code_admin = $request->country.$request->cc03_city.$request->number;
            $data = array(
                        'mehfil_code' => $mehfil_code, 
                        'mehfil_name' => $request->name, 
                        'mehfil_number' => $request->number, 
                        'mehfil_mobile' => $request->phone, 
                        'mehfil_media_cell_phone' => $request->mediaphone, 
                        'mehfil_email' => $request->email, 
                        'mehfil_address' => $request->address, 
                        'mehfil_city' => $request->cc03_city, 
                        'mehfil_country' => $request->country, 
                        'mehfil_coordinates' => $request->coordinates, 
                    );
    
            DB::table('mehfils')->where('id' , $id)->update($data);

            // get the last id to update the data
            $getIdForUpdate = DB::table('mehfils')->where('id' , $id)->first();

            $get_user_data = DB::table('users')->where('id' , $getIdForUpdate->admin_id)->first();



            $password = $request->password;        
            if (empty($password)) {
                $password = $get_user_data->admin_password;
            }else{
                $password = $request->password;
            }

            $data_admin = array(
                        'admin_username' => strtolower($mehfil_code_admin) , 
                        'admin_email' => $request->email ,
                        'admin_password' => $password , 
                        'admin_access_level' => 'mehfil' , 
                        'admin_status' => 1 , 
            );

            DB::table('users')->where('id' , $getIdForUpdate->admin_id)->update($data_admin);


            // to update city data in cities table
            $data_city = array(
                            'cc02_city' => $request->cc02_city , 
                            'cc03_city' => $request->cc03_city , 
                            'city_name_ur' => $request->city_name_ur , 
                        );

            DB::table('cities__names')->where('city_name_en' , $request->city)->update($data_city);


            $request->session()->flash('msg' , 'معلومات میں تبدیلی کر دی گیئ ہے۔');    
            return redirect('mahafil');

        }else{
            return redirect('mahafil/edit/'.$id)->withErrors($validator)->withInput();
        }
    }
    

}
