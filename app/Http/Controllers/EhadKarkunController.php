<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\EhadKarkun;

use PragmaRX\Countries\Package\Countries;


class EhadKarkunController extends Controller{

    public function showData(){

        $all_ehd_karkun = DB::table('ehad_karkuns')->get();
        return view('ehad_karkun.ehad_karkun')->with(compact('all_ehd_karkun'));
    }
    
    public function addEhadKarkunPage(){
       
        // $countries = new Countries();
        // $all_mulk = $countries->all();
        $all_mulk = DB::table('countries_names')->get();
        
        // foreach ($all_mulk as $key => $value) {
        //     if (isset($value->admin) && isset($value->translations->urd->common) ) {
                
        //         $data = array(
        //                     'cc02' => $value->cca2 , 
        //                     'cc03' => $value->cca3 , 
        //                     'country_name_en' => $value->admin , 
        //                     'country_name_ur' => $value->translations->urd->common , 
        //                 );

        //         DB::table('countries_names')->insert($data);
                
        //         $id = DB::getPdo()->lastInsertId();;

                
        //         $all_cities = $countries->where('cca2', $value->cca2)
        //                                 ->first()
        //                                 ->hydrate('cities')
        //                                 ->cities;

        //         foreach ($all_cities as $key1 => $value1) {

        //             $data1 = array(
        //                         'country_id' => $id , 
        //                         'cc02' => $value->cca2 , 
        //                         'cc03' => $value->cca3 , 
        //                         'city_name_en' => $key1, 
        //                     );

        //             DB::table('cities__names')->insert($data1);
        //         } // end of foreach
        //     } // end of if
        // } // end of foreach
        // dd($all_mulk->PAK);

        return view('ehad_karkun.add_ehad_karkun')->with(compact('all_mulk'));
    }

    public function insertEhadKarkun(Request $request){
        
        $validator = Validator::make($request->all() , [
            'id_number' => 'required',
            'name' => 'required',
            'name_en' => 'required',
            'fname' => 'required',
            'phone' => 'required',
            'email' => 'email',
            'cnic' => 'required',
            'address' => 'required',
            'city' => 'required',
            'country' => 'required',
            'dob' => 'required',
            'doe' => 'required',
            
            'cc02_city' => 'required',
            'cc03_city' => 'required',
            'city_name_ur' => 'required',
        ]);
        
        if ($validator->passes()) {

            $data_admin = array(
                        'admin_username' => $request->name_en , 
                        'admin_email' => $request->email , 
                        'admin_password' => 'usman' , 
                        'admin_access_level' => 'ehdkarkun' , 
                        'admin_status' => 1 , 
            );

            DB::table('users')->insert($data_admin);
            $last = DB::table('users')->orderBy('id', 'desc')->first();

            // dd($last);

            // dd($last->id);
            $data = array(
                        'ekp_id_number' => $request->id_number, 
                        'ekp_name_en' => $request->name_en, 
                        'ekp_name' => $request->name, 
                        'ekp_fname' => $request->fname, 
                        'ekp_phone' => $request->phone, 
                        'ekp_email' => $request->email, 
                        'ekp_address' => $request->address, 
                        'ekp_city' => $request->cc03_city, 
                        'ekp_country' => $request->country, 
                        'ekp_dob' => $request->dob, 
                        'ekp_doe' => $request->doe, 
                        'ekp_marfat' => $request->marfat, 
                        'ekp_cnic' => $request->cnic, 
                        'admin_id' => $last->id, 
                    );

            DB::table('ehad_karkuns')->insert($data);

            // to update city data in cities table
            $data_city = array(
                            'cc02_city' => $request->cc02_city , 
                            'cc03_city' => $request->cc03_city , 
                            'city_name_ur' => $request->city_name_ur , 
                        );

            DB::table('cities__names')->where('city_name_en' , $request->city)->update($data_city);


            $request->session()->flash('msg' , 'معلومات کا اندراج ہو چکا ہے.');
    
            return redirect('ehadkarkun/add');

        }else{
            return redirect('ehadkarkun/add')->withErrors($validator)->withInput();
        }
    }


    function editEhadKarkun($id, Request $request){

        // $countries = new Countries();
        // $all_mulk = $countries->all();
        $all_mulk = DB::table('countries_names')->get();

        $single_ehdkarkun = DB::table('ehad_karkuns')->where('id', $id)->first();

        $get_city_dtl = DB::table('cities__names')->where('cc03_city', $single_ehdkarkun->ekp_city)->first();

        if (!$single_ehdkarkun) {
            $request->session()->flash('msg' , 'آپکا مطلوبہ مواد موجود نہیں ہے۔');
            return redirect('ehadkarkun');
        }else{
            return view('ehad_karkun.edit_ehad_karkun')->with(compact(['single_ehdkarkun' , 'all_mulk' , 'get_city_dtl']));
        }
    
    }

    function updateEhadKarkun($id , Request $request){



        $validator = Validator::make($request->all() , [
            'id_number' => 'required',
            'name_en' => 'required',
            'name' => 'required',
            'fname' => 'required',
            'phone' => 'required',
            'email' => 'required|email',
            'cnic' => 'required',
            'address' => 'required',
            'city' => 'required',
            'doe' => 'required',
            'dob' => 'required',
            'country' => 'required',

            'cc02_city' => 'required',
            'cc03_city' => 'required',
            'city_name_ur' => 'required',
        ]);
        
        if ($validator->passes()) {

            $data = array(
                        'ekp_id_number' => $request->id_number,
                        'ekp_name_en' => $request->name_en,  
                        'ekp_name' => $request->name, 
                        'ekp_fname' => $request->fname, 
                        'ekp_phone' => $request->phone, 
                        'ekp_email' => $request->email, 
                        'ekp_address' => $request->address, 
                        'ekp_city' => $request->cc03_city, 
                        'ekp_country' => $request->country, 
                        'ekp_dob' => $request->dob, 
                        'ekp_doe' => $request->doe, 
                        'ekp_marfat' => $request->marfat, 
                        'ekp_cnic' => $request->cnic, 
                    );
    
            DB::table('ehad_karkuns')->where('id' , $id)->update($data);

            // to update city data in cities table
            $data_city = array(
                            'cc02_city' => $request->cc02_city , 
                            'cc03_city' => $request->cc03_city , 
                            'city_name_ur' => $request->city_name_ur , 
                        );

            DB::table('cities__names')->where('city_name_en' , $request->city)->update($data_city);


            // get the last id to update the data
            $getIdForUpdate = DB::table('ehad_karkuns')->where('id' , $id)->first();

            $data_admin = array(
                        'admin_username' => $request->name_en , 
                        'admin_email' => $request->email , 
                        'admin_password' => 'usman' , 
                        'admin_access_level' => 'ehdkarkun' , 
                        'admin_status' => 1 , 
            );

            DB::table('users')->where('id' , $getIdForUpdate->admin_id)->update($data_admin);


            $request->session()->flash('msg' , 'معلومات میں تبدیلی کر دی گیئ ہے۔');    
            return redirect('ehadkarkun');

        }else{
            return redirect('ehadkarkun/edit/'.$id)->withErrors($validator)->withInput();
        }
    }
    
    public function addnew()
    {
        
      $all_mulk = DB::table('countries_names')->get();
      $all_mahafil = DB::table('mehfils')->get();
  
        return view('ehad_karkun.ehad_karkun_multiple')->with(compact(['all_mulk', 'all_mahafil']));
    }

    public function adddMultipleUser(Request $request){
  
        // single variables
        $mulk_name = $request->mulk_name;
        $city_name = $request->city_name;
        $city_cc02 = $request->city_cc02;
        $city_cc03 = $request->city_cc03;
        $city_name_ur = $request->city_name_ur;
      //  $mehfil_id = $request->mehfil_id;


        // to update city data in cities table
        $data_city = array(
            'cc02_city' => $city_cc02,
            'cc03_city' => $city_cc03,
            'city_name_ur' => $city_name_ur,
        );
        DB::table('cities__names')->where('city_name_en', $city_name)->update($data_city);


        // arrays
        $hazri_code = $request->hazri_code;
        $name_en = $request->name_en;
     //   $fname_en = $request->fname_en;
        $name_ur = $request->name_ur;
        $fname_ur = $request->fname_ur;
        $mobile_number = $request->mobile_number;
        $cnic = $request->cnic;
        $marfat = $request->marfat;
        $dob = $request->dob;
        $doe = $request->doe;


        for ($i = 0; $i < count($hazri_code); $i++) {

            $hazri_code_loop =  $hazri_code[$i];
            $name_en_loop =  $name_en[$i];
         //   $fname_en_loop =  $fname_en[$i];
            $name_ur_loop =  $name_ur[$i];
            $fname_ur_loop =  $fname_ur[$i];
            $mobile_number_loop =  $mobile_number[$i];
            $cnic_loop =  $cnic[$i];
            $marfat_loop =  $marfat[$i];
            $dob_loop =  $dob[$i];
            $doe_loop =  $doe[$i];


            $final_age_date = '';
            if ($dob_loop == '') {
                $final_age_date  = '';  
            }else{
                // $total_age_years = $dob_loop . " years";
                // $date = date_create(date('Y-m-d'));
                // date_sub($date, date_interval_create_from_date_string($total_age_years));
                // $final_age_date = date_format($date, "Y-m-d");                

                $final_age_date = $dob_loop."-01-01";                
            }


            $final_ehad_date = '';
            if ($doe_loop == ''){
                $final_ehad_date = '';
            }else{
                // $total_ehad_years = $doe_loop . " years";
                // $date1 = date_create(date('Y-m-d'));
                // date_sub($date1, date_interval_create_from_date_string($total_ehad_years));
                // $final_ehad_date = date_format($date1, "Y-m-d");

                $final_ehad_date = $doe_loop."-01-01";                
            }

            $email = "ehdkarkun" . $hazri_code_loop . "@gmail.com";

            $data_admin = array(
                'admin_username' => $hazri_code_loop,
                'admin_email' => $email,
                'admin_password' => 'usman',
                'admin_access_level' => 'ehdkarkun',
                'admin_status' => 1,
            );

            DB::table('users')->insert($data_admin);
            $last = DB::table('users')->orderBy('id', 'desc')->first();

            $data = array(
                'ekp_id_number' => $hazri_code_loop,
                'ekp_name' => $name_ur_loop,
                'ekp_name_en' => $name_en_loop,
                'ekp_fname' => $fname_ur_loop,
               // 'kp_fname_en' => $fname_en_loop,
                'ekp_phone' => $mobile_number_loop,
                'ekp_email' => $email,
                'ekp_city' => $city_cc03,
                'ekp_country' => $mulk_name,
                'ekp_cnic' => $cnic_loop,
                'ekp_dob' => $final_age_date,
                'ekp_doe' => $final_ehad_date,
                'ekp_marfat' => $marfat_loop,
              //  'mehfil_id' => $mehfil_id,
                'admin_id' => $last->id,
            );

            DB::table('ehad_karkuns')->insert($data);
        }

        echo 'معلومات کا اندراج ہو چکا ہے.';
    }

}
