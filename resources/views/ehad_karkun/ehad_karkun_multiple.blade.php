<!DOCTYPE html>
<html lang="en" class="no-js">
<!-- start: HEAD -->

<head>
    <title> تمام کارکن </title>

    @include('inc.head')
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/UrduEditor.css')}}" />
    <script src="{{asset('assets/js/jquery.UrduEditor.js')}}"></script>


    <style>
    #contacts_table_wrapper .select2-container {
        width: 100px;
    }

    #contacts_table_wrapper .select2-container span.select2-chosen {
        text-align: right;
    }

    table.table thead .sorting {
        text-align: right;
        background: url('assets/plugins/DataTables/media/css/images/sort_both.png') no-repeat center left;
    }

    table.table thead .sorting_asc {
        text-align: right;
        background: url('assets/plugins/DataTables/media/css/images/sort_asc.png') no-repeat center left;
    }

    table.table thead .sorting_desc {
        text-align: right;
        background: url('assets/plugins/DataTables/media/css/images/sort_desc.png') no-repeat center left;
    }
    </style>
</head>

<!-- end: HEAD -->

<!-- start: BODY -->

<body class="rtl">
    <!-- start: HEADER -->
    <div class="navbar navbar-inverse navbar-fixed-top">
        <!-- start: TOP NAVIGATION CONTAINER -->
        <div class="container">
            <div class="navbar-header">
                <!-- start: RESPONSIVE MENU TOGGLER -->
                <button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
                    <span class="clip-list-2"></span>
                </button>
                <!-- end: RESPONSIVE MENU TOGGLER -->
                <!-- end: LOGO -->
            </div>
        </div>
        <!-- end: TOP NAVIGATION CONTAINER -->
    </div>
    <!-- end: HEADER -->

    <!-- start: MAIN CONTAINER -->
    <div class="main-container">
        <div class="navbar-content">
            <!-- start: SIDEBAR -->
            @include('inc.sidebar')
            <!-- end: SIDEBAR -->
        </div>
        <!-- start: PAGE -->
        <div class="main-content">

            <div class="container" style="min-height: 760px;">
                <!-- start: PAGE HEADER -->
                <div class="row">
                    <div class="col-sm-12">
                        <!-- start: PAGE TITLE & BREADCRUMB -->
                        <ol class="breadcrumb">
                            <li>
                                <!--- class="clip-file" -->
                                <i class=""></i>

                                تمام کارکنان

                            </li>
                            <li class="active">
                            </li>
                        </ol>

                        <div class="page-header">
                            <h1>
                                تمام کارکنان
                                <small style="font-size:25px;margin-right:30px;"></small>
                            </h1>
                        </div>
                        <!-- end: PAGE TITLE & BREADCRUMB -->
                    </div>
                </div>
                <!-- end: PAGE HEADER -->
                <!-- start: PAGE CONTENT -->

                <div class="row">
                    <div class="col-md-12">
                        <!-- start: DYNAMIC TABLE PANEL -->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <i class="fa fa-external-link-square"></i>
                                تمام کارکنان
                            </div>

                            @csrf
                            <div class="panel-body">

                                <div class="row">
                                    <div class="col-md-4">
                                        <div id="mulk_name_div">

                                            <select id="mulk_name" class="select2" style="width: 100%;">
                                                <option value=""> ملک کا نام </option>
                                                @foreach($all_mulk as $single_mulk)
                                                <option value="{{$single_mulk->cc02}}">{{$single_mulk->country_name_ur}}
                                                </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div id="city_name_div">
                                            <select class="select2" style="width: 100%;" name="city" id="urwausman">
                                                <option value=""> شہر کا نام </option>
                                            </select>
                                        </div>
                                    </div>


                                    <div class="col-md-4">
                                        <div id="mehfilname_div">
                                            <select class="select2" id="mehfil_id" style="width: 100%;">
                                                <option value=""> محفل کا نام </option>
                                                @foreach($all_mahafil as $single_mehfil)
                                                <option value="{{$single_mehfil->id}}">{{$single_mehfil->mehfil_name}}
                                                </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row" id="add_city_details">
                                    <div class="col-md-4 @if($errors->first('cc02_city')) has-error @endif">
                                        <div class='form-group'>

                                            <label class="control-label">
                                                <span class="symbol required"> دو حرفی نام لکھیں </span>
                                            </label>

                                            <div>
                                                <input class='form-control' name='cc02_city' id='cc02_city' type='text'
                                                    autocomplete="off" value="{{old('cc02_city')}}" required>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="col-md-4 @if($errors->first('cc03_city')) has-error @endif">
                                        <div class='form-group'>
                                            <label class="control-label">
                                                <span class="symbol required"> تین حرفی نام لکھیں </span>
                                            </label>

                                            <div>
                                                <input class='form-control' name='cc03_city' id='cc03_city' type='text'
                                                    autocomplete="off" value="{{old('cc03_city')}}" required>
                                            </div>

                                        </div>

                                    </div>

                                    <div class="col-md-4 @if($errors->first('city_name_ur')) has-error @endif">
                                        <div class='form-group'>
                                            <label class="control-label">
                                                <span class="symbol required"> اردو میں نام لکھیں </span>
                                            </label>
                                            <div>
                                                <input class='form-control' name='city_name_ur' id='city_name_ur'
                                                    type='text' autocomplete="off" value="{{old('city_name_ur')}}"
                                                    required>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <label> کتنے کارکنان شامل کرنا چاھتے ہیں؟ </label>
                                        <input type="number" id="karkunan_number" class="form-control">
                                    </div>

                                    <div class="col-md-6">
                                        <br>
                                        <button onclick="add_rows()" class="btn btn-primary">لائنیں شامل کریں</button>
                                    </div>
                                </div>



                                <div class="table-responsive">
                                    <table class="table-bordered" width="100%">
                                        <thead></thead>
                                    </table>
                                    <br><br>
                                    <table
                                        class="editabletable table table-striped table-bordered table-hover table-full-width"
                                        id="contacts_table">

                                        <tr>
                                            <td>حاضری کوڈ </td>
                                            <td> نام انگریزی میں </td>
                                            <td> ولدیت انگریزی میں </td>
                                            <td> نام اردو میں </td>
                                            <td> ولدیت اردو میں </td>
                                            <td> موبائل نمبر </td>
                                            <td> شناختی کارڈ نمبر </td>
                                            <td>معرفت </td>
                                            <td> عمر </td>
                                            <td> زمانہ عہد </td>
                                            <td></td>
                                        </tr>
                                        <tbody class="mytbody">
                                            <tr>
                                                <td>
                                                    <input type='number' class="textea form-control hazri_code" />
                                                </td>
                                                <td>
                                                    <input class="textea form-control name_en" />
                                                </td>
                                                <td>
                                                    <input class="textea form-control fname_en" />
                                                </td>
                                                <td>
                                                    <input class="textea form-control name_ur" />
                                                </td>
                                                <td>
                                                    <input class="textea form-control fname_ur" />
                                                </td>
                                                <td>
                                                    <input type='text' class="textea form-control mobile_number" />
                                                </td>
                                                <td>
                                                    <input type='number' class="textea form-control cnic" />
                                                </td>
                                                <td>
                                                    <input class="textea form-control marfat" />
                                                </td>
                                                <td>
                                                    <input type='number' class="textea form-control dob" />
                                                </td>
                                                <td>
                                                    <input type='number' class="textea form-control doe" />
                                                </td>
                                                <td></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <button class="btn btn-primary" onclick="insert_data()">ڈیٹا داخل کریں </button>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <!-- end: DYNAMIC TABLE PANEL -->
                </div>
            </div>

            <!-- end: PAGE CONTENT-->
        </div>
    </div>
    <!-- end: PAGE -->
    </div>
    <!-- end: MAIN CONTAINER -->























    <!-- start: FOOTER & scripts -->
    @include('inc.footer')

    <script type="text/javascript">
    $('.select2').select2();

    $('#mulk_name').on('change', function(e) {
        e.preventDefault();

        var mulkname = $("#mulk_name").val();


        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('[name="_token"]').val()
            }
        });

        /* Submit form data using ajax*/
        $.ajax({
            url: "{{url('karkun/cityname_fetch')}}",
            method: 'post',
            data: {
                mulkname: mulkname,
            },
            success: function(response) {

                $("#urwausman").html(response);
            }
        });
    });

    $('#urwausman').on('change', function(e) {
        e.preventDefault();

        var cityname = $("#urwausman").val();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('[name="_token"]').val()
            }
        });

        /* Submit form data using ajax*/
        $.ajax({
            url: "{{url('karkun/cityname_fetch_dtl')}}",
            method: 'post',
            dataType: 'JSON',
            data: {
                cityname: cityname,
            },
            success: function(response) {
                $("#cc02_city").val(response.cc02);
                $("#cc03_city").val(response.cc03);
                $("#city_name_ur").val(response.city_name_ur);
            }
        });
    });

    function add_rows() {
        var rows = $("#karkunan_number").val();

        var data = "";

        for (i = 0; i < rows; i++) {

            data += "<tr>";
            data += "<td><input type='number' class='textea form-control hazri_code' /></td>";
            data += "<td><input class='textea form-control name_en' /></div></td>";
            data += "<td><input class='textea form-control fname_en' /></div></td>";
            data += "<td><input class='textea form-control name_ur' /></div></td>";
            data += "<td><input class='textea form-control fname_ur' /></div></td>";
            data += "<td><input type='number' class='textea form-control mobile_number' /></div></td>";
            data += "<td><input type='number' class='textea form-control cnic' /></div></td>";
            data += "<td><input class='textea form-control marfat' /></div></td>";
            data += "<td><input type='number' class='textea form-control dob' /></div></td>";
            data += "<td><input type='number' class='textea form-control doe'/></div></td>";
            data += "<td><button onclick='remove_this_row(this)' class='btn btn-danger btn-sm'>-</button></td>"
            data += "</tr>";
        }

        $(".mytbody").append(data)
    }

$(document).on("keyup" , '.textea' , function() {
    $(this).css("border","white");
})
 
    function insert_data() {

        var mulk_name = $("#mulk_name :selected").val();
        var city_name = $("#urwausman :selected").val();
        var city_cc02 = $("#cc02_city").val();
        var city_cc03 = $("#cc03_city").val();
        var city_name_ur = $("#city_name_ur").val();
        var mehfil_id = $("#mehfil_id :selected").val();

        if (city_name == '' || city_cc02 == '' || city_cc03 == '' || mehfil_id == '') {
            // alert('برائے مہربانی تمام ڈیٹا داخل کریں۔ شکریہ۔');
            //return false;
        }

        hazri_code = [];
        name_en = [];
        fname_en = [];
        name_ur = [];
        fname_ur = [];
        mobile_number = [];
        cnic = [];
        marfat = [];
        dob = [];
        doe = [];

        var ok = 0;
        $(".hazri_code").each(function() {
            if ($(this).val().trim() == '') {
                $(this).css("border", "red solid 2px");
                $(this).focus();
                ok = 1;
                return false;
            }
            hazri_code.push($(this).val().trim());
        });

        $(".name_en").each(function() {
            // if ($(this).val().trim() == '') {
            //     $(this).css("border", "red solid 2px");
            //     // alert('برائے مہربانی تمام اندراجات کو پر کریں۔ ');
            //     ok = 1;
            //     $(this).focus();
            //     return false;
            // }

            name_en.push($(this).val().trim());
        });

        $(".fname_en").each(function() {
            // if ($(this).val().trim() == '') {
            //     $(this).css("border", "red solid 2px");
            //     // alert('برائے مہربانی تمام اندراجات کو پر کریں۔ ');
            //     $(this).focus();
            //     ok = 1;
            //     return false;
            // }
            fname_en.push($(this).val().trim());
        });

        $(".name_ur").each(function() {
            if ($(this).val().trim() == '') {
                $(this).css("border", "red solid 2px");
                // alert('برائے مہربانی تمام اندراجات کو پر کریں۔ ');
                $(this).focus();
                ok = 1;
                return false;
            }
            name_ur.push($(this).val().trim());
        });

        $(".fname_ur").each(function() {
            if ($(this).val().trim() == '') {
                $(this).css("border", "red solid 2px");
                // alert('برائے مہربانی تمام اندراجات کو پر کریں۔ ');
                $(this).focus();
                ok = 1;
                return false;
            }
            fname_ur.push($(this).val().trim());
        });

        $(".mobile_number").each(function() {
            if ($(this).val().trim() == '') {
                $(this).css("border", "red solid 2px");
                // alert('برائے مہربانی تمام اندراجات کو پر کریں۔ ');
                $(this).focus();
                ok = 1;
                return false;
            }
            mobile_number.push($(this).val().trim());
        });

        $(".cnic").each(function() {
            // if ($(this).val().trim() == '') {
            //     $(this).css("border", "red solid 2px");
            //     // alert('برائے مہربانی تمام اندراجات کو پر کریں۔ ');
            //     $(this).focus();
            //     ok = 1;
            //     return false;
            // }
            cnic.push($(this).val().trim());
        });

        $(".marfat").each(function() {
            // if ($(this).val().trim() == '') {
            //     $(this).css("border", "red solid 2px");
            //     // alert('برائے مہربانی تمام اندراجات کو پر کریں۔ ');
            //     $(this).focus();
            //     ok = 1;
            //     return false;
            // }
            marfat.push($(this).val().trim());
        });

        $(".dob").each(function() {
            // if ($(this).val().trim() == '') {
            //     $(this).css("border", "red solid 2px");
            //     // alert('برائے مہربانی تمام اندراجات کو پر کریں۔ ');
            //     $(this).focus();
            //     ok = 1;
            //     return false;
            // }
            dob.push($(this).val().trim());
        });

        $(".doe").each(function() {
            // if ($(this).val().trim() == '') {
            //     $(this).css("border", "red solid 2px");
            //     // alert('برائے مہربانی تمام اندراجات کو پر کریں۔ ');
            //     $(this).focus();
            //     ok = 1;
            //     return false;
            // }
            doe.push($(this).val().trim());
        });

        if (ok == 1) {
            alert('برائے مہربانی تمام اندراجات کو پر کریں۔ ');
            return false;
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('[name="_token"]').val()
            }
        });

        /* Submit form data using ajax*/
        $.ajax({
            url: "{{url('ehadkarkun/add/multiple')}}",
            method: 'post',
            // dataType: 'JSON',
            data: {
                // single variables
                mulk_name: mulk_name,
                city_name: city_name,
                city_cc02: city_cc02,
                city_cc03: city_cc03,
                city_name_ur: city_name_ur,
                mehfil_id: mehfil_id,
                // arrays 
                hazri_code: hazri_code,
                name_en: name_en,
                fname_en: fname_en,
                name_ur: name_ur,
                fname_ur: fname_ur,
                mobile_number: mobile_number,
                cnic: cnic,
                marfat: marfat,
                dob: dob,
                doe: doe,
            },
            success: function(response) {
                alert(response);
                window.location.reload();
            }


        });

    }

    $(document).on("keydown", "input", function(e) {
            // $(document).find("input").keydown(function(e){

            if (e.which == 37) { // right arrow
                $(this).closest('td').next().find('input').focus();

            } else if (e.which == 39) { // left arrow
                $(this).closest('td').prev().find('input').focus();

            } else if (e.which == 40) { // down arrow
                $(this).closest('tr').next().find('td:eq(' + $(this).closest('td').index() + ')').find('input').focus();

            } else if (e.which == 38) { // up arrow
                $(this).closest('tr').prev().find('td:eq(' + $(this).closest('td').index() + ')').find('input').focus();
            }
        });

    function remove_this_row(ths) {
        // $(ths).fadeOut();
        $(ths).closest('tr').remove();
    }

    $(document).ready(function() {

        jQuery.browser = {};
        (function() {
            jQuery.browser.msie = false;
            jQuery.browser.version = 0;
            if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
                jQuery.browser.msie = true;
                jQuery.browser.version = RegExp.$1;
            }
        })();

        $(document).find(".name_ur").UrduEditor();
        $(document).find("#mulk_name_div .select2-input").UrduEditor();
        $(document).find("#city_name_div .select2-input").UrduEditor();
        $(document).find("#mehfilname_div .select2-input").UrduEditor();
    });
    </script>


    <script>
        $('.textea').on('paste', function(e) {
            var $this = $(this);
            $.each(e.originalEvent.clipboardData.items, function(i, v) {

                // console.log(v);
                if (v.type === 'text/plain') {
                    v.getAsString(function(text) {
                        var x = $this.closest('td').index(),
                            y = $this.closest('tr').index(),
                            obj = {};
                        // console.log(x);
                        // console.log(y);
                        text = text.trim('\r\n');
                        // console.log(text);
                        $.each(text.split('\r\n'), function(i2, v2) {
                            // console.log(v2);
                            $.each(v2.split('\t'), function(i3, v3) {
                                var row = y + i2,
                                    col = x + i3;
                                obj['cell-' + row + '-' + col] = v3;
                                // console.log(v3);
                                $this.closest('tbody').find('tr:eq(' + row + ') td:eq(' + col + ') input').val(v3);
                            });
                        });
                        // $('div').text(JSON.stringify(obj));
                    });
                }
            });
            return false;

        });
    </script>



</body>
<!-- end: BODY -->

</html>