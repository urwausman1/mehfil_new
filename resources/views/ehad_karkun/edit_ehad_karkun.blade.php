<!DOCTYPE html>
<html lang="en" class="no-js">
    <!-- start: HEAD -->
    <head>
        <title>  عہد کارکن میں تبدیلی کریں  </title>

        @include('inc.head')
        <link rel="stylesheet" type="text/css" href="{{asset('assets/css/UrduEditor.css')}}" />  
        <script src="{{asset('assets/js/jquery.UrduEditor.js')}}"></script> 


        <style>
        .select2{
            width: 100% !important;
        }
        #contacts_table_wrapper .select2-container{
            width: 100px;
        }
        #contacts_table_wrapper .select2-container span.select2-chosen{
            text-align: right;
        }
        table.table thead .sorting{
            text-align: right;
            background: url('assets/plugins/DataTables/media/css/images/sort_both.png') no-repeat center left;
        }
        table.table thead .sorting_asc{
            text-align: right;
            background: url('assets/plugins/DataTables/media/css/images/sort_asc.png') no-repeat center left;
        }
        table.table thead .sorting_desc{
            text-align: right;
            background: url('assets/plugins/DataTables/media/css/images/sort_desc.png') no-repeat center left;
        }
        </style>
    </head>

    <!-- end: HEAD -->

    <!-- start: BODY -->
    <body class="rtl">    
        <!-- start: HEADER -->
        <div class="navbar navbar-inverse navbar-fixed-top">
            <!-- start: TOP NAVIGATION CONTAINER -->
            <div class="container">
                <div class="navbar-header">
                    <!-- start: RESPONSIVE MENU TOGGLER -->
                    <button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
                        <span class="clip-list-2"></span>
                    </button>
                    <!-- end: RESPONSIVE MENU TOGGLER -->
                    <!-- end: LOGO -->
                </div>
            </div>
            <!-- end: TOP NAVIGATION CONTAINER -->
        </div>
        <!-- end: HEADER -->

        <!-- start: MAIN CONTAINER -->
        <div class="main-container">
            <div class="navbar-content">
                <!-- start: SIDEBAR -->
                @include('inc.sidebar')
                <!-- end: SIDEBAR -->
            </div>
            <!-- start: PAGE -->
            <div class="main-content">

                <div class="container" style="min-height: 760px;">
                    <!-- start: PAGE HEADER -->
                    <div class="row">
                        <div class="col-sm-12">
                            <!-- start: PAGE TITLE & BREADCRUMB -->
                            <ol class="breadcrumb">
                                <li>
                                    <!--- class="clip-file" -->
                                    <i class=""></i>
                                        تبدیلی  معلومات برائے  عہد کارکن

                                </li>
                                <li class="active">
                                </li>
                                <!-- <li class="pull-left" style="margin-left: 30px;">
                                    <a href="admin/attendance">حاضری</a>
                                </li> -->

                            <!--<li class="search-box">
                                    <form class="sidebar-search">
                                        <div class="form-group">
                                            <input type="text" placeholder="Start Searching...">
                                            <button class="submit">
                                                <i class="clip-search-3"></i>
                                            </button>
                                        </div>
                                    </form>
                                </li>-->
                            </ol>

                            <div class="page-header">
                                <h1>
                                        تبدیلی  معلومات برائے  عہد کارکن

                                    <small style="font-size:25px;margin-right:30px;"></small>
                                </h1>
                            </div>
                            <!-- end: PAGE TITLE & BREADCRUMB -->
                        </div>
                    </div>
                    <!-- end: PAGE HEADER -->
                    <!-- start: PAGE CONTENT -->

                        @if(Session::has('msg'))
                            <div class="col-md-12">
                                <div class="alert alert-success">{{Session::get('msg')}}</div>
                            </div>
                        @endif

                        <div class="row">

                            <div class="col-md-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <i class="fa fa-external-link-square"></i>
                                        تبدیلی  معلومات برائے  عہد کارکن

                                    </div>
                                     
                                    <div class="panel-body">
                                    
                                    <form method="post" action="{{url('ehadkarkun/edit/'.$single_ehdkarkun->id)}}"> 
                                        @csrf 
                                        <div class="row">


                                          <div class="col-sm-2">
                                            <!-- has-error -->
                                            <div class="form-group @if($errors->any()) has-error @endif">
                                                                
                                                <label class="control-label"><span class="symbol required" >    آئی ڈی نمبر 
                                                </span></label>                                                
                                                <div>
                                                    <input type="number" autocomplete="off" name="id_number" id="id_number" class="form-control" value="{{old('id_number' , $single_ehdkarkun->ekp_id_number)}}" >
                                                    
                                                    @if($errors->any())
                                                    <p>@if($errors->first('id_number')) {{'نآئی ڈی نمبر کا اندراج  ضروری ہے۔'}} @endif </p>
                                                    @endif
                                                </div>
                                            </div>
                                          </div>

                                          <div class="col-sm-3">
                                            <!-- has-error -->
                                            <div class="form-group @if($errors->any()) has-error @endif">
                                                <label class="control-label"><span class="symbol required" > نام   انگریزی میں  </span></label>
                                                <div>
                                                    <input type="text" autocomplete="off" name="name_en" id="name_en" class="form-control" value="{{old('name_en' , $single_ehdkarkun->ekp_name_en)}}" >
                                                    @if($errors->any())
                                                        <p>@if($errors->first('name_en')) {{'نام کا اندراج  ضروری ہے۔'}} @endif </p>
                                                    @endif
                                                </div>
                                            </div>
                                          </div>


                                          <div class="col-sm-3">
                                            <!-- has-error -->
                                            <div class="form-group @if($errors->any()) has-error @endif">
                                                <label class="control-label"><span class="symbol required" > نام </span></label>
                                                <div>
                                                    <input type="text" autocomplete="off" name="name" id="name" class="form-control" value="{{old('name' , $single_ehdkarkun->ekp_name)}}" >
                                                    @if($errors->any())
                                                        <p>@if($errors->first('name')) {{'نام کا اندراج  ضروری ہے۔'}} @endif </p>
                                                    @endif
                                                </div>
                                            </div>
                                          </div>

                                          <div class="col-sm-4">
                                            <!-- has-error -->
                                            <div class="form-group @if($errors->any()) has-error @endif">
                                                <label class="control-label"><span class="symbol required" > ولدیت</span></label>
                                                <div>
                                                    <input type="text" autocomplete="off" name="fname" id="fname" class="form-control" value="{{old('fname' , $single_ehdkarkun->ekp_fname)}}" >
                                                    @if($errors->any())
                                                        <p>@if($errors->first('fname')) {{'والد کا نام ضروری ہے۔'}} @endif </p>
                                                    @endif
                                                </div>
                                            </div>
                                          </div>


                                        </div>

                                        <div class="row">
                                          <div class="col-sm-6">
                                            <!-- has-error -->
                                            <div class="form-group @if($errors->any()) has-error @endif">
                                                <label class="control-label"><span class="symbol required" > رابطہ نمبر </span></label>
                                                <div>
                                                    <input type="number" autocomplete="off" name="phone" id="phone" class="form-control" value="{{old('phone' , $single_ehdkarkun->ekp_phone)}}" >
                                                    @if($errors->any())
                                                        <p>@if($errors->first('phone')) {{'فون کا اندراج ضروری ہے۔'}} @endif </p>
                                                    @endif
                                                </div>
                                            </div>
                                          </div>

                                          <div class="col-sm-6">
                                            <!-- has-error -->
                                            <div class="form-group @if($errors->any()) has-error @endif">
                                                <label class="control-label"><span class="symbol required" > ایمیل </span></label>
                                                <div>
                                                    <input type="text" autocomplete="off" name="email" id="email" class="form-control" value="{{old('email' , $single_ehdkarkun->ekp_email)}}" >
                                                    @if($errors->any())
                                                        <p>@if($errors->first('email')) {{'ایمیل  کا اندراج ضروری ہے۔'}} @endif </p>
                                                    @endif
                                                </div>
                                            </div>
                                          </div>
                                        </div>

                                        <div class="row">
                                          <div class="col-sm-6">
                                            <!-- has-error -->
                                            <div class="form-group @if($errors->any()) has-error @endif">
                                                <label class="control-label"><span class="symbol required" > شناختی کارڈ نمبر</span></label>
                                                <div>
                                                    <input type="number" autocomplete="off" name="cnic" id="cnic" class="form-control" value="{{old('cnic' , $single_ehdkarkun->ekp_cnic)}}" >
                                                    @if($errors->any())
                                                        <p>@if($errors->first('cnic')) {{'شناختی  کارڈکا اندراج ضروری ہے۔'}} @endif </p>
                                                    @endif
                                                </div>
                                            </div>
                                          </div>

                                          <div class="col-sm-6">
                                            <!-- has-error -->
                                            <div class="form-group @if($errors->any()) has-error @endif">
                                                <label class="control-label"><span class="symbol required" > مکمل پتہ </span></label>
                                                <div>
                                                    <input type="text" autocomplete="off" name="address" id="address" class="form-control" value="{{old('address' , $single_ehdkarkun->ekp_address)}}" >
                                                    @if($errors->any())
                                                        <p>@if($errors->first('address')) {{'مکمل پتہ کا اندراج ضروری ہے۔'}} @endif </p>
                                                    @endif
                                                </div>
                                            </div>
                                          </div>
                                        </div>


                                        <div class="row">
                                          <div class="col-sm-6">
                                            <!-- has-error -->
                                            <div class="form-group @if($errors->any()) has-error @endif">
                                                <label class="control-label"><span class="symbol required" >
                                                
                                                    تاریخ پیدائش

                                                </span></label>
                                                <div>
                                                    <input type="date" autocomplete="off" name="dob" id="dob" class="form-control" value="{{old('dob'  , $single_ehdkarkun->ekp_dob)}}" >
                                                    @if($errors->any())
                                                        <p>@if($errors->first('dob')) {{' ترایخ پیدائش کا اندراج ضروری ہے۔  '}} @endif </p>
                                                    @endif
                                                </div>
                                            </div>
                                          </div>

                                          <div class="col-sm-6">
                                            <!-- has-error -->
                                            <div class="form-group @if($errors->any()) has-error @endif">
                                                <label class="control-label"><span class="symbol required" >  تاریخ عہد  </span></label>
                                                <div>
                                                    <input type="date" autocomplete="off" name="doe" id="doe" class="form-control" value="{{old('doe' , $single_ehdkarkun->ekp_doe)}}" >
                                                    @if($errors->any())
                                                        <p>@if($errors->first('doe')) {{'مکمل پتہ کا اندراج ضروری ہے۔'}} @endif </p>
                                                    @endif
                                                </div>
                                            </div>
                                          </div>
                                        </div>



                                        <div class="row">
                                          

                                          <div class="col-sm-4">
                                            <!-- has-error -->
                                            <div id="mulk_name_div" class="form-group @if($errors->any()) has-error @endif">
                                                <label style="margin-bottom: 15px !important;" class="control-label"><span class="symbol required" > ملک</span></label>
                                                <div>
                                                    <select class="select2" name="country" id="mulk_name">
                                                        <option value="{{old('country' , $single_ehdkarkun->ekp_country)}}">{{old('country' , $single_ehdkarkun->ekp_country)}}</option>


                                                        @foreach ($all_mulk as $value)
                                                                
                                                                <option value="{{$value->cc02}}">
                                                                    {{$value->country_name_ur}}
                                                                </option>

                                                        @endforeach

                                                    </select>
                                                    @if($errors->any())
                                                        <p>@if($errors->first('country')) {{'ملک کا اندراج ضروری ہے۔'}} @endif </p>
                                                    @endif
                                                </div>
                                            </div>
                                          </div>


                                          <div class="col-sm-4">
                                            <!-- has-error -->
                                            <div class="form-group @if($errors->any()) has-error @endif">
                                                <label style="margin-bottom: 15px !important;" class="control-label"><span class="symbol required" > شہر</span></label>
                                                <div>
                                                    <select class="select2" name="city" id="urwausman">
                                                        <option value="{{old('city' , $single_ehdkarkun->ekp_city)}}">{{old('city' , $single_ehdkarkun->ekp_city)}}</option>
                                                        
                                                    </select>
                                                    @if($errors->any())
                                                        <p>@if($errors->first('city')) {{'شہر کا اندراج ضروری ہے۔'}} @endif </p>
                                                    @endif
                                                </div>
                                            </div>
                                          </div>

                                          <div class="col-sm-4">
                                            <!-- has-error -->
                                            <div class="form-group @if($errors->any()) has-error @endif">
                                                <label class="control-label"><span class="" > معرفت </span></label>

                                                <div>
                                                    <input type="text" autocomplete="off" name="marfat" id="marfat" class="form-control" value="{{old('marfat' , $single_ehdkarkun->ekp_marfat)}}" >
                                                </div>

                                            </div>
                                          </div>

                                        </div>

                                        <div class="row" id="add_city_details">
                                            
                                            <div class='col-md-4'>
                                                <div class='form-group'>
                                                    <label>  دو حرفی نام لکھیں   </label>
                                                    <input class='form-control' name='cc02_city' id='cc02_city' type='text' value='{{$get_city_dtl->cc02_city}}' >
                                                </div>        
                                            </div>

                                            <div class='col-md-4'>
                                                <div class='form-group'>
                                                    <label>  تین  حرفی نام لکھیں    </label> 
                                                    <input class='form-control' name='cc03_city' id='cc03_city' type='text' value='{{$get_city_dtl->cc03_city}}' >
                                                </div>
                                            </div>

                                            <div class='col-md-4'>
                                                <div class='form-group'>
                                                    <label>  اردو میں نام لکھیں    </label>    
                                                    <input class='form-control' name='city_name_ur' id='city_name_ur' type='text' value='{{$get_city_dtl->city_name_ur}}' >
                                                </div>        
                                            </div>

                                        </div>


                                        <div class="row">
                                          <div class="col-sm-6">
                                            <!-- has-error -->
                                            <div class="form-group">
                                                <input type="submit" class="btn btn-sm btn-primary" name="submit" value="اندراج کریں">
                                            </div>

                                          </div>
                                        </div>

                                    </form>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

<!-- end: PAGE CONTENT-->
</div>
</div>
            <!-- end: PAGE -->
</div>
        <!-- end: MAIN CONTAINER -->



        <!-- start: FOOTER & scripts -->
            @include('inc.footer')
        
         <script type="text/javascript">
            $(document).ready(function() {
                $('#example-getting-started').multiselect();
                $('.select2').select2();
            });
            $(document).ready(function(){
                $(".btn_submit_P").click(function(){
                    $(".HazriKarkoonatten").val("P");
                });

                $(".btn_submit_L").click(function(){
                    $(".HazriKarkoonatten").val("L");
                });             

                $(".btn_submit_M").click(function(){
                    $(".HazriKarkoonatten").val("M");
                });
        });
        </script>
    </body>
    <!-- end: BODY -->
<script>
    $(document).ready(function() {
        $(document).find("#id_number").focus();
    });

$('#mulk_name').on('change' , function(e){
   e.preventDefault();

   var mulkname = $("#mulk_name").val();


   $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('[name="_token"]').val()
      }
  });
   
   /* Submit form data using ajax*/
   $.ajax({
      url: "{{url('karkun/cityname_fetch')}}",
      method: 'post',
      data: {
        mulkname : mulkname,
      },
      success: function(response){
         //------------------------

         $("#urwausman").html(response);
         //--------------------------
      }});
});


    $('#urwausman').on('change' , function(e){
        e.preventDefault();

        var cityname = $("#urwausman").val();


        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('[name="_token"]').val()
            }
        });

        /* Submit form data using ajax*/
        $.ajax({
            url: "{{url('karkun/cityname_fetch_dtl')}}",
            method: 'post',
            dataType: 'JSON',
            data: {
                cityname : cityname,
            },
            success: function(response){
                // $("#add_city_details").html(response);
                $("#cc02_city").val(response.cc02);
                $("#cc03_city").val(response.cc03);
                $("#city_name_ur").val(response.city_name_ur);
            }});
    });



</script>

<script>
$(document).ready(function () {

jQuery.browser = {};
(function () {
    jQuery.browser.msie = false;
    jQuery.browser.version = 0;
    if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
        jQuery.browser.msie = true;
        jQuery.browser.version = RegExp.$1;
    }
})();

    $(document).find("#name").UrduEditor();   
    $(document).find("#fname").UrduEditor();   
    $(document).find("#address").UrduEditor();   
    $(document).find("#marfat").UrduEditor();   
    $(document).find("#mulk_name_div .select2-input").UrduEditor();   
    // $(document).find("#mehfilname_div .select2-input").UrduEditor();   
});
</script>

</html>