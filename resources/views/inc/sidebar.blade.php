                <div class="main-navigation navbar-collapse collapse">
                    <!-- start: MAIN MENU TOGGLER BUTTON -->
                    <div class="navigation-toggler">
                        <i class="clip-chevron-left"></i>
                        <i class="clip-chevron-right"></i>
                    </div>
                    <!-- end: MAIN MENU TOGGLER BUTTON -->

                    <!-- start: MAIN NAVIGATION MENU -->
                    <ul class="main-navigation-menu">
                        <li class="active">
                            <a href="{{url('dashboard')}}"><i class="clip-home-3"></i>
                                <span class="title"> خو ش  آمد ید </span><span class="selected"></span>
                            </a>
                        </li>
                        
                        <li class="">
                            <a href="#"><i class="clip-home-3"></i>
                                <span class="title"> نو ٹس بورڈ </span><span class=""></span>
                            </a>
                        </li>


                        <li class="">
                            <a href="javascript:void(0)">
                                <i class="clip-pencil"></i>
                                <span class="title">   رپورٹنگ  </span><i class="icon-arrow"></i>
                                <span class="selected"></span>
                            </a>
                            <ul class="sub-menu" style="display: none;">
                                <li>
                                    <a href="{{url('reports/karkun')}}">
                                        <span class="title">   کارکنان    </span>
                                    </a>
                                </li>

                                <li>
                                    <a href="{{url('reports/hazri/karkun')}}">
                                        <span class="title">   حاضری کارکنان   </span>
                                    </a>
                                </li>

                                <li>
                                    <a href="{{url('reports/hazri/mehfil')}}">
                                        <span class="title">   حاضری  محافل   </span>
                                    </a>
                                </li>

                                <li>
                                    <a href="{{url('reports/hazri/mehfil/monthly')}}">
                                        <span class="title">     ماہانہ رپورٹ     </span>
                                    </a>
                                </li>


                            </ul>
                        </li>
                    @if(session('user_role') == 'ehdkarkun' || session('user_role') == 'superuser')
                    @endif

                    <li class="">
                        <a href="javascript:void(0)">
                            <i class="clip-pencil"></i>
                            <span class="title">عہد رجسٹر  </span><i class="icon-arrow"></i>
                            <span class="selected"></span>
                        </a>
                        <ul class="sub-menu" style="display: none;">
                            <li>
                                <a href="{{url('ehadregister')}}">
                                    <span class="title"> عہد رجسٹر  مکمل  </span>
                                </a>
                            </li>

                            <li>
                                <a href="{{url('ehadregister/add')}}">
                                    <span class="title"> نیا اضافہ  </span>
                                </a>
                            </li>

                        </ul>
                    </li>


                    <li class="">
                        <a href="javascript:void(0)">
                            <i class="fa fa-group"></i>
                            <span class="title">   کارکنان    </span><i class="icon-arrow"></i>
                            <span class="selected"></span>
                        </a>
                        <ul class="sub-menu" style="display: none;">
                            <li>
                                <a href="{{url('karkun/roster')}}">
                                    <span class="title">  کارکن روسٹر   </span><span class=""></span>
                                </a>
                            </li>

                            <li>
                                <a href="{{url('karkun')}}">
                                    <span class="title"> تمام کارکنان </span><span class=""></span>
                                </a>
                            </li>

                            <li>
                                <a href="{{url('karkun/add')}}">
                                    <span class="title"> نیا اضافہ  </span>
                                </a>
                            </li>

                        </ul>
                    </li>



                        @if(session('user_role') == 'ehdkarkun' || session('user_role') == 'superuser')


                    <li class="">
                        <a href="javascript:void(0)">
                            <i class="clip-pencil"></i>
                            <span class="title">   محافل   </span><i class="icon-arrow"></i>
                            <span class="selected"></span>
                        </a>
                        <ul class="sub-menu" style="display: none;">
                            <li>
                                <a href="{{url('mahafil')}}">
                                    <span class="title"> تمام  محافل   </span>
                                </a>
                            </li>

                            <li>
                                <a href="{{url('mahafil/add')}}">
                                    <span class="title"> نیا اضافہ  </span>
                                </a>
                            </li>

                        </ul>
                    </li>


                        @endif

                        @if(session('user_role') == 'superuser')

                            <li class="">
                                <a href="javascript:void(0)">
                                    <i class="fa fa-group"></i>
                                    <span class="title">    عہد کارکنان  </span><i class="icon-arrow"></i>
                                    <span class="selected"></span>
                                </a>
                                <ul class="sub-menu" style="display: none;">
                                    <li>
                                        <a href="{{url('ehadkarkun')}}">
                                            <span class="title"> تمام  عہد کارکن  </span>
                                        </a>
                                    </li>

                                    <li>
                                        <a href="{{url('ehadkarkun/add')}}">
                                            <span class="title"> نیا اضافہ  </span>
                                        </a>
                                    </li>

                                </ul>
                            </li>



                        @endif

                    <li class="">
                        <a href="javascript:void(0)">
                            <i class="clip-pencil"></i>
                            <span class="title"> ڈیوٹی روسٹرز   </span>
                            <i class="icon-arrow"></i>
                            <span class="selected"></span>
                        </a>
                        <ul class="sub-menu" style="display: none;">

                            @if(session('user_role') == 'ehdkarkun' || session('user_role') == 'superuser')

                            <li>
                                <a href="{{url('alldutyrosters')}}">
                                    <span class="title">تمام ڈیوٹی روسٹرز   (کارکنان)</span>
                                </a>
                            </li>

                            <li>
                                <a href="{{url('dutyrosterkarkun')}}">
                                    <span class="title"> ڈیوٹی روسٹر  (کارکنان) </span>
                                </a>
                            </li>

                            @endif

                            @if(session('user_role') == 'mehfil')
                            <li>
                                <a href="{{url('print_duty_roster/edit/'.session('mehfil_id'))}}">
                                    <span class="title"> ڈیوٹی روسٹر  پرنٹ  </span>
                                </a>
                            </li>
                            @endif

                            @if(session('user_role') == 'ehdkarkun' || session('user_role') == 'superuser')

                            <li>
                                <a href="{{url('dutyroster/ehadkameeti')}}">
                                    <span class="title"> ڈیوٹی روسٹر  (عہد کمیٹی ) </span>
                                </a>
                            </li>

                            @endif

                        </ul>
                    </li>





                        <li class="">
                             <a href="{{url('hazri/karkun')}}"><i class="clip-grid-6"></i>
                                <span class="title">   کارکنان حاضری </span><span class="" ></span>
                            </a>
                        </li>

                        @if(session('user_role') == 'ehdkarkun' || session('user_role') == 'superuser')

                            <li class="">
                                 <a href="{{url('hazri/ehad/karkun')}}"><i class="clip-grid-6"></i>
                                    <span class="title">   عہد کارکنان حاضری </span><span class="" ></span>
                                </a>
                            </li>

                        @endif
                       
                        <!-- <li class="">
                             <a href="#"><i class="clip-home-3"></i>
                                <span class="title">کارکنان حاضری اسٹیٹس  </span><span class="" ></span>
                            </a>
                        </li> -->





                            <li class="">
                                <a href="javascript:void(0)">
                                    <i class="clip-cog-2"></i>
                                    <span class="title">    دیگر معلومات   </span><i class="icon-arrow"></i>
                                    <span class="selected"></span>
                                </a>
                                <ul class="sub-menu" style="display: none;">
                                    <li>
                                        <a href="{{url('help')}}">
                                            <span class="title"> اردو کی بورڈ انسٹا لیشن کا طریقہ  </span>
                                        </a>
                                    </li>

                                    <li>
                                        <a href="{{url('committees')}}">
                                            <span class="title">کمیٹیاں</span>
                                        </a>
                                    </li>

                                    <!-- <li>
                                        <a href="{{url('finance')}}">
                                            <span class="title">فنانس</span>
                                        </a>
                                    </li> -->
                                </ul>
                            </li>

                         <li>
                             <a href="{{url('logout')}}"> <i class="clip-exit"></i>
                                <span class="title"> لا گ آو ٹ </span><span ></span>
                            </a>
                        </li>

                    </ul>
                    <!-- end: MAIN NAVIGATION MENU -->
                </div>
