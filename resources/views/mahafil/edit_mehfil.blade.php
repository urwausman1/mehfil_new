<!DOCTYPE html>
<html lang="en" class="no-js">
    <!-- start: HEAD -->
    <head>
        <title>  محفل کی معلومات میں تبدیلی کریں  </title>

        @include('inc.head')
        <link rel="stylesheet" type="text/css" href="{{asset('assets/css/UrduEditor.css')}}" />  
        <script src="{{asset('assets/js/jquery.UrduEditor.js')}}"></script> 

        <style>
        .select2{
            width: 100% !important;
        }
        #contacts_table_wrapper .select2-container{
            width: 100px;
        }
        #contacts_table_wrapper .select2-container span.select2-chosen{
            text-align: right;
        }
        table.table thead .sorting{
            text-align: right;
            background: url('assets/plugins/DataTables/media/css/images/sort_both.png') no-repeat center left;
        }
        table.table thead .sorting_asc{
            text-align: right;
            background: url('assets/plugins/DataTables/media/css/images/sort_asc.png') no-repeat center left;
        }
        table.table thead .sorting_desc{
            text-align: right;
            background: url('assets/plugins/DataTables/media/css/images/sort_desc.png') no-repeat center left;
        }
        </style>
    </head>

    <!-- end: HEAD -->

    <!-- start: BODY -->
    <body class="rtl">    
        <!-- start: HEADER -->
        <div class="navbar navbar-inverse navbar-fixed-top">
            <!-- start: TOP NAVIGATION CONTAINER -->
            <div class="container">
                <div class="navbar-header">
                    <!-- start: RESPONSIVE MENU TOGGLER -->
                    <button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
                        <span class="clip-list-2"></span>
                    </button>
                    <!-- end: RESPONSIVE MENU TOGGLER -->
                    <!-- end: LOGO -->
                </div>
            </div>
            <!-- end: TOP NAVIGATION CONTAINER -->
        </div>
        <!-- end: HEADER -->

        <!-- start: MAIN CONTAINER -->
        <div class="main-container">
            <div class="navbar-content">
                <!-- start: SIDEBAR -->
                @include('inc.sidebar')
                <!-- end: SIDEBAR -->
            </div>
            <!-- start: PAGE -->
            <div class="main-content">

                <div class="container" style="min-height: 760px;">
                    <!-- start: PAGE HEADER -->
                    <div class="row">
                        <div class="col-sm-12">
                            <!-- start: PAGE TITLE & BREADCRUMB -->
                            <ol class="breadcrumb">
                                <li>
                                    <!--- class="clip-file" -->
                                    <i class=""></i>
                                     تبدیلی  معلو مات برائے محافل
                                </li>
                                <li class="active">
                                </li>
                                <!-- <li class="pull-left" style="margin-left: 30px;">
                                    <a href="admin/attendance">حاضری</a>
                                </li> -->

                            <!--<li class="search-box">
                                    <form class="sidebar-search">
                                        <div class="form-group">
                                            <input type="text" placeholder="Start Searching...">
                                            <button class="submit">
                                                <i class="clip-search-3"></i>
                                            </button>
                                        </div>
                                    </form>
                                </li>-->
                            </ol>

                            <div class="page-header">
                                <h1>
                                     تبدیلی  معلو مات برائے محافل
                                    <small style="font-size:25px;margin-right:30px;"></small>
                                </h1>
                            </div>
                            <!-- end: PAGE TITLE & BREADCRUMB -->
                        </div>
                    </div>
                    <!-- end: PAGE HEADER -->
                    <!-- start: PAGE CONTENT -->

                        @if(Session::has('msg'))
                            <div class="col-md-12">
                                <div class="alert alert-success">{{Session::get('msg')}}</div>
                            </div>
                        @endif

                        <div class="row">

                            <div class="col-md-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <i class="fa fa-external-link-square"></i>
                                     تبدیلی  معلو مات برائے محافل
                                    </div>
                                     
                                    <div class="panel-body">
                                    
                                    <form method="post" action="{{url('mahafil/edit/'.$single_mehfil->id)}}"> 
                                        @csrf 
                                        <div class="row">
                                          <div class="col-sm-6">
                                            <!-- has-error -->
                                            <div class="form-group col-sm-12 @if($errors->first('number')) has-error @endif">
                                                <label class="control-label">
                                                    <span class="symbol required" >  
                                                             محفل کا نمبر 
                                                    </span>
                                                </label>
                                                <div>
                                                    <input type="number" autocomplete="off" name="number" id="number" class="form-control"  value="{{old('number' , $single_mehfil->mehfil_number)}}" >
                                                    @if($errors->any())
                                                        <p>@if($errors->first('number')) {{'نام کا اندراج  ضروری ہے۔'}} @endif </p>
                                                    @endif
                                                </div>
                                            </div>
                                          </div>

                                          <div class="col-sm-6">
                                            <!-- has-error -->
                                            <div class="form-group col-sm-12 @if($errors->first('name')) has-error @endif">
                                                <label class="control-label">
                                                    <span class="symbol required" >
                                                        محفل کا نام
                                                    </span>
                                                </label>
                                                <div>
                                                    <input type="text" autocomplete="off" name="name" id="name" class="form-control"  value="{{old('name' , $single_mehfil->mehfil_name)}}" >
                                                    @if($errors->any())
                                                        <p>@if($errors->first('name')) {{'والد کا نام ضروری ہے۔'}} @endif </p>
                                                    @endif
                                                </div>
                                            </div>
                                          </div>
                                        </div>

                                        <div class="row">
                                          <div class="col-sm-6">
                                            <!-- has-error -->
                                            <div class="form-group col-sm-12 @if($errors->first('mediaphone')) has-error @endif">
                                                <label class="control-label">
                                                    <span class="symbol required" >
                                                     میڈیا سیل  رابطہ نمبر 
                                                    </span>
                                                </label>
                                                <div>
                                                    <input type="number" autocomplete="off" name="mediaphone" id="mediaphone" class="form-control" value="{{old('mediaphone', $single_mehfil->mehfil_media_cell_phone)}}" >
                                                    @if($errors->any())
                                                        <p>@if($errors->first('mediaphone')) {{'فون کا اندراج ضروری ہے۔'}} @endif </p>
                                                    @endif
                                                </div>
                                            </div>
                                          </div>

                                          <div class="col-sm-6">
                                            <!-- has-error -->
                                            <div class="form-group col-sm-12 @if($errors->first('phone')) has-error @endif">
                                                <label class="control-label"><span class="symbol required" > رابطہ نمبر </span></label>
                                                <div>
                                                    <input type="number" autocomplete="off" name="phone" id="phone" class="form-control" value="{{old('phone', $single_mehfil->mehfil_mobile)}}" >
                                                    @if($errors->any())
                                                        <p>@if($errors->first('phone')) {{'فون نمبر کا اندراج ضروری ہے۔'}} @endif </p>
                                                    @endif
                                                </div>
                                            </div>
                                          </div>

                                        </div>

                                        <div class="row">
                                          <div class="col-sm-6">
                                            <!-- has-error -->
                                            <div class="form-group col-sm-12 @if($errors->first('email')) has-error @endif">
                                                <label class="control-label"><span class="symbol required" > ایمیل </span></label>
                                                <div>
                                                    <input type="email" autocomplete="off" name="email" id="email" class="form-control"  value="{{old('email', $single_mehfil->mehfil_email)}}" >
                                                    @if($errors->any())
                                                        <p>@if($errors->first('email')) {{'ایمیل  کا اندراج ضروری ہے۔'}} @endif </p>
                                                    @endif
                                                </div>
                                            </div>
                                          </div>

                                            @if(session('user_role') == 'superuser')
                                          <div class="col-sm-6">
                                            <!-- has-error -->
                                            <div class="form-group @if($errors->first('password')) has-error @endif">
                                                <label class="control-label"><span class="symbol required" >  پاسورڈ  </span></label>
                                                <div>
                                                    <input type="password" autocomplete="off" name="password" id="password" class="form-control"  value="{{old('password', $single_mehfil->admin_password)}}" required  >
                                                    @if($errors->any())
                                                        <p>@if($errors->first('password')) {{'  پاسورڈ کا اندراج ضروری ہے۔  '}} @endif </p>
                                                    @endif
                                                </div>
                                            </div>
                                          </div>
                                             @endif
                                        </div>


                                        <div class="row">
                                            
                                          <div class="col-sm-12">
                                            <!-- has-error -->
                                            <div class="form-group col-sm-12 @if($errors->first('address')) has-error @endif">
                                                <label class="control-label"><span class="symbol required" > مکمل پتہ </span></label>
                                                <div>
                                                    <input type="text" autocomplete="off" name="address" id="address" class="form-control"  value="{{old('address', $single_mehfil->mehfil_address)}}" >
                                                    @if($errors->any())
                                                        <p>@if($errors->first('address')) {{'مکمل پتہ کا اندراج ضروری ہے۔'}} @endif </p>
                                                    @endif
                                                </div>
                                            </div>
                                          </div>
                                        </div>

                                        <div class="row">



                                          <div class="col-sm-4">
                                            <!-- has-error -->
                                            <div id="mulk_name_div" class="form-group col-sm-12 @if($errors->first('country')) has-error @endif">
                                                <label style="margin-bottom: 15px !important;" class="control-label"><span class="symbol required" > ملک</span></label>
                                                <div>
                                                    <select class="select2" name="country" id="mulk_name">
                                                        <option value="{{old('country' , $single_mehfil->mehfil_country)}}">{{old('country' , $single_mehfil->mehfil_country)}}</option>

                                                        @foreach ($all_mulk as $key => $value)
                                                            
                                                            <option value="{{$value->cc02}}">
                                                                {{$value->country_name_ur}}
                                                            </option>

                                                        @endforeach

                                                    </select>
                                                    @if($errors->any())
                                                        <p>@if($errors->first('country')) {{'ملک کا اندراج ضروری ہے۔'}} @endif </p>
                                                    @endif
                                                </div>
                                            </div>
                                          </div>


                                          <div class="col-sm-4">
                                            <!-- has-error -->
                                            <div class="form-group col-sm-12 @if($errors->first('city')) has-error @endif">
                                                <label style="margin-bottom: 15px !important;" class="control-label"><span class="symbol required" > شہر</span></label>
                                                <div>
                                                    <select class="select2" name="city" id="urwausman">

                                                        <option value="{{old('city' , $single_mehfil->mehfil_city)}}">{{old('city' , $single_mehfil->mehfil_city)}}</option>

                                                    </select>
                                                    @if($errors->any())
                                                        <p>@if($errors->first('city')) {{'شہر کا اندراج ضروری ہے۔'}} @endif </p>
                                                    @endif
                                                </div>
                                            </div>
                                          </div>


                                          <div class="col-sm-4">
                                            <!-- has-error -->
                                            <div class="form-group col-sm-12 @if($errors->first('coordinates')) has-error @endif">
                                                <label class="control-label">
                                                    <span class="symbol required" > کو آرڈینیٹس</span></label>
                                                <div>
                                                    <input type="text" autocomplete="off" name="coordinates" id="coordinates" class="form-control"  value="{{old('coordinates' , $single_mehfil->mehfil_coordinates)}}" >
                                                </div>
                                            </div>
                                          </div>
                                        </div>


                                        <div class="row" id="add_city_details">
                                            
                                            <div class="col-md-4 @if($errors->first('cc02_city')) has-error @endif" >
                                                <div class='form-group col-sm-12'>
                                                    <label>  دو حرفی نام لکھیں   </lable>
                                                    <input class='form-control' name='cc02_city' id='cc02_city' type='text' value="{{old('cc02' , $get_city_dtl->cc02_city)}}" >
                                                </div>        
                                            </div>

                                            <div class="col-md-4 @if($errors->first('cc03_city')) has-error @endif">
                                                <div class='form-group col-sm-12'>
                                                    <label>  تین  حرفی نام لکھیں    </lable> 
                                                    <input class='form-control' name='cc03_city' id='cc03_city' type='text' value="{{old('cc03' , $get_city_dtl->cc03_city)}}" >
                                                </div>
                                            </div>

                                            <div class="col-md-4 @if($errors->first('city_name_ur')) has-error @endif">
                                                <div class='form-group col-sm-12'>
                                                    <label>  اردو میں نام لکھیں    </lable>    
                                                    <input class='form-control' name='city_name_ur' id='city_name_ur' type='text' value="{{old('city_name_ur' , $get_city_dtl->city_name_ur)}}" >
                                                </div>        
                                            </div>

                                        </div>


                                        <div class="row">
                                          <div class="col-sm-6">
                                            <!-- has-error -->
                                            <div class="form-group col-sm-12">
                                                <input type="submit" class="btn btn-sm btn-primary" name="submit" value="اندراج کریں">
                                            </div>

                                          </div>
                                        </div>

                                    </form>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

<!-- end: PAGE CONTENT-->
</div>
</div>
            <!-- end: PAGE -->
</div>
        <!-- end: MAIN CONTAINER -->



        <!-- start: FOOTER & scripts -->
            @include('inc.footer')
        
         <script type="text/javascript">
            $(document).ready(function() {
                $('#example-getting-started').multiselect();
                $('.select2').select2();
            });
            $(document).ready(function(){
                $(".btn_submit_P").click(function(){
                    $(".HazriKarkoonatten").val("P");
                });

                $(".btn_submit_L").click(function(){
                    $(".HazriKarkoonatten").val("L");
                });             

                $(".btn_submit_M").click(function(){
                    $(".HazriKarkoonatten").val("M");
                });
        });
        </script>
    </body>
    <!-- end: BODY -->
<script>
    $(document).ready(function() {
        $(document).find("#number").focus();
    });



$('#mulk_name').on('change' , function(e){
   e.preventDefault();

   var mulkname = $("#mulk_name").val();


   $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('[name="_token"]').val()
      }
  });
   
   /* Submit form data using ajax*/
   $.ajax({
      url: "{{url('karkun/cityname_fetch')}}",
      method: 'post',
      data: {
        mulkname : mulkname,
      },
      success: function(response){
         //------------------------

         $("#urwausman").html(response);
         //--------------------------
      }});
});


    $('#urwausman').on('change' , function(e){
        e.preventDefault();

        var cityname = $("#urwausman").val();


        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('[name="_token"]').val()
            }
        });

        /* Submit form data using ajax*/
        $.ajax({
            url: "{{url('karkun/cityname_fetch_dtl')}}",
            method: 'post',
            dataType: 'JSON',
            data: {
                cityname : cityname,
            },
            success: function(response){
                $("#cc02_city").val(response.cc02);
                $("#cc03_city").val(response.cc03);
                $("#city_name_ur").val(response.city_name_ur);
            }});
    });



</script>


<script>
$(document).ready(function () {

jQuery.browser = {};
(function () {
    jQuery.browser.msie = false;
    jQuery.browser.version = 0;
    if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
        jQuery.browser.msie = true;
        jQuery.browser.version = RegExp.$1;
    }
})();

    $(document).find("#name").UrduEditor();   
    $(document).find("#address").UrduEditor();   
    $(document).find("#coordinates").UrduEditor();   
    $(document).find("#mulk_name_div .select2-input").UrduEditor();   
});
</script>

</html>