<!DOCTYPE html>
<html lang="en" class="no-js">
<!-- start: HEAD -->
<head>
    <title>  کارکنان کی رپورٹنگ    </title>

    @include('inc.head')

    <style>

        .select2{
            width: 100%;
        }

        #contacts_table_wrapper .select2-container{
            width: 100px;
        }
        #contacts_table_wrapper .select2-container span.select2-chosen{
            text-align: right;
        }
        table.table thead .sorting{
            text-align: right;
            background: url('assets/plugins/DataTables/media/css/images/sort_both.png') no-repeat center left;
        }
        table.table thead .sorting_asc{
            text-align: right;
            background: url('assets/plugins/DataTables/media/css/images/sort_asc.png') no-repeat center left;
        }
        table.table thead .sorting_desc{
            text-align: right;
            background: url('assets/plugins/DataTables/media/css/images/sort_desc.png') no-repeat center left;
        }
    </style>
</head>

<!-- end: HEAD -->

<!-- start: BODY -->
<body class="rtl">    
    <!-- start: HEADER -->
    <div class="navbar navbar-inverse navbar-fixed-top">
        <!-- start: TOP NAVIGATION CONTAINER -->
        <div class="container">
            <div class="navbar-header">
                <!-- start: RESPONSIVE MENU TOGGLER -->
                <button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
                    <span class="clip-list-2"></span>
                </button>
                <!-- end: RESPONSIVE MENU TOGGLER -->
                <!-- end: LOGO -->
            </div>
        </div>
        <!-- end: TOP NAVIGATION CONTAINER -->
    </div>
    <!-- end: HEADER -->

    <!-- start: MAIN CONTAINER -->
    <div class="main-container">
        <div class="navbar-content">
            <!-- start: SIDEBAR -->
            @include('inc.sidebar')
            <!-- end: SIDEBAR -->
        </div>
        <!-- start: PAGE -->
        <div class="main-content">

            <div class="container" style="min-height: 760px;">
                <!-- start: PAGE HEADER -->
                <div class="row">
                    <div class="col-sm-12">
                        <!-- start: PAGE TITLE & BREADCRUMB -->
                        <ol class="breadcrumb">
                            <li>
                                <!--- class="clip-file" -->
                                <i class=""></i>
                               رپورٹنگ کارکن
                            </li>
                            <li class="active">
                            </li>
                        </ol>

                        <div class="page-header">
                            <h1>
                               رپورٹنگ کارکن
                                <small style="font-size:25px;margin-right:30px;"></small>
                            </h1>
                        </div>
                        <!-- end: PAGE TITLE & BREADCRUMB -->
                    </div>
                </div>
                <!-- end: PAGE HEADER -->
                <!-- start: PAGE CONTENT -->

                <div class="row">
                    <div class="col-md-12">
                        <!-- start: DYNAMIC TABLE PANEL -->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <i class="fa fa-external-link-square"></i>
                               رپورٹنگ کارکن
                            </div>


                            <div class="panel-body">

                                @if(Session::has('msg'))
                                <div class="col-md-12">
                                    <div class="alert alert-success">{{Session::get('msg')}}</div>
                                </div>
                                @endif



                                <form action="{{url('reports/karkun')}}" method="post">
                                    @csrf
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <input type="text" name="marfat_search" placeholder="معرفت درج کریں " class="form-control" value="@if(isset($marfat_search)) {{$marfat_search}} @else {{''}} @endif">
                                        </div>    

                                        <div class="col-sm-3">

                                            <select class="select2 form-control" name="mehfilname">
                                                <option value="@if(isset($mehfilname)) {{$mehfilname}}@endif">
                                                    @if(isset($mehfilname)) {{$get_name->mehfil_name}} @else {{' محفل کا ناتخاب کریں۔۔  '}} @endif
                                                </option>
                                                @foreach($all_mahafil as $single_mehfil)
                                                <option value="{{$single_mehfil->id}}">{{$single_mehfil->mehfil_name}}</option>
                                                @endforeach
                                            </select>

                                        </div>    

                                        <div class="col-sm-3">
                                            <select class="select2 form-control" name="mahina">
                                                <option value="@if(isset($mahina)) {{$mahina}} @else {{''}} @endif">  
                                                    
                                                @if(isset($mahina)) {{$mahina}} @else {{'  مہینہ منتخب کریں۔۔۔ '}} @endif

                                                </option>
                                                <option value="01"> جنوری</option>
                                                <option value="02">فروری</option>
                                                <option value="03">مارچ</option>
                                                <option value="04">اپریل</option>
                                                <option value="05"> مئی</option>
                                                <option value="06"> جون</option>
                                                <option value="07"> جولائی</option>
                                                <option value="08">اگست</option>
                                                <option value="09">ستمبر</option>
                                                <option value="10">اکتوبر</option>
                                                <option value="11"> نومبر</option>
                                                <option value="12"> دسمبر</option>
                                            </select>
                                        </div>  

                                        <div class="col-sm-2">
                                            @php 
                                                $year = date('Y');
                                            @endphp
                                            <select class="select2 form-control" name="saal">
                                                <option value="@if(isset($saal)) {{$saal}} @else {{'2020'}} @endif">

                                                @if(isset($saal)) {{$saal}} @else {{$year}} @endif

                                                </option>
                                                @for($i = $year - 1; $i > 1975; $i--)
                                                <option value="{{$i}}">{{$i}}</option>
                                                @endfor
                                            </select>
                                        </div>  

                                        <div class="col-sm-1">

                                            <button type="submit" class="btn btn-primary" role="button" style="position: relative;float: left;">

                                                تلاش کریں

                                            </button>


                                        </div>    

                                    </div>
                                    
                                </form>




                                <div class="table-responsive">
                                    <table class="table-bordered" width="100%">
                                        <thead></thead>
                                    </table>
                                    <br><br>

                                    <div id="contacts_table_wrapper" class="dataTables_wrapper form-inline" role="grid">

                                        <table class="table table-striped table-bordered table-hover table-full-width" id="contacts_table">

                                            <thead style="font-size: 19px !important;">
                                                <tr>
                                                    <th>نام</th>
                                                    <th>ولدیت</th>
                                                    <th>معرفت</th>
                                                    <th>عمر</th>
                                                    <!-- <th> عرصہ عہد</th> -->
                                                    <th>شناختی کارڈ نمبر</th>
                                                    <th>رابطہ نمبر</th>
                                                    <th>پتہ</th>
                                                    <th>  محفل کوڈ   </th>
                                                    <th>شہر</th>
                                                    <th> ملک</th>
                                                </tr>
                                            </thead>

                                            <?php 
                                            function getAge($dob,$condate){ 
                                                $birthdate = new DateTime(date("Y-m-d",  strtotime(implode('-', array_reverse(explode('/', $dob))))));
                                                $today= new DateTime(date("Y-m-d",  strtotime(implode('-', array_reverse(explode('/', $condate))))));           
                                                $age = $birthdate->diff($today)->y;

                                                return $age;
                                            }
                                            ?>

                                            <tbody>
                                                @if(isset($all_karkuns))
                                                @foreach($all_karkuns as $single_karkun)
                                                <tr>
                                                    <td>{{$single_karkun->kp_name}}</td>
                                                    <td>{{$single_karkun->kp_fname}}</td>

                                                    <td class="font_digit">{{$single_karkun->kp_marfat}}</td>

                                                    <td>
                                                        @php
                                                        $dob= $single_karkun->kp_dob; //date of Birth
                                                        $condate= date('Y-m-d'); //Certain fix Date of Age 
                                                        echo getAge($dob,$condate) . ' سال';
                                                        @endphp
                                                    </td>
                                                    <?php /*
                                                    <td>
                                                        @php 
                                                        $dob= $single_karkun->kp_doe; //date of Birth
                                                        $condate= date('Y-m-d'); //Certain fix Date of Age 
                                                        echo getAge($dob,$condate) . ' سال';
                                                        @endphp

                                                    </td>
                                                    */ ?>
                                                    <td class="font_digit">{{$single_karkun->kp_cnic}}</td>
                                                    <td class="font_digit">{{$single_karkun->kp_phone}}</td>
                                                    <td>{{$single_karkun->kp_address}}</td>
                                                    <td class="font_digit">
                                                        {{$single_karkun->mehfil_number. "_".$single_karkun->mehfil_city."_".$single_karkun->mehfil_country}}

                                                    </td>
                                                    <td class="font_digit">{{$single_karkun->kp_city}}</td>
                                                    <td class="font_digit">{{$single_karkun->kp_country}}</td>

                                                </tr>
                                                @endforeach
                                                @else
                                                    <tr>
                                                        <td>{{$all_data}}</td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                @endif
                                            </tbody>
                                        </table>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end: DYNAMIC TABLE PANEL -->
                    </div>
                </div>

                <!-- end: PAGE CONTENT-->
            </div>
        </div>
        <!-- end: PAGE -->
    </div>
    <!-- end: MAIN CONTAINER -->



    <!-- start: FOOTER & scripts -->
    @include('inc.footer')

    <script type="text/javascript">
        $(document).ready(function() {
            $('#example-getting-started').multiselect();
            $('.select2').select2();
        });

    </script>
</body>
<!-- end: BODY -->
</html>