<!DOCTYPE html>
<html lang="en" class="no-js">
<!-- start: HEAD -->
<head>
    <title>  محافل کی حاضرہ رپورٹ   </title>

    @include('inc.head')

    <style>

        @media print {
           .noprint {
              visibility: hidden;
           }
        }

        table th, td{
            text-align: center !important;
        }
        .lds-dual-ring {
          display: inline-block;
          width: 40px;
          height: 40px;
        }
        .lds-dual-ring:after {
          content: " ";
          display: block;
          width: 160px;
          height: 160px;
          margin: 20% auto;
          border-radius: 50%;
          border: 18px solid #fff;
          border-color: #fff transparent #fff transparent;
          animation: lds-dual-ring 1.2s linear infinite;
        }
        @keyframes lds-dual-ring {
          0% {
            transform: rotate(0deg);
          }
          100% {
            transform: rotate(360deg);
          }
        }


        .overlay {
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100vh;
            background: rgba(0,0,0,.8);
            z-index: 9999;
            opacity: 1;
            transition: all 0.5s;
        }
        .select2{
            width: 100%;
        }

    </style>
</head>

<!-- end: HEAD -->

<!-- start: BODY -->
<body class="rtl">    
    <!-- start: HEADER -->
    <div class="navbar navbar-inverse navbar-fixed-top noprint">
        <!-- start: TOP NAVIGATION CONTAINER -->
        <div class="container">
            <div class="navbar-header">
                <!-- start: RESPONSIVE MENU TOGGLER -->
                <button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
                    <span class="clip-list-2" ></span>
                </button>
                <!-- end: RESPONSIVE MENU TOGGLER -->
                <!-- end: LOGO -->
            </div>
        </div>
        <!-- end: TOP NAVIGATION CONTAINER -->
    </div>
    <!-- end: HEADER -->

    <!-- start: MAIN CONTAINER -->
    <div class="main-container">
        <div class="navbar-content">
            <!-- start: SIDEBAR -->
            @include('inc.sidebar')
            <!-- end: SIDEBAR -->
        </div>
        <!-- start: PAGE -->
        <div class="main-content">

            <div class="container" style="min-height: 760px;">
                <!-- start: PAGE HEADER -->
                <div class="row noprint">
                    <div class="col-sm-12">
                        <!-- start: PAGE TITLE & BREADCRUMB -->
                        <ol class="breadcrumb">
                            <li>
                                <!--- class="clip-file" -->
                                <i class=""></i>
                               رپورٹنگ  محافل
                            </li>
                            <li class="active">
                            </li>
                        </ol>

                        <div class="page-header">
                            <h1>
                               رپورٹنگ  محافل
                                <small style="font-size:25px;margin-right:30px;"></small>
                            </h1>
                        </div>
                        <!-- end: PAGE TITLE & BREADCRUMB -->
                    </div>
                </div>
                <!-- end: PAGE HEADER -->
                <!-- start: PAGE CONTENT -->

                <div class="row">
                    <div class="col-md-12">
                        <!-- start: DYNAMIC TABLE PANEL -->
                        <div class="panel panel-default">
                            <div class="panel-heading noprint">
                                <i class="fa fa-external-link-square"></i>
                               رپورٹنگ  محافل
                            </div>


                            <div class="panel-body">

                                @if(Session::has('msg'))
                                <div class="col-md-12 noprint">
                                    <div class="alert alert-success">{{Session::get('msg')}}</div>
                                </div>
                                @endif



                                <form action="{{url('reports/karkun')}}" id="newform" class="noprint">
                                    @csrf
                                    <div class="row noprint">
   

                                        <div class="col-sm-3 noprint">
                                            @csrf

                                            @if(isset($mehfil_id) && !empty($mehfil_id) )    
                                                <input type="hidden" name="mehfilname" id="mehfilname_is" value="{{$mehfil_id}}">  
                                                <input class="form-control" type="text" readonly="readonly" value="{{$all_mahafil->mehfil_name}}">
                                            @else

                                            <select class="select2" onchange="get_hazri(this)" name="mehfilname" id="mehfilname_is">
                                                <option value="@if(isset($mehfilname)) {{$mehfilname}}@endif">
                                                    @if(isset($mehfilname)) {{$get_name->mehfil_name}} @else {{' محفل کا  انتخاب کریں۔۔  '}} @endif
                                                </option>
                                                @foreach($all_mahafil as $single_mehfil)
                                                <option value="{{$single_mehfil->id}}">{{$single_mehfil->mehfil_name}}</option>
                                                @endforeach
                                            </select>
                                            @endif

                                        </div>    


                                        <div class="col-sm-3 noprint">
                                            <select  class="select2" onchange="get_hazri()" name="mahina" id="mahina">
                                                <option value="@if(isset($mahina)) {{$mahina}} @else {{''}} @endif">  
                                                    
                                        @if(isset($mahina)) {{$mahina}} @else {{'  مہینہ منتخب کریں۔۔۔ '}} @endif

                                                </option>
                                                <option value="01"> جنوری</option>
                                                <option value="02">فروری</option>
                                                <option value="03">مارچ</option>
                                                <option value="04">اپریل</option>
                                                <option value="05"> مئی</option>
                                                <option value="06"> جون</option>
                                                <option value="07"> جولائی</option>
                                                <option value="08">اگست</option>
                                                <option value="09">ستمبر</option>
                                                <option value="10">اکتوبر</option>
                                                <option value="11"> نومبر</option>
                                                <option value="12"> دسمبر</option>
                                            </select>
                                        </div>  

                                        <div class="col-sm-2 noprint">
                                            @php 
                                                $year = date('Y');
                                            @endphp
                                                <select onchange="get_hazri()" id="get_saal_hazri1" class="select2" name="saal">
                                                <option value="@if(isset($saal)) {{$saal}} @else {{''}} @endif">

                                                @if(isset($saal)) {{$saal}} @else {{'  سال کا  انتخاب کریں ۔۔  '}} @endif

                                                </option>
                                                @for($i = $year; $i > 1950; $i--)
                                                <option value="{{$i}}">{{$i}}</option>
                                                @endfor
                                            </select>
                                        </div>  

                                        <div class="col-sm-1">

                                            <a href="{{url('reports/hazri/mehfil')}}" type="submit" class="btn btn-primary" role="button" style="position: relative;float: left;">
                                                            ریفریش کریں
                                            </a>


                                        </div>    

                                    </div>

                                </form>
                                
                                <hr>
                                <!--<div class="table-responsive">
                                    <table class="table-bordered" width="100%">
                                        <thead></thead>
                                    </table>
                                    <br><br>

                                    <div id="contacts_table_wrapper" class="dataTables_wrapper form-inline" role="grid"> -->

                                <div>
                                    <div>
                                        <!-- <table class="table table-striped table-bordered table-hover table-full-width" id="contacts_table">-->

                                        <table class="urwausman" width="100%" border="1">

                                            <thead id="headis" style="font-size: 19px !important;">
                                                <tr>
                                                    <th>  کوڈ نمبر  </th>
                                                    <th>  نام  </th>
                                                    <th>  فون  </th>
                                                    <th> ڈیوٹی دن </th>
                                                    <th>  ڈیوٹی پر حاضریاں    </th>
                                                    <th>  کل حاضریاں    </th>
                                                    <th>  میٹنگ پر حاضری   </th>
                                                    <!-- <th>تاریخ </th> -->
                                                    <!-- <th>وقت </th> -->
                                                    <!-- <th>  سٹیٹس   </th> -->
                                                </tr>
                                            </thead>

                                            <tbody id="get_data" >
                                                <tr>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end: DYNAMIC TABLE PANEL -->
                    </div>
                </div>

                <!-- end: PAGE CONTENT-->
            </div>
        </div>
<div id="loader" class="lds-dual-ring hidden overlay noprint"></div>
        <!-- end: PAGE -->
    </div>
    <!-- end: MAIN CONTAINER -->



    <!-- start: FOOTER & scripts -->
    @include('inc.footer')

    <script type="text/javascript">
        $(document).ready(function() {
            $('#example-getting-started').multiselect();
            $('.select2').select2();
        });


    // function get_hazri(ths) {

    //     var mehfil_name = $(ths).val();

    //     $.ajaxSetup({
    //         headers: {
    //             'X-CSRF-TOKEN': $('[name="_token"]').val()
    //         }
    //     });

    //     /* Submit form data using ajax*/
    //     $.ajax({
    //         url: "{{url('reports/hazri/get/mahafil')}}",
    //         method: 'post',
    //         dataType: 'JSON',
    //         data: {
    //             mehfil_name : mehfil_name,
    //         },
    //         beforeSend: function() {
    //             $('#loader').removeClass('hidden');
    //         },
    //         success: function(response){
    //             $('#loader').addClass('hidden');
    //             $("#get_karkuns_hazri").html(response.options);
    //         }});       
    // }


    function get_hazri(ths) {

        var month_name = $("#mahina").val();
        var mehfil_name = $("#mehfilname_is").val();
        var year_name = $("#get_saal_hazri1").val();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('[name="_token"]').val()
            }
        });

        /* Submit form data using ajax*/
        $.ajax({
            url: "{{url('reports/hazri/get/mehfil/hazri')}}",
            method: 'post',
            dataType: 'JSON',
            data: {
                month_name : month_name,
                mehfil_name : mehfil_name,
                year_name : year_name,
            },
            beforeSend: function() {
                $('#loader').removeClass('hidden');
            },
            success: function(response){
                $('#loader').addClass('hidden');
                $("#get_data").html(response.tbody);
            }
            });       
    }


    </script>
</body>
<!-- end: BODY -->
</html>