<!DOCTYPE html>
<html lang="en" class="no-js">
<!-- start: HEAD -->
<head>
    <title>  ماہانہ رپورٹ  </title>

    @include('inc.head')

    <style>

        .switch {
            position: relative;
            top: 3px;
            display: inline-block;
            width: 80px;
            height: 24px;
            margin: 0;
        }
       
        .switch input {
            display: none;
        }

        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #ca2222;
            -webkit-transition: .4s;
            transition: .4s;
            border-radius: 34px;
        }

        .slider:before {
            position: absolute;
            content: "";
            height: 16px;
            width: 16px;
            left: 5px;
            bottom: 4px;
            background-color: white;
            -webkit-transition: .4s;
            transition: .4s;
            border-radius: 50%;
        }

        input:checked + .slider {
            background-color: #2ab934;
        }

        input:focus + .slider {
            box-shadow: 0 0 1px #2196F3;
        }

        input:checked + .slider:before {
            -webkit-transform: translateX(26px);
            -ms-transform: translateX(26px);
            transform: translateX(55px);
        }

        /*------ ADDED CSS ---------*/
        .slider:after
        {
            content:'  نہیں    ';
            color: white;
            display: block;
            position: absolute;
            transform: translate(-50%,-50%);
            top: 50%;
            left: 50%;
            font-size: 16px;
            font: 'urdu';
        }

        input:checked + .slider:after
        {  
            content:'  جی ہاں   ';
        }

        @media print {
            .noprint {
                visibility: hidden;
            }
        }

        table th, td{
            text-align: center !important;
        }

        .lds-dual-ring {
            display: inline-block;
            width: 40px;
            height: 40px;
        }
        .lds-dual-ring:after {
            content: " ";
            display: block;
            width: 160px;
            height: 160px;
            margin: 20% auto;
            border-radius: 50%;
            border: 18px solid #fff;
            border-color: #fff transparent #fff transparent;
            animation: lds-dual-ring 1.2s linear infinite;
        }
        @keyframes lds-dual-ring {
            0% {
                transform: rotate(0deg);
            }
            100% {
                transform: rotate(360deg);
            }
        }


        .overlay {
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100vh;
            background: rgba(0,0,0,.8);
            z-index: 9999;
            opacity: 1;
            transition: all 0.5s;
        }
        .select2{
            width: 100%;
        }

    </style>
</head>

<!-- end: HEAD -->

<!-- start: BODY -->
<body class="rtl">    
    <!-- start: HEADER -->
    <div class="navbar navbar-inverse navbar-fixed-top noprint">
        <!-- start: TOP NAVIGATION CONTAINER -->
        <div class="container">
            <div class="navbar-header">
                <!-- start: RESPONSIVE MENU TOGGLER -->
                <button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
                    <span class="clip-list-2" ></span>
                </button>
                <!-- end: RESPONSIVE MENU TOGGLER -->
                <!-- end: LOGO -->
            </div>
        </div>
        <!-- end: TOP NAVIGATION CONTAINER -->
    </div>
    <!-- end: HEADER -->

    <!-- start: MAIN CONTAINER -->
    <div class="main-container">
        <div class="navbar-content">
            <!-- start: SIDEBAR -->
            @include('inc.sidebar')
            <!-- end: SIDEBAR -->
        </div>
        <!-- start: PAGE -->
        <div class="main-content">

            <div class="container" style="min-height: 760px;">
                <!-- start: PAGE HEADER -->
                <div class="row noprint">
                    <div class="col-sm-12">
                        <!-- start: PAGE TITLE & BREADCRUMB -->
                        <ol class="breadcrumb">
                            <li>
                                <!--- class="clip-file" -->
                                <i class=""></i>
                                رپورٹنگ  محافل
                            </li>
                            <li class="active">
                            </li>
                        </ol>

                        <div class="page-header">
                            <h1>
                                رپورٹنگ  محافل
                                <small style="font-size:25px;margin-right:30px;"></small>
                            </h1>
                        </div>
                        <!-- end: PAGE TITLE & BREADCRUMB -->
                    </div>
                </div>
                <!-- end: PAGE HEADER -->
                <!-- start: PAGE CONTENT -->

                <div class="row">
                    <div class="col-md-12">
                        <!-- start: DYNAMIC TABLE PANEL -->
                        <div class="panel panel-default">
                            <div class="panel-heading noprint">
                                <i class="fa fa-external-link-square"></i>
                                رپورٹنگ  محافل
                            </div>


                            <div class="panel-body">

                                @if(Session::has('msg'))
                                <div class="col-md-12 noprint">
                                    <div class="alert alert-success">{{Session::get('msg')}}</div>
                                </div>
                                @endif


                                    <div class="row noprint">

                                    @csrf

                                        <div class="col-sm-3 noprint">
                                            @csrf
                                            <select class="select2" name="mehfilname" id="mehfilname_is">
                                                <option value="@if(isset($mehfilname)) {{$mehfilname}}@endif">
                                                    @if(isset($mehfilname)) {{$get_name->mehfil_name}} @else {{' محفل کا  انتخاب کریں۔۔  '}} @endif
                                                </option>
                                                @foreach($all_mahafil as $single_mehfil)
                                                <option value="{{$single_mehfil->id}}">{{$single_mehfil->mehfil_name}}</option>
                                                @endforeach
                                            </select>

                                        </div>    


                                        <div class="col-sm-3 noprint">
                                            <select  class="select2"  name="mahina" id="mahina">
                                                <option value="@if(isset($mahina)) {{$mahina}} @endif">  

                                                    @if(isset($mahina)) {{$mahina}} @else {{'  مہینہ منتخب کریں۔۔۔ '}} @endif

                                                </option>
                                                <option value="01"> جنوری</option>
                                                <option value="02">فروری</option>
                                                <option value="03">مارچ</option>
                                                <option value="04">اپریل</option>
                                                <option value="05"> مئی</option>
                                                <option value="06"> جون</option>
                                                <option value="07"> جولائی</option>
                                                <option value="08">اگست</option>
                                                <option value="09">ستمبر</option>
                                                <option value="10">اکتوبر</option>
                                                <option value="11"> نومبر</option>
                                                <option value="12"> دسمبر</option>
                                            </select>
                                        </div>  

                                        <div class="col-sm-2 noprint">
@php 
    $year = date('Y');
@endphp
                                            <select  id="get_saal_hazri1" class="select2" name="saal">
                                                <option value="@if(isset($saal)) {{$saal}} @endif">

                                                    @if(isset($saal)) {{$saal}} @else {{'  سال کا  انتخاب کریں ۔۔  '}} @endif

                                                </option>
                                                @for($i = $year; $i > 1950; $i--)
                                                <option value="{{$i}}">{{$i}}</option>
                                                @endfor
                                            </select>
                                        </div>  

                                        <div class="col-sm-3">

                                            <button onclick="get_hazri()" type="submit" class="btn btn-success" >
                                                رپورٹ نکالیں
                                            </button>

                                            <button onclick="refresh_page()" class="btn btn-primary" role="button" style="margin-right: 16px">
                                                ریفریش کریں
                                            </button>

                                            <div id="btn_edit"></div>

                                        </div>    

                                    </div>

                                    <hr>
                                    @csrf
                                        <div id="get_all_form"></div>
                                </div>
                            </div>
                            <!-- end: DYNAMIC TABLE PANEL -->
                        </div>
                    </div>

                    <!-- end: PAGE CONTENT-->
                </div>
            </div>
            <div id="loader" class="lds-dual-ring hidden overlay noprint"></div>
            <!-- end: PAGE -->
        </div>
        <!-- end: MAIN CONTAINER -->



        <!-- start: FOOTER & scripts -->
        @include('inc.footer')

        <script type="text/javascript">
            $(document).ready(function() {
                $('.select2').select2();
            });


            function submit_form(e) {
                e.preventDefault();
                var mahina = $("#mahina").val();
                var saal = $("#get_saal_hazri1").val();
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('[name="_token"]').val()
                    }
                });

                // $('#btn_submit').html('ڈیٹا  داخل  ہو  رہا ہے۔۔۔   ');
                /* Submit form data using ajax*/
                $('#loader').removeClass('hidden');
                $.ajax({
                    url: "{{url('insert_monthly_report_coordinators')}}",
                    method: 'post',
                    data: $('.table_report').serialize()  + '&' + $.param({mahina : mahina}) + '&' + $.param({saal : saal}) ,
                    success: function(response){
        
                        $('#loader').addClass('hidden');
                }});
            }



            function get_hazri() {

                var month_name = $("#mahina").val();
                var mehfil_name = $("#mehfilname_is").val();
                var year_name = $("#get_saal_hazri1").val();


                if (mehfil_name == '') {
                    alert('    محفل کا انتخاب ضروری ہے ۔!   ');
                    return false;
                }

                if (month_name == '') {
                    alert('    مہینہ کا انتخاب ضروری ہے ۔!   ');
                    return false;
                }

                if (year_name == '') {
                    alert('  سال  کا انتخاب ضروری ہے ۔!   ');
                    return false;
                }

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('[name="_token"]').val()
                    }
                });

                /* Submit form data using ajax*/
                $.ajax({
                    url: "{{url('reports/hazri/mehfil/monthly')}}",
                    method: 'post',
                    dataType: 'JSON',
                    data: {
                        month_name : month_name,
                        mehfil_name : mehfil_name,
                        year_name : year_name,
                    },
                    beforeSend: function() {
                        $('#loader').removeClass('hidden');
                    },
                    success: function(response){
                        $('#loader').addClass('hidden');
                        $("#get_all_form").html(response.all_data);
                        $("#btn_edit").html(response.btn_edit);
                    }});       
            }

            function refresh_page() {
                location.reload(true);
            }

            function edit_monthly_report(id, month, year, url) {

                var url = "/"+id+"/"+month+"/"+year;
                $.ajax({
                    url: "{{url('reports/hazri/mehfil/monthly/edit/')}}"+url,
                    method: 'post',
                    dataType: 'JSON',
                    data: {
                        mehfil_name : id,
                        month_name :  month,
                        year_name : year,
                    },
                    beforeSend: function() {
                        $('#loader').removeClass('hidden');
                    },
                    success: function(response){
                        $('#loader').addClass('hidden');
                        $("#get_all_form").html(response.all_data);
                        $("#btn_edit").html(response.btn_edit);
                    }});       
            }

</script>
</body>
<!-- end: BODY -->
</html>