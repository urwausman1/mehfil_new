<!DOCTYPE html>
<html lang="en" class="no-js">
<!-- start: HEAD -->
<head>
    <title>   حاضری برائے عہد کارکن   </title>
    @include('inc.head')

<style>
.switch {
  position: relative;
  display: inline-block;
  width: 90px;
  height: 34px;
}

.switch input {display:none;}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ca2222;
  -webkit-transition: .4s;
  transition: .4s;
   border-radius: 34px;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
  border-radius: 50%;
}

input:checked + .slider {
  background-color: #2ab934;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(55px);
}

/*------ ADDED CSS ---------*/
.slider:after
{
 content:'  غیر حاضر  ';
 color: white;
 display: block;
 position: absolute;
 transform: translate(-50%,-50%);
 top: 50%;
 left: 53%;
 font-size: 20px;
 font: 'urdu';
}

input:checked + .slider:after
{  
  content:'  حاضر  ';
}

        #contacts_table_wrapper .select2-container{
            width: 100px;
        }
        #contacts_table_wrapper .select2-container span.select2-chosen{
            text-align: right;
        }
        table.table thead .sorting{
            text-align: right;
            background: url('assets/plugins/DataTables/media/css/images/sort_both.png') no-repeat center left;
        }
        table.table thead .sorting_asc{
            text-align: right;
            background: url('assets/plugins/DataTables/media/css/images/sort_asc.png') no-repeat center left;
        }
        table.table thead .sorting_desc{
            text-align: right;
            background: url('assets/plugins/DataTables/media/css/images/sort_desc.png') no-repeat center left;
        }
</style>
</head>
<!-- end: HEAD -->
<!-- start: BODY -->
<body class="rtl">
    <!-- start: HEADER -->
    <div class="navbar navbar-inverse navbar-fixed-top">
        <!-- start: TOP NAVIGATION CONTAINER -->
        <div class="container">
            <div class="navbar-header">
                <!-- start: RESPONSIVE MENU TOGGLER -->
                <button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
                    <span class="clip-list-2"></span>
                </button>
                <!-- end: RESPONSIVE MENU TOGGLER -->
                <!-- end: LOGO -->
            </div>
        </div>
        <!-- end: TOP NAVIGATION CONTAINER -->
    </div>
    <!-- end: HEADER -->

    <!-- start: MAIN CONTAINER -->
    <div class="main-container">
        <div class="navbar-content">
            <!-- start: SIDEBAR -->
            @include('inc.sidebar')
            <!-- end: SIDEBAR -->
        </div>
        <!-- start: PAGE -->
        <div class="main-content">

            <div class="container" style="min-height: 760px;">
                <!-- start: PAGE HEADER -->
                <div class="row">
                    <div class="col-sm-12">
                        <!-- start: PAGE TITLE & BREADCRUMB -->
                        <ol class="breadcrumb">
                            <li>
                                <!--- class="clip-file" -->
                                <i class=""></i>

                                حاضری  برائے  سال  

                            </li>
                            <li class="active">
                            </li>
                        </ol>

                        <div class="page-header">

                            @php
                            $date = date('d');
                            $month = date('m');
                            $month_name = '';
                            $year = date('Y');

                            if($month == 1)
                                $month_name = 'جنوری   ' ;
                            elseif($month == 2)
                                $month_name = ' فروری  ' ;
                            elseif($month == 3)
                                $month_name = '  مارچ   ' ;
                            elseif($month == 4)
                                $month_name = '  اپریل  ' ;
                            elseif($month == 5)
                                $month_name = '  مئی  ' ;
                            elseif($month == 6)
                                $month_name = '  جون  ' ;
                            elseif($month == 7)
                                $month_name = '  جولائی  ' ;
                            elseif($month == 8)
                                $month_name = '  آگست  ' ;
                            elseif($month == 9)
                                $month_name = '  ستمبر   ' ;
                            elseif($month == 10)
                                $month_name = '  اکتوبر   ' ;
                            elseif($month == 11)
                                $month_name = '  نومبر   ' ;
                            elseif($month == 12)
                                $month_name = '  دسمبر   ' ;                            
                            @endphp

                            <h1>

                                حاضری  برائے  {{' ---- '. $date. ' / ' .$month_name.' / '.$year}}


                                <small style="font-size:25px;margin-right:30px;"></small>
                            </h1>
                        </div>
                        <!-- end: PAGE TITLE & BREADCRUMB -->
                    </div>
                </div>
                <!-- end: PAGE HEADER -->
                <!-- start: PAGE CONTENT -->

                @if(Session::has('msg'))
                <div class="col-md-12">
                    <div class="alert alert-success">{{Session::get('msg')}}</div>
                </div>
                @endif

                
                <div class="row">
                    @csrf

                    <div class="col-md-3">
                        <label for="">  تاریخ  حاضری  </label>
                        <input type="date" name="date_hazri" id="date_hazri" class="form-control" >
                    </div>


                    <div class="col-md-3">
                        <label for="">محفل کا نام</label>


                        <select name="mehfils" onchange="get_ehad_karkuns(this)" id="mehfil_id" class="urwasearch form-control">

                                <option value="">  محفل کا نام منتخب کریں۔۔۔  </option>

                            @foreach($all_mehfils as $single_mehfil)
                                <option value="{{$single_mehfil->id}}">{{$single_mehfil->mehfil_name}}</option>
                            @endforeach
                        </select>
                    </div>


                    <div class="col-md-3">
                        <label for="">   عہد کارکن   </label>
                        <select name="ehad_karkun" class="all_ehad_karkuns form-control" id="ehad_karkun_id"></select>
                    </div>


                    <div class="col-md-3">
                        <label> اندراج  </label>
                        <br>
                        <button class="btn btn-primary submit_btn" onclick="submit_btn()"  >
                            اندراج کریں
                        </button>
                    </div>

                </div>
            
                <br>



        <div class="table-responsive">
         <table class="table-bordered" width="100%">
             <thead></thead>
         </table>
         <br><br>

        <div id="contacts_table_wrapper" class="dataTables_wrapper form-inline" role="grid">

                <table class="table table-striped table-bordered table-hover table-full-width" id="contacts_table">
                        <thead>
                            <tr class="m-auto" >
                                <th>   تاریخ   </th>
                                <th>   نام   </th>
                            </tr>
                        </thead>
                        <tbody class="istbody"></tbody>


                </table>

            
            </div>
        </div>

        <!-- end: PAGE CONTENT-->
    </div>
    </div>
    <!-- end: PAGE -->
    </div>
    <!-- end: MAIN CONTAINER -->



    <!-- start: FOOTER & scripts -->
    @include('inc.footer')

    <script type="text/javascript">


    function get_ehad_karkuns(ths) {
        
        var mehfil_id = $(ths).val();
        var date_hazri = $("#date_hazri").val();

        $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('[name="_token"]').val()
          }
        });

        $.ajax({
            url: "{{url('get_all_ehad_karkuns')}}",
            method: 'post',
            dataType: 'JSON',
            data: {mehfil_id : mehfil_id , date_hazri : date_hazri},
            success: function(response){
                $(".all_ehad_karkuns").html(response.options);
                $(".istbody").html(response.tbody);
        }});

    }

    function submit_btn() {
        
        var mehfil_id = $("#mehfil_id").val();
        var ehad_karkun_id = $("#ehad_karkun_id").val();
        var date_hazri = $("#date_hazri").val();

        if(ehad_karkun_id == '' || ehad_karkun_id == null || mehfil_id == ''  || mehfil_id == null){
            alert("   برایے مہربانی تمام   ڈیٹا داخل کریں ۔ ");
            return false;
        }


        $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('[name="_token"]').val()
          }
        });

        $.ajax({
            url: "{{url('insert_hazri_ehad_karkun')}}",
            method: 'post',
            data: {
                    mehfil_id : mehfil_id ,
                    ehad_karkun_id : ehad_karkun_id,
                    date_hazri : date_hazri
                },
            beforeSend: function() {
                $(".submit_btn").html('   ڈیٹا کا اندراج ہو رہا ہے۔۔۔۔   ');                
            },
            success: function(response){
                $(".submit_btn").html(response);
        }});

    }


    </script>
</body>
<!-- end: BODY -->
<script>
// $(document).ready(function() {
//     $(document).find("#name").focus();
// });

$(document).ready(function() {
    // sortTable()
});

</script>

</html>