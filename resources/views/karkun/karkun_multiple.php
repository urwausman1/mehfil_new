<!DOCTYPE html>
<html lang="en" class="no-js">
<!-- start: HEAD -->

<head>
    <title> تمام کارکن </title>

    @include('inc.head')

    <style>
        #contacts_table_wrapper .select2-container {
            width: 100px;
        }

        #contacts_table_wrapper .select2-container span.select2-chosen {
            text-align: right;
        }

        table.table thead .sorting {
            text-align: right;
            background: url('assets/plugins/DataTables/media/css/images/sort_both.png') no-repeat center left;
        }

        table.table thead .sorting_asc {
            text-align: right;
            background: url('assets/plugins/DataTables/media/css/images/sort_asc.png') no-repeat center left;
        }

        table.table thead .sorting_desc {
            text-align: right;
            background: url('assets/plugins/DataTables/media/css/images/sort_desc.png') no-repeat center left;
        }
    </style>
</head>

<!-- end: HEAD -->

<!-- start: BODY -->

<body class="rtl">
    <!-- start: HEADER -->
    <div class="navbar navbar-inverse navbar-fixed-top">
        <!-- start: TOP NAVIGATION CONTAINER -->
        <div class="container">
            <div class="navbar-header">
                <!-- start: RESPONSIVE MENU TOGGLER -->
                <button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
                    <span class="clip-list-2"></span>
                </button>
                <!-- end: RESPONSIVE MENU TOGGLER -->
                <!-- end: LOGO -->
            </div>
        </div>
        <!-- end: TOP NAVIGATION CONTAINER -->
    </div>
    <!-- end: HEADER -->

    <!-- start: MAIN CONTAINER -->
    <div class="main-container">
        <div class="navbar-content">
            <!-- start: SIDEBAR -->
            @include('inc.sidebar')
            <!-- end: SIDEBAR -->
        </div>
        <!-- start: PAGE -->
        <div class="main-content">

            <div class="container" style="min-height: 760px;">
                <!-- start: PAGE HEADER -->
                <div class="row">
                    <div class="col-sm-12">
                        <!-- start: PAGE TITLE & BREADCRUMB -->
                        <ol class="breadcrumb">
                            <li>
                                <!--- class="clip-file" -->
                                <i class=""></i>

                                تمام کارکنان

                            </li>
                            <li class="active">
                            </li>
                        </ol>

                        <div class="page-header">
                            <h1>
                                تمام کارکنان
                                <small style="font-size:25px;margin-right:30px;"></small>
                            </h1>
                        </div>
                        <!-- end: PAGE TITLE & BREADCRUMB -->
                    </div>
                </div>
                <!-- end: PAGE HEADER -->
                <!-- start: PAGE CONTENT -->

                <div class="row">
                    <div class="col-md-12">
                        <!-- start: DYNAMIC TABLE PANEL -->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <i class="fa fa-external-link-square"></i>
                                تمام کارکنان
                            </div>


                            <div class="panel-body">

                                <div class="row">
                                    <div class="col-md-4">
                                        <select>
                                            <option value=""> ملک کا نام </option>
                                        </select>
                                    </div>

                                    <div class="col-md-4">
                                        <select>
                                            <option value=""> شہیر کا نام </option>
                                        </select>
                                    </div>

                                    <div class="col-md-4">
                                        <select>
                                            <option value=""> محفل کا نام </option>
                                        </select>
                                    </div>

                                </div>
                                <div class="table-responsive">
                                    <table class="table-bordered" width="100%">
                                        <thead></thead>
                                    </table>
                                    <br><br>
                                    <table class="editabletable table table-striped table-bordered table-hover table-full-width" id="contacts_table">

                                        <tr>
                                            <td>حاضری کوڈ </td>
                                            <td> نام انگریزی میں </td>
                                            <td> ولدیت انگریزی میں </td>
                                            <td> نام اردو میں </td>
                                            <td> ولدیت اردو میں </td>
                                            <td> موبائل نمبر </td>
                                            <td> شناختی کارڈ نمبر </td>
                                            <td>معرفت </td>
                                            <td> تاریخ ولادت </td>
                                            <td> تاریخ عہد </td>
                                        </tr>

                                        <tr>
                                            <td class="hazri_code">
                                                <div contenteditable></div>
                                            </td>
                                            <td class="name_en">
                                                <div contenteditable></div>
                                            </td>
                                            <td class="fname_en">
                                                <div contenteditable></div>
                                            </td>
                                            <td class="name_ur">
                                                <div contenteditable></div>
                                            </td>
                                            <td class="fname_ur">
                                                <div contenteditable></div>
                                            </td>
                                            <td class="mobile_number">
                                                <div contenteditable></div>
                                            </td>
                                            <td class="cnic">
                                                <div contenteditable></div>
                                            </td>
                                            <td class="marfat">
                                                <div contenteditable></div>
                                            </td>
                                            <td class="dob">
                                                <div contenteditable></div>
                                            </td>
                                            <td class="doe">
                                                <div contenteditable></div>
                                            </td>
                                        </tr>

                                    </table>

                                </div>
                            </div>

                        </div>
                    </div>
                    <!-- end: DYNAMIC TABLE PANEL -->
                </div>
            </div>

            <!-- end: PAGE CONTENT-->
        </div>
    </div>
    <!-- end: PAGE -->
    </div>
    <!-- end: MAIN CONTAINER -->



    <!-- start: FOOTER & scripts -->
    @include('inc.footer')

    <script type="text/javascript">
        $(document).ready(function() {
            $('#example-getting-started').multiselect();
        });
        $(document).ready(function() {
            $(".btn_submit_P").click(function() {
                $(".HazriKarkoonatten").val("P");
            });

            $(".btn_submit_L").click(function() {
                $(".HazriKarkoonatten").val("L");
            });

            $(".btn_submit_M").click(function() {
                $(".HazriKarkoonatten").val("M");
            });
        });
    </script>
</body>
<!-- end: BODY -->

</html>