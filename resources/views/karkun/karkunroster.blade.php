<!DOCTYPE html>
<html lang="en" class="no-js">
    <!-- start: HEAD -->
    <head>
        <title> تمام کارکن  </title>

        @include('inc.head')

        <style>
        #contacts_table_wrapper .select2-container{
            width: 100px;
        }
        #contacts_table_wrapper .select2-container span.select2-chosen{
            text-align: right;
        }
        table.table thead .sorting{
            text-align: right;
            background: url('assets/plugins/DataTables/media/css/images/sort_both.png') no-repeat center left;
        }
        table.table thead .sorting_asc{
            text-align: right;
            background: url('assets/plugins/DataTables/media/css/images/sort_asc.png') no-repeat center left;
        }
        table.table thead .sorting_desc{
            text-align: right;
            background: url('assets/plugins/DataTables/media/css/images/sort_desc.png') no-repeat center left;
        }
        </style>
    </head>

    <!-- end: HEAD -->

    <!-- start: BODY -->
    <body class="rtl">    
        <!-- start: HEADER -->
        <div class="navbar navbar-inverse navbar-fixed-top">
            <!-- start: TOP NAVIGATION CONTAINER -->
            <div class="container">
                <div class="navbar-header">
                    <!-- start: RESPONSIVE MENU TOGGLER -->
                    <button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
                        <span class="clip-list-2"></span>
                    </button>
                    <!-- end: RESPONSIVE MENU TOGGLER -->
                    <!-- end: LOGO -->
                </div>
            </div>
            <!-- end: TOP NAVIGATION CONTAINER -->
        </div>
        <!-- end: HEADER -->

        <!-- start: MAIN CONTAINER -->
        <div class="main-container">
            <div class="navbar-content">
                <!-- start: SIDEBAR -->
                @include('inc.sidebar')
                <!-- end: SIDEBAR -->
            </div>
            <!-- start: PAGE -->
            <div class="main-content">

                <div class="container" style="min-height: 760px;">
                    <!-- start: PAGE HEADER -->
                    <div class="row">
                        <div class="col-sm-12">
                            <!-- start: PAGE TITLE & BREADCRUMB -->

                            <div class="page-header">
                                <h1>
                                    اس مہینے کی حاضری والے تمام کارکنان
                                    <small style="font-size:25px;margin-right:30px;"></small>
                                </h1>
                            </div>
                            <!-- end: PAGE TITLE & BREADCRUMB -->
                        </div>
                    </div>
                    <!-- end: PAGE HEADER -->
                    <!-- start: PAGE CONTENT -->
                    
<div class="row">
<div class="col-md-12">
     

        <div class="table-responsive">
                @if(isset($not_showing) && ($not_showing != '' || $not_showing != null))
                <div class="row">
                    <center>
                        <h1>{{$not_showing}}</h1>
                    </center>
                </div>
                @else

            <table class="table table-striped table-bordered table-hover table-full-width" >
                
                <thead style="font-size: 19px !important;">
                    <tr>
                        <th style="text-align: inherit !important;"> آئی ڈی نمبر </th>
                        <th style="text-align: inherit !important;">نام</th>
                        <th style="text-align: inherit !important;">ولدیت</th>
                        
                        <!-- <th style="text-align: inherit !important;">شناختی کارڈ نمبر</th> -->
                        <th style="text-align: inherit !important;"> فون نمبر  </th>
                        <th>  تفصیل  </th>
                    </tr>
                </thead>

                <tbody>
                    <?php 

                // function getAge($dob,$condate){ 
                //     $birthdate = new DateTime(date("Y-m-d",  strtotime(implode('-', array_reverse(explode('/', $dob))))));
                //     $today= new DateTime(date("Y-m-d",  strtotime(implode('-', array_reverse(explode('/', $condate))))));           
                //     $age = $birthdate->diff($today)->y;

                //     return $age;
                // }

                    ?>


                        @foreach($all_karkun as $single_karkun)
                        <tr>

                            <td class="font_digit">{{$single_karkun->kp_id_number }}</td>
                            <td>{{$single_karkun->kp_name }}</td>
                            <td>{{$single_karkun->kp_fname }}</td>
                            <!-- <td>{{$single_karkun->kp_cnic }}</td> -->
                            <td class="font_digit">{{$single_karkun->kp_phone }}</td>
                            <td></td>
                        </tr>
                        @endforeach
                </tbody>
            </table>
                @endif

</div>
</div>
    <!-- end: DYNAMIC TABLE PANEL -->
</div>
</div>

<!-- end: PAGE CONTENT-->
</div>
</div>
            <!-- end: PAGE -->
</div>
        <!-- end: MAIN CONTAINER -->



        <!-- start: FOOTER & scripts -->
            @include('inc.footer')
        
         <script type="text/javascript">
            $(document).ready(function() {
                $('#example-getting-started').multiselect();
            });
            $(document).ready(function(){
                $(".btn_submit_P").click(function(){
                    $(".HazriKarkoonatten").val("P");
                });

                $(".btn_submit_L").click(function(){
                    $(".HazriKarkoonatten").val("L");
                });             

                $(".btn_submit_M").click(function(){
                    $(".HazriKarkoonatten").val("M");
                });
        });
        </script>
    </body>
    <!-- end: BODY -->
</html>