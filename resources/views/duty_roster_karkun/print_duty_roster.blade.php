<!DOCTYPE html>
<html>

<head>
    <title></title>

    <!-- <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/> -->
    <meta id="myViewport" name="viewport" content="width=1280">

    <link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/main.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/main-responsive.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/rtl-version.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/print.css') }}" type="text/css" media="print" />

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.5.3/jspdf.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.5.3/jspdf.debug.js"></script>

    <script src="https://html2canvas.hertzen.com/dist/html2canvas.js"></script>

<style>
    .footer_text {
	display: block;
	font-size: 20px;
	white-space: normal;
}
.color-red {
	color: red;
}

.duty_roster_print_table.table>thead>tr>th {
	word-spacing: 8px;
}
.naam_mubarak {
    width: 300px;
    height: auto;
    margin: 0;
}

.mehfil_num {
	font-size: 22px;
	margin-bottom: 30px;
	margin-right: 6px;
	font-weight: 600;
}
</style>
    <script>
        var pdf, page_section, HTML_Width, HTML_Height, top_left_margin, PDF_Width, PDF_Height, canvas_image_width,
            canvas_image_height;



        function calculatePDF_height_width(selector, index) {
            page_section = $(selector).eq(index);
            HTML_Width = page_section.width();
            HTML_Height = page_section.height();
            top_left_margin = 15;
            PDF_Width = HTML_Width + (top_left_margin * 2);
            PDF_Height = (PDF_Width * 1.2) + (top_left_margin * 2);
            canvas_image_width = HTML_Width;
            canvas_image_height = HTML_Height;
        }



        //Generate PDF
        function generatePDF() {
            pdf = "";
            $("#downloadbtn").hide();
            $("#genmsg").show();
            html2canvas($(".container")[0], {
                allowTaint: true
            }).then(function(canvas) {

                calculatePDF_height_width(".container", 0);
                var imgData = canvas.toDataURL("image/png", 1.0);

                pdf = new jsPDF('p', 'pt', [PDF_Width, PDF_Height]);

                pdf.addImage(imgData, 'JPG', top_left_margin, top_left_margin, HTML_Width, HTML_Height);
                pdf.save("dutyroster.pdf");
            });


            // html2canvas($(".container:eq(1)")[0], { allowTaint: true }).then(function(canvas) {

            //     calculatePDF_height_width(".container",1);

            //     var imgData = canvas.toDataURL("image/png", 1.0);
            //     pdf.addPage(PDF_Width, PDF_Height);
            //     pdf.addImage(imgData, 'JPG', top_left_margin, top_left_margin, HTML_Width, HTML_Height);

            //             pdf.save("HTML-Document.pdf");


            // });


            // html2canvas($(".container:eq(2)")[0], { allowTaint: true }).then(function(canvas) {

            //     calculatePDF_height_width(".container",2);

            //     var imgData = canvas.toDataURL("image/png", 1.0);
            //     pdf.addPage(PDF_Width, PDF_Height);
            //     pdf.addImage(imgData, 'JPG', top_left_margin, top_left_margin, HTML_Width, HTML_Height);



            //         //console.log((page_section.length-1)+"==="+index);
            //         setTimeout(function() {

            //             //Save PDF Doc  
            //             pdf.save("HTML-Document.pdf");

            //             //Generate BLOB object
            //             var blob = pdf.output("blob");

            //             //Getting URL of blob object
            //             var blobURL = URL.createObjectURL(blob);

            //             //Showing PDF generated in iFrame element
            //             var iframe = document.getElementById('sample-pdf');
            //             iframe.src = blobURL;

            //             //Setting download link
            //             var downloadLink = document.getElementById('pdf-download-link');
            //             downloadLink.href = blobURL;

            //     $("#sample-pdf").slideDown();


            //     $("#downloadbtn").show();
            //     $("#genmsg").hide();
            //           }, 0);
            //   });
        }
        // var filename = "murshad.pdf"; //
        // var options1 = {
        //     //background: '#fff',
        //     pagesplit: true,
        // };

        // var doc1 =  new jsPDF('p', 'pt', 'a4', true);
        // doc1.internal.scaleFactor = 2.20; 
        // $('#myViewport').attr('content','width=1280');
        //  doc1.addHTML($('.murshad_mohsin'), options1, function() {
        //  //alert('1'); 
        //   //doc.save(filename + ".pdf");    

        // var pdf1  = doc1.output();
        // var reqData = btoa(pdf1); 
        // doc1.save( filename , true);
        // });

    </script>
</head>

<body>
    <div class="main-container main-container-flex">
        <!-- <button onclick="generatePDF()">  پرنٹ نکالیں   </button> -->
        <div class="">
            <img src="../../assets/images/borderimg1.jpg" class="bg_border" />
            <div class="row">
                <div class="col-md-12" id="forborder">
                    <!-- <div class="" id="forborder" style="border: 10px solid black; border-style: dashed;"> -->
                    <div id="table_header">
                        <!-- <h3 style="font-size: 25px;">السلام عليكم ورحمة الله وبركاته</h3> -->
                        <h2 class="salam">السلام علیکم ورحمۃ اللہ و برکاتہ </h2>
                        <img class="naam_mubarak" src="{{ asset('assets/images/header_name_new.png') }}" width="300px" />
                        <span class="mehfil_num">
                            کی 
                            <!-- {{ $single_mehfil->mehfil_number . ' ' }} - -->
                            <!-- {{ $single_mehfil->mehfil_address }} -->
                            {{ $single_mehfil->mehfil_name }}
                        </span>
                        @php
                        $date = date('d');
                        $month = date('m');
                        $month_name = '';
                        $year = date('Y');

                        if($month == 1)
                        $month_name = 'جنوری ' ;
                        elseif($month == 2)
                        $month_name = ' فروری ' ;
                        elseif($month == 3)
                        $month_name = ' مارچ ' ;
                        elseif($month == 4)
                        $month_name = ' اپریل ' ;
                        elseif($month == 5)
                        $month_name = ' مئی ' ;
                        elseif($month == 6)
                        $month_name = ' جون ' ;
                        elseif($month == 7)
                        $month_name = ' جولائی ' ;
                        elseif($month == 8)
                        $month_name = ' آگست ' ;
                        elseif($month == 9)
                        $month_name = ' ستمبر ' ;
                        elseif($month == 10)
                        $month_name = ' اکتوبر ' ;
                        elseif($month == 11)
                        $month_name = ' نومبر ' ;
                        elseif($month == 12)
                        $month_name = ' دسمبر ' ;
                        @endphp

                        <h3 class="duty_schedule"><span class="mr-2">ڈیوٹی شیڈول برائے</span>
                            -{{ $month_name . $year }}
                        </h3>
                    </div>
                    {{-- <div class="col-md-1"></div>
                    --}}
                    <div class="col-md-12">
                        <table class="table duty_roster_print_table" id="myTable">
                            <thead>
                                <tr>
                                    <th class="text-center">دن</th>
                                    <th class="text-center">عہد کارکن</th>
                                    <th class="text-center">مین ڈیوٹی  گیٹیاں</th>
                                    <th class="text-center">کمپیوٹر</th>
                                    <th class="text-center">سیکورٹی</th>
                                </tr>
                            </thead>
                            <tbody>

                                @php
                                $week_day_name = array(

                                'friday' => 'جمعہ',

                                'saturday' => 'ہفتہ',

                                'sunday' => 'اتوار',

                                'monday' => 'سوموار',

                                'tuesday' => 'منگل',

                                'wednesday' => 'بدھ',

                                'thursday' => 'جمعرات'

                                );

                                foreach ($week_day_name as $key => $day){

                                $monthly_co_arr = [];

                                foreach($karkun_duty_rosters as $single_duty){

                                if($single_duty->k_duty_days != $key){
                                continue;
                                }
                                @endphp
                                <tr>
                                    <td>
                                        <div>
                                            {{ $day }}
                                        </div>
                                    </td>
                                    <td>
                                        <div>
                                            {{ $single_duty->ekp_name }}
                                            <br>
                                            @php
                                            $remove92 = substr($single_duty->ekp_phone , "2");
                                            @endphp
                                            <span class="font-10">{{ '0' . $remove92 }}</span>
                                        </div>
                                    </td>

                                    <td class="float_unset">
                                        <div class="col-xs-12">
                                            @php
                                            $co = '';
                                            $day_coordinator = $single_duty->day_coordinator;
                                            $main_duty = explode(',' , $single_duty->k_main_gatian);

                                            $col = 0;
                                            if(sizeof($main_duty) == 2){
                                                $col = 6;
                                            }elseif(sizeof($main_duty) == 3){
                                                $col = 4;
                                            }else{
                                                $col = 3;
                                            }

                                            foreach($main_duty as $single_md){
                                            $all_karkun = DB::table('karkuns')->where('id' ,
                                            $single_md)->first();
                                            @endphp

                                            <div class="col-xs-{{$col}}">
                                                @php
                                                if($day_coordinator == $single_md)
                                                {
                                                $co = "underline_co";
                                                @endphp
                                                <span class="{{ $co }}">

                                                    {{ $all_karkun->kp_name }}
                                                    <span class="font-10">({{ $all_karkun->kp_id_number }})</span>

                                                </span>
                                                @php }else{ @endphp

                                                {{ $all_karkun->kp_name }}
                                                <span class="font-10">({{ $all_karkun->kp_id_number }})</span>

                                                @php } @endphp

                                                <!-- <br> -->
                                                <!-- <span class="font-10">{{ $all_karkun->kp_phone }}</span> -->
                                            </div>
                                            @php } @endphp
                                        </div>
                                    </td>

                                    <td class="float_unset">
                                        <div class="col-xs-12">
                                            @php
                                            $k_cycle_stand = explode(',' , $single_duty->k_cycle_stand);
                                            foreach($k_cycle_stand as $single_md){
                                            $all_karkun = DB::table('karkuns')->where('id' ,
                                            $single_md)->first();
                                            @endphp
                                            <div class="col-xs-12">

                                                @php
                                                if($day_coordinator == $single_md)
                                                {
                                                $co = "underline_co";
                                                @endphp

                                                <span class="{{ $co }}">

                                                    {{ $all_karkun->kp_name }}
                                                    <span class="font-10">({{ $all_karkun->kp_id_number }})</span>

                                                </span>
                                                @php }else{ @endphp

                                                {{ $all_karkun->kp_name }}
                                                <span class="font-10">({{ $all_karkun->kp_id_number }})</span>

                                                @php } @endphp
                                                <!-- <br> -->
                                                <!-- <span class="font-10">{{ str_replace('92-', '0', $all_karkun->kp_phone) }}</span> -->
                                            </div>
                                            @php } @endphp
                                        </div>
                                    </td>


                                    <td>
                                        <div class="col-xs-12">
                                            @php
                                            $k_security = explode(',' , $single_duty->k_security);

                                            foreach($k_security as $single_md){
                                            $all_karkun = DB::table('karkuns')->where('id' ,
                                            $single_md)->first();

                                            @endphp

                                            <div class="col-xs-12">

                                                @php
                                                if($day_coordinator == $single_md)
                                                {
                                                $co = "underline_co";
                                                @endphp
                                                <span class="{{ $co }}">

                                                    {{ $all_karkun->kp_name }}

                                                    <span class="font-10">({{ $all_karkun->kp_id_number }})</span>

                                                </span>

                                                @php }else{ @endphp

                                                {{ $all_karkun->kp_name }}
                                                <span class="font-10">({{ $all_karkun->kp_id_number }})</span>

                                                @php } @endphp

                                                <!-- <br> -->
                                                <!-- <span class="font-10">{{ str_replace('92-', '0', $all_karkun->kp_phone) }}</span> -->
                                            </div>

                                            @php } @endphp
                                        </div>

                                        @php
                                        $monthly_cordinator = explode(',' , $single_duty->monthly_cordinator);

                                        foreach($monthly_cordinator as $single_md){
                                        $all_karkun_coord = DB::table('karkuns')->where('id' , $single_md)->first();

                                        $monthly_co_arr[] = $all_karkun_coord;
                                        //array_push($monthly_co_arr , $all_karkun_coord->kp_name);
                                        }
                                        @endphp

                                    </td>

                                </tr>

                                @php } } @endphp

                                <tr class="noBorder">
                                    <td colspan="5">
                                        <div class="row">
                                            <div class="col-xs-2">
                                                <span class="coordinator_txt">
                                                    کو آرڈینیٹر
                                                </span>
                                            </div>
                                            <div class="col-xs-4">
                                                @foreach ($monthly_co_arr as $single)
                                                    <div class="vl font-20">
                                                        {{ $single->kp_name }}
                                                        <br>
                                                        <!-- <span style="font-size: 12px;">({{ '1' }})</span> -->
                                                        <span class="font-10">{{ $single->kp_phone }}</span>
                                                    </div>
                                                @endforeach
                                            </div>
                                            <div class="col-xs-6">
                                                <p id="footer_text" class="footer_text">
                                                    <span class="color-red">نوٹ:</span>
                                                    تمام بھائ اپنی ڈیوٹی والے دن محفل سے 15 سے 20 منٹ پہلے
                                                    آئیں،محفل پر ماہانہ %50 حاضری کو یقینی بنائیں
                                                </p>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            $(document).ready(function() {
                // generatePDF();
            });

        </script>
</body>

</html>
