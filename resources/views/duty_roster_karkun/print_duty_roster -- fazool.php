        @include('inc.head')

<link rel="stylesheet" href="{{asset('assets/plugins/bootstrap/css/bootstrap.min.css')}}">
<!-- start: PAGE CONTENT -->
<style type="text/css">
  .name-wrapper {
    display: inline-block; 
    max-width: 150px;
  }
  .name-wrapper .name,.name-wrapper .phone {
    display: block;
    white-space: initial;
    word-break: break-all;
  }
  .name-wrapper .name{
    font-size: 18px;
  }
  .name-wrapper .phone{
  	font-size: 9px;
  }
  td{
  	height: 100px;
  }
</style>

<div class="row">

  <div class="col-md-12">

    <div class="panel panel-default">

      <div class="">
        <div id="table_header" style="display: none;text-align: center !important;">
          <h1><img src="{{asset('assets/images/flag.jpg')}}" width="150" /></h1>
          <!-- <h3 style="font-size: 25px;">السلام عليكم ورحمة الله وبركاته</h3> -->
          <h1><img src="{{asset('assets/images/header_name.png')}}" width="300" style="margin-top: -20px;" /> <span style="font-size: 25px;font-family: 'urdu' !important;"> کی    


              
            </span>

          </h1>
          
          <h3 style="font-size: 20px;margin-top: -10px;">ڈیوٹی شیڈول     
          

              
          </h3>
        </div>
        

        <div class="col-md-12">
          <div class="">          


    <table class="table-bordered" width="100%">
    <thead></thead>
    </table>
<div class="col-md-12">
<table class="table-bordered" id="dr-table" width="100%">

  <thead>
    <tr>
      <td class="t-day">دن</td>
      <td class="t-ehad-ca">عہد کارکن</td>
      <!-- <td class="t-day-co">ڈے کوارڈینیٹر</td> -->
      <td class="t-main-duty">مین ڈیوٹی گیٹیاں</td>
      <td class="t-naat">کمپیوٹر</td>
      <td class="t-cycle">سیکورٹی</td>
    </tr>

  </thead>
  <tbody>
    <tr>
      <td class="t-day">
          <div class="text-center">
              جمعہ
          </div>
      </td>

       <td class="t-ehad-ca">
            <select id="form-field-select-3" name="ehd_karkun_juma" class="form-control search-select select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                @if($all_k_duty_roster_friday)
                <option value="{{$all_k_duty_roster_friday->ehad_karkun}}">
                  {{$all_k_duty_roster_friday->ekp_name}}
                </option>
              @else
                <option value="">  عہد  کارکن۔۔۔ </option>
                @endif

                @foreach($all_ehad_karkun as $single_ehad_karkun)
                    <option value="{{$single_ehad_karkun->id}}">{{$single_ehad_karkun->ekp_name}}</option>
                @endforeach
            </select>
       </td>

      <td class="t-main-duty">
            <select multiple name="main_duty_juma[]" id="form-field-select-4" class="urwajuma_md form-control search-select select2-hidden-accessible" tabindex="-1" aria-hidden="true">

                @foreach($all_karkun as $single_karkun)
                    @php 
                      $fmd_id = $single_karkun->id; 
                      @endphp
                    <option 
                    @if(isset($fmd))
                      @if(in_array($fmd_id , $fmd))
                        selected="selected" 
                      @endif
                    @endif
                    value="{{$single_karkun->id}}">

                        {{$single_karkun->kp_name}}

                    </option>
                @endforeach
            </select>
      </td>

      <td class="t-naat">
            <select multiple name="cycle_stand_juma[]" id="form-field-select-4" class="urwajuma_cs form-control search-select select2-hidden-accessible" tabindex="-1" aria-hidden="true">

                @foreach($all_karkun as $single_karkun)
                      @php 
                        $fcs_id = $single_karkun->id; 
                      @endphp
                    <option 

                      @if(in_array($fcs_id , $fcs))
                        selected="selected" 
                      @endif

                      value="{{$single_karkun->id}}">

                        {{$single_karkun->kp_name}}

                    </option>
                @endforeach

            </select>
      </td>

      <td class="t-cycle">
            <select multiple name="security_juma[]" id="form-field-select-4" class="urwajuma_s form-control search-select select2-hidden-accessible" tabindex="-1" aria-hidden="true">

                @foreach($all_karkun as $single_karkun)
                      @php 
                        $fs_id = $single_karkun->id; 
                      @endphp
                    <option 

                      @if(in_array($fs_id , $fs))
                        selected="selected" 
                      @endif

                      value="{{$single_karkun->id}}">

                        {{$single_karkun->kp_name}}

                    </option>
                @endforeach


            </select>
      </td>

    </tr>

    <tr>
      <td class="t-day">
          <div class="text-center">
              ہفتہ
          </div>
      </td>

       <td class="t-ehad-ca">
            <select id="form-field-select-3" name="ehd_karkun_hafta" class="form-control search-select select2-hidden-accessible" tabindex="-1" aria-hidden="true">

              @if($all_k_duty_roster_saturday)
                <option value="{{$all_k_duty_roster_saturday->ehad_karkun}}">
                  {{$all_k_duty_roster_saturday->ekp_name}}
                </option>
              @else
                <option value="">  عہد  کارکن۔۔۔ </option>
              @endif
                @foreach($all_ehad_karkun as $single_ehad_karkun)
                    <option value="{{$single_ehad_karkun->id}}">{{$single_ehad_karkun->ekp_name}}</option>
                @endforeach
            </select>
       </td>

      <td class="t-main-duty">
            <select multiple id="form-field-select-4" name="main_duty_hafta[]" class="form-control search-select select2-hidden-accessible" tabindex="-1" aria-hidden="true">

                @foreach($all_karkun as $single_karkun)
                      @php 
                        $smd_id = $single_karkun->id; 
                      @endphp
                    <option 

                      @if(in_array($smd_id , $smd))
                        selected="selected" 
                      @endif

                      value="{{$single_karkun->id}}">

                        {{$single_karkun->kp_name}}

                    </option>
                @endforeach

            </select>
      </td>

      <td class="t-naat">
            <select multiple id="form-field-select-4" name="cycle_stand_hafta[]" class="form-control search-select select2-hidden-accessible" tabindex="-1" aria-hidden="true">

                @foreach($all_karkun as $single_karkun)
                      @php 
                        $scs_id = $single_karkun->id; 
                      @endphp
                    <option 

                      @if(in_array($scs_id , $scs))
                        selected="selected" 
                      @endif

                      value="{{$single_karkun->id}}">

                        {{$single_karkun->kp_name}}

                    </option>
                @endforeach

            </select>
      </td>

      <td class="t-cycle">
            <select multiple id="form-field-select-4" name="security_hafta[]" class="form-control search-select select2-hidden-accessible" tabindex="-1" aria-hidden="true">

                @foreach($all_karkun as $single_karkun)
                      @php 
                        $ss_id = $single_karkun->id; 
                      @endphp
                    <option 

                      @if(in_array($ss_id , $ss))
                        selected="selected" 
                      @endif

                      value="{{$single_karkun->id}}">

                        {{$single_karkun->kp_name}}

                    </option>
                @endforeach

            </select>
      </td>
    </tr>
  
    <tr>
      <td class="t-day">
          <div class="text-center">
              اتوار
          </div>
      </td>

       <td class="t-ehad-ca">
            <select id="form-field-select-3" name="ehd_karkun_itwar" class="form-control search-select select2-hidden-accessible" tabindex="-1" aria-hidden="true">

              @if($all_k_duty_roster_sunday)
                <option value="{{$all_k_duty_roster_sunday->ehad_karkun}}">
                  {{$all_k_duty_roster_sunday->ekp_name}}
                </option>
              @else
                <option value="">  عہد  کارکن۔۔۔ </option>
              @endif

                @foreach($all_ehad_karkun as $single_ehad_karkun)
                    <option value="{{$single_ehad_karkun->id}}">{{$single_ehad_karkun->ekp_name}}</option>
                @endforeach

            </select>
       </td>

      <td class="t-main-duty">

            <select multiple id="form-field-select-4" name="main_duty_itwar[]" class="form-control search-select select2-hidden-accessible" tabindex="-1" aria-hidden="true">

                @foreach($all_karkun as $single_karkun)
                      @php 
                        $snmd_id = $single_karkun->id; 
                      @endphp
                    <option 

                      @if(in_array($snmd_id , $snmd))
                        selected="selected" 
                      @endif

                      value="{{$single_karkun->id}}">

                        {{$single_karkun->kp_name}}

                    </option>
                @endforeach

            </select>

      </td>

      <td class="t-naat">
            <select multiple id="form-field-select-4" name="cycle_stand_itwar[]" class="form-control search-select select2-hidden-accessible" tabindex="-1" aria-hidden="true">

                @foreach($all_karkun as $single_karkun)
                      @php 
                        $sncs_id = $single_karkun->id; 
                      @endphp
                    <option 

                      @if(in_array($sncs_id , $sncs))
                        selected="selected" 
                      @endif

                      value="{{$single_karkun->id}}">

                        {{$single_karkun->kp_name}}

                    </option>
                @endforeach

            </select>      
      </td>

      <td class="t-cycle">
            <select multiple id="form-field-select-4" name="security_itwar[]" class="form-control search-select select2-hidden-accessible" tabindex="-1" aria-hidden="true">

                @foreach($all_karkun as $single_karkun)
                      @php 
                        $sns_id = $single_karkun->id; 
                      @endphp
                    <option 

                      @if(in_array($sns_id , $sns))
                        selected="selected" 
                      @endif

                      value="{{$single_karkun->id}}">

                        {{$single_karkun->kp_name}}

                    </option>
                @endforeach

            </select>
      </td>

    </tr>
  
    <tr>
      <td class="t-day">
          <div class="text-center">
              سوموار
          </div>
      </td>

       <td class="t-ehad-ca">
            <select id="form-field-select-3" name="ehd_karkun_peer" class="form-control search-select select2-hidden-accessible" tabindex="-1" aria-hidden="true">

              @if($all_k_duty_roster_monday)
                <option value="{{$all_k_duty_roster_monday->ehad_karkun}}">
                  {{$all_k_duty_roster_monday->ekp_name}}
                </option>
              @else
                <option value="">  عہد  کارکن۔۔۔ </option>
              @endif

                @foreach($all_ehad_karkun as $single_ehad_karkun)
                    <option value="{{$single_ehad_karkun->id}}">{{$single_ehad_karkun->ekp_name}}</option>
                @endforeach
            </select>
       </td>

      <td class="t-main-duty">
            <select multiple id="form-field-select-4" name="main_duty_peer[]" class="form-control search-select select2-hidden-accessible" tabindex="-1" aria-hidden="true">

                @foreach($all_karkun as $single_karkun)
                      @php 
                        $mmd_id = $single_karkun->id; 
                      @endphp
                    <option 

                      @if(in_array($mmd_id , $mmd))
                        selected="selected" 
                      @endif

                      value="{{$single_karkun->id}}">

                        {{$single_karkun->kp_name}}

                    </option>
                @endforeach

            </select>
      </td>

      <td class="t-naat">
            <select multiple id="form-field-select-4" name="cycle_stand_peer[]" class="form-control search-select select2-hidden-accessible" tabindex="-1" aria-hidden="true">

                @foreach($all_karkun as $single_karkun)
                      @php 
                        $mcs_id = $single_karkun->id; 
                      @endphp
                    <option 

                      @if(in_array($mcs_id , $mcs))
                        selected="selected" 
                      @endif

                      value="{{$single_karkun->id}}">

                        {{$single_karkun->kp_name}}

                    </option>
                @endforeach

            </select>
      </td>

      <td class="t-cycle">
            <select multiple id="form-field-select-4" name="security_peer[]" class="form-control search-select select2-hidden-accessible" tabindex="-1" aria-hidden="true">

                @foreach($all_karkun as $single_karkun)
                      @php 
                        $ms_id = $single_karkun->id; 
                      @endphp
                    <option 

                      @if(in_array($ms_id , $ms))
                        selected="selected" 
                      @endif

                      value="{{$single_karkun->id}}">

                        {{$single_karkun->kp_name}}

                    </option>
                @endforeach

            </select>
      </td>

    </tr>
  
    <tr>
      <td class="t-day">
          <div class="text-center">
              منگل
          </div>
      </td>

       <td class="t-ehad-ca">
            <select id="form-field-select-3" name="ehd_karkun_mangal" class="form-control search-select select2-hidden-accessible" tabindex="-1" aria-hidden="true">

              @if($all_k_duty_roster_tuesday)
                <option value="{{$all_k_duty_roster_tuesday->ehad_karkun}}">
                  {{$all_k_duty_roster_tuesday->ekp_name}}
                </option>
              @else
                <option value="">  عہد  کارکن۔۔۔ </option>
              @endif

                @foreach($all_ehad_karkun as $single_ehad_karkun)
                    <option value="{{$single_ehad_karkun->id}}">{{$single_ehad_karkun->ekp_name}}</option>
                @endforeach
            </select>
       </td>

      <td class="t-main-duty">
            <select multiple id="form-field-select-4" name="main_duty_mangal[]" class="form-control search-select select2-hidden-accessible" tabindex="-1" aria-hidden="true">

                @foreach($all_karkun as $single_karkun)
                      @php 
                        $tmd_id = $single_karkun->id; 
                      @endphp
                    <option 

                      @if(in_array($tmd_id , $tmd))
                        selected="selected" 
                      @endif

                      value="{{$single_karkun->id}}">

                        {{$single_karkun->kp_name}}

                    </option>
                @endforeach

            </select>
      </td>

      <td class="t-naat">
            <select multiple id="form-field-select-4" name="cycle_stand_mangal[]" class="form-control search-select select2-hidden-accessible" tabindex="-1" aria-hidden="true">

                @foreach($all_karkun as $single_karkun)
                      @php 
                        $tcs_id = $single_karkun->id; 
                      @endphp
                    <option 

                      @if(in_array($tcs_id , $tcs))
                        selected="selected" 
                      @endif

                      value="{{$single_karkun->id}}">

                        {{$single_karkun->kp_name}}

                    </option>
                @endforeach

            </select>
      </td>

      <td class="t-cycle">
            <select multiple id="form-field-select-4" name="security_mangal[]" class="form-control search-select select2-hidden-accessible" tabindex="-1" aria-hidden="true">

                @foreach($all_karkun as $single_karkun)
                      @php 
                        $ts_id = $single_karkun->id; 
                      @endphp
                    <option 

                      @if(in_array($ts_id , $ts))
                        selected="selected" 
                      @endif

                      value="{{$single_karkun->id}}">

                        {{$single_karkun->kp_name}}

                    </option>
                @endforeach

            </select>
      </td>
    </tr>

    <tr>
      <td class="t-day">
          <div class="text-center">
              بدھ
          </div>
      </td>

    

       <td class="t-ehad-ca">
            <select id="form-field-select-3" name="ehd_karkun_budh" class="form-control search-select select2-hidden-accessible" tabindex="-1" aria-hidden="true">

              @if($all_k_duty_roster_wednesday)
                <option value="{{$all_k_duty_roster_wednesday->ehad_karkun}}">
                  {{$all_k_duty_roster_wednesday->ekp_name}}
                </option>
              @else
                <option value="">  عہد  کارکن۔۔۔ </option>
              @endif

                @foreach($all_ehad_karkun as $single_ehad_karkun)
                    <option value="{{$single_ehad_karkun->id}}">{{$single_ehad_karkun->ekp_name}}</option>
                @endforeach
            </select>
       </td>

      <td class="t-main-duty">
            <select multiple id="form-field-select-4" name="main_duty_budh[]" class="form-control search-select select2-hidden-accessible" tabindex="-1" aria-hidden="true">

                @foreach($all_karkun as $single_karkun)
                      @php 
                        $wmd_id = $single_karkun->id; 
                      @endphp
                    <option 

                      @if(in_array($wmd_id , $wmd))
                        selected="selected" 
                      @endif

                      value="{{$single_karkun->id}}">

                        {{$single_karkun->kp_name}}

                    </option>
                @endforeach

            </select>
      </td>

      <td class="t-naat">
            <select multiple id="form-field-select-4" name="cycle_stand_budh[]" class="form-control search-select select2-hidden-accessible" tabindex="-1" aria-hidden="true">

                @foreach($all_karkun as $single_karkun)
                      @php 
                        $wcs_id = $single_karkun->id; 
                      @endphp
                    <option 

                      @if(in_array($wcs_id , $wcs))
                        selected="selected" 
                      @endif

                      value="{{$single_karkun->id}}">

                        {{$single_karkun->kp_name}}

                    </option>
                @endforeach

            </select>
      </td>

      <td class="t-cycle">
            <select multiple id="form-field-select-4" name="security_budh[]" class="form-control search-select select2-hidden-accessible" tabindex="-1" aria-hidden="true">

                @foreach($all_karkun as $single_karkun)
                      @php 
                        $ws_id = $single_karkun->id; 
                      @endphp
                    <option 

                      @if(in_array($ws_id , $ws))
                        selected="selected" 
                      @endif

                      value="{{$single_karkun->id}}">

                        {{$single_karkun->kp_name}}

                    </option>
                @endforeach
            </select>
      </td>

    </tr>
  
    <tr>
      <td class="t-day">
          <div class="text-center">
              جمعرات
          </div>
      </td>

        <td class="t-ehad-ca"> 
            <select id="form-field-select-3" name="ehd_karkun_jumerat" class="form-control search-select select2-hidden-accessible" tabindex="-1" aria-hidden="true">

              @if($all_k_duty_roster_thursday)
                <option value="{{$all_k_duty_roster_thursday->ehad_karkun}}">
                  {{$all_k_duty_roster_thursday->ekp_name}}
                </option>
              @else
                <option value="">  عہد  کارکن۔۔۔ </option>
              @endif

                @foreach($all_ehad_karkun as $single_ehad_karkun)
                    <option value="{{$single_ehad_karkun->id}}">{{$single_ehad_karkun->ekp_name}}</option>
                @endforeach
            </select>
        </td>

        <td class="t-main-duty">
            <select multiple id="form-field-select-4" name="main_duty_jumerat[]" class="form-control search-select select2-hidden-accessible" tabindex="-1" aria-hidden="true">

                @foreach($all_karkun as $single_karkun)
                      @php 
                        $thmd_id = $single_karkun->id; 
                      @endphp
                    <option 

                      @if(in_array($thmd_id , $thmd))
                        selected="selected" 
                      @endif

                      value="{{$single_karkun->id}}">

                        {{$single_karkun->kp_name}}

                    </option>
                @endforeach

            </select>
        </td>

      <td class="t-naat">
            <select multiple id="form-field-select-4" name="cycle_stand_jumerat[]" class="form-control search-select select2-hidden-accessible" tabindex="-1" aria-hidden="true">

                @foreach($all_karkun as $single_karkun)
                      @php 
                        $thcs_id = $single_karkun->id; 
                      @endphp
                    <option 

                      @if(in_array($thcs_id , $thcs))
                        selected="selected" 
                      @endif

                      value="{{$single_karkun->id}}">

                        {{$single_karkun->kp_name}}

                    </option>
                @endforeach

            </select>
      </td>

      <td class="t-cycle">
            <select multiple id="form-field-select-4" name="security_jumerat[]" class="form-control search-select select2-hidden-accessible" tabindex="-1" aria-hidden="true">

                @foreach($all_karkun as $single_karkun)
                      @php 
                        $ths_id = $single_karkun->id; 
                      @endphp
                    <option 

                      @if(in_array($ths_id , $ths))
                        selected="selected" 
                      @endif

                      value="{{$single_karkun->id}}">

                        {{$single_karkun->kp_name}}

                    </option>
                @endforeach

            </select>
      </td>
    </tr>

    <tr>
        <td class="t-day">
          <div class="text-center">
          ماہانہ کوارڈینیٹر
          </div>
        </td>

      <td colspan="4">
            <select multiple id="form-field-select-4" name="mahana_coordinator[]" class="form-control search-select select2-hidden-accessible" tabindex="-1" aria-hidden="true">


              @foreach($all_ehad_karkun as $single_ehad_karkun)
                      @php 
                        $m_c_id = $single_ehad_karkun->id; 
                      @endphp
                    <option 

                      @if(in_array($m_c_id , $m_c))
                        selected="selected" 
                      @endif

                      value="{{$single_ehad_karkun->id}}">

                        {{$single_ehad_karkun->ekp_name}}

                    </option>
                @endforeach

            </select>
      </td>
  </tr>
  </tbody>
  </table>
  <h1 id="footer_text" style="display: none;font-size: 20px;">
      <span style="color: red;">نوٹ:</span>
       تمام بھائ اپنی ڈیوٹی والے دن محفل والے ٹائم سے 15 سے 20 منٹ پہلے آئیں،محفل پر ماہانہ %50 حاضری کو یقینی بنائیں ملتان شریف منگل ڈیوٹی پر ہر دو ماہ میں ایک بار جانا ضروری ہے۔
  </h1>
  





          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script src="{{asset('assets/js/jquery.js')}}"></script>
<script type="text/javascript">
  $(document).ready(function(){
    window.print();
    setTimeout("closePrintView()", 1000);
    
  });

</script>

            @include('inc.footer')
