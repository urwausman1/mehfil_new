<!DOCTYPE html>
<html>
<head>
  <title></title>

  <!-- <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/> -->

    <link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/main.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/main-responsive.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/rtl-version.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/print.css') }}" type="text/css" media="print" />

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.3/jspdf.min.js"></script>
    <script src="https://html2canvas.hertzen.com/dist/html2canvas.js"></script>



<script>
  var pdf,page_section,HTML_Width,HTML_Height,top_left_margin,PDF_Width,PDF_Height,canvas_image_width,canvas_image_height;
  
  
  
  function calculatePDF_height_width(selector,index){
    page_section = $(selector).eq(index);
    HTML_Width = page_section.width();
    HTML_Height = page_section.height();
    top_left_margin = 15;
    PDF_Width = HTML_Width + (top_left_margin * 2);
    PDF_Height = (PDF_Width * 1.2) + (top_left_margin * 2);
    canvas_image_width = HTML_Width;
    canvas_image_height = HTML_Height;
  }
  



    //Generate PDF
    function generatePDF() {
        pdf = "";
    $("#downloadbtn").hide();
    $("#genmsg").show();
        html2canvas($(".container")[0], { allowTaint: true }).then(function(canvas) {

            calculatePDF_height_width(".container",0);
            var imgData = canvas.toDataURL("image/png", 1.0);

            pdf = new jsPDF('p', 'pt', [PDF_Width, PDF_Height]);

            pdf.addImage(imgData, 'JPG', top_left_margin, top_left_margin, HTML_Width, HTML_Height);
            pdf.save("dutyroster.pdf");
        });

        // html2canvas($(".container:eq(1)")[0], { allowTaint: true }).then(function(canvas) {

        //     calculatePDF_height_width(".container",1);
      
        //     var imgData = canvas.toDataURL("image/png", 1.0);
        //     pdf.addPage(PDF_Width, PDF_Height);
        //     pdf.addImage(imgData, 'JPG', top_left_margin, top_left_margin, HTML_Width, HTML_Height);

        //             pdf.save("HTML-Document.pdf");


        // });


        // html2canvas($(".container:eq(2)")[0], { allowTaint: true }).then(function(canvas) {

        //     calculatePDF_height_width(".container",2);
      
        //     var imgData = canvas.toDataURL("image/png", 1.0);
        //     pdf.addPage(PDF_Width, PDF_Height);
        //     pdf.addImage(imgData, 'JPG', top_left_margin, top_left_margin, HTML_Width, HTML_Height);


           
        //         //console.log((page_section.length-1)+"==="+index);
        //         setTimeout(function() {

        //             //Save PDF Doc  
        //             pdf.save("HTML-Document.pdf");

        //             //Generate BLOB object
        //             var blob = pdf.output("blob");

        //             //Getting URL of blob object
        //             var blobURL = URL.createObjectURL(blob);

        //             //Showing PDF generated in iFrame element
        //             var iframe = document.getElementById('sample-pdf');
        //             iframe.src = blobURL;

        //             //Setting download link
        //             var downloadLink = document.getElementById('pdf-download-link');
        //             downloadLink.href = blobURL;

        //     $("#sample-pdf").slideDown();
            
            
        //     $("#downloadbtn").show();
        //     $("#genmsg").hide();
        //           }, 0);
        //   });
    };

    </script>


    <style type="text/css">
        .main-container {
            margin-top: 0;
            display: flex;
            justify-content: center;
            align-items: center;
        }

        #forborder {
            border: 20px solid transparent;
            padding: 16px 0;
            border-image: url(http://localhost/Mehfil/public/assets/images/borderimg2.png) 22 round;
            width: 800px;
            margin: 50px 50px;
            font-weight: 600;
            color: #000;
            text-align: center;
            font-family: 'urdu' !important;
        }

        @media print {
            * {
                -webkit-print-color-adjust: exact;
            }
        }

        .salam {
            margin: 0 0 24px;
            line-height: normal;
            margin-top: -20px;
        }

        .naam_mubarak {
            font-size: 22px;
            margin-bottom: 30px;
            margin-right: 6px;
            font-weight: 600;
        }

        .duty_schedule,
        .duty_schedule span {
            font-size: 24px;
            margin-bottom: 32px;
            font-weight: 600;
            margin: 6px 8px 32px;
        }

        table th,
        tr,
        td {
            border: 2px solid black;
            text-align: center
        }

        .table>thead>tr>th {
            font-size: 28px;
            border-bottom: 1px solid #000;
            color: #000;
        }

        .table>tbody>tr>td div {
            font-size: 14px;
            color: #000;
        }

        .w-100 {
            width: 100%;
        }

        .vl {
            border-left: 3px solid black;
            height: 60px;
            position: absolute;
            right: 100%;
            margin-left: -3px;
            top: 0;
        }

        table tr.noBorder td {
            border: 0;
        }

        .bg_border {
            height: 1010px;
            width: 850px;
            position: absolute;
            top: 0;
            bottom: 0;
            left: 0;
            right: 0;
            margin: 0 auto;
        }

        .table {
            /* width: 640px; */
            margin: 0 auto;
        }

        .font-10 {
            font-size: 10px
        }

        .mr-2 {
            margin-right: 2px;
        }

    </style>

</head>
 


<body>
    <button onclick="generatePDF()"> Print is</button>

  <div class="main-container">

    <div class="container">
      
      <img src="../../assets/images/borderimg.jpg" class="bg_border" />
      <div class="row">

          <div class="col-md-12" id="forborder">
          <!-- <div class="" id="forborder" style="border: 10px solid black; border-style: dashed;"> -->
            <div class="" >

              <div class="">
              <div id="table_header">
                  <!-- <h3 style="font-size: 25px;">السلام عليكم ورحمة الله وبركاته</h3> -->
                  <h2 class="salam">السلام علیکم ورحمۃ اللہ و برکاتہ </h2>
                  <img src="{{ asset('assets/images/header_name_new.png') }}" width="300" />
                  <span class="naam_mubarak">
                      کی محفل نمبر {{ $single_mehfil->mehfil_number . ' ' }} -

                      {{ $single_mehfil->mehfil_address }}
                  </span>
                  @php
                  $date = date('d');
                  $month = date('m');
                  $month_name = '';
                  $year = date('Y');

                  if($month == 1)
                  $month_name = 'جنوری   ' ;
                  elseif($month == 2)
                  $month_name = ' فروری  ' ;
                  elseif($month == 3)
                  $month_name = '  مارچ   ' ;
                  elseif($month == 4)
                  $month_name = '  اپریل  ' ;
                  elseif($month == 5)
                  $month_name = '  مئی  ' ;
                  elseif($month == 6)
                  $month_name = '  جون  ' ;
                  elseif($month == 7)
                  $month_name = '  جولائی  ' ;
                  elseif($month == 8)
                  $month_name = '  آگست  ' ;
                  elseif($month == 9)
                  $month_name = '  ستمبر   ' ;
                  elseif($month == 10)
                  $month_name = '  اکتوبر   ' ;
                  elseif($month == 11)
                  $month_name = '  نومبر   ' ;
                  elseif($month == 12)
                  $month_name = '  دسمبر   ' ;                            
                  @endphp

                <h3 class="duty_schedule"><span class="mr-2">ڈیوٹی شیڈول برائے</span>
                    -{{$month_name.$year}}
                </h3>

                </div>


                <div class="col-md-1"></div>
                <div class="col-md-10">
                <div class="">
                <table class="table" id="myTable">

                      <thead>
                        <tr>
                          <th class="text-center">دن</th>
                          <th class="text-center">عہد کارکن</th>
                          <th class="text-center" >مین ڈیوٹیاں گیٹیاں</th>
                          <th class="text-center">سیکورٹی</th>
                          <th class="text-center">کمپیوٹر</th>                   
                        </tr>

                      </thead>
                      <tbody>

                        @php
                        $week_day_name = array( 

                        'friday' => 'جمعہ',

                        'saturday' => 'ہفتہ',

                        'sunday' => 'اتوار',

                        'monday' => 'سوموار',

                        'tuesday' => 'منگل',

                        'wednesday' => 'بدھ',

                        'thursday' => 'جمعرات'

                        );

                        foreach ($week_day_name as $key => $day){

                        $monthly_co_arr = [];

                        foreach($karkun_duty_rosters as $single_duty){

                        if($single_duty->k_duty_days != $key){
                        continue;
                      }
                      @endphp
                      <tr>

                      <td style="width: 8%;">
                        <div>
                            {{ $day }}
                        </div>
                      </td>


                        <td style="width: 10%;">
                            <div>
                                {{$single_duty->ekp_name}}
                                <br>
                                @php 
                                  $remove92 = substr($single_duty->ekp_phone , "2");
                                @endphp
                                <span class="font-10">{{"0".$remove92}}</span>
                            </div>
                        </td>

                        <td style="width: 52%;">
                            <div class="row">
                                @php
                                $main_duty = explode(',' , $single_duty->k_main_gatian);
                                foreach($main_duty as $single_md){
                                $all_karkun = DB::table('karkuns')->where('id' ,
                                $single_md)->first();
                                @endphp
                                <div class="col-xs-4">
                                    {{ $all_karkun->kp_name }}
                                    <span class="font-10">({{ $all_karkun->id }})</span>
                                    <!-- <br> -->
                                    <!-- <span class="font-10">{{ $all_karkun->kp_phone }}</span> -->
                                </div>
                                @php } @endphp
                            </div>
                        </td>

                        <td style="width: 15%;">
                            <div class="row">
                                @php
                                $k_cycle_stand = explode(',' , $single_duty->k_cycle_stand);
                                foreach($k_cycle_stand as $single_md){
                                $all_karkun = DB::table('karkuns')->where('id' ,
                                $single_md)->first();
                                @endphp
                                <div class="col-xs-12">
                                    {{$all_karkun->kp_name}}
                                    <span class="font-10">({{$all_karkun->id}})</span>
                                    <!-- <br> -->
                                    <!-- <span class="font-10">{{str_replace("92-" , "0" , $all_karkun->kp_phone)}}</span> -->
                                </div>
                                @php } @endphp
                            </div>
                        </td>


                        <td style="width: 15%;">
                            <div class="row">
                                @php
                                $k_security = explode(',' , $single_duty->k_security);

                                foreach($k_security as $single_md){
                                $all_karkun = DB::table('karkuns')->where('id' ,
                                $single_md)->first();

                                @endphp

                                <div class="col-xs-12">
                                    {{ $all_karkun->kp_name }}
                                    <span class="font-10">({{ $all_karkun->id }})</span>
                                    <!-- <br> -->
                                    <!-- <span class="font-10">{{str_replace("92-" , "0" , $all_karkun->kp_phone)}}</span> -->
                                </div>

                                @php } @endphp
                            </div>


                            @php
                            $monthly_cordinator = explode(',' , $single_duty->monthly_cordinator);

                            foreach($monthly_cordinator as $single_md){
                            $all_karkun_coord = DB::table('karkuns')->where('id' , $single_md)->first();

                            array_push($monthly_co_arr , $all_karkun_coord->kp_name);
                            }
                            @endphp

                        </td>

                    </tr>

                    @php } } @endphp

                    <tr class="noBorder">
                      <td colspan="5">

                        <div class="row">
                          <div class="col-xs-2" style="float:right; font-size: 25px;" >
                            کو آڈینیٹر          
                          </div>

                          <div class="col-xs-4" style="float:right; ">

                            <div class="row">

                              @foreach($monthly_co_arr as $single)

                              <div class="col-xs-6" style="float:right;font-size: 20px;"  >
                                {{$single}}
                                <!-- <span style="font-size: 12px;">({{'1'}})</span> -->
                                <span style="font-size: 12px;">{{'223232'}}</span>
                                <div class="vl"></div>
                              </div>

                              @endforeach    

                            </div>

                          </div>

                          <div class="col-xs-6" style="float:right;">
                            <p id="footer_text" style="display: block;font-size: 20px;">
                              <span style="color: red;">نوٹ:</span>
                            تمام بھائ اپنی ڈیوٹی والے دن محفل سے 15 سے 20 منٹ پہلے آئیں،محفل پر ماہانہ %50 حاضری کو یقینی بنائیں        </p>

                          </div>
                        </div>
                      </td>
                    </tr>


                  </tbody>
                </table>

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

  <div id="elementH"></div>

<!-- <script src="{{asset('assets/js/jquery.js')}}"></script>
<script src="{{asset('assets/plugins/bootstrap/js/bootstrap.min.js')}}"></script> -->
<!-- <script type="text/javascript">
  $(document).ready(function(){
  //   window.print();
  //   setTimeout("closePrintView()", 1000);

  // });
</script> -->
<!-- 
<script src="https://code.jquery.com/jquery-1.12.3.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/0.9.0rc1/jspdf.min.js"></script> -->

</body>

</html>