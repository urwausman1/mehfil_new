<!DOCTYPE html>
<html>

<head>
    <title></title>

    <link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/main.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/main-responsive.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/rtl-version.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/print.css') }}" type="text/css" media="print" />


    <style type="text/css">
        #forborder {
            border: 10px solid transparent;
            padding: 15px;
            /* border-image: url(https://www.w3schools.com/cssref/border.png) 30 round; */
            border-image: url("{{ asset('assets/images/borderimg.png') }}") 30 round;
        }

        @media print {
            * {
                -webkit-print-color-adjust: exact;
            }
        }

        table th,
        tr,
        td {
            border: 2px solid black;
        }

        .vl {
            border-left: 3px solid black;
            height: 60px;
            position: absolute;
            right: 100%;
            margin-left: -3px;
            top: 0;
        }

        table tr.noBorder td {
            border: 0;
        }

        .bg_border {
            height: 1100px;
            width: 850px;
            position: absolute;
            top: 0;
            bottom: 0;
            left: 0;
            right: 0;
            margin: 0 auto;
        }

        .table {
            width: 640px;
            margin: 0 auto;
        }

    </style>

</head>

<body>
    <div class="main-container main-container-flex">
        {{-- <img src="../../assets/images/borderimg.jpg" class="bg_border" />
        --}}
        <div class="container">
            <div class="row">
                <div class="col-md-12" id="forborder">
                    <!-- <div class="" id="forborder" style="border: 10px solid black; border-style: dashed;"> -->
                    <div class="">

                        <div class="">
                            <div id="table_header" style="text-align: center !important;">

                                <!-- <h3 style="font-size: 25px;">السلام عليكم ورحمة الله وبركاته</h3> -->
                                <h2 style="line-height: 2;margin:0;">السلام علیکم ورحمۃ اللہ و برکاتہ </h2>


                                <img src="{{ asset('assets/images/header_name_new.png') }}" width="300"
                                    style="margin-top: -20px;" />

                                <span style="font-style: bold; font-size: 25px">
                                    کی محفل نمبر {{ $single_mehfil->mehfil_number . ' ' }}

                                    ({{ $single_mehfil->mehfil_address }})

                                </span>

                                @php
                                $date = date('d');
                                $month = date('m');
                                $month_name = '';
                                $year = date('Y');

                                if($month == 1)
                                $month_name = 'جنوری ' ;
                                elseif($month == 2)
                                $month_name = ' فروری ' ;
                                elseif($month == 3)
                                $month_name = ' مارچ ' ;
                                elseif($month == 4)
                                $month_name = ' اپریل ' ;
                                elseif($month == 5)
                                $month_name = ' مئی ' ;
                                elseif($month == 6)
                                $month_name = ' جون ' ;
                                elseif($month == 7)
                                $month_name = ' جولائی ' ;
                                elseif($month == 8)
                                $month_name = ' آگست ' ;
                                elseif($month == 9)
                                $month_name = ' ستمبر ' ;
                                elseif($month == 10)
                                $month_name = ' اکتوبر ' ;
                                elseif($month == 11)
                                $month_name = ' نومبر ' ;
                                elseif($month == 12)
                                $month_name = ' دسمبر ' ;
                                @endphp


                                <h3 style="font-size: 25px;margin-top: -10px;">ڈیوٹی شیڈول
                                    برائے

                                    {{ $month_name . ' / ' . $year }}

                                </h3>
                            </div>
                            <div class="col-md-2"></div>
                            <div class="col-md-8">
                                <div class="">
                                    <table class="table" id="myTable">
                                        <thead>
                                            <tr>
                                                <th class="text-center">دن</th>
                                                <th class="text-center">عہد کارکن</th>
                                                <th class="text-center">مین ڈیوٹیاں گیٹیاں</th>
                                                <th class="text-center">سیکورٹی</th>
                                                <th class="text-center">کمپیوٹر</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php
                                            $week_day_name = array(

                                            'friday' => 'جمعہ',

                                            'saturday' => 'ہفتہ',

                                            'sunday' => 'اتوار',

                                            'monday' => 'سوموار',

                                            'tuesday' => 'منگل',

                                            'wednesday' => 'بدھ',

                                            'thursday' => 'جمعرات'

                                            );

                                            foreach ($week_day_name as $key => $day){

                                            $monthly_co_arr = [];

                                            foreach($karkun_duty_rosters as $single_duty){

                                            if($single_duty->k_duty_days != $key){
                                            continue;
                                            }
                                            @endphp
                                            <tr>

                                                <td class="text-center" style="font-size: 20px;">
                                                    <div class="text-center">
                                                        {{ $day }}
                                                    </div>
                                                </td>

                                                <td class="text-center">

                                                    {{ $single_duty->ekp_name }}
                                                    <br>
                                                    <span style="font-size: 10px;">{{ $single_duty->ekp_phone }}</span>
                                                </td>

                                                <td class="text-center">

                                                    <div class="row">
                                                        @php
                                                        $main_duty = explode(',' , $single_duty->k_main_gatian);

                                                        foreach($main_duty as $single_md){
                                                        $all_karkun = DB::table('karkuns')->where('id' ,
                                                        $single_md)->first();
                                                        @endphp

                                                        <div class="col-xs-4" style="float:right;">
                                                            {{ $all_karkun->kp_name }}
                                                            <span
                                                                style="font-size: 10px;">({{ $all_karkun->id }})</span>
                                                            <br>
                                                            <span
                                                                style="font-size: 10px;">{{ $all_karkun->kp_phone }}</span>
                                                        </div>


                                                        @php } @endphp

                                                    </div>
                                                </td>
                                                <td class="text-center" style="width: 12%;">

                                                    <div class="row">
                                                        @php
                                                        $k_cycle_stand = explode(',' , $single_duty->k_cycle_stand);

                                                        foreach($k_cycle_stand as $single_md){
                                                        $all_karkun = DB::table('karkuns')->where('id' ,
                                                        $single_md)->first();

                                                        @endphp

                                                        <div class="col-xs-12" style="float:right;">
                                                            {{ $all_karkun->kp_name }}
                                                            <span
                                                                style="font-size: 10px;">({{ $all_karkun->id }})</span>
                                                            <br>
                                                            <span
                                                                style="font-size: 10px;">{{ $all_karkun->kp_phone }}</span>
                                                        </div>


                                                        @php } @endphp
                                                    </div>
                                                </td>

                                                <td class="text-center" style="width: 12%;">
                                                    <div class="row">
                                                        @php
                                                        $k_security = explode(',' , $single_duty->k_security);

                                                        foreach($k_security as $single_md){
                                                        $all_karkun = DB::table('karkuns')->where('id' ,
                                                        $single_md)->first();

                                                        @endphp

                                                        <div class="col-xs-12" style="float:right;">
                                                            {{ $all_karkun->kp_name }}
                                                            <span
                                                                style="font-size: 10px;">({{ $all_karkun->id }})</span>
                                                            <br>
                                                            <span
                                                                style="font-size: 10px;">{{ $all_karkun->kp_phone }}</span>
                                                        </div>


                                                        @php } @endphp
                                                    </div>

                                                    @php
                                                    $monthly_cordinator = explode(',' ,
                                                    $single_duty->monthly_cordinator);

                                                    foreach($monthly_cordinator as $single_md){
                                                    $all_ehad_karkun = DB::table('ehad_karkuns')->where('id' ,
                                                    $single_md)->first();

                                                    array_push($monthly_co_arr , $all_ehad_karkun->ekp_name);
                                                    }
                                                    @endphp

                                                </td>
                                            </tr>

                                            @php } } @endphp

                                            <tr class="noBorder">
                                                <td colspan="5">

                                                    <div class="row">
                                                        <div class="col-xs-2" style="float:right; font-size: 25px;">
                                                            کو آڈینیٹر
                                                        </div>

                                                        <div class="col-xs-4" style="float:right; ">

                                                            <div class="row">

                                                                @foreach ($monthly_co_arr as $single)

                                                                    <div class="col-xs-6"
                                                                        style="float:right;font-size: 20px;">
                                                                        {{ $single }}
                                                                        <!-- <span style="font-size: 12px;">({{ '1' }})</span> -->
                                                                        <span
                                                                            style="font-size: 12px;">{{ '223232' }}</span>
                                                                        <div class="vl"></div>
                                                                    </div>

                                                                @endforeach

                                                            </div>

                                                        </div>

                                                        <div class="col-xs-6" style="float:right;">
                                                            <p id="footer_text" style="display: block;font-size: 20px;">
                                                                <span style="color: red;">نوٹ:</span>
                                                                تمام بھائ اپنی ڈیوٹی والے دن محفل سے 15 سے 20 منٹ پہلے
                                                                آئیں،محفل پر ماہانہ %50 حاضری کو یقینی بنائیں </p>

                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>


                                        </tbody>
                                    </table>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="{{ asset('assets/js/jquery.js') }}"></script>
    <script src="{{ asset('assets/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            window.print();
            setTimeout("closePrintView()", 1000);

        });

    </script>
</body>

</html>
