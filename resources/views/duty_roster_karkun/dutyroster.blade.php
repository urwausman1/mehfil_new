<!DOCTYPE html>
<html lang="en" class="no-js">
<!-- start: HEAD -->

<head>
    <title> ڈیٹی روسٹر برائے کارکنان </title>

    @include('inc.head')

    <style>
        #contacts_table_wrapper .select2-container {
            width: 100px;
        }

        #contacts_table_wrapper .select2-container span.select2-chosen {
            text-align: right;
        }

        table.table thead .sorting {
            text-align: right;
            background: url('assets/plugins/DataTables/media/css/images/sort_both.png') no-repeat center left;
        }

        table.table thead .sorting_asc {
            text-align: right;
            background: url('assets/plugins/DataTables/media/css/images/sort_asc.png') no-repeat center left;
        }

        table.table thead .sorting_desc {
            text-align: right;
            background: url('assets/plugins/DataTables/media/css/images/sort_desc.png') no-repeat center left;
        }



        #s2id_monthly_coordinators .select2-choices {
            width: 100% !important;
        }

    </style>

</head>

<!-- end: HEAD -->

<!-- start: BODY -->

<body class="rtl">
    <!-- start: HEADER -->
    <div class="navbar navbar-inverse navbar-fixed-top">
        <!-- start: TOP NAVIGATION CONTAINER -->
        <div class="container">
            <div class="navbar-header">
                <!-- start: RESPONSIVE MENU TOGGLER -->
                <button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
                    <span class="clip-list-2"></span>
                </button>
                <!-- end: RESPONSIVE MENU TOGGLER -->
                <!-- end: LOGO -->
            </div>
        </div>
        <!-- end: TOP NAVIGATION CONTAINER -->
    </div>
    <!-- end: HEADER -->

    <!-- start: MAIN CONTAINER -->
    <div class="main-container">
        <div class="navbar-content">
            <!-- start: SIDEBAR -->
            @include('inc.sidebar')
            <!-- end: SIDEBAR -->
        </div>
        <!-- start: PAGE -->
        <div class="main-content">
            <div class="container" style="min-height: 760px;">
                <!-- start: PAGE HEADER -->
                <div class="row">
                    <div class="col-sm-12">
                        <!-- start: PAGE TITLE & BREADCRUMB -->
                        <ol class="breadcrumb">
                            <li>
                                <!--- class="clip-file" -->
                                <i class=""></i>
                                تمام عہد کارکنان
                            </li>
                            <li class="active">
                            </li>
                            <!-- <li class="pull-left" style="margin-left: 30px;">
                                    <a href="admin/attendance">حاضری</a>
                                </li> -->
                            <!--<li class="search-box">
                                    <form class="sidebar-search">
                                        <div class="form-group">
                                            <input type="text" placeholder="Start Searching...">
                                            <button class="submit">
                                                <i class="clip-search-3"></i>
                                            </button>
                                        </div>
                                    </form>
                                </li>-->
                        </ol>
                        <div class="page-header">
                            <h1>
                                تمام عہد کارکنان
                                <small style="font-size:25px;margin-right:30px;"></small>
                            </h1>
                        </div>
                        <!-- end: PAGE TITLE & BREADCRUMB -->
                    </div>
                </div>
                <!-- end: PAGE HEADER -->
                <!-- start: PAGE CONTENT -->

                @if (Session::has('msg'))
                    <div class="col-md-12">
                        <div class="alert alert-success">{{ Session::get('msg') }}</div>
                    </div>
                @endif
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <i class="fa fa-external-link-square"></i>
                                تمام عہد کارکنان
                            </div>
                            <div class="panel-body">
                                <!-- complete form for hazri -->
                                <form method="post" action="{{ url('dutyrosterkarkun/add') }}">
                                    @csrf
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label for="">محفل کا نام</label>
                                            @if (session('user_role') == 'mehfil')
                                                <input type="text" name="mehfils"
                                                    value="{{ $single_mehfil->mehfil_name }}" class="form-control">
                                            @else
                                                <select name="mehfils" style="width: 25% !important;"
                                                    class="urwasearch">
                                                    <option value="">محفل کا انتخاب کریں۔۔۔</option>
                                                    @foreach ($all_mehfils as $single_mehfil)
                                                        <option
                                                            value="{{ 'dutyrosterkarkun/edit/' . $single_mehfil->id }}">
                                                            {{ $single_mehfil->mehfil_name }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            @endif
                                        </div>
                                    </div>
                                    <br>
                                    <div class="col-md-12 d-none">
                                        <div class="table-responsive_karkun">
                                            <table class="table-bordered">
                                                <thead></thead>
                                            </table>
                                            <div class="col-md-12">
                                                <table class="table-bordered" id="dr-table">
                                                    <thead>
                                                        <tr>
                                                            <td class="t-day">دن</td>
                                                            <td class="t-ehad-ca">عہد کارکن</td>
                                                            <!-- <td class="t-day-co">ڈے کوارڈینیٹر</td> -->
                                                            <td class="t-main-duty">مین ڈیوٹی گیٹیاں</td>
                                                            <td class="t-naat">کمپیوٹر</td>
                                                            <td class="t-cycle">سیکورٹی</td>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td class="t-day">
                                                                <div class="text-center">
                                                                    جمعہ
                                                                </div>
                                                            </td>
                                                            <td class="t-ehad-ca">
                                                                <select id="form-field-select-3" name="ehd_karkun_juma"
                                                                    class="form-control search-select select2-hidden-accessible"
                                                                    tabindex="-1" aria-hidden="true">
                                                                    <option val=""> عہد کارکن کا نام۔۔۔</option>
                                                                    @foreach ($all_ehad_karkun as $single_ehad_karkun)
                                                                        <option value="{{ $single_ehad_karkun->id }}">
                                                                            {{ $single_ehad_karkun->ekp_name }}
                                                                        </option>
                                                                    @endforeach
                                                                </select>
                                                            </td>
                                                            <td class="t-main-duty">
                                                                <select multiple name="main_duty_juma[]"
                                                                    id="form-field-select-4"
                                                                    class="form-control search-select select2-hidden-accessible"
                                                                    tabindex="-1" aria-hidden="true">
                                                                    @foreach ($all_karkun as $single_karkun)
                                                                        <option value="{{ $single_karkun->id }}">
                                                                            {{ $single_karkun->kp_name }}
                                                                        </option>
                                                                    @endforeach
                                                                </select>
                                                            </td>
                                                            <td class="t-naat">
                                                                <select multiple name="cycle_stand_juma[]"
                                                                    id="form-field-select-4"
                                                                    class="form-control search-select select2-hidden-accessible"
                                                                    tabindex="-1" aria-hidden="true">
                                                                    @foreach ($all_karkun as $single_karkun)
                                                                        <option value="{{ $single_karkun->id }}">
                                                                            {{ $single_karkun->kp_name }}
                                                                        </option>
                                                                    @endforeach
                                                                </select>
                                                            </td>
                                                            <td class="t-cycle">
                                                                <select multiple name="security_juma[]"
                                                                    id="form-field-select-4"
                                                                    class="form-control search-select select2-hidden-accessible"
                                                                    tabindex="-1" aria-hidden="true">
                                                                    @foreach ($all_karkun as $single_karkun)
                                                                        <option value="{{ $single_karkun->id }}">
                                                                            {{ $single_karkun->kp_name }}
                                                                        </option>
                                                                    @endforeach
                                                                </select>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="t-day">
                                                                <div class="text-center">
                                                                    ہفتہ
                                                                </div>
                                                            </td>
                                                            <td class="t-ehad-ca">
                                                                <select id="form-field-select-3" name="ehd_karkun_hafta"
                                                                    class="form-control search-select select2-hidden-accessible"
                                                                    tabindex="-1" aria-hidden="true">
                                                                    <option val=""> عہد کارکن کا نام۔۔۔</option>
                                                                    @foreach ($all_ehad_karkun as $single_ehad_karkun)
                                                                        <option value="{{ $single_ehad_karkun->id }}">
                                                                            {{ $single_ehad_karkun->ekp_name }}
                                                                        </option>
                                                                    @endforeach
                                                                </select>
                                                            </td>

                                                            <td class="t-main-duty">
                                                                <select multiple id="form-field-select-4"
                                                                    name="main_duty_hafta[]"
                                                                    class="form-control search-select select2-hidden-accessible"
                                                                    tabindex="-1" aria-hidden="true">
                                                                    @foreach ($all_karkun as $single_karkun)
                                                                        <option value="{{ $single_karkun->id }}">
                                                                            {{ $single_karkun->kp_name }}
                                                                        </option>
                                                                    @endforeach
                                                                </select>
                                                            </td>

                                                            <td class="t-naat">
                                                                <select multiple id="form-field-select-4"
                                                                    name="cycle_stand_hafta[]"
                                                                    class="form-control search-select select2-hidden-accessible"
                                                                    tabindex="-1" aria-hidden="true">
                                                                    @foreach ($all_karkun as $single_karkun)
                                                                        <option value="{{ $single_karkun->id }}">
                                                                            {{ $single_karkun->kp_name }}
                                                                        </option>
                                                                    @endforeach
                                                                </select>
                                                            </td>

                                                            <td class="t-cycle">
                                                                <select multiple id="form-field-select-4"
                                                                    name="security_hafta[]"
                                                                    class="form-control search-select select2-hidden-accessible"
                                                                    tabindex="-1" aria-hidden="true">
                                                                    @foreach ($all_karkun as $single_karkun)
                                                                        <option value="{{ $single_karkun->id }}">
                                                                            {{ $single_karkun->kp_name }}
                                                                        </option>
                                                                    @endforeach
                                                                </select>
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td class="t-day">
                                                                <div class="text-center">
                                                                    اتوار
                                                                </div>
                                                            </td>

                                                            <td class="t-ehad-ca">
                                                                <select id="form-field-select-3" name="ehd_karkun_itwar"
                                                                    class="form-control search-select select2-hidden-accessible"
                                                                    tabindex="-1" aria-hidden="true">
                                                                    <option val=""> عہد کارکن کا نام۔۔۔</option>
                                                                    @foreach ($all_ehad_karkun as $single_ehad_karkun)
                                                                        <option value="{{ $single_ehad_karkun->id }}">
                                                                            {{ $single_ehad_karkun->ekp_name }}
                                                                        </option>
                                                                    @endforeach
                                                                </select>
                                                            </td>

                                                            <td class="t-main-duty">

                                                                <select multiple id="form-field-select-4"
                                                                    name="main_duty_itwar[]"
                                                                    class="form-control search-select select2-hidden-accessible"
                                                                    tabindex="-1" aria-hidden="true">
                                                                    @foreach ($all_karkun as $single_karkun)
                                                                        <option value="{{ $single_karkun->id }}">
                                                                            {{ $single_karkun->kp_name }}
                                                                        </option>
                                                                    @endforeach
                                                                </select>

                                                            </td>

                                                            <td class="t-naat">
                                                                <select multiple id="form-field-select-4"
                                                                    name="cycle_stand_itwar[]"
                                                                    class="form-control search-select select2-hidden-accessible"
                                                                    tabindex="-1" aria-hidden="true">
                                                                    @foreach ($all_karkun as $single_karkun)
                                                                        <option value="{{ $single_karkun->id }}">
                                                                            {{ $single_karkun->kp_name }}
                                                                        </option>
                                                                    @endforeach
                                                                </select>
                                                            </td>

                                                            <td class="t-cycle">
                                                                <select multiple id="form-field-select-4"
                                                                    name="security_itwar[]"
                                                                    class="form-control search-select select2-hidden-accessible"
                                                                    tabindex="-1" aria-hidden="true">
                                                                    @foreach ($all_karkun as $single_karkun)
                                                                        <option value="{{ $single_karkun->id }}">
                                                                            {{ $single_karkun->kp_name }}
                                                                        </option>
                                                                    @endforeach
                                                                </select>
                                                            </td>

                                                        </tr>

                                                        <tr>
                                                            <td class="t-day">
                                                                <div class="text-center">
                                                                    سوموار
                                                                </div>
                                                            </td>

                                                            <td class="t-ehad-ca">
                                                                <select id="form-field-select-3" name="ehd_karkun_peer"
                                                                    class="form-control search-select select2-hidden-accessible"
                                                                    tabindex="-1" aria-hidden="true">
                                                                    <option val=""> عہد کارکن کا نام۔۔۔</option>
                                                                    @foreach ($all_ehad_karkun as $single_ehad_karkun)
                                                                        <option value="{{ $single_ehad_karkun->id }}">
                                                                            {{ $single_ehad_karkun->ekp_name }}
                                                                        </option>
                                                                    @endforeach
                                                                </select>
                                                            </td>

                                                            <td class="t-main-duty">
                                                                <select multiple id="form-field-select-4"
                                                                    name="main_duty_peer[]"
                                                                    class="form-control search-select select2-hidden-accessible"
                                                                    tabindex="-1" aria-hidden="true">
                                                                    @foreach ($all_karkun as $single_karkun)
                                                                        <option value="{{ $single_karkun->id }}">
                                                                            {{ $single_karkun->kp_name }}
                                                                        </option>
                                                                    @endforeach
                                                                </select>
                                                            </td>

                                                            <td class="t-naat">
                                                                <select multiple id="form-field-select-4"
                                                                    name="cycle_stand_peer[]"
                                                                    class="form-control search-select select2-hidden-accessible"
                                                                    tabindex="-1" aria-hidden="true">
                                                                    @foreach ($all_karkun as $single_karkun)
                                                                        <option value="{{ $single_karkun->id }}">
                                                                            {{ $single_karkun->kp_name }}
                                                                        </option>
                                                                    @endforeach
                                                                </select>
                                                            </td>

                                                            <td class="t-cycle">
                                                                <select multiple id="form-field-select-4"
                                                                    name="security_peer[]"
                                                                    class="form-control search-select select2-hidden-accessible"
                                                                    tabindex="-1" aria-hidden="true">
                                                                    @foreach ($all_karkun as $single_karkun)
                                                                        <option value="{{ $single_karkun->id }}">
                                                                            {{ $single_karkun->kp_name }}
                                                                        </option>
                                                                    @endforeach
                                                                </select>
                                                            </td>

                                                        </tr>

                                                        <tr>
                                                            <td class="t-day">
                                                                <div class="text-center">
                                                                    منگل
                                                                </div>
                                                            </td>

                                                            <td class="t-ehad-ca">
                                                                <select id="form-field-select-3"
                                                                    name="ehd_karkun_mangal"
                                                                    class="form-control search-select select2-hidden-accessible"
                                                                    tabindex="-1" aria-hidden="true">
                                                                    <option val=""> عہد کارکن کا نام۔۔۔</option>
                                                                    @foreach ($all_ehad_karkun as $single_ehad_karkun)
                                                                        <option value="{{ $single_ehad_karkun->id }}">
                                                                            {{ $single_ehad_karkun->ekp_name }}
                                                                        </option>
                                                                    @endforeach
                                                                </select>
                                                            </td>

                                                            <td class="t-main-duty">
                                                                <select multiple id="form-field-select-4"
                                                                    name="main_duty_mangal[]"
                                                                    class="form-control search-select select2-hidden-accessible"
                                                                    tabindex="-1" aria-hidden="true">
                                                                    @foreach ($all_karkun as $single_karkun)
                                                                        <option value="{{ $single_karkun->id }}">
                                                                            {{ $single_karkun->kp_name }}
                                                                        </option>
                                                                    @endforeach
                                                                </select>
                                                            </td>

                                                            <td class="t-naat">
                                                                <select multiple id="form-field-select-4"
                                                                    name="cycle_stand_mangal[]"
                                                                    class="form-control search-select select2-hidden-accessible"
                                                                    tabindex="-1" aria-hidden="true">
                                                                    @foreach ($all_karkun as $single_karkun)
                                                                        <option value="{{ $single_karkun->id }}">
                                                                            {{ $single_karkun->kp_name }}
                                                                        </option>
                                                                    @endforeach
                                                                </select>
                                                            </td>

                                                            <td class="t-cycle">
                                                                <select multiple id="form-field-select-4"
                                                                    name="security_mangal[]"
                                                                    class="form-control search-select select2-hidden-accessible"
                                                                    tabindex="-1" aria-hidden="true">
                                                                    @foreach ($all_karkun as $single_karkun)
                                                                        <option value="{{ $single_karkun->id }}">
                                                                            {{ $single_karkun->kp_name }}
                                                                        </option>
                                                                    @endforeach
                                                                </select>
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td class="t-day">
                                                                <div class="text-center">
                                                                    بدھ
                                                                </div>
                                                            </td>



                                                            <td class="t-ehad-ca">
                                                                <select id="form-field-select-3" name="ehd_karkun_budh"
                                                                    class="form-control search-select select2-hidden-accessible"
                                                                    tabindex="-1" aria-hidden="true">
                                                                    <option val=""> عہد کارکن کا نام۔۔۔</option>
                                                                    @foreach ($all_ehad_karkun as $single_ehad_karkun)
                                                                        <option value="{{ $single_ehad_karkun->id }}">
                                                                            {{ $single_ehad_karkun->ekp_name }}
                                                                        </option>
                                                                    @endforeach
                                                                </select>
                                                            </td>

                                                            <td class="t-main-duty">
                                                                <select multiple id="form-field-select-4"
                                                                    name="main_duty_budh[]"
                                                                    class="form-control search-select select2-hidden-accessible"
                                                                    tabindex="-1" aria-hidden="true">
                                                                    @foreach ($all_karkun as $single_karkun)
                                                                        <option value="{{ $single_karkun->id }}">
                                                                            {{ $single_karkun->kp_name }}
                                                                        </option>
                                                                    @endforeach
                                                                </select>
                                                            </td>

                                                            <td class="t-naat">
                                                                <select multiple id="form-field-select-4"
                                                                    name="cycle_stand_budh[]"
                                                                    class="form-control search-select select2-hidden-accessible"
                                                                    tabindex="-1" aria-hidden="true">
                                                                    @foreach ($all_karkun as $single_karkun)
                                                                        <option value="{{ $single_karkun->id }}">
                                                                            {{ $single_karkun->kp_name }}
                                                                        </option>
                                                                    @endforeach
                                                                </select>
                                                            </td>

                                                            <td class="t-cycle">
                                                                <select multiple id="form-field-select-4"
                                                                    name="security_budh[]"
                                                                    class="form-control search-select select2-hidden-accessible"
                                                                    tabindex="-1" aria-hidden="true">
                                                                    @foreach ($all_karkun as $single_karkun)
                                                                        <option value="{{ $single_karkun->id }}">
                                                                            {{ $single_karkun->kp_name }}
                                                                        </option>
                                                                    @endforeach
                                                                </select>
                                                            </td>

                                                        </tr>

                                                        <tr>
                                                            <td class="t-day">
                                                                <div class="text-center">
                                                                    جمعرات
                                                                </div>
                                                            </td>

                                                            <td class="t-ehad-ca">
                                                                <select id="form-field-select-3"
                                                                    name="ehd_karkun_jumerat"
                                                                    class="form-control search-select select2-hidden-accessible"
                                                                    tabindex="-1" aria-hidden="true">
                                                                    <option val=""> عہد کارکن کا نام۔۔۔</option>
                                                                    @foreach ($all_ehad_karkun as $single_ehad_karkun)
                                                                        <option value="{{ $single_ehad_karkun->id }}">
                                                                            {{ $single_ehad_karkun->ekp_name }}
                                                                        </option>
                                                                    @endforeach
                                                                </select>
                                                            </td>

                                                            <td class="t-main-duty">
                                                                <select multiple id="form-field-select-4"
                                                                    name="main_duty_jumerat[]"
                                                                    class="form-control search-select select2-hidden-accessible"
                                                                    tabindex="-1" aria-hidden="true">
                                                                    @foreach ($all_karkun as $single_karkun)
                                                                        <option value="{{ $single_karkun->id }}">
                                                                            {{ $single_karkun->kp_name }}
                                                                        </option>
                                                                    @endforeach
                                                                </select>
                                                            </td>

                                                            <td class="t-naat">
                                                                <select multiple id="form-field-select-4"
                                                                    name="cycle_stand_jumerat[]"
                                                                    class="form-control search-select select2-hidden-accessible"
                                                                    tabindex="-1" aria-hidden="true">
                                                                    @foreach ($all_karkun as $single_karkun)
                                                                        <option value="{{ $single_karkun->id }}">
                                                                            {{ $single_karkun->kp_name }}
                                                                        </option>
                                                                    @endforeach
                                                                </select>
                                                            </td>

                                                            <td class="t-cycle">
                                                                <select multiple id="form-field-select-4"
                                                                    name="security_jumerat[]"
                                                                    class="form-control search-select select2-hidden-accessible"
                                                                    tabindex="-1" aria-hidden="true">
                                                                    @foreach ($all_karkun as $single_karkun)
                                                                        <option value="{{ $single_karkun->id }}">
                                                                            {{ $single_karkun->kp_name }}
                                                                        </option>
                                                                    @endforeach
                                                                </select>
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td class="t-day">
                                                                <div class="text-center">
                                                                    ماہانہ کوارڈینیٹر
                                                                </div>
                                                            </td>

                                                            <td colspan="4">
                                                                <select multiple id="form-field-select-4"
                                                                    name="mahana_coordinator[]"
                                                                    class="form-control search-select select2-hidden-accessible"
                                                                    tabindex="-1" aria-hidden="true">
                                                                    @foreach ($all_ehad_karkun as $single_ehad_karkun)
                                                                        <option value="{{ $single_ehad_karkun->id }}">
                                                                            {{ $single_ehad_karkun->ekp_name }}
                                                                        </option>
                                                                    @endforeach
                                                                </select>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <h1 id="footer_text" style="display: none;font-size: 20px;"><span
                                                        style="color: red;">نوٹ:</span> تمام بھائ اپنی ڈیوٹی والے دن
                                                    محفل والے ٹائم سے 15 سے 20 منٹ پہلے آئیں،محفل پر ماہانہ %50 حاضری کو
                                                    یقینی بنائیں ملتان شریف منگل ڈیوٹی پر ہر دو ماہ میں ایک بار جانا
                                                    ضروری ہے۔</h1>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <hr>
                                                <div>
                                                    <button id="btn_submit" type="submit" value="Submit"
                                                        class="btn btn-blue btn-block w-150">اندراج کریں</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <!-- complete form for hazri -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- end: PAGE CONTENT-->
        </div>
    </div>
    <!-- end: PAGE -->
    </div>
    <!-- end: MAIN CONTAINER -->



    <!-- start: FOOTER & scripts -->
    @include('inc.footer')

    <script type="text/javascript">
        $(document).ready(function() {
            $('#example-getting-started').multiselect();
        });

        $(document).ready(function() {
            $(document).find(".urwasearch").select2();
        });

        $('.urwasearch').on('change', function() {
            // alert('fasdfds');
            var value = $(document).find('.urwasearch option:selected').val();
            location.href = value;
        });

    </script>
</body>
<!-- end: BODY -->
<script>
    $(document).ready(function() {
        $(document).find("#name").focus();
    });

</script>

</html>
