<!DOCTYPE html>
<html lang="en" class="no-js">
<!-- start: HEAD -->

<head>
    <title> ڈیوٹی روسٹر برائے کارکنان </title>

    @include('inc.head')

    <style>
        .select2-container-multi .select2-choices {
            height: auto !important;
        }



        #contacts_table_wrapper .select2-container {
            width: 100px;
        }

        #contacts_table_wrapper .select2-container span.select2-chosen {
            text-align: right;
        }

        table.table thead .sorting {
            text-align: right;
            background: url('assets/plugins/DataTables/media/css/images/sort_both.png') no-repeat center left;
        }

        table.table thead .sorting_asc {
            text-align: right;
            background: url('assets/plugins/DataTables/media/css/images/sort_asc.png') no-repeat center left;
        }

        table.table thead .sorting_desc {
            text-align: right;
            background: url('assets/plugins/DataTables/media/css/images/sort_desc.png') no-repeat center left;
        }

    </style>

</head>

<!-- end: HEAD -->

<!-- start: BODY -->

<body class="rtl">
    <!-- start: HEADER -->
    <div class="navbar navbar-inverse navbar-fixed-top">
        <!-- start: TOP NAVIGATION CONTAINER -->
        <div class="container">
            <div class="navbar-header">
                <!-- start: RESPONSIVE MENU TOGGLER -->
                <button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
                    <span class="clip-list-2"></span>
                </button>
                <!-- end: RESPONSIVE MENU TOGGLER -->
                <!-- end: LOGO -->
            </div>
        </div>
        <!-- end: TOP NAVIGATION CONTAINER -->
    </div>
    <!-- end: HEADER -->

    <!-- start: MAIN CONTAINER -->
    <div class="main-container">
        <div class="navbar-content">
            <!-- start: SIDEBAR -->
            @include('inc.sidebar')
            <!-- end: SIDEBAR -->
        </div>
        <!-- start: PAGE -->
        <div class="main-content">

            <div class="container" style="min-height: 760px;">
                <!-- start: PAGE HEADER -->
                <div class="row">
                    <div class="col-sm-12">
                        <!-- start: PAGE TITLE & BREADCRUMB -->
                        <ol class="breadcrumb">
                            <li>
                                <!--- class="clip-file" -->
                                <i class=""></i>
                                ڈیوٹی روسٹر برائے کارکنان
                            </li>
                            <li class="active">
                            </li>
                        </ol>

                        <div class="page-header">
                            <h1>
                                ڈیوٹی روسٹر برائے کارکنان
                                <small style="font-size:25px;margin-right:30px;"></small>
                            </h1>
                        </div>
                        <!-- end: PAGE TITLE & BREADCRUMB -->
                    </div>
                </div>
                <!-- end: PAGE HEADER -->

                @if (Session::has('msg'))
                    <div class="col-md-12">
                        <div class="alert alert-success">{{ Session::get('msg') }}</div>
                    </div>
                @endif

                @if(!empty($not_save_msg))
                    <div class="col-md-12">
                        <div class="alert alert-danger">{{ $not_save_msg }}</div>
                    </div>
                @endif

                <!-- start: PAGE CONTENT -->

                @if (isset($security_msg) && $security_msg != '')

                    <div class="row">
                        <center>
                            <h1>{{ $security_msg }}</h1>
                        </center>
                    </div>

                @else

                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <i class="fa fa-external-link-square"></i>
                                    ڈیوٹی روسٹر برائے کارکنان
                                </div>
                                <div class="panel-body">

                                    <?php
                                    // $mahfil_name = array(
                                    // '80'=>'ادریسیہ میڈیا سیل فیصل آباد',
                                    // '1'=>'محفل نمبر ۱ (بٹالہ کالونی) فیصل آباد',
                                    // '2'=>'محفل نمبر ۲ (منصور آباد) فیصل آباد',
                                    // '3'=>'محفل نمبر ۳ (بولےدی جگھی) فیصل آباد',
                                    // '4'=>'محفل نمبر ۴ (غلام محمد آباد) فیصل آباد',
                                    // '5'=>'محفل نمبر ۵ (بڑا شیر سنگھ والا) فیصل آباد',
                                    // '6'=>'محفل نمبر ۶ (گوبند پورہ) فیصل آباد',
                                    // '7'=>'محفل نمبر ۷ (گلفشاں کالونی) فیصل آباد',
                                    // '8'=>'محفل نمبر ۸ (سمن آباد) فیصل آباد',
                                    // '9'=>'محفل نمبر ۹ (جڑانوالہ روڈ) فیصل آباد');
                                    ?>
                                    <div id="table_header" style="display: none;text-align: center !important;">
                                        <h1><img src="{{ asset('assets/images/flag.jpg') }}" width="200" /></h1>
                                        <!-- <h3 style="font-size: 25px;">السلام عليكم ورحمة الله وبركاته</h3> -->
                                        <h1><img src="{{ asset('assets/images/header_name.png') }}" width="400" /> <span
                                                style="font-size: 25px;font-family: JameelNooriNastaleeq, sans-serif !important;">
                                                کی </span></h1>
                                        <h3 style="font-size: 20px;">ڈیوٹی شیڈول جنوری 2018</h3>
                                    </div>
                                    <!-- complete form for hazri -->
                                    <form method="post" action="{{ url('dutyrosterkarkun/add') }}">
                                        @csrf
                                        <div class="row">

                                            <div class="col-md-6">
                                                <label for="">محفل کا نام</label>

                                                @if (session('user_role') == 'mehfil')

                                                    @php
                                                    $mehfil_id_session = session('mehfil_id');
                                                    @endphp

                                                    <input type="hidden" name="mehfils"
                                                        value="{{ $mehfil_id_session }}">
                                                        مین ڈیوٹیاں گیٹیاں	
                                                    <input type="text" style="width: 25% !important;" disabled=""
                                                        readonly="" value="{{ $mehfil_name_ne }}" class="form-control">

                                                @else

                                                    <select name="mehfils" style="width: 40% !important;"
                                                        class="urwasearch">
                                                        @if ($all_k_duty_roster_friday)
                                                            <option selected=""
                                                                value="{{ $all_k_duty_roster_friday->mehfil_id }}">
                                                                {{ $all_k_duty_roster_friday->mehfil_name }}
                                                            </option>
                                                        @else
                                                            <option value="{{ $mehfil_id }}"> {{ $mehfil_name_ne }}
                                                            </option>
                                                        @endif

                                                        @foreach ($all_mehfils as $single_mehfil)
                                                            <option value="{{ $single_mehfil->id }}">
                                                                {{ $single_mehfil->mehfil_name }}
                                                            </option>
                                                        @endforeach
                                                    </select>

                                                @endif
                                            </div>
                                            <div class="col-md-6">
                                                <a href="{{ url('print_duty_roster/edit/' . $mehfil_id) }}"
                                                    class="btn btn-sucess print_btn" id="print_btn">پر نٹ
                                                    نکا لیں </a>
                                            </div>
                                        </div>
                                        <br>

                                        <div class="col-md-12">
                                            <div class="table-responsive">
                                                <table class="table-bordered" width="100%">
                                                    <thead></thead>
                                                </table>
                                                <div class="col-md-12">
                                                    <table class="table-bordered" id="dr-table" width="100%">

                                                        <thead>
                                                            <tr>
                                                                <td class="t-day">دن</td>
                                                                <td class="t-ehad-ca">عہد کارکن</td>
                                                                <td class="t-day-co">ڈے کوارڈینیٹر</td>
                                                                <td class="t-main-duty">مین ڈیوٹی گیٹیاں</td>
                                                                <td class="t-naat">کمپیوٹر</td>
                                                                <td class="t-cycle">  سیکیورٹی   </td>
                                                            </tr>

                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td class="t-day">
                                                                    <div class="text-center">
                                                                        جمعہ
                                                                    </div>
                                                                </td>

                                                                <td class="t-ehad-ca">
    
<!--                                                                     <select id="form-field-select-3" 
                                                                        name="ehd_karkun_juma"
                                                                        class="form-control search-select select2-hidden-accessible"
                                                                        tabindex="-1" aria-hidden="true" > -->


                                                                        @if ($all_k_duty_roster_friday && !empty($all_k_duty_roster_friday->ehad_karkun) )
                                                                          
    <input type="hidden" name="ehd_karkun_juma" value="{{ $all_k_duty_roster_friday->ehad_karkun }}">
    {{ $all_k_duty_roster_friday->ekp_name }}


                                                                          <!--   <option
                                                                                value="{{ $all_k_duty_roster_friday->ehad_karkun }}">
                                                                                {{ $all_k_duty_roster_friday->ekp_name }}
                                                                            </option>-->
                                                                        @else

                                                                            @if (isset($get_ehad_karkun_juma) && !empty($get_ehad_karkun_juma))

    <input type="hidden" name="ehd_karkun_juma" value="{{ $get_ehad_karkun_juma->ehad_karkun_id }}">
    <span style="color: red !important;">{{ $get_ehad_karkun_juma->ekp_name }}</span>


<!--                                                                                 <option
                                                                                    value="{{ $get_ehad_karkun_juma->ehad_karkun_id }}">
                                                                                    {{ $get_ehad_karkun_juma->ekp_name }}
                                                                                </option> -->
                                                                            @else
<!--                                                                                 <option value=""> عہد کارکن کا نام۔۔۔
                                                                                </option> -->
                                                                            @endif

                                                                        @endif

                                                                        <!-- @foreach ($all_ehad_karkun as $single_ehad_karkun)
                                                                            <option
                                                                                value="{{ $single_ehad_karkun->id }}">
                                                                                {{ $single_ehad_karkun->ekp_name }}
                                                                            </option>
                                                                        @endforeach -->
                                                                    <!-- </select> -->
                                                                </td>


                                                                <td>
                                                                    <select id="form-field-select-3"
                                                                        name="day_coordinator_juma"
                                                                        class="form-control search-select select2-hidden-accessible"
                                                                        tabindex="-1" aria-hidden="true">

                                                                        @foreach ($all_karkun as $single_karkun)

                                                                            @php
                                                                            $fdco_id = $single_karkun->id;
                                                                            @endphp
                                                                            <option @if (isset($fdco))
                                                                                @if (in_array($fdco_id, $fdco))
                                                                                    selected="selected"
                                                                                @endif
                                                                        @endif
                                                                        value="{{ $single_karkun->id }}">


                                                                        {{ $single_karkun->kp_name . ' / ' . $single_karkun->kp_fname . ' - ' . $single_karkun->kp_id_number }}

                                                                        </option>

                @endforeach
                </select>
                </td>



                <td class="t-main-duty">
                    <select multiple name="main_duty_juma[]" id="form-field-select-4"
                        class="urwajuma_md form-control search-select select2-hidden-accessible" tabindex="-1"
                        aria-hidden="true">

                        @foreach ($all_karkun as $single_karkun)
                            @php
                            $fmd_id = $single_karkun->id;
                            @endphp
                            <option @if (isset($fmd))
                                @if (in_array($fmd_id, $fmd))
                                    selected="selected"
                                @endif
                        @endif
                        value="{{ $single_karkun->id }}">

                        {{ $single_karkun->kp_name . ' / ' . $single_karkun->kp_fname . ' - ' . $single_karkun->kp_id_number }}
                        </option>
                        @endforeach
                    </select>
                </td>

                <td class="t-naat">
                    <select multiple name="cycle_stand_juma[]" id="form-field-select-4"
                        class="urwajuma_cs form-control search-select select2-hidden-accessible" tabindex="-1"
                        aria-hidden="true">

                        @foreach ($all_karkun as $single_karkun)
                            @php
                            $fcs_id = $single_karkun->id;
                            @endphp
                            <option @if (in_array($fcs_id, $fcs))
                                selected="selected"
                        @endif

                        value="{{ $single_karkun->id }}">

                        {{ $single_karkun->kp_name . ' / ' . $single_karkun->kp_fname . ' - ' . $single_karkun->kp_id_number }}

                        </option>
                        @endforeach

                    </select>
                </td>

                <td class="t-cycle">
                    <select multiple name="security_juma[]" id="form-field-select-4"
                        class="urwajuma_s form-control search-select select2-hidden-accessible" tabindex="-1"
                        aria-hidden="true">

                        @foreach ($all_karkun as $single_karkun)
                            @php
                            $fs_id = $single_karkun->id;
                            @endphp
                            <option @if (in_array($fs_id, $fs))
                                selected="selected"
                        @endif

                        value="{{ $single_karkun->id }}">

                        {{ $single_karkun->kp_name . ' / ' . $single_karkun->kp_fname . ' - ' . $single_karkun->kp_id_number }}

                        </option>
                        @endforeach


                    </select>
                </td>
                </tr>

                <tr>
                    <td class="t-day">
                        <div class="text-center">
                            ہفتہ
                        </div>
                    </td>

                    <td class="t-ehad-ca">
<!--                         <select id="form-field-select-3" name="ehd_karkun_hafta"
                            class="form-control search-select select2-hidden-accessible" tabindex="-1"
                            aria-hidden="true" disabled="disabled"> -->

                            @if ($all_k_duty_roster_saturday && !empty($all_k_duty_roster_saturday->ehad_karkun))

    <input type="hidden" name="ehd_karkun_hafta" value="{{ $all_k_duty_roster_saturday->ehad_karkun }}">
    {{ $all_k_duty_roster_saturday->ekp_name }}
<!--                                 <option value="{{ $all_k_duty_roster_saturday->ehad_karkun }}">
                                    {{ $all_k_duty_roster_saturday->ekp_name }}
                                </option> -->
                            @else

                                @if (isset($get_ehad_karkun_hafta))

    <input type="hidden" name="ehd_karkun_hafta" value="{{ $get_ehad_karkun_hafta->ehad_karkun_id }}">
    <span style="color: red !important;">{{ $get_ehad_karkun_hafta->ekp_name }}</span>

<!--                                     <option value="{{ $get_ehad_karkun_hafta->ehad_karkun_id }}">
                                        {{ $get_ehad_karkun_hafta->ekp_name }}
                                    </option> -->
                                @else
                                    <!-- <option value=""> عہد کارکن کا نام۔۔۔</option> -->
                                @endif


                            @endif
<!--                             @foreach ($all_ehad_karkun as $single_ehad_karkun)
                                <option value="{{ $single_ehad_karkun->id }}">{{ $single_ehad_karkun->ekp_name }}
                                </option>
                            @endforeach -->
                        <!-- </select> -->
                    </td>


                    <td>
                        <select id="form-field-select-3" name="day_coordinator_hafta"
                            class="form-control search-select select2-hidden-accessible" tabindex="-1"
                            aria-hidden="true">
                            <option value=""> منتخب کریں۔۔۔ </option>
                            @foreach ($all_karkun as $single_karkun)
                                @php
                                $sdco_id = $single_karkun->id;
                                @endphp
                                <option @if (isset($sdco))
                                    @if (in_array($sdco_id, $sdco))
                                        selected="selected"
                                    @endif
                            @endif
                            value="{{ $single_karkun->id }}">
                            {{ $single_karkun->kp_name . ' / ' . $single_karkun->kp_fname . ' - ' . $single_karkun->kp_id_number }}

                            </option>

                            @endforeach
                        </select>
                    </td>


                    <td class="t-main-duty">
                        <select multiple id="form-field-select-4" name="main_duty_hafta[]"
                            class="form-control search-select select2-hidden-accessible" tabindex="-1"
                            aria-hidden="true">

                            @foreach ($all_karkun as $single_karkun)
                                @php
                                $smd_id = $single_karkun->id;
                                @endphp
                                <option @if (in_array($smd_id, $smd))
                                    selected="selected"
                            @endif

                            value="{{ $single_karkun->id }}">

                            {{ $single_karkun->kp_name . ' / ' . $single_karkun->kp_fname . ' - ' . $single_karkun->kp_id_number }}

                            </option>
                            @endforeach

                        </select>
                    </td>

                    <td class="t-naat">
                        <select multiple id="form-field-select-4" name="cycle_stand_hafta[]"
                            class="form-control search-select select2-hidden-accessible" tabindex="-1"
                            aria-hidden="true">

                            @foreach ($all_karkun as $single_karkun)
                                @php
                                $scs_id = $single_karkun->id;
                                @endphp
                                <option @if (in_array($scs_id, $scs))
                                    selected="selected"
                            @endif

                            value="{{ $single_karkun->id }}">

                            {{ $single_karkun->kp_name . ' / ' . $single_karkun->kp_fname . ' - ' . $single_karkun->kp_id_number }}

                            </option>
                            @endforeach

                        </select>
                    </td>

                    <td class="t-cycle">
                        <select multiple id="form-field-select-4" name="security_hafta[]"
                            class="form-control search-select select2-hidden-accessible" tabindex="-1"
                            aria-hidden="true">

                            @foreach ($all_karkun as $single_karkun)
                                @php
                                $ss_id = $single_karkun->id;
                                @endphp
                                <option @if (in_array($ss_id, $ss))
                                    selected="selected"
                            @endif

                            value="{{ $single_karkun->id }}">

                            {{ $single_karkun->kp_name . ' / ' . $single_karkun->kp_fname . ' - ' . $single_karkun->kp_id_number }}

                            </option>
                            @endforeach

                        </select>
                    </td>
                </tr>

                <tr>
                    <td class="t-day">
                        <div class="text-center">
                            اتوار
                        </div>
                    </td>

                    <td class="t-ehad-ca">
<!--                         <select id="form-field-select-3" name="ehd_karkun_itwar"
                            class="form-control search-select select2-hidden-accessible" tabindex="-1"
                            aria-hidden="true" disabled="disabled"> -->

                            @if ($all_k_duty_roster_sunday && !empty($all_k_duty_roster_sunday->ehad_karkun))

    <input type="hidden" name="ehd_karkun_itwar" value="{{ $all_k_duty_roster_sunday->ehad_karkun }}">
    {{ $all_k_duty_roster_sunday->ekp_name }}

<!--                                 <option value="{{ $all_k_duty_roster_sunday->ehad_karkun }}">
                                    {{ $all_k_duty_roster_sunday->ekp_name }}
                                </option> -->
                            @else

                                @if (isset($get_ehad_karkun_itwar))

    <input type="hidden" name="ehd_karkun_itwar" value="{{ $get_ehad_karkun_itwar->ehad_karkun_id }}">
    <span style="color: red !important;">{{ $get_ehad_karkun_itwar->ekp_name }}</span>


<!--                                     <option value="{{ $get_ehad_karkun_itwar->ehad_karkun_id }}">
                                        {{ $get_ehad_karkun_itwar->ekp_name }}
                                    </option> -->
                                @else
                                    <!-- <option value=""> عہد کارکن کا نام۔۔۔</option> -->
                                @endif

                            @endif

<!--                             @foreach ($all_ehad_karkun as $single_ehad_karkun)
                                <option value="{{ $single_ehad_karkun->id }}">{{ $single_ehad_karkun->ekp_name }}
                                </option>
                            @endforeach -->

                        <!-- </select> -->
                    </td>


                    <td>
                        <select id="form-field-select-3" name="day_coordinator_itwar"
                            class="form-control search-select select2-hidden-accessible" tabindex="-1"
                            aria-hidden="true">

                            <option value=""> منتخب کریں۔۔۔ </option>

                            @foreach ($all_karkun as $single_karkun)
                                @php
                                $sndco_id = $single_karkun->id;
                                @endphp
                                <option @if (isset($sndco))
                                    @if (in_array($sndco_id, $sndco))
                                        selected="selected"
                                    @endif
                            @endif
                            value="{{ $single_karkun->id }}">
                            {{ $single_karkun->kp_name . ' / ' . $single_karkun->kp_fname . ' - ' . $single_karkun->kp_id_number }}
                            </option>
                            @endforeach

                        </select>
                    </td>


                    <td class="t-main-duty">

                        <select multiple id="form-field-select-4" name="main_duty_itwar[]"
                            class="form-control search-select select2-hidden-accessible" tabindex="-1"
                            aria-hidden="true">

                            @foreach ($all_karkun as $single_karkun)
                                @php
                                $snmd_id = $single_karkun->id;
                                @endphp
                                <option @if (in_array($snmd_id, $snmd))
                                    selected="selected"
                            @endif

                            value="{{ $single_karkun->id }}">

                            {{ $single_karkun->kp_name . ' / ' . $single_karkun->kp_fname . ' - ' . $single_karkun->kp_id_number }}

                            </option>
                            @endforeach

                        </select>

                    </td>

                    <td class="t-naat">
                        <select multiple id="form-field-select-4" name="cycle_stand_itwar[]"
                            class="form-control search-select select2-hidden-accessible" tabindex="-1"
                            aria-hidden="true">

                            @foreach ($all_karkun as $single_karkun)
                                @php
                                $sncs_id = $single_karkun->id;
                                @endphp
                                <option @if (in_array($sncs_id, $sncs))
                                    selected="selected"
                            @endif

                            value="{{ $single_karkun->id }}">

                            {{ $single_karkun->kp_name . ' / ' . $single_karkun->kp_fname . ' - ' . $single_karkun->kp_id_number }}

                            </option>
                            @endforeach

                        </select>
                    </td>

                    <td class="t-cycle">
                        <select multiple id="form-field-select-4" name="security_itwar[]"
                            class="form-control search-select select2-hidden-accessible" tabindex="-1"
                            aria-hidden="true">

                            @foreach ($all_karkun as $single_karkun)
                                @php
                                $sns_id = $single_karkun->id;
                                @endphp
                                <option @if (in_array($sns_id, $sns))
                                    selected="selected"
                            @endif

                            value="{{ $single_karkun->id }}">

                            {{ $single_karkun->kp_name . ' / ' . $single_karkun->kp_fname . ' - ' . $single_karkun->kp_id_number }}

                            </option>
                            @endforeach

                        </select>
                    </td>

                </tr>

                <tr>
                    <td class="t-day">
                        <div class="text-center">
                            سوموار
                        </div>
                    </td>

                    <td class="t-ehad-ca">
<!--                         <select id="form-field-select-3" name="ehd_karkun_peer"
                            class="form-control search-select select2-hidden-accessible" tabindex="-1"
                            aria-hidden="true" disabled="disabled"> -->

                            @if ($all_k_duty_roster_monday && !empty($all_k_duty_roster_monday->ehad_karkun))

    <input type="hidden" name="ehd_karkun_peer" value="{{ $all_k_duty_roster_monday->ehad_karkun }}">
    {{ $all_k_duty_roster_monday->ekp_name }}

<!--                                 <option value="{{ $all_k_duty_roster_monday->ehad_karkun }}">
                                    {{ $all_k_duty_roster_monday->ekp_name }}
                                </option> -->
                            @else

                                @if (isset($get_ehad_karkun_peer))

    <input type="hidden" name="ehd_karkun_peer" value="{{ $get_ehad_karkun_peer->ehad_karkun_id }}">
    <span style="color: red !important;">{{ $get_ehad_karkun_peer->ekp_name }}</span>

<!--                                     <option value="{{ $get_ehad_karkun_peer->ehad_karkun_id }}">
                                        {{ $get_ehad_karkun_peer->ekp_name }}
                                    </option> -->
                                @else
                                    <!-- <option value=""> عہد کارکن کا نام۔۔۔</option> -->
                                @endif

                            @endif

<!--                             @foreach ($all_ehad_karkun as $single_ehad_karkun)
                                <option value="{{ $single_ehad_karkun->id }}">{{ $single_ehad_karkun->ekp_name }}
                                </option>
                            @endforeach
                        </select> -->
                    </td>


                    <td>
                        <select id="form-field-select-3" name="day_coordinator_peer"
                            class="form-control search-select select2-hidden-accessible" tabindex="-1"
                            aria-hidden="true">


                            @foreach ($all_karkun as $single_karkun)
                                @php
                                $mdco_id = $single_karkun->id;
                                @endphp
                                <option @if (isset($mdco))
                                    @if (in_array($mdco_id, $mdco))
                                        selected="selected"
                                    @endif
                            @endif
                            value="{{ $single_karkun->id }}">
                            {{ $single_karkun->kp_name . ' / ' . $single_karkun->kp_fname . ' - ' . $single_karkun->kp_id_number }}
                            </option>
                            @endforeach
                        </select>
                    </td>


                    <td class="t-main-duty">
                        <select multiple id="form-field-select-4" name="main_duty_peer[]"
                            class="form-control search-select select2-hidden-accessible" tabindex="-1"
                            aria-hidden="true">

                            @foreach ($all_karkun as $single_karkun)
                                @php
                                $mmd_id = $single_karkun->id;
                                @endphp
                                <option @if (in_array($mmd_id, $mmd))
                                    selected="selected"
                            @endif

                            value="{{ $single_karkun->id }}">

                            {{ $single_karkun->kp_name . ' / ' . $single_karkun->kp_fname . ' - ' . $single_karkun->kp_id_number }}

                            </option>
                            @endforeach

                        </select>
                    </td>

                    <td class="t-naat">
                        <select multiple id="form-field-select-4" name="cycle_stand_peer[]"
                            class="form-control search-select select2-hidden-accessible" tabindex="-1"
                            aria-hidden="true">

                            @foreach ($all_karkun as $single_karkun)
                                @php
                                $mcs_id = $single_karkun->id;
                                @endphp
                                <option @if (in_array($mcs_id, $mcs))
                                    selected="selected"
                            @endif

                            value="{{ $single_karkun->id }}">

                            {{ $single_karkun->kp_name . ' / ' . $single_karkun->kp_fname . ' - ' . $single_karkun->kp_id_number }}

                            </option>
                            @endforeach

                        </select>
                    </td>

                    <td class="t-cycle">
                        <select multiple id="form-field-select-4" name="security_peer[]"
                            class="form-control search-select select2-hidden-accessible" tabindex="-1"
                            aria-hidden="true">

                            @foreach ($all_karkun as $single_karkun)
                                @php
                                $ms_id = $single_karkun->id;
                                @endphp
                                <option @if (in_array($ms_id, $ms))
                                    selected="selected"
                            @endif

                            value="{{ $single_karkun->id }}">

                            {{ $single_karkun->kp_name . ' / ' . $single_karkun->kp_fname . ' - ' . $single_karkun->kp_id_number }}

                            </option>
                            @endforeach

                        </select>
                    </td>

                </tr>

                <tr>
                    <td class="t-day">
                        <div class="text-center">
                            منگل
                        </div>
                    </td>

                    <td class="t-ehad-ca">
<!--                         <select id="form-field-select-3" name="ehd_karkun_mangal"
                            class="form-control search-select select2-hidden-accessible" tabindex="-1"
                            aria-hidden="true" disabled="disabled"> -->


                            @if ($all_k_duty_roster_tuesday && !empty($all_k_duty_roster_tuesday->ehad_karkun))

    <input type="hidden" name="ehd_karkun_mangal" value="{{ $all_k_duty_roster_tuesday->ehad_karkun }}">
    {{ $all_k_duty_roster_tuesday->ekp_name }}

<!--                                 <option value="{{ $all_k_duty_roster_tuesday->ehad_karkun }}">
                                    {{ $all_k_duty_roster_tuesday->ekp_name }}
                                </option> -->
                            @else

                                @if (isset($get_ehad_karkun_mangal))

    <input type="hidden" name="ehd_karkun_mangal" value="{{ $get_ehad_karkun_mangal->ehad_karkun_id }}">
    <span style="color: red !important;">{{ $get_ehad_karkun_mangal->ekp_name }}</span>


<!--                                     <option value="{{ $get_ehad_karkun_mangal->ehad_karkun_id }}">
                                        {{ $get_ehad_karkun_mangal->ekp_name }}
                                    </option> -->
                                @else
                                    <!-- <option value=""> عہد کارکن کا نام۔۔۔</option> -->
                                @endif

                            @endif

                            <!-- @foreach ($all_ehad_karkun as $single_ehad_karkun)
                                <option value="{{ $single_ehad_karkun->id }}">{{ $single_ehad_karkun->ekp_name }}
                                </option>
                            @endforeach
                        </select> -->
                    </td>


                    <td>
                        <select id="form-field-select-3" name="day_coordinator_mangal"
                            class="form-control search-select select2-hidden-accessible" tabindex="-1"
                            aria-hidden="true">

                            <option value=""> منتخب کریں۔۔۔ </option>

                            @foreach ($all_karkun as $single_karkun)
                                @php
                                $tdco_id = $single_karkun->id;
                                @endphp
                                <option @if (isset($tdco))
                                    @if (in_array($tdco_id, $tdco))
                                        selected="selected"
                                    @endif
                            @endif
                            value="{{ $single_karkun->id }}">
                            {{ $single_karkun->kp_name . ' / ' . $single_karkun->kp_fname . ' - ' . $single_karkun->kp_id_number }}
                            </option>
                            @endforeach
                        </select>
                    </td>


                    <td class="t-main-duty">
                        <select multiple id="form-field-select-4" name="main_duty_mangal[]"
                            class="form-control search-select select2-hidden-accessible" tabindex="-1"
                            aria-hidden="true">

                            @foreach ($all_karkun as $single_karkun)
                                @php
                                $tmd_id = $single_karkun->id;
                                @endphp
                                <option @if (in_array($tmd_id, $tmd))
                                    selected="selected"
                            @endif

                            value="{{ $single_karkun->id }}">

                            {{ $single_karkun->kp_name . ' / ' . $single_karkun->kp_fname . ' - ' . $single_karkun->kp_id_number }}

                            </option>
                            @endforeach

                        </select>
                    </td>

                    <td class="t-naat">
                        <select multiple id="form-field-select-4" name="cycle_stand_mangal[]"
                            class="form-control search-select select2-hidden-accessible" tabindex="-1"
                            aria-hidden="true">

                            @foreach ($all_karkun as $single_karkun)
                                @php
                                $tcs_id = $single_karkun->id;
                                @endphp
                                <option @if (in_array($tcs_id, $tcs))
                                    selected="selected"
                            @endif

                            value="{{ $single_karkun->id }}">

                            {{ $single_karkun->kp_name . ' / ' . $single_karkun->kp_fname . ' - ' . $single_karkun->kp_id_number }}

                            </option>
                            @endforeach

                        </select>
                    </td>

                    <td class="t-cycle">
                        <select multiple id="form-field-select-4" name="security_mangal[]"
                            class="form-control search-select select2-hidden-accessible" tabindex="-1"
                            aria-hidden="true">

                            @foreach ($all_karkun as $single_karkun)
                                @php
                                $ts_id = $single_karkun->id;
                                @endphp
                                <option @if (in_array($ts_id, $ts))
                                    selected="selected"
                            @endif

                            value="{{ $single_karkun->id }}">

                            {{ $single_karkun->kp_name . ' / ' . $single_karkun->kp_fname . ' - ' . $single_karkun->kp_id_number }}

                            </option>
                            @endforeach

                        </select>
                    </td>
                </tr>

                <tr>
                    <td class="t-day">
                        <div class="text-center">
                            بدھ
                        </div>
                    </td>



                    <td class="t-ehad-ca">
<!--                         <select id="form-field-select-3" name="ehd_karkun_budh"
                            class="form-control search-select select2-hidden-accessible" tabindex="-1"
                            aria-hidden="true" disabled="disabled"> -->

                            @if ($all_k_duty_roster_wednesday && !empty($all_k_duty_roster_wednesday->ehad_karkun))

    <input type="hidden" name="ehd_karkun_budh" value="{{ $all_k_duty_roster_wednesday->ehad_karkun }}">
    {{ $all_k_duty_roster_wednesday->ekp_name }}

<!--                                 <option value="{{ $all_k_duty_roster_wednesday->ehad_karkun }}">
                                    {{ $all_k_duty_roster_wednesday->ekp_name }}
                                </option> -->
                            @else

                                @if (isset($get_ehad_karkun_budh))

    <input type="hidden" name="ehd_karkun_budh" value="{{ $get_ehad_karkun_budh->ehad_karkun_id }}">
    <span style="color: red !important;">{{ $get_ehad_karkun_budh->ekp_name }}</span>


<!--                                     <option value="{{ $get_ehad_karkun_budh->ehad_karkun_id }}">
                                        {{ $get_ehad_karkun_budh->ekp_name }}
                                    </option> -->
                                @else
                                    <!-- <option value=""> عہد کارکن کا نام۔۔۔</option> -->
                                @endif

                            @endif

<!--                             @foreach ($all_ehad_karkun as $single_ehad_karkun)
                                <option value="{{ $single_ehad_karkun->id }}">{{ $single_ehad_karkun->ekp_name }}
                                </option>
                            @endforeach
                        </select> -->
                    </td>


                    <td>
                        <select id="form-field-select-3" name="day_coordinator_budh"
                            class="form-control search-select select2-hidden-accessible" tabindex="-1"
                            aria-hidden="true">

                            <option value=""> منتخب کریں۔۔۔ </option>

                            @foreach ($all_karkun as $single_karkun)
                                @php
                                $wdco_id = $single_karkun->id;
                                @endphp
                                <option @if (isset($wdco))
                                    @if (in_array($wdco_id, $wdco))
                                        selected="selected"
                                    @endif
                            @endif
                            value="{{ $single_karkun->id }}">
                            {{ $single_karkun->kp_name . ' / ' . $single_karkun->kp_fname . ' - ' . $single_karkun->kp_id_number }}
                            </option>
                            @endforeach

                        </select>
                    </td>


                    <td class="t-main-duty">
                        <select multiple id="form-field-select-4" name="main_duty_budh[]"
                            class="form-control search-select select2-hidden-accessible" tabindex="-1"
                            aria-hidden="true">

                            @foreach ($all_karkun as $single_karkun)
                                @php
                                $wmd_id = $single_karkun->id;
                                @endphp
                                <option @if (in_array($wmd_id, $wmd))
                                    selected="selected"
                            @endif

                            value="{{ $single_karkun->id }}">

                            {{ $single_karkun->kp_name . ' / ' . $single_karkun->kp_fname . ' - ' . $single_karkun->kp_id_number }}

                            </option>
                            @endforeach

                        </select>
                    </td>

                    <td class="t-naat">
                        <select multiple id="form-field-select-4" name="cycle_stand_budh[]"
                            class="form-control search-select select2-hidden-accessible" tabindex="-1"
                            aria-hidden="true">

                            @foreach ($all_karkun as $single_karkun)
                                @php
                                $wcs_id = $single_karkun->id;
                                @endphp
                                <option @if (in_array($wcs_id, $wcs))
                                    selected="selected"
                            @endif

                            value="{{ $single_karkun->id }}">

                            {{ $single_karkun->kp_name . ' / ' . $single_karkun->kp_fname . ' - ' . $single_karkun->kp_id_number }}

                            </option>
                            @endforeach

                        </select>
                    </td>

                    <td class="t-cycle">
                        <select multiple id="form-field-select-4" name="security_budh[]"
                            class="form-control search-select select2-hidden-accessible" tabindex="-1"
                            aria-hidden="true">

                            @foreach ($all_karkun as $single_karkun)
                                @php
                                $ws_id = $single_karkun->id;
                                @endphp
                                <option @if (in_array($ws_id, $ws))
                                    selected="selected"
                            @endif

                            value="{{ $single_karkun->id }}">

                            {{ $single_karkun->kp_name . ' / ' . $single_karkun->kp_fname . ' - ' . $single_karkun->kp_id_number }}

                            </option>
                            @endforeach
                        </select>
                    </td>

                </tr>

                <tr>
                    <td class="t-day">
                        <div class="text-center">
                            جمعرات
                        </div>
                    </td>

                    <td class="t-ehad-ca">
<!--                         <select id="form-field-select-3" name="ehd_karkun_jumerat"
                            class="form-control search-select select2-hidden-accessible" tabindex="-1"
                            aria-hidden="true" disabled="disabled"> -->

                            @if ($all_k_duty_roster_thursday && !empty($all_k_duty_roster_thursday->ehad_karkun))

    <input type="hidden" name="ehd_karkun_jumerat" value="{{ $all_k_duty_roster_thursday->ehad_karkun }}">
    {{ $all_k_duty_roster_thursday->ekp_name }}

<!--                                 <option value="{{ $all_k_duty_roster_thursday->ehad_karkun }}">
                                    {{ $all_k_duty_roster_thursday->ekp_name }}
                                </option> -->
                            @else

                                @if (isset($get_ehad_karkun_jumerat))

    <input type="hidden" name="ehd_karkun_jumerat" value="{{ $get_ehad_karkun_jumerat->ehad_karkun_id }}">
    <span style="color: red !important;">{{ $get_ehad_karkun_jumerat->ekp_name }}</span>



<!--                                     <option value="{{ $get_ehad_karkun_jumerat->ehad_karkun_id }}">
                                        {{ $get_ehad_karkun_jumerat->ekp_name }}
                                    </option> -->
                                @else
                                    <!-- <option value=""> عہد کارکن کا نام۔۔۔</option> -->
                                @endif

                            @endif

<!--                             @foreach ($all_ehad_karkun as $single_ehad_karkun)
                                <option value="{{ $single_ehad_karkun->id }}">{{ $single_ehad_karkun->ekp_name }}
                                </option>
                            @endforeach
                        </select> -->
                    </td>


                    <td>
                        <select id="form-field-select-3" name="day_coordinator_jumerat"
                            class="form-control search-select select2-hidden-accessible" tabindex="-1"
                            aria-hidden="true">

                            <option value=""> منتخب کریں۔۔۔ </option>

                            @foreach ($all_karkun as $single_karkun)
                                @php
                                $thdco_id = $single_karkun->id;
                                @endphp
                                <option @if (isset($thdco))
                                    @if (in_array($thdco_id, $thdco))
                                        selected="selected"
                                    @endif
                            @endif
                            value="{{ $single_karkun->id }}">
                            {{ $single_karkun->kp_name . ' / ' . $single_karkun->kp_fname . ' - ' . $single_karkun->kp_id_number }}
                            </option>
                            @endforeach
                        </select>
                    </td>


                    <td class="t-main-duty">
                        <select multiple id="form-field-select-4" name="main_duty_jumerat[]"
                            class="form-control search-select select2-hidden-accessible" tabindex="-1"
                            aria-hidden="true">

                            @foreach ($all_karkun as $single_karkun)
                                @php
                                $thmd_id = $single_karkun->id;
                                @endphp
                                <option @if (in_array($thmd_id, $thmd))
                                    selected="selected"
                            @endif

                            value="{{ $single_karkun->id }}">

                            {{ $single_karkun->kp_name . ' / ' . $single_karkun->kp_fname . ' - ' . $single_karkun->kp_id_number }}

                            </option>
                            @endforeach

                        </select>
                    </td>

                    <td class="t-naat">
                        <select multiple id="form-field-select-4" name="cycle_stand_jumerat[]"
                            class="form-control search-select select2-hidden-accessible" tabindex="-1"
                            aria-hidden="true">

                            @foreach ($all_karkun as $single_karkun)
                                @php
                                $thcs_id = $single_karkun->id;
                                @endphp
                                <option @if (in_array($thcs_id, $thcs))
                                    selected="selected"
                            @endif

                            value="{{ $single_karkun->id }}">

                            {{ $single_karkun->kp_name . ' / ' . $single_karkun->kp_fname . ' - ' . $single_karkun->kp_id_number }}

                            </option>
                            @endforeach

                        </select>
                    </td>

                    <td class="t-cycle">
                        <select multiple id="form-field-select-4" name="security_jumerat[]"
                            class="form-control search-select select2-hidden-accessible" tabindex="-1"
                            aria-hidden="true">

                            @foreach ($all_karkun as $single_karkun)
                                @php
                                $ths_id = $single_karkun->id;
                                @endphp
                                <option @if (in_array($ths_id, $ths))
                                    selected="selected"
                            @endif

                            value="{{ $single_karkun->id }}">

                            {{ $single_karkun->kp_name . ' / ' . $single_karkun->kp_fname . ' - ' . $single_karkun->kp_id_number }}

                            </option>
                            @endforeach

                        </select>
                    </td>
                </tr>

                <tr>
                    <td class="t-day">
                        <div class="text-center">
                            ماہانہ کوارڈینیٹر
                        </div>
                    </td>

                    <td colspan="4">
                        <select multiple id="form-field-select-4" name="mahana_coordinator[]"
                            class="form-control search-select select2-hidden-accessible" tabindex="-1"
                            aria-hidden="true">


                            @foreach ($all_karkun as $single_ehad_karkun)
                                @php
                                $m_c_id = $single_ehad_karkun->id;
                                @endphp
                                <option @if (in_array($m_c_id, $m_c))
                                    selected="selected"
                            @endif

                            value="{{ $single_ehad_karkun->id }}">

                            {{ $single_ehad_karkun->kp_name . ' / ' . $single_ehad_karkun->kp_fname . ' - ' . $single_ehad_karkun->kp_id_number }}

                            </option>
                            @endforeach

                        </select>
                    </td>
                </tr>
                </tbody>
                </table>
                <h1 id="footer_text" style="display: none;font-size: 20px;">
                    <span style="color: red;">نوٹ:</span>
                    تمام بھائ اپنی ڈیوٹی والے دن محفل والے ٹائم سے 15 سے 20 منٹ پہلے آئیں،محفل پر ماہانہ %50 حاضری کو
                    یقینی بنائیں ملتان شریف منگل ڈیوٹی پر ہر دو ماہ میں ایک بار جانا ضروری ہے۔
                </h1>

            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <hr>
                <div>
                    <button id="btn_submit" type="submit" value="Submit" class="btn btn-blue btn-block w-150">اندراج
                        کریں</button>
                </div>
            </div>
        </div>
    </div>

    </form>
    <!-- complete form for hazri -->




    </div>
    </div>
    </div>

    </div>
    <!-- end: PAGE CONTENT-->
    @endif

    </div>

    </div>
    <!-- end: PAGE -->
    </div>
    </div>
    <!-- end: MAIN CONTAINER -->

    <!-- start: FOOTER & scripts -->
    @include('inc.footer')

    <script>
        $(document).ready(function() {
            $(document).find(".urwasearch").select2();
        });

        $('.urwasearch').on('change', function() {
            // alert('fasdfds');
            var value = $(document).find('.urwasearch option:selected').val();
            location.href = value;
        });



        // $(function() {
        //     $('.urwajuma_md').change(function(e) {

        //       var urwajuma_md = [];
        //       var urwajuma_cs = [];
        //       var urwajuma_s = [];

        //       $('.urwajuma_md :selected').each(function(){
        //           value = $(this).val();
        //           urwajuma_md.push(value); 
        //       });

        //       $('.urwajuma_cs :selected').each(function(){
        //           value = $(this).val();
        //           urwajuma_cs.push(value); 
        //       });

        //       $('.urwajuma_s :selected').each(function(){
        //           value = $(this).val();
        //           urwajuma_s.push(value); 
        //       });

        //       var selected = $(e.target).val();
        //       upload = 0;
        //       for (var i = 0; i < selected.length; i++) {
        //         if( 
        //           // $.inArray(selected[i], urwajuma_md) !== -1 || 
        //             $.inArray(selected[i], urwajuma_cs) !== -1 || 
        //             $.inArray(selected[i], urwajuma_s) !== -1 ) {
        //             upload = 1;
        //         }        
        //       }
        //       if (upload == 1) {
        //         alert('found');
        //       }
        //     }); 
        // });

        // $(document).ready(function() {

        //     $('.urwajuma_cs').change(function(e) {
        //       console.log(e);
        //       var urwajuma_md = [];
        //       var urwajuma_cs = [];
        //       var urwajuma_s = [];

        //           $('.urwajuma_md :selected').each(function(){
        //               value = $(this).val();
        //               urwajuma_md.push(value); 
        //           });

        //           $('.urwajuma_cs :selected').each(function(){
        //               value = $(this).val();
        //               urwajuma_cs.push(value); 
        //           });

        //           $('.urwajuma_s :selected').each(function(){
        //               value = $(this).val();
        //               urwajuma_s.push(value); 
        //           });

        //         var selected = $(e.target).val();
        //         var upload = 0;

        //       for (var i = 0; i < selected.length; i++) {
        //         if( 
        //             // $.inArray(selected[i], urwajuma_cs) !== -1 ||
        //             $.inArray(selected[i], urwajuma_md) !== -1 || 
        //             $.inArray(selected[i], urwajuma_s) !== -1 ) {

        //             $(this).closest('td').find('.select2-search-choice').last().remove();
        //             // $(this).closest('td').find('.select2-search-choice').last().css('background' , 'lightblue');     

        //             // $(this).closest('td').find('.select2-search-choice').children().slice(0, n).remove();

        //             // var wanted_id = $(this).attr("data-val");
        //             // var wanted_option = $('.urwajuma_cs option[value="'+ wanted_id +'"]');
        //             // wanted_option.prop('selected', true);

        //             upload = 1;
        //         }          
        //       }
        //       if (upload == 1) {
        //       }
        //     }); 
        // });

        // $(document).ready(function(){
        //   $('#print_btn').click(function(){
        //     window.print();
        //   });
        // });





    </script>

</body>
<!-- end: BODY -->

</html>
