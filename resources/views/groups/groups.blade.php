<!DOCTYPE html>
<html lang="en" class="no-js">
<!-- start: HEAD -->
<head>
    <title>  گروپس    </title>

    @include('inc.head')

    <style>
        th , td{
            text-align : center !important;
        }
        #contacts_table_wrapper .select2-container{
            width: 100px;
        }
        #contacts_table_wrapper .select2-container span.select2-chosen{
            text-align: right;
        }
        table.table thead .sorting{
            text-align: right;
            background: url('assets/plugins/DataTables/media/css/images/sort_both.png') no-repeat center left;
        }
        table.table thead .sorting_asc{
            text-align: right;
            background: url('assets/plugins/DataTables/media/css/images/sort_asc.png') no-repeat center left;
        }
        table.table thead .sorting_desc{
            text-align: right;
            background: url('assets/plugins/DataTables/media/css/images/sort_desc.png') no-repeat center left;
        }


        .lds-dual-ring {
            display: inline-block;
            width: 40px;
            height: 40px;
        }
        .lds-dual-ring:after {
            content: " ";
            display: block;
            width: 160px;
            height: 160px;
            margin: 20% auto;
            border-radius: 50%;
            border: 18px solid #fff;
            border-color: #fff transparent #fff transparent;
            animation: lds-dual-ring 1.2s linear infinite;
        }
        @keyframes lds-dual-ring {
            0% {
                transform: rotate(0deg);
            }
            100% {
                transform: rotate(360deg);
            }
        }

        .overlay {
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100vh;
            background: rgba(0,0,0,.8);
            z-index: 9999;
            opacity: 1;
            transition: all 0.5s;
        }

    </style>
</head>

<!-- end: HEAD -->

<!-- start: BODY -->
<body class="rtl">    
    <!-- start: HEADER -->
    <div class="navbar navbar-inverse navbar-fixed-top">
        <!-- start: TOP NAVIGATION CONTAINER -->
        <div class="container">
            <div class="navbar-header">
                <!-- start: RESPONSIVE MENU TOGGLER -->
                <button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
                    <span class="clip-list-2"></span>
                </button>
                <!-- end: RESPONSIVE MENU TOGGLER -->
                <!-- end: LOGO -->
            </div>
        </div>
        <!-- end: TOP NAVIGATION CONTAINER -->
    </div>
    <!-- end: HEADER -->

    <!-- start: MAIN CONTAINER -->
    <div class="main-container">
        <div class="navbar-content">
            <!-- start: SIDEBAR -->
            @include('inc.sidebar')
            <!-- end: SIDEBAR -->
        </div>
        <!-- start: PAGE -->
        <div class="main-content">

            <div class="container" style="min-height: 760px;">
                <!-- start: PAGE HEADER -->
                <div class="row">
                    <div class="col-sm-12">
                        <!-- start: PAGE TITLE & BREADCRUMB -->
                        <ol class="breadcrumb">
                            <li>
                                <i class=""></i>
                                {{$committee_name}}      
                            </li>
                            <li class="active">
                            </li>
                        </ol>

                        <div class="page-header">
                            <h1>
                                {{$committee_name}}     
                                <small style="font-size:25px;margin-right:30px;"></small>
                            </h1>
                        </div>
                        <!-- end: PAGE TITLE & BREADCRUMB -->
                    </div>
                </div>
                <!-- end: PAGE HEADER -->
                <!-- start: PAGE CONTENT -->

                <div class="row">
                    <div class="col-md-12">
                        <!-- start: DYNAMIC TABLE PANEL -->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <i class="fa fa-external-link-square"></i>
                                {{$committee_name}}      
                            </div>


                            <div class="panel-body">

                                <div class="row">
                                        <center><h1 style="color: red !important">{{$committee_name}}</h1></center>
                                    <!-- <div class="col-md-12">

                                        <label for="checkAll">   تمام افراد منتخب کریں   </label>
                                        <input type="checkbox" id="checkAll">
                                    </div> -->
                                </div>

                                <div class="row">
                                    <!-- selected persons -->
                                    <div class="col-md-6"> 
                                        <div class="table-responsive">
                                            <div id="contacts_table_wrapper" class="dataTables_wrapper form-inline" role="grid">
                                                <form id="submit_group_form">
                                                    @csrf
                                                    <input type="hidden" id="check_level" name="check_level" value="{{$check_level}}">
                                                    
                                                    <input type="hidden" id="city_logged_in" name="city_logged_in" value="{{$city_logged_in}}">

                                                    <input type="hidden" name="get_id_mehfil" id="get_id_mehfil" value="@if(isset($get_id_mehfil)){{$get_id_mehfil}}@endif">
                                                    
                                                    <input type="hidden" name="committee_id" id="committee_id" value="{{$committee_id}}">

                                                    <!-- <table class="table table-striped table-bordered table-hover table-full-width" id="contacts_table"> -->
                                                    <table width="100%" border="1">
                                                        <thead style="font-size: 19px !important;">
                                                            <tr>
                                                                <th>   منتخب کریں  </th>
                                                                <th>  کوڈ   </th>
                                                                <th> نام</th>
                                                                <th> فون نمبر </th>
                                                            </tr>
                                                        </thead>

                                                        <tbody class="add_row_selected">
                                                            
                                                            @if($check_level == 'city')

                                                                @foreach($get_all_selected as $single_selected)
                                                                    <tr class="on_row_click_selected"
                                                                            data-id="{{$single_selected->id}}" 
                                                                            data-code="{{$single_selected->ekp_id_number}}" 
                                                                            data-phone="{{$single_selected->ekp_phone}}" 
                                                                            data-name="{{$single_selected->ekp_name}}">

                                                                        <td>
                                                                            <input type="checkbox" value="{{$single_selected->id}}" name="ids[]" checked="checked" class="form-control checked_all">
                                                                        </td>

                                                                        <td>{{$single_selected->ekp_id_number}}</td>
                                                                        <td>{{$single_selected->ekp_name}}</td>
                                                                        <td>{{$single_selected->ekp_phone}}</td>
                                                                    </tr>
                                                                @endforeach
                                                            @else

                                                                @foreach($get_all_selected as $single_selected)
                                                                    <tr class="on_row_click_selected"
                                                                            data-id="{{$single_selected->id}}" 
                                                                            data-phone="{{$single_selected->kp_phone}}" 
                                                                            data-code="{{$single_selected->kp_id_number}}" 
                                                                            data-name="{{$single_selected->kp_name}}">

                                                                        <td>
                                                                            <input type="checkbox" value="{{$single_selected->id}}" name="ids[]" checked="checked" class="form-control checked_all">
                                                                        </td>

                                                                        <td>{{$single_selected->kp_id_number}}</td>
                                                                        <td>{{$single_selected->kp_name}}</td>
                                                                        <td>{{$single_selected->kp_phone}}</td>
                                                                    </tr>
                                                                @endforeach

                                                            @endif

                                                        </tbody>
                                                    </table>
                                                </form>
                                            </div>
                                            <div id="loader" class="lds-dual-ring hidden overlay noprint"></div>
                                        </div>
                                    </div>

                                    <!-- non selected persons -->
                                    <div class="col-md-6"> 
                                        <div class="table-responsive">
                                            <div id="contacts_table_wrapper" class="dataTables_wrapper form-inline" role="grid">
                                                <form id="submit_group_form">
                                                    @csrf
                                                    <input type="hidden" id="check_level" name="check_level" value="{{$check_level}}">
                                                    
                                                    <input type="hidden" id="city_logged_in" name="city_logged_in" value="{{$city_logged_in}}">

                                                    <input type="hidden" name="get_id_mehfil" id="get_id_mehfil" value="@if(isset($get_id_mehfil)){{$get_id_mehfil}}@endif">
                                                    
                                                    <input type="hidden" name="committee_id" id="committee_id" value="{{$committee_id}}">

                                                    <!-- <table class="table table-striped table-bordered table-hover table-full-width" id="contacts_table"> -->
                                                    <table width="100%" border="1">
                                                        <thead style="font-size: 19px !important;">
                                                            <tr>
                                                                <th>   منتخب کریں  </th>
                                                                <th> کوڈ  </th>
                                                                <th> نام</th>
                                                                <th>  فون نمبر  </th>
                                                            </tr>
                                                        </thead>

                                                        <tbody class="add_row_deselected">

                                                            @if($check_level == 'city')

                                                                @foreach($get_all_members_not_selected as $single_not_selected)
                                                                    <tr class="on_row_click_not_selected" 
                                                                                data-id="{{$single_not_selected->id}}" 
                                                                                data-phone="{{$single_not_selected->ekp_phone}}" 
                                                                                data-code="{{$single_not_selected->ekp_id_number}}" 
                                                                                data-name="{{$single_not_selected->ekp_name}}">
                                                                        <td>
                                                                            <input type="checkbox" value="{{$single_not_selected->id}}" name="ids[]" class="form-control checked_all">
                                                                        </td>

                                                                        <td>{{$single_not_selected->ekp_id_number}}</td>
                                                                        <td>{{$single_not_selected->ekp_name}}</td>
                                                                        <td>{{$single_not_selected->ekp_phone}}</td>
                                                                    </tr>
                                                                @endforeach

                                                            @else

                                                                @foreach($get_all_members_not_selected as $single_not_selected)
                                                                    <tr class="on_row_click_not_selected" 
                                                                                data-id="{{$single_not_selected->id}}" 
                                                                                data-code="{{$single_not_selected->kp_id_number}}" 
                                                                                data-phone="{{$single_not_selected->kp_phone}}" 
                                                                                data-name="{{$single_not_selected->kp_name}}">
                                                                        <td>
                                                                            <input type="checkbox" value="{{$single_not_selected->id}}" name="ids[]" class="form-control checked_all">
                                                                        </td>

                                                                        <td>{{$single_not_selected->kp_id_number}}</td>
                                                                        <td>{{$single_not_selected->kp_name}}</td>
                                                                        <td>{{$single_not_selected->kp_phone}}</td>
                                                                    </tr>
                                                                @endforeach

                                                            @endif

                                                        </tbody>
                                                    </table>

                                                    <!-- <div class="row">
                                                        <div class="col-md-6">
                                                            <button id="submit" onclick="submit_group(event)" class="btn btn-primary"> اندراج کریں </button>
                                                        </div>
                                                    </div> -->
                                                </form>
                                            </div>
                                            <div id="loader" class="lds-dual-ring hidden overlay noprint"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

<!-- end: PAGE CONTENT-->
            </div>
        </div>
<!-- end: PAGE -->
    </div>
<!-- end: MAIN CONTAINER -->



<!-- start: FOOTER & scripts -->
@include('inc.footer')

<script type="text/javascript">

    function submit_group(e) {
        e.preventDefault();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('[name="_token"]').val()
            }
        });

        /* Submit form data using ajax*/
        $.ajax({
            url: "{{url('groups/add')}}",
            method: 'post',
            // dataType: 'JSON',
            data: $('#submit_group_form').serialize(),
            beforeSend: function() {
                $('#loader').removeClass('hidden');
            },
            success: function(response){
                $('#loader').addClass('hidden');
                $("#submit").html(response);
        }});       
    }


    $("#checkAll").click(function () {
        $('.checked_all').not(this).prop('checked', this.checked);
    });


    // $('.on_row_click_selected').click(function (event) {
    //     if (event.target.type !== 'checkbox') {
    //         $(':checkbox', this).trigger('click');
    //     }
    // });


    $(document).on('click', '.on_row_click_selected' , function (event) {

        var check_level = $("#check_level").val(); 
        var city_logged_in = $("#city_logged_in").val();
        var get_id_mehfil = $("#get_id_mehfil").val();
        var committee_id = $("#committee_id").val();

        var id = $(this).attr("data-id");
        var name = $(this).attr("data-name");
        var code = $(this).attr("data-code");
        var phone = $(this).attr("data-phone");
    
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('[name="_token"]').val()
            }
        });

        $(this).remove();

        /* Submit form data using ajax*/
        $.ajax({
            url: "{{url('groups/delete/single')}}",
            method: 'post',
            // dataType: 'JSON',
            data: {
                check_level : check_level,
                city_logged_in : city_logged_in,
                get_id_mehfil : get_id_mehfil,
                committee_id : committee_id,
                id : id,
            },
            beforeSend: function() {
                $('#loader').removeClass('hidden');
            },
            success: function(response){
                $('#loader').addClass('hidden');
            
                $('.add_row_deselected').prepend("<tr style='height: 38px;' class='on_row_click_not_selected' data-id='"+id+"' data-name='"+name+"' data-code='"+code+"' data-phone='"+phone+"'><td><input type='checkbox' ></td><td>"+code+"</td><td>"+name+"</td><td>"+phone+"</td></tr>");

        }});  

    });





    $(document).on('click', '.on_row_click_not_selected' , function (event) {

        var check_level = $("#check_level").val(); 
        var city_logged_in = $("#city_logged_in").val();
        var get_id_mehfil = $("#get_id_mehfil").val();
        var committee_id = $("#committee_id").val();

        var id = $(this).attr("data-id");
        var name = $(this).attr("data-name");
        var code = $(this).attr("data-code");
        var phone = $(this).attr("data-phone");
    
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('[name="_token"]').val()
            }
        });

        $(this).remove();

        /* Submit form data using ajax*/
        $.ajax({
            url: "{{url('groups/add/single')}}",
            method: 'post',
            // dataType: 'JSON',
            data: {
                check_level : check_level,
                city_logged_in : city_logged_in,
                get_id_mehfil : get_id_mehfil,
                committee_id : committee_id,
                id : id,
            },
            beforeSend: function() {
                $('#loader').removeClass('hidden');
            },
            success: function(response){
                $('#loader').addClass('hidden');
            
                $('.add_row_selected').prepend("<tr style='height: 38px;' class='on_row_click_selected' data-id='"+id+"' data-name='"+name+"' data-phone='"+phone+"' data-code='"+code+"'><td><input type='checkbox' name='ids[]' checked='checked'></td><td>"+code+"</td><td>"+name+"</td><td>"+phone+"</td></tr>");

        }});  

    });

</script>
</body>
<!-- end: BODY -->
</html>