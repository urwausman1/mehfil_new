<!DOCTYPE html>
<html lang="en" class="no-js">
<!-- start: HEAD -->

<head>
    <title> ڈیوٹی روسٹر عہد کارکنان </title>

    @include('inc.head')

    <style>
        #contacts_table_wrapper .select2-container {
            width: 100px;
        }

        #contacts_table_wrapper .select2-container span.select2-chosen {
            text-align: right;
        }

        table.table thead .sorting {
            text-align: right;
            background: url('assets/plugins/DataTables/media/css/images/sort_both.png') no-repeat center left;
        }

        table.table thead .sorting_asc {
            text-align: right;
            background: url('assets/plugins/DataTables/media/css/images/sort_asc.png') no-repeat center left;
        }

        table.table thead .sorting_desc {
            text-align: right;
            background: url('assets/plugins/DataTables/media/css/images/sort_desc.png') no-repeat center left;
        }

    </style>

</head>

<!-- end: HEAD -->

<!-- start: BODY -->

<body class="rtl">
    <!-- start: HEADER -->
    <div class="navbar navbar-inverse navbar-fixed-top">
        <!-- start: TOP NAVIGATION CONTAINER -->
        <div class="container">
            <div class="navbar-header">
                <!-- start: RESPONSIVE MENU TOGGLER -->
                <button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
                    <span class="clip-list-2"></span>
                </button>
                <!-- end: RESPONSIVE MENU TOGGLER -->
                <!-- end: LOGO -->
            </div>
        </div>
        <!-- end: TOP NAVIGATION CONTAINER -->
    </div>
    <!-- end: HEADER -->

    <!-- start: MAIN CONTAINER -->
    <div class="main-container duty_rosster_page">
        <div class="navbar-content">
            <!-- start: SIDEBAR -->
            @include('inc.sidebar')
            <!-- end: SIDEBAR -->
        </div>
        <!-- start: PAGE -->
        <div class="main-content">
            <div class="container" style="min-height: 760px;">
                <!-- start: PAGE HEADER -->
                <div class="row">
                    <div class="col-sm-12">
                        <!-- start: PAGE TITLE & BREADCRUMB -->
                        <ol class="breadcrumb">
                            <li>
                                <!--- class="clip-file" -->
                                <i class=""></i>
                                ڈیوٹی روسٹر عہد کارکنان
                            </li>
                            <li class="active">
                            </li>
                        </ol>
                        <div class="page-header">
                            <h1>
                                ڈیوٹی روسٹر عہد کارکنان
                                <small style="font-size:25px;margin-right:30px;"></small>
                            </h1>
                        </div>
                        <!-- end: PAGE TITLE & BREADCRUMB -->
                    </div>
                </div>
                <!-- end: PAGE HEADER -->
                <!-- start: PAGE CONTENT -->

                @if (Session::has('msg'))
                    <div class="col-md-12">
                        <div class="alert alert-success">{{ Session::get('msg') }}</div>
                    </div>
                @endif
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <i class="fa fa-external-link-square"></i>
                                ڈیوٹی روسٹر عہد کارکنان
                            </div>
                            <div class="panel-body overflow_overlay">
                                <!-- complete form for duty -->
                                <form id="complete_form">
                                    @csrf
                                    <input type="hidden" name="city" value="{{ session('city') }}">
                                    <br>
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table
                                                class="table table-striped table-bordered table-hover table-full-width">
                                                <thead>
                                                    <tr>
                                                        <td class="t-day"> محفل کا نام </td>
                                                        <td class="t-day"> جمعہ </td>
                                                        <td class="t-day"> ہفتہ </td>
                                                        <td class="t-day"> اتوار </td>
                                                        <td class="t-day"> سوموار </td>
                                                        <td class="t-day"> منگل </td>
                                                        <td class="t-day"> بدھ </td>
                                                        <td class="t-day"> جمعرات </td>
                                                    </tr>

                                                </thead>
                                                <tbody>
                                                    @foreach ($all_mehfils as $single_mehfil)
                                                        <tr>
                                                            <td class="t-day">
                                                                <input type="hidden" name="mehfil_id[]"
                                                                    value="{{ $single_mehfil->id }}">
                                                                <div class="text-center font-17">
                                                                    {{ $single_mehfil->mehfil_name }}
                                                                </div>
                                                            </td>

                                                            <td class="">

                                                                <select name="ehad_karkun_juma[]"
                                                                    id="form-field-select-4"
                                                                    onchange="checkvalue_friday(this)"
                                                                    class="form-control search-select search_select_friday select2-hidden-accessible"
                                                                    tabindex="-1" aria-hidden="true">
                                                                    @php
                                                                    $mehfil_id = $single_mehfil->id;
                                                                    @endphp

                                                                    <option value=""> عہد کارکن منتخب کریں۔۔۔ </option>

                                                                    @if (isset($check_duty_roster) && sizeof($check_duty_roster) > 0)
                                                                        @foreach ($check_duty_roster as $single_duty_roster)

                                                                            @if ($single_duty_roster->mehfil_id == $mehfil_id && $single_duty_roster->day == 'friday')
                                                                                <option selected="selected"
                                                                                    value="{{ $single_duty_roster->ehad_karkun_id }}">
                                                                                    {{ $single_duty_roster->ekp_name . ' / ' . $single_duty_roster->ekp_fname . ' - ' . $single_duty_roster->ekp_id_number }}
                                                                                </option>
                                                                            @endif

                                                                        @endforeach
                                                                    @endif

                                                                    @foreach ($all_ehad_karkun as $single_ehad_karkun)
                                                                        <option value="{{ $single_ehad_karkun->id }}">
                                                                            {{ $single_ehad_karkun->ekp_name . ' / ' . $single_ehad_karkun->ekp_fname . ' - ' . $single_ehad_karkun->ekp_id_number }}
                                                                        </option>
                                                                    @endforeach
                                                                </select>

                                                            </td>

                                                            <td class="">

                                                                <select name="ehad_karkun_hafta[]"
                                                                    id="form-field-select-4"
                                                                    onchange="checkvalue_saturday(this)"
                                                                    class="form-control search-select search_select_saturday select2-hidden-accessible"
                                                                    tabindex="-1" aria-hidden="true">

                                                                    <option value=""> عہد کارکن منتخب کریں۔۔۔ </option>

                                                                    @if (isset($check_duty_roster) && sizeof($check_duty_roster) > 0)
                                                                        @foreach ($check_duty_roster as $single_duty_roster)

                                                                            @if ($single_duty_roster->mehfil_id == $mehfil_id && $single_duty_roster->day == 'saturday')
                                                                                <option selected="selected"
                                                                                    value="{{ $single_duty_roster->ehad_karkun_id }}">
                                                                                    {{ $single_duty_roster->ekp_name . ' / ' . $single_duty_roster->ekp_fname . ' - ' . $single_duty_roster->ekp_id_number }}
                                                                                </option>
                                                                            @endif

                                                                        @endforeach
                                                                    @endif

                                                                    @foreach ($all_ehad_karkun as $single_ehad_karkun)
                                                                        <option value="{{ $single_ehad_karkun->id }}">
                                                                            {{ $single_ehad_karkun->ekp_name . ' / ' . $single_ehad_karkun->ekp_fname . ' - ' . $single_ehad_karkun->ekp_id_number }}
                                                                        </option>
                                                                    @endforeach
                                                                </select>

                                                            </td>

                                                            <td class="">

                                                                <select name="ehad_karkun_itwar[]"
                                                                    id="form-field-select-4"
                                                                    onchange="checkvalue_sunday(this)"
                                                                    class="form-control search-select search_select_sunday select2-hidden-accessible"
                                                                    tabindex="-1" aria-hidden="true">

                                                                    <option value=""> عہد کارکن منتخب کریں۔۔۔ </option>

                                                                    @if (isset($check_duty_roster) && sizeof($check_duty_roster) > 0)
                                                                        @foreach ($check_duty_roster as $single_duty_roster)

                                                                            @if ($single_duty_roster->mehfil_id == $mehfil_id && $single_duty_roster->day == 'sunday')
                                                                                <option selected="selected"
                                                                                    value="{{ $single_duty_roster->ehad_karkun_id }}">
                                                                                    {{ $single_duty_roster->ekp_name . ' / ' . $single_duty_roster->ekp_fname . ' - ' . $single_duty_roster->ekp_id_number }}
                                                                                </option>
                                                                            @endif

                                                                        @endforeach
                                                                    @endif

                                                                    @foreach ($all_ehad_karkun as $single_ehad_karkun)
                                                                        <option value="{{ $single_ehad_karkun->id }}">
                                                                            {{ $single_ehad_karkun->ekp_name . ' / ' . $single_ehad_karkun->ekp_fname . ' - ' . $single_ehad_karkun->ekp_id_number }}
                                                                        </option>
                                                                    @endforeach
                                                                </select>

                                                            </td>

                                                            <td class="">

                                                                <select name="ehad_karkun_peer[]"
                                                                    id="form-field-select-4"
                                                                    onchange="checkvalue_monday(this)"
                                                                    class="form-control search-select search_select_monday select2-hidden-accessible"
                                                                    tabindex="-1" aria-hidden="true">

                                                                    <option value=""> عہد کارکن منتخب کریں۔۔۔ </option>

                                                                    @if (isset($check_duty_roster) && sizeof($check_duty_roster) > 0)
                                                                        @foreach ($check_duty_roster as $single_duty_roster)

                                                                            @if ($single_duty_roster->mehfil_id == $mehfil_id && $single_duty_roster->day == 'monday')
                                                                                <option selected="selected"
                                                                                    value="{{ $single_duty_roster->ehad_karkun_id }}">
                                                                                    {{ $single_duty_roster->ekp_name . ' / ' . $single_duty_roster->ekp_fname . ' - ' . $single_duty_roster->ekp_id_number }}
                                                                                </option>
                                                                            @endif

                                                                        @endforeach
                                                                    @endif

                                                                    @foreach ($all_ehad_karkun as $single_ehad_karkun)
                                                                        <option value="{{ $single_ehad_karkun->id }}">
                                                                            {{ $single_ehad_karkun->ekp_name . ' / ' . $single_ehad_karkun->ekp_fname . ' - ' . $single_ehad_karkun->ekp_id_number }}
                                                                        </option>
                                                                    @endforeach
                                                                </select>

                                                            </td>

                                                            <td class="">

                                                                <select name="ehad_karkun_mangal[]"
                                                                    onchange="checkvalue_tuesday(this)"
                                                                    id="form-field-select-4"
                                                                    class="form-control search-select search_select_tuesday select2-hidden-accessible"
                                                                    tabindex="-1" aria-hidden="true">

                                                                    <option value=""> عہد کارکن منتخب کریں۔۔۔ </option>

                                                                    @if (isset($check_duty_roster) && sizeof($check_duty_roster) > 0)
                                                                        @foreach ($check_duty_roster as $single_duty_roster)

                                                                            @if ($single_duty_roster->mehfil_id == $mehfil_id && $single_duty_roster->day == 'tuesday')

                                                                                <option selected="selected"
                                                                                    value="{{ $single_duty_roster->ehad_karkun_id }}">
                                                                                    {{ $single_duty_roster->ekp_name . ' / ' . $single_duty_roster->ekp_fname . ' - ' . $single_duty_roster->ekp_id_number }}
                                                                                </option>

                                                                            @endif

                                                                        @endforeach
                                                                    @endif

                                                                    @foreach ($all_ehad_karkun as $single_ehad_karkun)
                                                                        <option value="{{ $single_ehad_karkun->id }}">
                                                                            {{ $single_ehad_karkun->ekp_name . ' / ' . $single_ehad_karkun->ekp_fname . ' - ' . $single_ehad_karkun->ekp_id_number }}
                                                                        </option>
                                                                    @endforeach
                                                                </select>

                                                            </td>

                                                            <td class="">

                                                                <select name="ehad_karkun_budh[]"
                                                                    onchange="checkvalue_wednesday(this)"
                                                                    id="form-field-select-4"
                                                                    class="form-control search-select search_select_wednesday select2-hidden-accessible"
                                                                    tabindex="-1" aria-hidden="true">

                                                                    <option value=""> عہد کارکن منتخب کریں۔۔۔ </option>

                                                                    @if (isset($check_duty_roster) && sizeof($check_duty_roster) > 0)
                                                                        @foreach ($check_duty_roster as $single_duty_roster)

                                                                            @if ($single_duty_roster->mehfil_id == $mehfil_id && $single_duty_roster->day == 'wednesday')
                                                                                <option selected="selected"
                                                                                    value="{{ $single_duty_roster->ehad_karkun_id }}">
                                                                                    {{ $single_duty_roster->ekp_name . ' / ' . $single_duty_roster->ekp_fname . ' - ' . $single_duty_roster->ekp_id_number }}
                                                                                </option>
                                                                            @endif

                                                                        @endforeach
                                                                    @endif

                                                                    @foreach ($all_ehad_karkun as $single_ehad_karkun)
                                                                        <option value="{{ $single_ehad_karkun->id }}">
                                                                            {{ $single_ehad_karkun->ekp_name . ' / ' . $single_ehad_karkun->ekp_fname . ' - ' . $single_ehad_karkun->ekp_id_number }}
                                                                        </option>
                                                                    @endforeach
                                                                </select>

                                                            </td>

                                                            <td class="">

                                                                <select name="ehad_karkun_jumerat[]"
                                                                    onchange="checkvalue_thursday(this)"
                                                                    id="form-field-select-4"
                                                                    class="form-control search-select search_select_thursday select2-hidden-accessible"
                                                                    tabindex="-1" aria-hidden="true">

                                                                    <option value=""> عہد کارکن منتخب کریں۔۔۔ </option>

                                                                    @if (isset($check_duty_roster) && sizeof($check_duty_roster) > 0)
                                                                        @foreach ($check_duty_roster as $single_duty_roster)

                                                                            @if ($single_duty_roster->mehfil_id == $mehfil_id && $single_duty_roster->day == 'thursday')
                                                                                <option selected="selected"
                                                                                    value="{{ $single_duty_roster->ehad_karkun_id }}">
                                                                                    {{ $single_duty_roster->ekp_name . ' / ' . $single_duty_roster->ekp_fname . ' - ' . $single_duty_roster->ekp_id_number }}
                                                                                </option>
                                                                            @endif

                                                                        @endforeach
                                                                    @endif

                                                                    @foreach ($all_ehad_karkun as $single_ehad_karkun)
                                                                        <option value="{{ $single_ehad_karkun->id }}">
                                                                            {{ $single_ehad_karkun->ekp_name . ' / ' . $single_ehad_karkun->ekp_fname . ' - ' . $single_ehad_karkun->ekp_id_number }}
                                                                        </option>
                                                                    @endforeach
                                                                </select>

                                                            </td>
                                                        </tr>

                                                    @endforeach

                                                </tbody>
                                            </table>
                                        </div>
                                        <input type="hidden" name="juma_error">
                                        <input type="hidden" name="hafta_error">
                                        <input type="hidden" name="itwar_error">
                                        <input type="hidden" name="peer_error">
                                        <input type="hidden" name="mangal_error">
                                        <input type="hidden" name="budh_error">
                                        <input type="hidden" name="jumerat_error">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <hr>
                                                <div class="text-center">
                                                    <button id="btn_submit" type="submit" value="Submit"
                                                        class="btn btn-blue mb-16 enter_btn">اندراج کریں</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <!-- complete form for hazri -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end: PAGE CONTENT-->
        </div>
    </div>
    <!-- end: PAGE -->
    </div>
    <!-- end: MAIN CONTAINER -->



    <!-- start: FOOTER & scripts -->
    @include('inc.footer')

    <script type="text/javascript">
        $(document).ready(function() {
            $('#example-getting-started').multiselect();
        });

        $(document).ready(function() {
            $(document).find(".urwasearch").select2();
        });

        $('.urwasearch').on('change', function() {
            // alert('fasdfds');
            var value = $(document).find('.urwasearch option:selected').val();
            location.href = value;
        });

    </script>
</body>
<!-- end: BODY -->
<script>
    $(document).ready(function() {
        $(document).find("#name").focus();
    });


    function checkvalue_friday(ths) {

        var up_ok = 0;
        $('.search_select_friday').each(function() {
            if ($(document).find('.search_select_friday option[value=' + $(ths).val() + ']:selected').length >
                1) {
                $(ths).closest('td').find(".select2-chosen").css("color", "red");
                up_ok = 1;
            }
        });
        if (up_ok == 1) {
            alert('ایک شخص کو ایک دن میں دو محفلوں پر مقرر نہیں کیا جا سکتا۔ انتخاب درست کریں!');
            $(document).find('[name="juma_error"]').val('1');
            return false;
        } else {
            $(document).find('[name="juma_error"]').val('0');
            $(ths).closest('td').find(".select2-chosen").css("color", "black");
        }
    }

    function checkvalue_saturday(ths) {

        var up_ok = 0;
        $('.search_select_saturday').each(function() {
            if ($(document).find('.search_select_saturday option[value=' + $(ths).val() + ']:selected').length >
                1) {
                $(ths).closest('td').find(".select2-chosen").css("color", "red");
                up_ok = 1;
            }
        });
        if (up_ok == 1) {
            alert('ایک شخص کو ایک دن میں دو محفلوں پر مقرر نہیں کیا جا سکتا۔ انتخاب درست کریں!');
            $(document).find('[name="hafta_error"]').val('1');
            return false;
        } else {
            $(document).find('[name="hafta_error"]').val('0');
            $(ths).closest('td').find(".select2-chosen").css("color", "black");
        }
    }

    function checkvalue_sunday(ths) {

        var up_ok = 0;
        $('.search_select_sunday').each(function() {
            if ($(document).find('.search_select_sunday option[value=' + $(ths).val() + ']:selected').length >
                1) {
                $(ths).closest('td').find(".select2-chosen").css("color", "red");
                up_ok = 1;
            }
        });
        if (up_ok == 1) {
            alert('ایک شخص کو ایک دن میں دو محفلوں پر مقرر نہیں کیا جا سکتا۔ انتخاب درست کریں!');
            $(document).find('[name="itwar_error"]').val('1');
            return false;
        } else {
            $(document).find('[name="itwar_error"]').val('0');
            $(ths).closest('td').find(".select2-chosen").css("color", "black");
        }
    }

    function checkvalue_monday(ths) {

        var up_ok = 0;
        $('.search_select_monday').each(function() {
            if ($(document).find('.search_select_monday option[value=' + $(ths).val() + ']:selected').length >
                1) {

                $(ths).closest('td').find(".select2-chosen").css("color", "red");

                up_ok = 1;
            }
        });
        if (up_ok == 1) {
            alert('ایک شخص کو ایک دن میں دو محفلوں پر مقرر نہیں کیا جا سکتا۔ انتخاب درست کریں!');
            $(document).find('[name="peer_error"]').val('1');
            return false;
        } else {
            $(document).find('[name="peer_error"]').val('0');
            $(ths).closest('td').find(".select2-chosen").css("color", "black");
        }
    }


    function checkvalue_tuesday(ths) {

        var up_ok = 0;
        $('.search_select_tuesday').each(function() {
            if ($(document).find('.search_select_tuesday option[value=' + $(ths).val() + ']:selected').length >
                1) {
                $(ths).closest('td').find(".select2-chosen").css("color", "red");
                up_ok = 1;
            }
        });
        if (up_ok == 1) {
            alert('ایک شخص کو ایک دن میں دو محفلوں پر مقرر نہیں کیا جا سکتا۔ انتخاب درست کریں!');
            $(document).find('[name="mangal_error"]').val('1');
            return false;
        } else {
            $(document).find('[name="mangal_error"]').val('0');
            $(ths).closest('td').find(".select2-chosen").css("color", "black");
        }
    }


    function checkvalue_wednesday(ths) {

        var up_ok = 0;
        $('.search_select_wednesday').each(function() {
            if ($(document).find('.search_select_wednesday option[value=' + $(ths).val() + ']:selected')
                .length > 1) {
                $(ths).closest('td').find(".select2-chosen").css("color", "red");
                up_ok = 1;
            }
        });
        if (up_ok == 1) {
            alert('ایک شخص کو ایک دن میں دو محفلوں پر مقرر نہیں کیا جا سکتا۔ انتخاب درست کریں!');
            $(document).find('[name="budh_error"]').val('1');
            return false;
        } else {
            $(document).find('[name="budh_error"]').val('0');
            $(ths).closest('td').find(".select2-chosen").css("color", "black");
        }
    }



    function checkvalue_thursday(ths) {

        var up_ok = 0;
        $('.search_select_thursday').each(function() {
            if ($(document).find('.search_select_thursday option[value=' + $(ths).val() + ']:selected').length >
                1) {

                $(ths).closest('td').find(".select2-chosen").css("color", "red");
                up_ok = 1;
            }
        });

        if (up_ok == 1) {
            alert('ایک شخص کو ایک دن میں دو محفلوں پر مقرر نہیں کیا جا سکتا۔ انتخاب درست کریں!');
            $(document).find('[name="jumerat_error"]').val('1');
            return false;
        } else {
            $(document).find('[name="jumerat_error"]').val('0');
            $(ths).closest('td').find(".select2-chosen").css("color", "black");
        }
    }

    $('#btn_submit').click(function(e) {
        e.preventDefault();

        juma_error = $(document).find('[name="juma_error"]').val();
        hafta_error = $(document).find('[name="hafta_error"]').val();
        itwar_error = $(document).find('[name="itwar_error"]').val();
        peer_error = $(document).find('[name="peer_error"]').val();
        mangal_error = $(document).find('[name="mangal_error"]').val();
        budh_error = $(document).find('[name="budh_error"]').val();
        jumerat_error = $(document).find('[name="jumerat_error"]').val();

        if (juma_error == 1 ||
            hafta_error == 1 ||
            itwar_error == 1 ||
            peer_error == 1 ||
            mangal_error == 1 ||
            budh_error == 1 ||
            jumerat_error == 1) {

            alert("انتخاب درست کریں۔۔");
            return false;
        }


        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('[name="_token"]').val()
            }
        });

        $('#btn_submit').html('Sending..');

        /* Submit form data using ajax*/
        $.ajax({
            url: "{{ url('dutyroster/ehadkameeti/add') }}",
            method: 'post',
            data: $('#complete_form').serialize(),
            success: function(response) {
                //------------------------
                $('#btn_submit').html('Submit');
                //--------------------------
            }
        });
    });

</script>

</html>
