<!DOCTYPE html>




<html lang="en" class="no-js">
<!-- start: HEAD -->

<head>
    <title> حاضری برائے کارکن </title>
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link
        href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap"
        rel="stylesheet"> @include('inc.head')
    <style>
        .switch {
            position: relative;
            top: 3px;
            display: inline-block;
            width: 80px;
            height: 24px;
            margin: 0;
        }

        .switch input {
            display: none;
        }

        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #ca2222;
            -webkit-transition: .4s;
            transition: .4s;
            border-radius: 34px;
        }

        .slider:before {
            position: absolute;
            content: "";
            height: 16px;
            width: 16px;
            left: 5px;
            bottom: 4px;
            background-color: white;
            -webkit-transition: .4s;
            transition: .4s;
            border-radius: 50%;
        }

        input:checked+.slider {
            background-color: #2ab934;
        }

        input:focus+.slider {
            box-shadow: 0 0 1px #2196F3;
        }

        input:checked+.slider:before {
            -webkit-transform: translateX(26px);
            -ms-transform: translateX(26px);
            transform: translateX(55px);
        }

        /*------ ADDED CSS ---------*/
        .slider:after {
            content: '  غیر حاضر  ';
            color: white;
            display: block;
            position: absolute;
            transform: translate(-50%, -50%);
            top: 50%;
            left: 50%;
            font-size: 16px;
            font: 'urdu';
        }

        input:checked+.slider:after {
            content: '  حاضر  ';
        }

        #contacts_table_wrapper .select2-container {
            width: 100px;
        }

        #contacts_table_wrapper .select2-container span.select2-chosen {
            text-align: right;
        }

        table.table thead .sorting {
            text-align: right;
            background: url('assets/plugins/DataTables/media/css/images/sort_both.png') no-repeat center left;
        }

        table.table thead .sorting_asc {
            text-align: right;
            background: url('assets/plugins/DataTables/media/css/images/sort_asc.png') no-repeat center left;
        }

        table.table thead .sorting_desc {
            text-align: right;
            background: url('assets/plugins/DataTables/media/css/images/sort_desc.png') no-repeat center left;
        }

        .custom_tbl_height {
            height: 500px;
            overflow: overlay;
            padding-right: 17px;
            direction: ltr;
            border: 1px solid #ccc;
            box-shadow: 0px 8px 20px 0px #cecece;
        }

        .custom_tbl_height table.table {
            direction: rtl;
            margin-bottom: 0 !important;
        }

        .custom_tbl_height .dataTables_wrapper .table>thead>tr>th {
            text-align: center;
            position: sticky;
            top: -1px;
            background: #fff;
            border-bottom: 1px solid #ccc !important;
            z-index: 200;
        }

        .custom_tbl_height hr {
            margin-top: 0;
            margin-bottom: 0;
            border: 0;
            border-top: 1px solid #ccc;
            position: sticky;
            top: 50px;
            width: 100%;
        }
        .hazari_page .form-control {
            font-family: sans-serif !important;
            font-size: 14px !important;
            font-weight: 500;
        }
    </style>


</head>

<!-- end: HEAD -->

<!-- start: BODY -->

<body class="rtl">
    <!-- start: HEADER -->
    <div class="navbar navbar-inverse navbar-fixed-top">
        <!-- start: TOP NAVIGATION CONTAINER -->
        <div class="container">
            <div class="navbar-header">
                <!-- start: RESPONSIVE MENU TOGGLER -->
                <button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
                    <span class="clip-list-2"></span>
                </button>
                <!-- end: RESPONSIVE MENU TOGGLER -->
                <!-- end: LOGO -->
            </div>
        </div>
        <!-- end: TOP NAVIGATION CONTAINER -->
    </div>
    <!-- end: HEADER -->

    <!-- start: MAIN CONTAINER -->
    <div class="main-container">
        <div class="navbar-content">
            <!-- start: SIDEBAR -->
            @include('inc.sidebar')
            <!-- end: SIDEBAR -->
        </div>
        <!-- start: PAGE -->
        <div class="main-content">


            <div class="container" style="min-height: 760px;">
                <!-- start: PAGE HEADER -->
                <div class="row">
                    <div class="col-sm-12">
                        <!-- start: PAGE TITLE & BREADCRUMB -->
                        <ol class="breadcrumb">
                            <li>
                                <!--- class="clip-file" -->
                                <i class=""></i>

                                حاضری برائے سال

                            </li>
                            <li class="active">
                            </li>
                        </ol>

                        <div class="page-header">

                            @php
                                $date = date('d');
                                $month = date('m');
                                $month_name = '';
                                $year = date('Y');
                                
                                if ($month == 1) {
                                    $month_name = 'جنوری ';
                                } elseif ($month == 2) {
                                    $month_name = ' فروری ';
                                } elseif ($month == 3) {
                                    $month_name = ' مارچ ';
                                } elseif ($month == 4) {
                                    $month_name = ' اپریل ';
                                } elseif ($month == 5) {
                                    $month_name = ' مئی ';
                                } elseif ($month == 6) {
                                    $month_name = ' جون ';
                                } elseif ($month == 7) {
                                    $month_name = ' جولائی ';
                                } elseif ($month == 8) {
                                    $month_name = ' آگست ';
                                } elseif ($month == 9) {
                                    $month_name = ' ستمبر ';
                                } elseif ($month == 10) {
                                    $month_name = ' اکتوبر ';
                                } elseif ($month == 11) {
                                    $month_name = ' نومبر ';
                                } elseif ($month == 12) {
                                    $month_name = ' دسمبر ';
                                }
                            @endphp

                            <h1>

                                حاضری برائے {{ ' ---- ' . $date . ' / ' . $month_name . ' / ' . $year }}


                                <small style="font-size:25px;margin-right:30px;"></small>
                            </h1>
                        </div>
                        <!-- end: PAGE TITLE & BREADCRUMB -->
                    </div>
                </div>
                <!-- end: PAGE HEADER -->

                @if (isset($not_showing))
                    <div class="row">
                        <center>
                            <h1>{{ $not_showing }}</h1>
                        </center>
                    </div>
                @else
                    <!-- start: PAGE CONTENT -->

                    @if (Session::has('msg'))
                        <div class="col-md-12">
                            <div class="alert alert-success">{{ Session::get('msg') }}</div>
                        </div>
                    @endif

                    <form id="complete_form">
                        @csrf

                        <div class="row">

                            <div class="col-md-12">
                                <label for="">محفل کا نام</label>

                                @if (isset($get_mehfil_id))

                                    <input type="hidden" name="mehfils" value="{{ $get_mehfil_id->id }}">
                                    <input type="text" style="width: 25% !important;" readonly="" disabled=""
                                        value="{{ $get_mehfil_id->mehfil_name }}">

                                @endif

                                <?php
                                /*
                                <select name="mehfils" style="width: 25% !important;" class="urwasearch">

                                    @if (isset($get_mehfil_id))
                                        <option value="{{ $get_mehfil_id->id }}">{{ $get_mehfil_id->mehfil_name }}
                                        </option>
                                    @else
                                        <option value=""> محفل کا نام منتخب کریں۔۔۔ </option>
                                    @endif

                                    @foreach ($all_mehfils as $single_mehfil)
                                        <option value="{{ $single_mehfil->id }}">{{ $single_mehfil->mehfil_name }}
                                        </option>
                                    @endforeach
                                </select>
                                */
                                ?>
                            </div>

                            <br>
                            <br>

                            <div class="col-md-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <i class="fa fa-external-link-square"></i>
                                        تمام عہد کارکنان
                                    </div>
                                    <!-- <center>
                                    <button type="button" class="btn btn-primary">منتخب کریں</button>
                                    <button type="button" class="btn btn-danger">منتخب نہیں</button>
                                </center> -->
                                    <div class="panel-body ">

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="col-md-3">
                                                    <div class="form-group hazari_page">
                                                        <label> کارکن بھائیوں کی کل تعداد </label>
                                                        <input type="number" name="current_hazri" id="current_hazri_i"
                                                            value="{{ $present_karkuns_count }}" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group hazari_page">
                                                        <label> محفل پر شامل ہونے والے تمام بھائیوں کی کل تعداد</label>
                                                        <input type="number" name="average_hazri" id="average_hazri_i"
                                                            value="@if(isset($avarage_hazri_karkun_ki_tadad)){{$avarage_hazri_karkun_ki_tadad}}@endif" class="form-control">
                                                    </div>
                                                </div>

                                                <div class="col-md-3">
                                                    <div class="form-group hazari_page">
                                                        <label> تاریخ منتخب کریں </label>
                                                        @php
                                                            $dateis = '';
                                                            if (isset($datess)) {
                                                                $dateis = $datess;
                                                            } else {
                                                                $dateis = date('Y-m-d');
                                                            }
                                                        @endphp
                                                        <input type="date" id="selected_dates" name="selected_date"
                                                            value="{{ $dateis }}" class="form-control">
                                                    </div>
                                                </div>


                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label> عہد کارکن کا انتخاب کریں </label>
                                                        <select name="ehad_karkun_id" id="ehad_karkun_id"
                                                            class="form-control">

                                                            @if (isset($hazri_ehad_karkuns_name_id) && !empty($hazri_ehad_karkuns_name_id))
                                                                <option
                                                                    value="{{ $hazri_ehad_karkuns_name_id->id }}">
                                                                    {{ $hazri_ehad_karkuns_name_id->ekp_id_number . ' / ' . $hazri_ehad_karkuns_name_id->ekp_name . ' / ' . $hazri_ehad_karkuns_name_id->ekp_fname }}
                                                                </option>
                                                            @elseif(isset($ehad_karkun_duty_roster) &&
                                                                !empty($ehad_karkun_duty_roster))
                                                                <option value="{{ $ehad_karkun_duty_roster->id }}">
                                                                    {{ $ehad_karkun_duty_roster->ekp_id_number . ' / ' . $ehad_karkun_duty_roster->ekp_name . ' / ' . $ehad_karkun_duty_roster->ekp_fname }}
                                                                </option>
                                                            @else
                                                                <option value=""> عہد کارکن منتخب کریں۔۔۔ </option>
                                                            @endif
                                                            <option value=""> غیر حاضر </option>
                                                            @foreach ($get_ehad_karkuns as $single_ekarkun)
                                                                <option value="{{ $single_ekarkun->id }}">
                                                                    {{ $single_ekarkun->ekp_id_number . ' / ' . $single_ekarkun->ekp_name . ' / ' . $single_ekarkun->ekp_fname }}
                                                                </option>
                                                            @endforeach
                                                        </select>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="table-responsive custom_tbl_height">
                                            <table class="table table-bordered table-hover" width="100%">
                                                <thead></thead>
                                            </table>
                                            <div id="contacts_table_wrapper" class="dataTables_wrapper form-inline"
                                                role="grid">
                                                <!-- <table class="table table-striped table-bordered table-hover table-full-width" id="contacts_table"> -->
                                                @if (isset($hazri_karkuns))
                                                    <hr>
                                                    <table class="table table-bordered table-hover" id="myTable">
                                                        <thead>
                                                            <tr class="m-auto">
                                                                <th> حاضری </th>
                                                                <th> حاضری کوڈ </th>
                                                                <th>نام</th>
                                                                <th> ولدیت </th>
                                                                <th>اوقات</th>
                                                                <!-- <th>تاریخ</th> -->
                                                            </tr>
                                                        </thead>
                                                        <tbody>

                                                            @foreach ($hazri_karkuns as $single_hazri)

                                                                @php
                                                                    $backgroundc = '';
                                                                    $checked = '';
                                                                    $data_var = '';
                                                                    $att_value = 'a';
                                                                    $flag1 = 0;
                                                                    if ($single_hazri->status == 'p') {
                                                                        $att_value = 'p';
                                                                        $checked = "checked='checked'";
                                                                    }
                                                                    
                                                                    if (in_array($single_hazri->karkun_id, $arr_gatian) || in_array($single_hazri->karkun_id, $arr_cs) || in_array($single_hazri->karkun_id, $arr_s)) {
                                                                        $flag1 = 1;
                                                                        $backgroundc = 'background:#baeeff;';
                                                                        $data_var = $single_hazri->karkun_id;
                                                                    }
                                                                    
                                                                    $times = explode(':', $single_hazri->times);
                                                                    $dates = explode('-', $single_hazri->dates);
                                                                    
                                                                    $min_or_am = str_split($times[1], 2);
                                                                @endphp


                                                                <tr style="{{ $backgroundc }}" class="sortable_row">
                                                                    <td style="width: 10%;">
                                                                        <label class="switch">
                                                                            <input type="checkbox"
                                                                                onchange="hazir_ghairhazir('{{ $single_hazri->id }}' , this)"
                                                                                id="c{{ $single_hazri->id }}"
                                                                                @php echo $checked; @endphp>


                                                                            <div class="slider round"></div>
                                                                        </label>
                                                                        <input type="hidden" name="hidden_hazri[]"
                                                                            id="hidden_hazri{{ $single_hazri->id }}"
                                                                            value="{{ $att_value }}" />
                                                                    </td>

                                                                    <td class="font_digit">
                                                                        {{ $single_hazri->kp_id_number }}</td>

                                                                    <td class="urwa_order"
                                                                        data-ids="{{ $data_var }}">

                                                                        <input type="hidden" name="flag1[]"
                                                                            value="{{ $flag1 }}">

                                                                        <input type="hidden" name="karkun_id[]"
                                                                            value="{{ $single_hazri->karkun_id }}">

                                                                        {{ $single_hazri->kp_name }}
                                                                    </td>

                                                                    <td>{{ $single_hazri->kp_fname }}</td>

                                                                    <td style="width: 20%;">
                                                                        <select name="hour[]">
                                                                            <option value="{{ $times[0] }}">
                                                                                {{ $times[0] }} </option>
                                                                            <option value="01">1</option>
                                                                            <option value="02">2</option>
                                                                            <option value="03">3</option>
                                                                            <option value="04">4</option>
                                                                            <option value="05">5</option>
                                                                            <option value="06">6</option>
                                                                            <option value="07">7</option>
                                                                            <option value="08">8</option>
                                                                            <option value="09">9</option>
                                                                            <option value="10">10</option>
                                                                            <option value="11">11</option>
                                                                            <option value="12">12</option>
                                                                        </select>

                                                                        <select name="minute[]">
                                                                            <option value="{{ $min_or_am[0] }}">
                                                                                {{ $min_or_am[0] }}</option>
                                                                            <option value="01">1</option>
                                                                            <option value="02">2</option>
                                                                            <option value="03">3</option>
                                                                            <option value="04">4</option>
                                                                            <option value="05">5</option>
                                                                            <option value="06">6</option>
                                                                            <option value="07">7</option>
                                                                            <option value="08">8</option>
                                                                            <option value="09">9</option>
                                                                            <option value="10">10</option>
                                                                            <option value="11">11</option>
                                                                            <option value="12">12</option>
                                                                            <option value="13">13</option>
                                                                            <option value="14">14</option>
                                                                            <option value="15">15</option>
                                                                            <option value="16">16</option>
                                                                            <option value="17">17</option>
                                                                            <option value="18">18</option>
                                                                            <option value="19">19</option>
                                                                            <option value="20">20</option>
                                                                            <option value="21">21</option>
                                                                            <option value="22">22</option>
                                                                            <option value="23">23</option>
                                                                            <option value="24">24</option>
                                                                            <option value="25">25</option>
                                                                            <option value="26">26</option>
                                                                            <option value="27">27</option>
                                                                            <option value="28">28</option>
                                                                            <option value="29">29</option>
                                                                            <option value="30">30</option>
                                                                            <option value="31">31</option>
                                                                            <option value="32">32</option>
                                                                            <option value="33">33</option>
                                                                            <option value="34">34</option>
                                                                            <option value="35">35</option>
                                                                            <option value="36">36</option>
                                                                            <option value="37">37</option>
                                                                            <option value="38">38</option>
                                                                            <option value="39">39</option>
                                                                            <option value="40">40</option>
                                                                            <option value="41">41</option>
                                                                            <option value="42">42</option>
                                                                            <option value="43">43</option>
                                                                            <option value="44">44</option>
                                                                            <option value="45">45</option>
                                                                            <option value="46">46</option>
                                                                            <option value="47">47</option>
                                                                            <option value="48">48</option>
                                                                            <option value="49">49</option>
                                                                            <option value="50">50</option>
                                                                            <option value="51">51</option>
                                                                            <option value="52">52</option>
                                                                            <option value="53">53</option>
                                                                            <option value="54">54</option>
                                                                            <option value="55">55</option>
                                                                            <option value="56">56</option>
                                                                            <option value="57">57</option>
                                                                            <option value="58">58</option>
                                                                            <option value="59">59</option>
                                                                            <option value="60">60</option>
                                                                        </select>

                                                                        <select name="am_pm[]">
                                                                            @php
                                                                                $time = '';
                                                                                if ($min_or_am[1] == 'am') {
                                                                                    $time = 'صبح ';
                                                                                } else {
                                                                                    $time = '  شام   ';
                                                                                }
                                                                                
                                                                            @endphp

                                                                            <option value="{{ $min_or_am[1] }}">
                                                                                {{ $time }}</option>
                                                                            <option value="am"> صبخ </option>
                                                                            <option value="pm"> شام </option>
                                                                        </select>
                                                                    </td>

                                                                    <!--  <td style="width: 15%;">
                                                            <select name="tareekh[]">
                                                                <option value="{{ $dates[2] }}">{{ $dates[2] }}</option>
                                                                <option value="01">1</option>
                                                                <option value="02">2</option>
                                                                <option value="03">3</option>
                                                                <option value="04">4</option>
                                                                <option value="05">5</option>
                                                                <option value="06">6</option>
                                                                <option value="07">7</option>
                                                                <option value="08">8</option>
                                                                <option value="09">9</option>
                                                                <option value="10">10</option>
                                                                <option value="11">11</option>
                                                                <option value="12">12</option>
                                                                <option value="13">13</option>
                                                                <option value="14">14</option>
                                                                <option value="15">15</option>
                                                                <option value="16">16</option>
                                                                <option value="17">17</option>
                                                                <option value="18">18</option>
                                                                <option value="19">19</option>
                                                                <option value="20">20</option>
                                                                <option value="21">21</option>
                                                                <option value="22">22</option>
                                                                <option value="23">23</option>
                                                                <option value="24">24</option>
                                                                <option value="25">25</option>
                                                                <option value="26">26</option>
                                                                <option value="27">27</option>
                                                                <option value="28">28</option>
                                                                <option value="29">29</option>
                                                                <option value="30">30</option>
                                                                <option value="31">31</option>
                                                            </select>
                                                        </td> -->
                                                                </tr>

                                                            @endforeach

                                                            {{-- <tr>
                                                        <td colspan="5">
                                                            <button id="btn_submit" class="btn btn-primary hazari_btn">داخل کریں</button>
                                                        </td>
                                                    </tr> --}}
                                                        </tbody>
                                                    </table>
                                                @else
                                                    <table class="table table-bordered table-hover" id="myTable">
                                                        <thead>
                                                            <tr class="m-auto">
                                                                <th> حاضری</th>
                                                                <th> حاضری کوڈ </th>
                                                                <th>نام</th>
                                                                <th> ولدیت </th>
                                                                <th>اوقات</th>
                                                                <!-- <th>تاریخ</th> -->
                                                            </tr>
                                                        </thead>
                                                        <tbody>

                                                            @foreach ($all_karkun as $key => $single_karkun)


                                                                @php
                                                                    $backgroundc = '';
                                                                    $checked = '';
                                                                    $data_var = '';
                                                                    $att_value = 'a';
                                                                    $flag1 = 0;
                                                                    if (in_array($single_karkun->id, $arr_gatian) || in_array($single_karkun->id, $arr_cs) || in_array($single_karkun->id, $arr_s)) {
                                                                        $backgroundc = 'background:#baeeff;';
                                                                        $checked = "checked='checked'";
                                                                        $data_var = $single_karkun->id;
                                                                        $att_value = 'p';
                                                                        $flag1 = 1;
                                                                    }
                                                                    
                                                                @endphp


                                                                {{-- <tr style="{{$backgroundc}}" data-id={{$key}} class="sortable_row" onclick="hazaricheck(this)" > --}}
                                                                <tr style="{{ $backgroundc }}"
                                                                    class="sortable_row hazriCheckradioTr">
                                                                    <td style="width: 10%;">
                                                                        <label class="switch">
                                                                            {{-- <input type="checkbox" class="hazriCheckradio" data-vals="{{$att_value}}" onclick="hazariDirect(this)" onchange="hazir_ghairhazir('{{$single_karkun->id}}' , this)" id="c{{$single_karkun->id}}" @php echo $checked; @endphp> --}}
                                                                            <input type="checkbox"
                                                                                class="hazriCheckradio"
                                                                                data-vals="{{ $att_value }}"
                                                                                onchange="hazir_ghairhazir('{{ $single_karkun->id }}' , this)"
                                                                                id="c{{ $single_karkun->id }}"
                                                                                @php echo $checked; @endphp>

                                                                            <div class="slider round"></div>
                                                                        </label>
                                                                        {{-- <input type="hidden" class="hazriCheckbox" name="hidden_hazri[]" data-id={{$key}} id="hidden_hazri{{$single_karkun->id}}" value="{{$att_value}}" /> --}}
                                                                        <input type="hidden" class="hazriCheckbox"
                                                                            name="hidden_hazri[]"
                                                                            id="hidden_hazri{{ $single_karkun->id }}"
                                                                            value="{{ $att_value }}" />
                                                                    </td>

                                                                    <td class="font_digit">
                                                                        {{ $single_karkun->kp_id_number }}</td>

                                                                    <td class="urwa_order"
                                                                        data-ids="{{ $data_var }}">

                                                                        <input type="hidden" name="flag1[]"
                                                                            value="{{ $flag1 }}">

                                                                        <input type="hidden" name="karkun_id[]"
                                                                            value="{{ $single_karkun->id }}">

                                                                        {{ $single_karkun->kp_name }}
                                                                    </td>

                                                                    <td>{{ $single_karkun->kp_fname }}</td>





                                                                    <td style="width: 20%;">
                                                                        <select name="hour[]">
                                                                            <option value="">گھنٹے</option>
                                                                            <option value="01">1</option>
                                                                            <option value="02">2</option>
                                                                            <option value="03">3</option>
                                                                            <option value="04">4</option>
                                                                            <option value="05">5</option>
                                                                            <option value="06">6</option>
                                                                            <option value="07">7</option>
                                                                            <option value="08">8</option>
                                                                            <option value="09">9</option>
                                                                            <option value="10">10</option>
                                                                            <option value="11">11</option>
                                                                            <option value="12">12</option>
                                                                        </select>

                                                                        <select name="minute[]">
                                                                            <option value="">منٹ</option>
                                                                            <option value="01">1</option>
                                                                            <option value="02">2</option>
                                                                            <option value="03">3</option>
                                                                            <option value="04">4</option>
                                                                            <option value="05">5</option>
                                                                            <option value="06">6</option>
                                                                            <option value="07">7</option>
                                                                            <option value="08">8</option>
                                                                            <option value="09">9</option>
                                                                            <option value="10">10</option>
                                                                            <option value="11">11</option>
                                                                            <option value="12">12</option>
                                                                            <option value="13">13</option>
                                                                            <option value="14">14</option>
                                                                            <option value="15">15</option>
                                                                            <option value="16">16</option>
                                                                            <option value="17">17</option>
                                                                            <option value="18">18</option>
                                                                            <option value="19">19</option>
                                                                            <option value="20">20</option>
                                                                            <option value="21">21</option>
                                                                            <option value="22">22</option>
                                                                            <option value="23">23</option>
                                                                            <option value="24">24</option>
                                                                            <option value="25">25</option>
                                                                            <option value="26">26</option>
                                                                            <option value="27">27</option>
                                                                            <option value="28">28</option>
                                                                            <option value="29">29</option>
                                                                            <option value="30">30</option>
                                                                            <option value="31">31</option>
                                                                            <option value="32">32</option>
                                                                            <option value="33">33</option>
                                                                            <option value="34">34</option>
                                                                            <option value="35">35</option>
                                                                            <option value="36">36</option>
                                                                            <option value="37">37</option>
                                                                            <option value="38">38</option>
                                                                            <option value="39">39</option>
                                                                            <option value="40">40</option>
                                                                            <option value="41">41</option>
                                                                            <option value="42">42</option>
                                                                            <option value="43">43</option>
                                                                            <option value="44">44</option>
                                                                            <option value="45">45</option>
                                                                            <option value="46">46</option>
                                                                            <option value="47">47</option>
                                                                            <option value="48">48</option>
                                                                            <option value="49">49</option>
                                                                            <option value="50">50</option>
                                                                            <option value="51">51</option>
                                                                            <option value="52">52</option>
                                                                            <option value="53">53</option>
                                                                            <option value="54">54</option>
                                                                            <option value="55">55</option>
                                                                            <option value="56">56</option>
                                                                            <option value="57">57</option>
                                                                            <option value="58">58</option>
                                                                            <option value="59">59</option>
                                                                            <option value="60">60</option>
                                                                        </select>

                                                                        <select name="am_pm[]">
                                                                            <option value="pm"> شام </option>
                                                                            <option value="am"> صبخ </option>
                                                                            <!-- <option value=""> منتخب کریں۔۔۔ </option> -->
                                                                        </select>
                                                                    </td>

                                                                    <!-- <td style="width: 15%;">
                                                            <select name="tareekh[]">
                                                                <option value="">تاریخ</option>
                                                                <option value="01">1</option>
                                                                <option value="02">2</option>
                                                                <option value="03">3</option>
                                                                <option value="04">4</option>
                                                                <option value="05">5</option>
                                                                <option value="06">6</option>
                                                                <option value="07">7</option>
                                                                <option value="08">8</option>
                                                                <option value="09">9</option>
                                                                <option value="10">10</option>
                                                                <option value="11">11</option>
                                                                <option value="12">12</option>
                                                                <option value="13">13</option>
                                                                <option value="14">14</option>
                                                                <option value="15">15</option>
                                                                <option value="16">16</option>
                                                                <option value="17">17</option>
                                                                <option value="18">18</option>
                                                                <option value="19">19</option>
                                                                <option value="20">20</option>
                                                                <option value="21">21</option>
                                                                <option value="22">22</option>
                                                                <option value="23">23</option>
                                                                <option value="24">24</option>
                                                                <option value="25">25</option>
                                                                <option value="26">26</option>
                                                                <option value="27">27</option>
                                                                <option value="28">28</option>
                                                                <option value="29">29</option>
                                                                <option value="30">30</option>
                                                                <option value="31">31</option>
                                                            </select>
                                                        </td> -->
                                                                </tr>

                                                            @endforeach

                                                            {{-- <tr>
                                                        <td colspan="5">
                                                         
                                                        </td>
                                                    </tr> --}}
                                                        </tbody>
                                                    </table>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 text-center">
                                                <button id="btn_submit" class="btn btn-primary hazari_btn">داخل
                                                    کریں</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <br>
                    <div class="table-responsive">
                        <table class="table-bordered" width="100%">
                            <thead></thead>
                        </table>
                        <div id="contacts_table_wrapper" class="dataTables_wrapper form-inline" role="grid">

                            <table class="table table-striped table-bordered table-hover table-full-width"
                                id="contacts_table">
                                <thead>
                                    <tr class="m-auto">
                                        <th> تاریخ </th>
                                        <th> دن </th>
                                        <th> نام </th>
                                        <th> ٹائم </th>
                                        <th> حاضر کارکنان </th>
                                        <th> کل تعداد </th>
                                        <th> عہد کارکن </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($get_hazri as $single_hazri)
                                        <tr>
                                            <td class="font_digit">{{ $single_hazri->dates }} </td>
                                            <td>
                                                @php
                                                    $day = date('l', strtotime($single_hazri->dates));
                                                    $day_name = '';
                                                    if ($day == 'Friday') {
                                                        $day_name = 'جمعہ ';
                                                    } elseif ($day == 'Saturday') {
                                                        $day_name = 'ہفتہ';
                                                    } elseif ($day == 'Sunday') {
                                                        $day_name = ' اتوار ';
                                                    } elseif ($day == 'Monday') {
                                                        $day_name = 'سوموار ';
                                                    } elseif ($day == 'Tuesday') {
                                                        $day_name = 'منگل ';
                                                    } elseif ($day == 'Wednesday') {
                                                        $day_name = ' بدھ  ';
                                                    } elseif ($day == 'Thursday') {
                                                        $day_name = 'جمعرات ';
                                                    }
                                                @endphp
                                                {{ $day_name }}
                                            </td>
                                            <td>{{ $single_hazri->mehfil_name }}</td>
                                            <td class="font_digit">{{ $single_hazri->times }}</td>
                                            <td class="font_digit">
                                                @php
                                                    $total_hazrian = DB::table('hazri_karkuns')
                                                        ->where('status', 'p')
                                                        ->where('mehfil_id', $get_mehfil_id->id)
                                                        ->where('dates', $single_hazri->dates)
                                                        ->get();
                                                    echo sizeof($total_hazrian);
                                                @endphp
                                            </td>
                                            <td class="font_digit">
                                                @php
                                                    $total_hazrian = DB::table('average_hazris')
                                                        ->where('mehfil_id', $get_mehfil_id->id)
                                                        ->where('dates', $single_hazri->dates)
                                                        ->first();
                                                    echo $total_hazrian->average_visitors;
                                                @endphp
                                            </td>

                                            <td>
                                                @php
                                                    $ehad_karkun_hazri = DB::table('hazri_ehad_karkuns')
                                                        ->join('ehad_karkuns', 'hazri_ehad_karkuns.ehad_karkun_id', '=', 'ehad_karkuns.id')
                                                        ->select('hazri_ehad_karkuns.*', 'ehad_karkuns.id', 'ehad_karkuns.ekp_name')
                                                        ->where('mehfil_id', $get_mehfil_id->id)
                                                        ->where('dates', $single_hazri->dates)
                                                        ->first();
                                                    if (!empty($ehad_karkun_hazri)) {
                                                        echo $ehad_karkun_hazri->ekp_name;
                                                    }
                                                @endphp
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>

                @endif
                <!-- end: PAGE CONTENT-->
            </div>
        </div>
        <!-- end: PAGE -->
    </div>
    <!-- end: MAIN CONTAINER -->



    <!-- start: FOOTER & scripts -->
    @include('inc.footer')

    <script type="text/javascript">
        $(document).ready(function() {
            $('#example-getting-started').multiselect();
        });
        $(document).ready(function() {
            $(".btn_submit_P").click(function() {
                $(".HazriKarkoonatten").val("P");
            });

            $(".btn_submit_L").click(function() {
                $(".HazriKarkoonatten").val("L");
            });

            $(".btn_submit_M").click(function() {
                $(".HazriKarkoonatten").val("M");
            });
        });


        function hazir_ghairhazir(id, ths) {
            // alert(id);
            if ($(ths).prop('checked')) {
                newCount = parseInt($('#current_hazri_i').val()) + 1;
                $(document).find('#hidden_hazri' + id).val('p');
            } else {
                newCount = parseInt($('#current_hazri_i').val()) - 1;
                $(document).find('#hidden_hazri' + id).val('a');
            }
            $('#current_hazri_i').val(newCount);
        }


        $('#btn_submit').click(function(e) {
            e.preventDefault();

            var average_hazri_i = $("#average_hazri_i").val();
            var ehad_karkun_id = $("#ehad_karkun_id").val();

            if (average_hazri_i == '' || average_hazri_i == null) {
                alert("آج محفل میں آنے والوں کی تعداد  بھی شامل کریں۔ شکریہ۔");
                $("#average_hazri_i").focus();
                return false;
            }

            // if (ehad_karkun_id == '' || ehad_karkun_id == null) {
            //     alert("عہد کارکن کا  اندراج ضروری ہے۔");
            //     $("#ehad_karkun_id").focus();
            //     return false;
            // }
            // alert('usman');

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('[name="_token"]').val()
                }
            });

            $('#btn_submit').html('ڈیٹا  داخل  ہو  رہا ہے۔۔۔   ');

            /* Submit form data using ajax*/
            $.ajax({
                url: "{{ url('hazri/karkun/add') }}",
                method: 'post',
                data: $('#complete_form').serialize(),
                success: function(response) {
                    location.reload();
                    //------------------------
                    $('#btn_submit').html('  ڈیٹا  داخل ہو چکا ہے   ');
                    //--------------------------
                }
            });
        });




        // for sorting of table
        // function sortTable() {

        //     var sorted = $('#myTable tbody tr').sort(function(ab, bc) {
        //       var a = $(ab).find('td:first').attr('data-ids');
        //       var b = $(bc).find('td:first').attr('data-ids');
        //       // console.log(a);
        //       // console.log(b);
        //       return a.localeCompare(b, false, {numeric: true})
        //     });

        //     $('#myTable tbody').html(sorted)
        // }


        // $(document).ready(function(){
        //     // a simple compare function, used by the sort below
        //     var compare_rows = function (a,b){
        //       var a_val = $(a).text().toLowerCase();
        //       var b_val = $(b).text().toLowerCase();
        //       if (a_val<b_val){
        //         return 1;
        //       }
        //       if (a_val>b_val){
        //         return -1;
        //       }
        //       return 0;
        //     };

        //     // the actual sort
        //     $('#myTable .sortable_row').sort(compare_rows).appendTo('#myTable');

        // });

    </script>
</body>
<!-- end: BODY -->
<script>
    // $(document).ready(function() {
    //     $(document).find("#name").focus();
    // });

    // $(document).ready(function() {
    //     // sortTable()
    // });



    $('#selected_dates').change(function() {
        var date = $(this).val();
        location.href = "{{ url('hazri/karkun/') }}/" + date;
    });

</script>

</html>
