<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCitiesNamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cities__names', function (Blueprint $table) {
            $table->id();
            $table->integer('country_id')->nullable();
            $table->string('cc02')->nullable();
            $table->string('cc03')->nullable();
            $table->string('cc02_city')->nullable();
            $table->string('cc03_city')->nullable();
            $table->string('city_name_en')->nullable();
            $table->string('city_name_ur')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cities__names');
    }
}
