<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHazriKarkunsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hazri_karkuns', function (Blueprint $table) {
            $table->id();
            $table->integer('mehfil_id')->nullable();
            $table->integer('karkun_id')->nullable();
            $table->string('status')->nullable();
            $table->string('day_name')->nullable();
            $table->integer('flag')->nullable();
            $table->string('dates')->nullable();
            $table->string('times')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hazri_karkuns');
    }
}
