<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEhadRegistersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ehad_registers', function (Blueprint $table) {
            $table->id();
            $table->string('er_id_number')->nullable();
            $table->string('er_name')->nullable();
            $table->string('er_name_en')->nullable();
            $table->string('er_fname')->nullable();
            $table->string('er_phone')->nullable();
            $table->string('er_email')->nullable();
            $table->string('er_address')->nullable();
            $table->string('er_city')->nullable();
            $table->string('er_country')->nullable();
            $table->string('er_cnic')->nullable();
            $table->string('er_dob')->nullable();
            $table->string('er_doe')->nullable();
            $table->string('er_marfat')->nullable();
            $table->integer('mehfil_id')->nullable();
            // $table->integer('admin_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ehad_registers');
    }
}
