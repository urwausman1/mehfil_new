<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKarkunsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('karkuns', function (Blueprint $table) {
            $table->id();
            $table->string('kp_id_number')->nullable();
            $table->string('kp_name')->nullable();
            $table->string('kp_name_en')->nullable();
            $table->string('kp_fname')->nullable();
            $table->string('kp_fname_en')->nullable();
            $table->string('kp_address')->nullable();
            $table->string('kp_city')->nullable();
            $table->string('kp_country')->nullable();
            $table->string('kp_cnic')->nullable();
            $table->string('kp_phone')->nullable();
            $table->string('kp_email')->nullable();
            $table->string('kp_dob')->nullable();
            $table->string('kp_doe')->nullable();
            $table->string('kp_notes')->nullable();
            $table->string('kp_marfat')->nullable();
            $table->string('committee_id')->nullable();
            $table->string('mehfil_id')->nullable();
            $table->string('last_wazaif_date')->nullable();
            $table->integer('admin_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('karkuns');
    }
}
