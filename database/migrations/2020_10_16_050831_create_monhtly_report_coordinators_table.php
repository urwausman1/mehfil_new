<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMonhtlyReportCoordinatorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('monhtly_report_coordinators', function (Blueprint $table) {
            $table->id();
            $table->integer('mehfil_id')->nullable();
            $table->string('meeting_date')->nullable();
            $table->integer('month')->nullable();
            $table->integer('year')->nullable();
            $table->string('printing_date')->nullable();
            $table->text('dump_data')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('monhtly_report_coordinators');
    }
}
