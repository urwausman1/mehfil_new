<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAllGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('all_groups', function (Blueprint $table) {
            $table->id();
            $table->integer('com_id')->nullable();
            $table->string('category')->nullable();
            $table->string('city_name')->nullable();
            $table->integer('mehfil_id')->nullable();
            $table->integer('karkun_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('all_groups');
    }
}
