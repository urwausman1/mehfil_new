<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEhadKarkunDutyRostersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ehad_karkun_duty_rosters', function (Blueprint $table) {
            $table->id();
            $table->string('day')->nullable();
            $table->integer('mehfil_id')->nullable();
            $table->integer('ehad_karkun_id')->nullable();
            $table->string('city')->nullable();
            $table->integer('month')->nullable();
            $table->integer('year')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ehad_karkun_duty_rosters');
    }
}
