<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKarkunDutyRostersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('karkun_duty_rosters', function (Blueprint $table) {
            $table->id();
            $table->string('k_duty_days')->nullable();
            $table->string('k_main_gatian')->nullable();
            $table->string('k_cycle_stand')->nullable();
            $table->string('k_security')->nullable();
            $table->string('ehad_karkun')->nullable();
            $table->string('mehfil_id')->nullable();
            $table->string('day_coordinator')->nullable();
            $table->string('monthly_cordinator')->nullable();
            $table->string('month_name')->nullable();
            $table->string('year_name')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('karkun_duty_rosters');
    }
}
