<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCountriesNamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('countries_names', function (Blueprint $table) {
            $table->id();
            $table->string('cc02')->nullable();
            $table->string('cc03')->nullable();
            $table->string('country_name_en')->nullable();
            $table->string('country_name_ur')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('countries_names');
    }
}
