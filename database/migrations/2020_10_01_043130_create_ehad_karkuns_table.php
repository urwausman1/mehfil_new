<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEhadKarkunsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ehad_karkuns', function (Blueprint $table) {
            $table->id();
            $table->string('ekp_id_number')->nullable();
            $table->string('ekp_name')->nullable();
            $table->string('ekp_name_en')->nullable();
            $table->string('ekp_fname')->nullable();
            $table->string('ekp_phone')->nullable();
            $table->string('ekp_email')->nullable();
            $table->string('ekp_address')->nullable();
            $table->string('ekp_city')->nullable();
            $table->string('ekp_country')->nullable();
            $table->string('ekp_cnic')->nullable();
            $table->string('ekp_dob')->nullable();
            $table->string('ekp_doe')->nullable();
            $table->string('ekp_marfat')->nullable();
            $table->integer('admin_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ehad_karkuns');
    }
}
