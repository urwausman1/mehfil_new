<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\CommitteesController;
use App\Http\Controllers\FinanceController;
use App\Http\Controllers\EhadKarkunController;
use App\Http\Controllers\MehfilController;
use App\Http\Controllers\KarkunController;
use App\Http\Controllers\KarkunDutyRosterController;
use App\Http\Controllers\EhadKameetiDutyRosterController;
use App\Http\Controllers\HazriKarkunController;
use App\Http\Controllers\EhadRegisterController;
use App\Http\Controllers\ReportingController;
use App\Http\Controllers\All_GroupsController;
// use App\Http\Controllers\Auth;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('login');
    // return view('welcome');
});


// route to show the login form
Route::get('login', [UserController::class, 'showLogin']);
// route to process the form
Route::post('login', [UserController::class, 'doLogin']);

Route::group(['middleware' => ['LoginMiddleware']], function () {

	Route::get('logout', [UserController::class, 'doLogout']);

	Route::get('dashboard', [DashboardController::class, 'showDashboard']);


	// to perform complete crud function in committee table
	Route::get('committees', [CommitteesController::class, 'showData']);
	Route::get('committees/add', [CommitteesController::class, 'addCommitteesPage']);
	Route::post('committees/add', [CommitteesController::class, 'insertCommittee']);
	Route::get('committees/edit/{id}', [CommitteesController::class, 'editCommittee']);
	Route::post('committees/edit/{id}', [CommitteesController::class, 'updateCommittee']);
	Route::get('committees/delete/{id}', [CommitteesController::class, 'delCommittee']);

	// to perform complete crud function in finance table
	Route::get('finance', [FinanceController::class, 'showData']);
	Route::get('finance/add', [FinanceController::class, 'addFinancePage']);
	Route::post('finance/add', [FinanceController::class, 'insertFinance']);
	Route::get('finance/edit/{id}', [FinanceController::class, 'editFinance']);
	Route::post('finance/edit/{id}', [FinanceController::class, 'updateFinance']);
	Route::get('finance/delete/{id}', [FinanceController::class, 'delFinance']);


	// to perform complete crud function in ehad_karkun table
	Route::get('ehadkarkun', [EhadKarkunController::class, 'showData']);
	Route::get('ehadkarkun/add', [EhadKarkunController::class, 'addEhadKarkunPage']);
	Route::post('ehadkarkun/add', [EhadKarkunController::class, 'insertEhadKarkun']);
	Route::get('ehadkarkun/edit/{id}', [EhadKarkunController::class, 'editEhadKarkun']);
	Route::post('ehadkarkun/edit/{id}', [EhadKarkunController::class, 'updateEhadKarkun']);
	Route::get('ehadkarkun/delete/{id}', [EhadKarkunController::class, 'delEhadKarkun']);


	// to perform complete crud function in ehad_register table
	Route::get('ehadregister', [EhadRegisterController::class, 'showData']);
	Route::get('ehadregister/add', [EhadRegisterController::class, 'addEhadRegisterPage']);
	Route::post('ehadregister/add', [EhadRegisterController::class, 'insertEhadRegister']);
	Route::get('ehadregister/edit/{id}', [EhadRegisterController::class, 'editEhadRegister']);
	Route::post('ehadregister/edit/{id}', [EhadRegisterController::class, 'updateEhadRegister']);
	Route::get('ehadregister/delete/{id}', [EhadRegisterController::class, 'delEhadRegister']);


	// to perform complete crud function in mehfils table
	Route::get('mahafil', [MehfilController::class, 'showData']);
	Route::get('mahafil/add', [MehfilController::class, 'addMehfilPage']);
	Route::post('mahafil/add', [MehfilController::class, 'insertMehfil']);
	Route::get('mahafil/edit/{id}', [MehfilController::class, 'editMehfil']);
	Route::post('mahafil/edit/{id}', [MehfilController::class, 'updateMehfil']);
	Route::get('mahafil/delete/{id}', [MehfilController::class, 'delMehfil']);
	// Route::post('karkun/cityname_fetch', [KarkunController::class, 'cityNameGet']);


	// to perform complete crud function in karkun table
	Route::get('karkun', [KarkunController::class, 'showData']);
	Route::get('karkun/add', [KarkunController::class, 'addKarkunPage']);
	Route::post('karkun/add', [KarkunController::class, 'insertKarkun']);
	Route::get('karkun/add/multiple', [KarkunController::class, 'insertKarkun_multiple']);
	Route::post('karkun/add/multiple/data', [KarkunController::class, 'insertKarkun_multiple_karkun']);

	Route::get('karkun/roster', [KarkunController::class, 'get_duty_waly_karkun']);

	Route::get('karkun/edit/{id}', [KarkunController::class, 'editKarkun']);
	Route::post('karkun/edit/{id}', [KarkunController::class, 'updateKarkun']);
	Route::get('karkun/delete/{id}', [KarkunController::class, 'delKarkun']);
	Route::post('karkun/cityname_fetch', [KarkunController::class, 'cityNameGet']);
	Route::post('karkun/cityname_fetch_dtl', [KarkunController::class, 'cityNameRelevantDtl']);


	// to perform complete crud function in karkun duty roster table
	Route::get('alldutyrosters', [KarkunDutyRosterController::class, 'showDataForAll']);
	Route::get('dutyrosterkarkun', [KarkunDutyRosterController::class, 'showData']);
	Route::post('dutyrosterkarkun/add', [KarkunDutyRosterController::class, 'insertDutyKarkun']);
	Route::get('dutyrosterkarkun/edit/{id}', [KarkunDutyRosterController::class, 'editDutyKarkun']);
	Route::get('print_duty_roster/edit/{id}', [KarkunDutyRosterController::class, 'printDutyKarkun']);
	// Route::get('print_duty_roster', [KarkunDutyRosterController::class, 'printDutyKarkun']);


	// to perform complete crud function in ehad kameeti duty roster table
	// Route::get('alldutyrosters/ehadkameeti', [EhadKameetiDutyRosterController::class, 'showDataForAll']);
	Route::get('dutyroster/ehadkameeti', [EhadKameetiDutyRosterController::class, 'showData']);
	Route::post('dutyroster/ehadkameeti/add', [EhadKameetiDutyRosterController::class, 'insertDutyKarkun']);
	Route::get('dutyroster/ehadkameeti/{id}', [EhadKameetiDutyRosterController::class, 'editDutyKarkun']);



	// Route::get('hazri/karkun', function(){
	// 	return view('hazri_karkun.hazri_karkun');
	// });


	// to perform complete crud function in karkun hazri table
	// Route::get('hazri/karkun', [KarkunDutyRosterController::class, 'showDataForAll']);
	Route::get('hazri/karkun/{datess?}', [HazriKarkunController::class, 'showData']);
	Route::post('hazri/karkun/add', [HazriKarkunController::class, 'insertHazriKarkun']);
	Route::get('hazri/karkun/edit/{id}', [HazriKarkunController::class, 'editHazriKarkun']);

	// hazri for ehad karkuns
	Route::get('hazri/ehad/karkun', [HazriKarkunController::class, 'showData_ehad']);
	Route::post('get_all_ehad_karkuns', [HazriKarkunController::class, 'get_all_ehad_karkuns']);
	Route::post('insert_hazri_ehad_karkun', [HazriKarkunController::class, 'insert_hazri_ehad_karkun']);
	// Route::get('hazri/ehad/karkun/edit/{id}', [HazriKarkunController::class, 'editHazriKarkun_ehad']);




	// these are just for reporting purposes
	Route::get('reports/karkun', [ReportingController::class, 'showData']);
	Route::post('reports/karkun', [ReportingController::class, 'get_data_accordingly']);

	Route::get('reports/hazri/karkun', [ReportingController::class, 'showData_hazri']);

	Route::post('reports/hazri/get/karkuns', [ReportingController::class, 'get_karkun_name_according_to_mehfil']);
	Route::post('reports/hazri/get/hazri', [ReportingController::class, 'filter_karkun_and_mehfil']);


	Route::get('reports/hazri/mehfil', [ReportingController::class, 'showData_hazri_mehfil']);
	Route::post('reports/hazri/get/mehfil/hazri', [ReportingController::class, 'filter_mehfil']);

	Route::get('reports/hazri/mehfil/monthly', [ReportingController::class, 'showData_hazri_mehfil_monthly']);
	Route::post('reports/hazri/mehfil/monthly', [ReportingController::class, 'mehfil_monthly_report']);
	Route::post('insert_monthly_report_coordinators', [ReportingController::class, 'insert_mehfil_monthly_report_coordinator']);
	// edit mehfil monthly report
	Route::post('reports/hazri/mehfil/monthly/edit/{id}/{month}/{year}', [ReportingController::class, 'mehfil_monthly_report_edit']);
	// Route::post('reports/hazri/get/mahafil', [ReportingController::class, 'get_hazri_details_for_mehfil']);


	// get the groups and insert and edit their members
	Route::get('groups/{id}', [All_GroupsController::class, 'show_data']);
	Route::post('groups/add', [All_GroupsController::class, 'insert_data']);
	Route::post('groups/add/single', [All_GroupsController::class, 'insert_data_single']);
	Route::post('groups/delete/single', [All_GroupsController::class, 'delete_data_single']);
	// Route::post('groups/{id}', [All_GroupsController::class, '']);
	// Route::post('groups/{id}', [All_GroupsController::class, '']);


	// to add multiple users in ehad karkun table from excel sheet or by hand
	Route::get('ehadkarkun/addnew/multiple',[EhadKarkunController::class,'addnew']);
	Route::post('ehadkarkun/add/multiple',[EhadKarkunController::class,'adddMultipleUser']);


	Route::get('help', function () {
	    return view('help');
	});

}); // end of middleware
